import 'package:e_travel/app/modules/widgets/custom_text.dart';
import 'package:e_travel/app/util/consts/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class CustomButton extends StatelessWidget {
  const CustomButton(
      {Key? key,
      required this.text,
      this.fontSize,
      this.color,
      this.onClick,
      this.height,
      this.width,
      this.colorText,
      this.radiusBottomLeft,
      this.radiusBottomRight,
      this.radiusTopLeft,
      this.radiusTopRight})
      : super(key: key);

  final String text;
  final double? height;
  final double? width;
  final double? fontSize;
  final Function? onClick;
  final Color? color, colorText;
  final double? radiusBottomLeft,
      radiusBottomRight,
      radiusTopLeft,
      radiusTopRight;

  @override
  Widget build(BuildContext context) {
    return InkWell(
        onTap: () => onClick!(),
        child: Container(
          width: width ?? 283.w,
          height: height ?? 45.h,
          decoration: BoxDecoration(
            color: color ?? primaryColor,
            borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(radiusBottomLeft ?? 50.r),
                bottomRight: Radius.circular(radiusBottomRight ?? 50.r),
                topLeft: Radius.circular(radiusTopLeft ?? 50.r),
                topRight: Radius.circular(radiusTopRight ?? 50.r)),
          ),
          child: Align(
            alignment: FractionalOffset.center,
            child: CustomText(
                text: text,
                fontSize: fontSize ?? 17.sp,
                color: colorText ?? whiteColor,
                fontWeight: FontWeight.bold),
          ),
        ));
  }
}
