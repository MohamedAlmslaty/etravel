import 'package:e_travel/app/util/consts/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get/get.dart';

class LoadingWidget extends StatelessWidget {
  final bool isDialog;

  LoadingWidget({this.isDialog = false});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: _buildBody(context),
    );
  }

  _buildBody(BuildContext context) {
    if (isDialog) {
      return SpinKitPulse(
        color: primaryColor,
      );
    } else {
      return Container(
          height: Get.height,
          width: Get.width,
          child: Image.asset(
            "assets/images/loading.gif",
            fit: BoxFit.contain,
          ));
    }
  }
}
