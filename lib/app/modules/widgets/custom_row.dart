import 'package:e_travel/app/util/consts/colors.dart';
import 'package:e_travel/app/modules/widgets/custom_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class CustomRow extends StatelessWidget {
  const CustomRow({
    Key? key,
    required this.title,
    this.text,
    required this.image,
    this.reTitle,
    this.reText,
    this.home = true,
    this.onClick,
    this.height,
    this.width,
    this.colorText,
  }) : super(key: key);

  final String title;
  final String? reTitle, text, reText;
  final String image;
  final bool home;
  final double? height;
  final double? width;
  final Function? onClick;
  final Color? colorText;

  @override
  Widget build(BuildContext context) {
    String local = Get.find<GetStorage>().read('language');
    return InkWell(
        onTap: () => onClick!(),
        child: Row(
          children: [
            Container(
              width:  100.w,
              height: height ?? 50.h,
              decoration: BoxDecoration(
                color: primaryColor,
                borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(local == 'ar' ? 0.r : 50.r),
                    bottomRight: Radius.circular(local == 'ar' ? 50.r : 0.r),
                    topLeft: Radius.circular(local == 'ar' ? 0.r : 50.r),
                    topRight: Radius.circular(local == 'ar' ? 50.r : 0.r)),
              ),
              child: Align(
                alignment: FractionalOffset.center,
                child: SvgPicture.asset(
                  image,
                  width: 35.h,
                  height: 35.h,
                  color: secondaryColor,
                ),
              ),
            ),
            Container(
                width: 230.w,
                height: height ?? 50.h,
                padding: EdgeInsets.only(right: 15.w, top: 5.h, left: 25.w),
                decoration: BoxDecoration(
                  color: whiteColor,
                  borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(local == 'ar' ? 50.r : 0.r),
                      bottomRight: Radius.circular(local == 'ar' ? 0.r : 50.r),
                      topLeft: Radius.circular(local == 'ar' ? 50.r : 0.r),
                      topRight: Radius.circular(local == 'ar' ? 0.r : 50.r)),
                ),
                child: home
                    ? Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              CustomText(
                                text: title,
                                fontSize: 12.sp,
                                color: textColor,
                                maxLines: 1,
                              ),
                              Container(
                                width: width ?? 180.w,
                                child: CustomText(
                                    text: text ?? '',
                                    fontSize: 10.sp,
                                    color: primaryColor,
                                    fontWeight: FontWeight.bold),
                              ),
                            ],
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              CustomText(
                                text: reTitle ?? '',
                                fontSize: 12.sp,
                                color: textColor,
                              ),
                              CustomText(
                                  text: reText ?? '',
                                  fontSize: 10.sp,
                                  color: primaryColor,
                                  fontWeight: FontWeight.bold),
                            ],
                          )
                        ],
                      )
                    : Center(
                        child: CustomText(
                          text: title,
                          fontSize: 18.sp,
                          fontWeight: FontWeight.bold,
                          color: blackColor,
                        ),
                      ))
          ],
        ));
  }
}
