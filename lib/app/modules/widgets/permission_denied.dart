import 'package:e_travel/app/modules/widgets/custom_text.dart';
import 'package:e_travel/app/routes/app_pages.dart';
import 'package:e_travel/app/util/consts/colors.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
class PermissionDenied extends StatelessWidget {
@override
  Widget build(BuildContext context) {
    return Container(
      alignment: AlignmentDirectional.center,
      padding: EdgeInsets.symmetric(horizontal: 30),
      height: Get.height/2,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
              Container(
                width: 150.w,
                height: 150.h,
                decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    gradient: LinearGradient(begin: Alignment.bottomLeft, end: Alignment.topRight, colors: [
                      primaryColor.withOpacity(0.7),
                      primaryColor.withOpacity(0.05),
                    ])),
                child: Icon(
                  Icons.https,
                  color: Theme.of(context).scaffoldBackgroundColor,
                  size: 70,
                ),
              ),

          SizedBox(height: 15.h),
          Opacity(
            opacity: 0.4,
            child: CustomText(
              text:'you must signin to access to this section'.tr,
              fontSize: 24.sp,
              color: blackColor,
              maxLines: 2,
              textAlign: TextAlign.center,
            ),
          ),
          SizedBox(height: 50.h),
          TextButton(
            onPressed: () {
              Get.toNamed(Routes.AUTH);
            },
            style: TextButton.styleFrom(
              padding: EdgeInsets.symmetric(vertical: 12.h, horizontal: 70.w),
              backgroundColor: primaryColor,
              shape: StadiumBorder(),
            ),

            child: CustomText(
              text:'login'.tr,
              fontSize: 16.sp,

            ),
          ),
          SizedBox(height: 20.h),

        ],
      ),
    );
  }
}
