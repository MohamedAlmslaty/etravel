import 'package:e_travel/app/util/consts/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class CustomText extends StatelessWidget {
  const CustomText({Key? key, required this.text, this.color, this.fontSize, this.fontWeight, this.maxLines, this.textAlign, this.textDecoration}) : super(key: key);

  final String text;
  final double? fontSize;
  final int? maxLines;
  final Color? color;
  final TextDecoration? textDecoration;
  final TextAlign? textAlign;
  final FontWeight? fontWeight;

  @override
  Widget build(BuildContext context) {
    return Text(text,
        softWrap: true,
        overflow: TextOverflow.ellipsis,
        maxLines: maxLines,
        textAlign: textAlign,
        textScaleFactor: 1.0,
        style: TextStyle(fontSize: fontSize ?? 14.sp, fontWeight: fontWeight ?? FontWeight.w400, color: color ?? whiteColor, decoration: textDecoration));
  }
}
