import 'package:e_travel/app/util/consts/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

class RadiusCircular extends StatelessWidget {
  const RadiusCircular({Key? key, required this.child, this.radius})
      : super(key: key);

  final Widget child;
  final double? radius;

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
        borderRadius: BorderRadius.circular(radius ?? 10.r),
        child: Container(
            color: whiteColor, height: 45.h, width: Get.width, child: child));
  }
}
