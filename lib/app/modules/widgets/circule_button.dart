import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';

class CirculeButton extends StatelessWidget {
  final String asset;
  final Color color;
  final Function onPressed;

  const CirculeButton({
    Key? key,
    required this.asset,
    required this.color,
    required this.onPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 5),
      child: TextButton(
        onPressed: () => onPressed(),
        child: SizedBox(child: SvgPicture.asset(asset)),
        style: TextButton.styleFrom(
            backgroundColor: color,
            shape: CircleBorder(),
            minimumSize: Size(Get.width * .13, Get.width * .13)),
      ),
    );
  }
}
