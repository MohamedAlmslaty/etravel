import 'package:e_travel/app/data/repositories/wallet_repository.dart';
import 'package:e_travel/app/util/consts/colors.dart';
import 'package:e_travel/app/modules/widgets/custom_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class WalletAmount extends StatelessWidget {
  const WalletAmount({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 130.w,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(10.r)),
      ),
      child: Row(
        children: [
          Icon(
            Icons.account_balance_wallet_outlined,
            color: secondaryColor,
          ),
          SizedBox(
            width: 5.w,
          ),
          Obx(()=>loadingBalance.value
              ?SizedBox():CustomText(
            text: Get.find<GetStorage>().hasData('wallet_amount')
                ?  walletBalance.value.toString() +
                    'LY'.tr
                : '' ,
            fontSize: 12.sp,
            color: whiteColor,
            fontWeight: FontWeight.bold,
          ),
          ),
        ],
      ),
    );
  }
}
