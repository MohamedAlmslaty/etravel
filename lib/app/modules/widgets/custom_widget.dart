import 'package:e_travel/app/modules/widgets/custom_text.dart';
import 'package:e_travel/app/modules/widgets/wallet_amount.dart';
import 'package:e_travel/app/util/consts/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class CustomWidget extends StatelessWidget {
  const CustomWidget({
    Key? key,
    required this.title,
    this.cancelText,
    this.image,
    this.height,
    this.onDownload,
    this.onShare,
    this.onCancel,
    this.dividePnr,
    this.onEdit,
    this.icon = false,
    this.main = false,
    this.showCancel = false,
    this.showEdit = false,
    this.initialBooking = false,
    this.showDividePnr = false,
    required this.child,
  }) : super(key: key);

  final String title;
  final String? cancelText ;
  final String? image;
  final bool icon;
  final bool main;
  final bool showCancel;
  final bool showEdit;
  final bool initialBooking;
  final bool showDividePnr;
  final Function? onShare;
  final Function? onCancel;
  final Function? dividePnr;
  final Function? onEdit;
  final Function? onDownload;
  final double? height;
  final Widget child;

  @override
  Widget build(BuildContext context) {
    return Column(children: [
      SizedBox(
        height: 44.h,
      ),
      Padding(
        padding: EdgeInsets.symmetric(horizontal: 20.w, vertical: 10.h),
        child: Row(
          children: [
            IconButton(
                onPressed: () => Get.back(),
                icon: image == null
                    ? Icon(
                  Icons.arrow_back,
                  color: secondaryColor,
                )
                    : SvgPicture.asset(
                  image!,
                  height: 25.h,
                  width: 25.w,
                  color: secondaryColor,
                )),
            SizedBox(
              width: 10.w,
            ),
            CustomText(
              text: title,
              fontSize: 16.sp,
            ),
            Spacer(),
            icon
                ? showDividePnr
                    ? showEdit
                        ? PopupMenuButton(
                            icon: Icon(
                              Icons.menu,
                              color: secondaryColor,
                            ),
                            itemBuilder: (context) {
                              return [
                                PopupMenuItem<int>(
                                    value: 0,
                                    child: ListTile(
                                      trailing: Icon(
                                        Icons.share,
                                        color: secondaryColor,
                                        size: 22,
                                      ),
                                      title: CustomText(
                                        text: 'sharing'.tr,
                                        color: blackColor,
                                      ),
                                    )),
                                PopupMenuItem<int>(
                                    value: 1,
                                    child: ListTile(
                                      trailing: Icon(
                                        Icons.open_in_new,
                                        color: secondaryColor,
                                        size: 22,
                                      ),
                                      title: CustomText(
                                        text: 'view ticket'.tr,
                                        color: blackColor,
                                      ),
                                    )),
                                PopupMenuItem<int>(
                                    value: 2,
                                    child: ListTile(
                                      trailing: Icon(
                                        Icons.edit,
                                        color: secondaryColor,
                                        size: 22,
                                      ),
                                      title: CustomText(
                                        text: 'separating a pnr'.tr,
                                        color: blackColor,
                                      ),
                                    )),
                                PopupMenuItem<int>(
                                    value: 3,
                                    child: ListTile(
                                      trailing: Icon(
                                        Icons.edit_calendar,
                                        color: secondaryColor,
                                        size: 22,
                                      ),
                                      title: CustomText(
                                        text: 'Booking edit'.tr,
                                        color: blackColor,
                                      ),
                                    )),
                                if(showCancel)
                                PopupMenuItem<int>(
                                    value: 4,
                                    child: initialBooking
                                        ? ListTile(
                                            trailing: Icon(
                                              Icons.add,
                                              color: secondaryColor,
                                              size: 22,
                                            ),
                                            title: CustomText(
                                              text: 'ConfirmSubscription'.tr,
                                              color: blackColor,
                                            ),
                                          )
                                        : showCancel
                                            ? ListTile(
                                                trailing: Icon(
                                                  Icons.cancel_outlined,
                                                  color: secondaryColor,
                                                  size: 22,
                                                ),
                                                title: CustomText(
                                                  text: cancelText.toString(),
                                                  color: blackColor,
                                                ),
                                              )
                                            : SizedBox()),
                              ];
                            },
                            onSelected: (value) {
                              if (value == 0) {
                                onShare!();
                              } else if (value == 1) {
                                onDownload!();
                              } else if (value == 2) {
                                dividePnr!();
                              } else if (value == 3) {
                                onEdit!();
                              } else if (value == 4 &&showCancel) {
                                onCancel!();
                              }
                            })
                        : PopupMenuButton(
                            icon: Icon(
                              Icons.menu,
                              color: secondaryColor,
                            ),
                            itemBuilder: (context) {
                              return [
                                PopupMenuItem<int>(
                                    value: 0,
                                    child: ListTile(
                                      trailing: Icon(
                                        Icons.share,
                                        color: secondaryColor,
                                        size: 22,
                                      ),
                                      title: CustomText(
                                        text: 'sharing'.tr,
                                        color: blackColor,
                                      ),
                                    )),
                                PopupMenuItem<int>(
                                    value: 1,
                                    child: ListTile(
                                      trailing: Icon(
                                        Icons.open_in_new,
                                        color: secondaryColor,
                                        size: 22,
                                      ),
                                      title: CustomText(
                                        text: 'view ticket'.tr,
                                        color: blackColor,
                                      ),
                                    )),
                                PopupMenuItem<int>(
                                    value: 2,
                                    child: ListTile(
                                      trailing: Icon(
                                        Icons.edit,
                                        color: secondaryColor,
                                        size: 22,
                                      ),
                                      title: CustomText(
                                        text: 'separating a pnr'.tr,
                                        color: blackColor,
                                      ),
                                    )),
                                if(showCancel)
                                PopupMenuItem<int>(
                                    value: 3,
                                    child: initialBooking
                                        ? ListTile(
                                            trailing: Icon(
                                              Icons.add,
                                              color: secondaryColor,
                                              size: 22,
                                            ),
                                            title: CustomText(
                                              text: 'ConfirmSubscription'.tr,
                                              color: blackColor,
                                            ),
                                          )
                                        : showCancel
                                            ? ListTile(
                                                trailing: Icon(
                                                  Icons.cancel_outlined,
                                                  color: secondaryColor,
                                                  size: 22,
                                                ),
                                                title: CustomText(
                                                  text: cancelText!,
                                                  color: blackColor,
                                                ),
                                              )
                                            : SizedBox()),
                              ];
                            },
                            onSelected: (value) {
                              if (value == 0) {
                                onShare!();
                              } else if (value == 1) {
                                onDownload!();
                              } else if (value == 2) {
                                dividePnr!();
                              } else if (value == 3&&showCancel) {
                                onCancel!();
                              }
                            })
                    : showEdit
                        ? PopupMenuButton(
                            icon: Icon(
                              Icons.menu,
                              color: secondaryColor,
                            ),
                            itemBuilder: (context) {
                              return [
                                PopupMenuItem<int>(
                                    value: 0,
                                    child: ListTile(
                                      trailing: Icon(
                                        Icons.share,
                                        color: secondaryColor,
                                        size: 22,
                                      ),
                                      title: CustomText(
                                        text: 'sharing'.tr,
                                        color: blackColor,
                                      ),
                                    )),
                                PopupMenuItem<int>(
                                    value: 1,
                                    child: ListTile(
                                      trailing: Icon(
                                        Icons.open_in_new,
                                        color: secondaryColor,
                                        size: 22,
                                      ),
                                      title: CustomText(
                                        text: 'view ticket'.tr,
                                        color: blackColor,
                                      ),
                                    )),
                                PopupMenuItem<int>(
                                    value: 2,
                                    child: ListTile(
                                      trailing: Icon(
                                        Icons.edit_calendar,
                                        color: secondaryColor,
                                        size: 22,
                                      ),
                                      title: CustomText(
                                        text: 'Booking edit'.tr,
                                        color: blackColor,
                                      ),
                                    )),
                                if(showCancel)
                                PopupMenuItem<int>(
                                    value: 3,
                                    child: initialBooking
                                        ? ListTile(
                                            trailing: Icon(
                                              Icons.add,
                                              color: secondaryColor,
                                              size: 22,
                                            ),
                                            title: CustomText(
                                              text: 'ConfirmSubscription'.tr,
                                              color: blackColor,
                                            ),
                                          )
                                        : showCancel
                                            ? ListTile(
                                                trailing: Icon(
                                                  Icons.cancel_outlined,
                                                  color: secondaryColor,
                                                  size: 22,
                                                ),
                                                title: CustomText(
                                                  text: cancelText!,
                                                  color: blackColor,
                                                ),
                                              )
                                            : SizedBox()),
                              ];
                            },
                            onSelected: (value) {
                              if (value == 0) {
                                onShare!();
                              } else if (value == 1) {
                                onDownload!();
                              } else if (value == 2) {
                                onEdit!();
                              } else if (value == 3&&showCancel) {
                                onCancel!();
                              }
                            })
                        : PopupMenuButton(
                            icon: Icon(
                              Icons.menu,
                              color: secondaryColor,
                            ),
                            itemBuilder: (context) {
                              return [
                                PopupMenuItem<int>(
                                    value: 0,
                                    child: ListTile(
                                      trailing: Icon(
                                        Icons.share,
                                        color: secondaryColor,
                                        size: 22,
                                      ),
                                      title: CustomText(
                                        text: 'sharing'.tr,
                                        color: blackColor,
                                      ),
                                    )),
                                PopupMenuItem<int>(
                                    value: 1,
                                    child: ListTile(
                                      trailing: Icon(
                                        Icons.open_in_new,
                                        color: secondaryColor,
                                        size: 22,
                                      ),
                                      title: CustomText(
                                        text: 'view ticket'.tr,
                                        color: blackColor,
                                      ),
                                    )),
                                if(showCancel)
                                PopupMenuItem<int>(
                                    value: 3,
                                    child: initialBooking
                                        ? ListTile(
                                            trailing: Icon(
                                              Icons.add,
                                              color: secondaryColor,
                                              size: 22,
                                            ),
                                            title: CustomText(
                                              text: 'ConfirmSubscription'.tr,
                                              color: blackColor,
                                            ),
                                          )
                                        : showCancel
                                            ? ListTile(
                                                trailing: Icon(
                                                  Icons.cancel_outlined,
                                                  color: secondaryColor,
                                                  size: 22,
                                                ),
                                                title: CustomText(
                                                  text: cancelText!,
                                                  color: blackColor,
                                                ),
                                              )
                                            : SizedBox()),
                              ];
                            },
                            onSelected: (value) {
                              if (value == 0) {
                                onShare!();
                              } else if (value == 1) {
                                onDownload!();
                              } else if (value == 3 &&showCancel) {
                                onCancel!();
                              }
                            })
                : SizedBox(),
            Get.find<GetStorage>().hasData('token') && main
                ? WalletAmount()
                : SizedBox()
          ],
        ),
      ),
      Container(
          height: height ?? Get.height / 1.3,
          width: Get.width,
          decoration: BoxDecoration(
            color: secondaryColor,
            borderRadius: BorderRadius.all(Radius.circular(50.r)),
          ),
          child: child),
    ]);
  }
}
