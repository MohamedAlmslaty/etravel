
import 'package:e_travel/app/util/consts/base.dart';
import 'package:e_travel/app/util/consts/colors.dart';
import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:e_travel/app/data/models/slider.dart' as s;

class SliderImageWidget extends StatelessWidget {
  RxList<s.Slider> sliders;

  SliderImageWidget({key, required this.sliders,this.height});

  final RxInt _current = 0.obs;
  final double? height;
  final CarouselController _controller = CarouselController();

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        ClipRRect(
          borderRadius: BorderRadius.circular(20.0.r), // Set the border radius
          child:CarouselSlider(
          options: CarouselOptions(
              autoPlay: true,
              autoPlayInterval: const Duration(seconds: 5),
              height: height??164.h,
              viewportFraction: 1.0,
              onPageChanged: (index, reason) {
                _current.value = index;
              }),
          items: sliders.map((slider) {
            return Builder(
              builder: (BuildContext context) {
                return Container(
                  width: Get.width,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(20.0.r),
                  ),
                  child: Image.network(
                  imageUrl(slider.imageUrl!),
                    fit: BoxFit.cover,
                  ),
                );
              },
            );
          }).toList(),
        ),
        ),
        Positioned(
          top: 144.h,
          right: 150.w,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: sliders.asMap().entries.map((entry) {
              return Obx(
                () => GestureDetector(
                  onTap: () => _controller.animateToPage(entry.key),
                  child: Container(
                    width: _current.value == entry.key ? 16.w : 8.w,
                    height: 8.h,
                    margin: EdgeInsets.symmetric(horizontal: 4.w),
                    decoration: _current.value == entry.key
                        ? BoxDecoration(
                            color: primaryColor,
                            borderRadius: BorderRadius.circular(4),
                          )
                        : BoxDecoration(
                            shape: BoxShape.circle, color: greyColor.withOpacity(0.5)),
                  ),
                ),
              );
            }).toList(),
          ),
        )
      ],
    );
  }
}
