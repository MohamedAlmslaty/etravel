import 'package:e_travel/app/modules/widgets/custom_text.dart';
import 'package:e_travel/app/util/consts/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class CustomButtonWithIcon extends StatelessWidget {
  const CustomButtonWithIcon(
      {Key? key,
      required this.text,
      required this.onClick,
      required this.icon,
      required this.textColor,
      required this.iconColor,
      required this.backgroundButton,
      })
      : super(key: key);

  final String text;
  final Function onClick;
  final IconData icon;
  final Color iconColor,textColor,backgroundButton;

  @override
  Widget build(BuildContext context) {
    return   ElevatedButton.icon(
      style: ButtonStyle(
          backgroundColor: MaterialStateProperty.all(backgroundButton),
          shape: MaterialStateProperty.all<RoundedRectangleBorder>(
              RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(18.0),

              )
          )
      ),
      onPressed: () =>onClick(),
      icon: Icon(icon,color: iconColor,),
      label: CustomText(text:text,color: textColor,),
    );
  }
}
