import 'package:e_travel/app/modules/package_details/controllers/confirm_subscription_controller.dart';
import 'package:e_travel/app/modules/widgets/custom_button.dart';
import 'package:e_travel/app/util/consts/colors.dart';
import 'package:e_travel/app/modules/widgets/custom_text.dart';
import 'package:e_travel/app/modules/widgets/custom_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinbox/cupertino.dart';
import 'package:get/get.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class ConfirmSubscriptionView extends GetView<ConfirmSubscriptionController> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<ConfirmSubscriptionController>(
      init: ConfirmSubscriptionController(),
      initState: (_) {},
      builder: (_con) => Scaffold(
        resizeToAvoidBottomInset: false,
        bottomNavigationBar: Container(
          width: Get.width,
          height: 150.h,
          padding: EdgeInsets.only(top: 10.h, right: 50.w, left: 50.w),
            decoration: BoxDecoration(
              color: whiteColor,
              borderRadius:
              BorderRadius.only(topLeft: Radius.circular(20.r),topRight: Radius.circular(20.r)),
            ),
          child:Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  CustomText(text: 'Total'.tr,
                    color: blackColor,
                  fontSize: 18.sp,),
                 Obx(()=> CustomText(text: '${_con.total.value} ${'LY'.tr}',
                 color: blackColor,
                   fontSize: 18.sp,)),
                ],
              ),
              CustomButton(
                onClick: () => controller.addSubscription(),
                text: 'ConfirmSubscription'.tr,
                height: 40.w,
                width: 283.w,
                color: primaryColor,
                colorText: whiteColor,
                radiusBottomLeft: 10.0.r,
                radiusBottomRight: 10.0.r,
                radiusTopLeft: 10.0.r,
                radiusTopRight: 10.0.r,
              ),
            ],
          )
        ),
        body:  CustomWidget(
                title: 'ConfirmSubscription'.tr,
                height: Get.height/1.5,
                child: Padding(
                    padding:
                        EdgeInsets.only(top: 20.h, right: 20.w, left: 20.w),
                    child: Column(
                      children: [
                        Container(
                          padding: EdgeInsets.symmetric(
                              horizontal: 15.w, vertical: 10.h),
                          decoration: BoxDecoration(
                            color: whiteColor,
                            borderRadius:
                                BorderRadius.all(Radius.circular(20.r)),
                          ),
                          child: Row(
                            mainAxisAlignment:
                            MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Container(
                                child: Row(
                                  children: <Widget>[
                                    Container(
                                      child: Icon(
                                        Icons.person,
                                        color: primaryColor,
                                        size: 25,
                                      ),
                                    ),
                                    SizedBox(width: 10.w,),
                                    Container(
                                      padding: EdgeInsets.only(
                                        left: 20.w,
                                      ),
                                      child: CustomText(
                                        text:'The number of person'.tr,
                                        fontSize: 18.sp,
                                        color: primaryColor,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              Container(
                                  width: 150.w,
                                  child: CupertinoSpinBox(
                                    incrementIcon: Icon(Icons.add_circle_outline,color: secondaryColor,),
                                    decrementIcon: Icon(Icons.remove_circle_outline,color: secondaryColor,),
                                    decoration: BoxDecoration(
                                      color: whiteColor,
                                    ),
                                    textStyle: TextStyle(
                                      color: primaryColor,
                                    ),
                                    min: 1,
                                    max: 10,
                                    value: _con.people.value.toDouble(),
                                    onChanged: (value) {
                                      _con.people.value = value.toInt();
                                      _con.total.value=_con.people.value*_con.package.value.totalPrice;
                                    },
                                  )),
                            ],
                          ),),

                      ],
                    ))),
      ),
    );
  }
}
