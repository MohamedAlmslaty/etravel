import 'package:cached_network_image/cached_network_image.dart';
import 'package:e_travel/app/modules/package_details/controllers/package_details_controller.dart';
import 'package:e_travel/app/modules/widgets/custom_text.dart';
import 'package:e_travel/app/modules/widgets/custom_widget.dart';
import 'package:e_travel/app/modules/widgets/loading_widget.dart';
import 'package:e_travel/app/routes/app_pages.dart';
import 'package:e_travel/app/util/consts/base.dart';
import 'package:e_travel/app/util/consts/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class PackageDetailsView extends GetView<PackageDetailsController> {
  bool local = Get.find<GetStorage>().read('language') == 'ar';

  @override
  Widget build(BuildContext context) {
    return GetBuilder<PackageDetailsController>(
      init: PackageDetailsController(),
      initState: (_) {},
      builder: (_con) => Scaffold(
        body: _con.loading.value
            ? LoadingWidget()
            : CustomWidget(
                title: 'Package Details'.tr,
                child: Padding(
                    padding: EdgeInsets.only(top: 20.h, right: 20.w, left: 20.w),
                    child: Column(
                      children: [
                        Container(
                          height: 150.h,
                          child: CachedNetworkImage(
                            imageUrl: imageUrl(_con.package.imageUrl),
                            imageBuilder: (context, imageProvider) => Container(
                              height: 150,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(20.0),
                                image: DecorationImage(
                                  image: imageProvider,
                                  fit: BoxFit.cover,
                                ),
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 10.h,
                        ),
                        Container(
                          padding: EdgeInsets.symmetric(horizontal: 15.w, vertical: 10.h),
                          decoration: BoxDecoration(
                            color: whiteColor,
                            borderRadius: BorderRadius.all(Radius.circular(20.r)),
                          ),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              SizedBox(
                                height: 8,
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Text(
                                    local ? _con.package.nameAr : _con.package.nameEn,
                                    overflow: TextOverflow.fade,
                                    softWrap: true,
                                    maxLines: 1,
                                    style: TextStyle(fontSize: 17, fontWeight: FontWeight.bold),
                                  ),
                                ],
                              ),
                              Divider(indent: 8),
                              SizedBox(
                                height: 16,
                              ),
                              Text(
                                'Package Details'.tr,
                                style: TextStyle(fontSize: 17, fontWeight: FontWeight.bold),
                              ),
                              Divider(indent: 8),
                              Text(local ? _con.package.detailsAr : _con.package.detailsEn),
                            ],
                          ),
                        ),
                        Spacer(),
                        ElevatedButton(
                          style: ElevatedButton.styleFrom(
                              //backgroundColor: primaryColor,
                              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(25.r)),
                              padding: EdgeInsets.symmetric(horizontal: 20.w, vertical: 8.h)),
                          onPressed: () {
                            Get.find<GetStorage>().hasData('token') ? Get.toNamed(Routes.CONFIRM_SUBSCRIPTION, arguments: _con.package) : Get.toNamed(Routes.AUTH);
                          },
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Container(
                                padding: EdgeInsets.symmetric(horizontal: 15.w, vertical: 3.h),
                                decoration: BoxDecoration(
                                  color: secondaryColor,
                                  borderRadius: BorderRadius.all(Radius.circular(20.r)),
                                ),
                                child: CustomText(text: "subscribe now".tr, color: blackColor),
                              ),
                              CustomText(text: '${_con.package.totalPrice}  ' + "LY".tr)
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 25.h,
                        )
                      ],
                    ))),
      ),
    );
  }
}
