import 'package:e_travel/app/modules/package_details/controllers/confirm_subscription_controller.dart';
import 'package:e_travel/app/modules/package_details/controllers/package_details_controller.dart';
import 'package:get/get.dart';
class PackageDetailsBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<PackageDetailsController>(
      () => PackageDetailsController(),
    ); Get.lazyPut<ConfirmSubscriptionController>(
      () => ConfirmSubscriptionController(),
    );
  }
}
