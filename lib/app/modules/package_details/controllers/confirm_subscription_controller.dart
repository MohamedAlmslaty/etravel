import 'package:e_travel/app/data/models/package_subscription.dart';
import 'package:e_travel/app/data/models/tourist_packages.dart';
import 'package:e_travel/app/data/repositories/package_subscription_repository.dart';
import 'package:e_travel/app/modules/wallet/controllers/wallet_controller.dart';
import 'package:e_travel/app/util/consts/loading.dart';
import 'package:e_travel/app/util/consts/style.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';

class ConfirmSubscriptionController extends GetxController {
  final _packageSubscriptionRepo = PackageSubscriptionRepository.instance;
  Rx<PackageSubscription> packageSubscription = PackageSubscription().obs;
  Rx<TouristPackages> package = TouristPackages().obs;
  final people = 1.obs;
  final total = 0.0.obs;

  @override
  void onInit() {
    if (Get.arguments != null) package.value = Get.arguments;

    total.value = people.value * package.value.totalPrice;
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}

  addSubscription() {
    loading();
    packageSubscription.value.paymentType = 'WALLET';
    packageSubscription.value.persons = people.value;
    packageSubscription.value.touristPackage = package.value;
    _packageSubscriptionRepo.addPackageSubscription(packageSubscription.value).then((value) {
      EasyLoading.dismiss();

      if (value) {
        EasyLoading.dismiss();
        Get.back();
        Get.back();
        Get.put(WalletController()).loadBalance();
        showSnackBar(title: 'successful'.tr, message: 'Successfully subscribed to a package'.tr);
      }
    });
  }
}
