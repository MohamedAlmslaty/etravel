import 'package:e_travel/app/data/models/tourist_packages.dart';
import 'package:e_travel/app/data/repositories/tourist_packages_repository.dart';
import 'package:get/get.dart';

class PackageDetailsController extends GetxController {
  final _packageRepo = TouristPackagesRepository.instance;
  final loading = false.obs;

  final int _packageId = Get.arguments;
  final Rx<TouristPackages> _package = TouristPackages().obs;

  TouristPackages get package => _package.value;

  @override
  void onInit() {
    loadPackage();
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}

  loadPackage() async {
    try {
      loading.value = true;
      _package.value = await _packageRepo.getTouristPackagePublic(_packageId);
      update();
    } catch (e) {
      print(e.toString());
    } finally {
      loading.value = false;
    }
  }
}
