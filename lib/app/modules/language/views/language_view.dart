import 'package:e_travel/app/modules/language/controllers/language_controller.dart';
import 'package:e_travel/app/translation/lang_service.dart';
import 'package:e_travel/app/util/consts/colors.dart';
import 'package:e_travel/app/modules/widgets/custom_text.dart';
import 'package:e_travel/app/modules/widgets/custom_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class LanguageView extends GetView<LanguageController> {
  LanguageView();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: CustomWidget(
            title: 'Languages'.tr,
            child: Column(
              children: [
                Container(
                  padding: EdgeInsets.symmetric(vertical: 5.h),
                  margin:
                      EdgeInsets.symmetric(horizontal: 20.w, vertical: 40.h),
                  decoration: BoxDecoration(
                    color: primaryColor,
                    borderRadius: BorderRadius.all(Radius.circular(30.r)),
                  ),
                  child: Column(
                    children: List.generate(LocalizationService.langs.length,
                        (index) {
                      var _lang = LocalizationService.langs.elementAt(index);
                      return RadioListTile(
                        activeColor: secondaryColor,
                        value: _lang,
                        groupValue: Get.locale.toString(),
                        onChanged: (value) {
                          controller.updateLocale(value);
                        },
                        title: CustomText(
                          text: _lang.tr,
                          fontSize: 14.sp,
                        ),
                      );
                    }).toList(),
                  ),
                )
              ],
            )));
  }
}
