import 'dart:io';

import 'package:e_travel/app/modules/widgets/custom_text.dart';
import 'package:e_travel/app/modules/widgets/custom_widget.dart';
import 'package:e_travel/app/routes/app_pages.dart';
import 'package:e_travel/app/util/consts/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:share_plus/share_plus.dart';

class WelcomeView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: secondaryColor,
        body:  Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(
                  height: 20.h,
                ),
                SvgPicture.asset(
                  'assets/images/logo.svg',
                  width: 150.h,
                  height: 150.h,
                  fit: BoxFit.fill,
                ),
                SizedBox(
                  height: 100.h,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    InkWell(
                      onTap: ()=>Get.toNamed(Routes.MAIN),
                      child: Image.asset(
                        'assets/images/icon1.png',
                        width: 120.w,
                        height: 120.h,
                      ),
                    ) ,
                    InkWell(
                      onTap: ()=>Get.toNamed(Routes.MAIN_HOTEL),
                        child: Image.asset(
                          'assets/images/icon2.png',
                          width: 120.w,
                          height: 120.h,
                        ),
                      ) ,
                  ],
                )


              ],
            )));
  }
}
