import 'package:e_travel/app/modules/splash/controllers/splash_controller.dart';
import 'package:e_travel/app/util/consts/colors.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SplashView extends GetView<SplashController> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<SplashController>(
        init: SplashController(),
        initState: (_) {},
        builder: (_con) => Scaffold(
              backgroundColor: secondaryColor,
              body: Container(
                  height: Get.height,
                  width: Get.width,
                  child: Image.asset(
                    "assets/images/splash.gif",
                    fit: BoxFit.contain,
                  )),
            ));
  }
}
