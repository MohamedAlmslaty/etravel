import 'dart:async';
import 'dart:io';

import 'package:e_travel/app/routes/app_pages.dart';
import 'package:e_travel/app/util/fcm_config.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:notification_permissions/notification_permissions.dart';

class SplashController extends GetxController {
  @override
  Future<void> onInit() async {
    FbConfig.init();

    PermissionStatus permissionStatus = await NotificationPermissions.getNotificationPermissionStatus();
    if (permissionStatus == PermissionStatus.denied || permissionStatus == PermissionStatus.unknown) {
      NotificationPermissions.requestNotificationPermissions(iosSettings: NotificationSettingsIos(alert: true, sound: true, badge: true), openSettings: false);
    }
    if (Platform.isIOS) {
      if (permissionStatus == PermissionStatus.provisional) {
        NotificationPermissions.requestNotificationPermissions(iosSettings: NotificationSettingsIos(alert: true, sound: true, badge: true), openSettings: false);
      }
    }

    Future.delayed(Duration(seconds: 5), () {
      if (!Get.find<GetStorage>().hasData('first')) {
        Get.find<GetStorage>().write('first', true);
        Get.offAndToNamed(Routes.OnBOARD);
      } else {
        Get.offAndToNamed(Routes.WELCOME);
      }
    });
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}
}
