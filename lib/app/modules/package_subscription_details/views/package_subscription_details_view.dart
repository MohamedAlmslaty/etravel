import 'package:e_travel/app/modules/package_subscription_details/controllers/package_subscription_details_controller.dart';
import 'package:e_travel/app/modules/widgets/custom_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:intl/intl.dart';

class PackageSubscriptionDetailsView extends GetView<PackageSubscriptionDetailsController> {
  bool local = Get.find<GetStorage>().read('language') == 'ar';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomWidget(
        title: 'PackageSubscription'.tr,
        child: Column(
          children: [
            GetBuilder<PackageSubscriptionDetailsController>(
              init: PackageSubscriptionDetailsController(),
              builder: (_) => Obx(
                () => _.package.id != null
                    ? Expanded(
                        child: ListView(
                          padding: EdgeInsets.only(top: 30.h, left: 15.w, right: 15.w, bottom: 30.h),
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(top: 4.0, left: 8, right: 8),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Text("subscription number".tr + _.package.id.toString()),
                                  Container(
                                    padding: EdgeInsets.symmetric(horizontal: 10, vertical: 2),
                                    decoration: BoxDecoration(color: _.package.getColor(), borderRadius: BorderRadius.circular(24)),
                                    child: Row(
                                      children: <Widget>[
                                        Text(
                                          _.package.getText(),
                                          style: TextStyle(color: Colors.white),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(top: 4.0, left: 8, right: 8),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Text("Subscription date".tr + DateFormat('yyyy-MM-dd', 'en').format(_.package.createdDate!).toString()),
                                  // OutlinedButton(
                                  //   onPressed: () {},
                                  //   child: Row(
                                  //     children: <Widget>[
                                  //       Text(
                                  //         'print'.tr,
                                  //         style: TextStyle(color: primaryColor),
                                  //       ),
                                  //       Icon(
                                  //         Icons.print,
                                  //         color: primaryColor,
                                  //         size: 16.sp,
                                  //       )
                                  //     ],
                                  //   ),
                                  // )
                                ],
                              ),
                            ),
                            Divider(indent: 8),
                            Card(
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      "Subscription details".tr,
                                      style: TextStyle(fontWeight: FontWeight.bold),
                                    ),
                                    Divider(indent: 8),
                                    SizedBox(
                                      height: 10.h,
                                    ),
                                    Text("Package name".tr + '${local ? _.package.touristPackage!.nameAr : _.package.touristPackage!.nameEn}'),
                                    SizedBox(
                                      height: 10.h,
                                    ),
                                    Text("Total".tr + " : " + _.package.totalPrice.toString() + 'LY'.tr),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      )
                    : Center(child: CircularProgressIndicator()),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
