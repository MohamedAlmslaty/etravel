import 'package:e_travel/app/data/models/package_subscription.dart';
import 'package:e_travel/app/data/repositories/package_subscription_repository.dart';
import 'package:get/get.dart';

class PackageSubscriptionDetailsController extends GetxController {
  final _packageRepo = PackageSubscriptionRepository.instance;
  final loading = false.obs;
  final int _packageId = Get.arguments;
  final Rx<PackageSubscription> _package = PackageSubscription().obs;
  PackageSubscription get package => _package.value;
  @override
  void onInit() {
    findPackageSubscriptionById(_packageId);
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}

  void findPackageSubscriptionById(id) async {
    try {
      loading.value = true;
      _package.value = await _packageRepo.findPackageSubscriptionById(_packageId);
    } catch (e) {
      print(e.toString());
    } finally {
      loading.value = false;
    }
  }
}
