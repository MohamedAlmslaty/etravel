import 'dart:async';
import 'package:e_travel/app/data/models/voucher_dealer.dart';
import 'package:e_travel/app/data/repositories/voucher_dealer_repository.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:url_launcher/url_launcher.dart';

class VoucherController extends GetxController {
  final _voucher = VoucherDealerRepository.instance;
  RxList<VoucherDealer> dealers = <VoucherDealer>[].obs;
  final _loading = false.obs;

  bool get loaded => _loading.value;
  final Set<Marker> markers = new Set();

  @override
  void onInit() {
    super.onInit();
    loadDealers();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}


  Future<void> loadDealers() async {
    final response = await _voucher.getVoucherDealers();
    dealers.assignAll(response);
    _loading.value = true;
    update();
    if (dealers.isNotEmpty) {

      for (int i = 0; i < dealers.length; i++) {
        markers.add(Marker( //add first marker
          markerId: MarkerId(dealers[i].id.toString()),
          position: LatLng(dealers[i].lat!, dealers[i].lng!),
          //position of marker
          infoWindow: InfoWindow( //popup info
            title: dealers[i].name,
            snippet: dealers[i].mobileNo,
          //   onTap: () =>
          //       _launchUrl(
          //           'https://www.google.com/maps/search/?api=1&query=' +
          //               dealers[i].lat.toString() +
          //               ',' +
          //               dealers[i].lng.toString() +
          //               '&travelmode=driving&dir_action=navigate'),
           ),
          icon: BitmapDescriptor.defaultMarker, //Icon for Marker
        ));
      }
    }
  }

  Future<void> _launchUrl(url) async {
    if (!await launchUrl(Uri.parse(url))) {
      throw 'Could not launch $url';
    }
  }
}
