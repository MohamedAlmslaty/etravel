import 'dart:async';

import 'package:e_travel/app/modules/widgets/custom_text.dart';
import 'package:e_travel/app/util/consts/colors.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

import '../controllers/voucher_controller.dart';

class VoucherView extends GetView<VoucherController> {
  Completer<GoogleMapController> _controller = Completer();

  static final CameraPosition _kGooglePlex = CameraPosition(
    target: LatLng(32.8734154, 13.1924344),
    zoom: 11,
  );

  @override
  Widget build(BuildContext context) {
    return GetBuilder<VoucherController>(
      init: VoucherController(),
      builder: (_) {
        return Scaffold(
            appBar: AppBar(
              title: CustomText(text: 'VoucherDealer'.tr,color: whiteColor,),
              leading: IconButton(icon: Icon(Icons.arrow_back_ios_new),onPressed: ()=>Get.back(),),
            ),
            body: Obx(
          () => _.loaded
              ? GoogleMap(
                  markers: _.markers,
                  mapType: MapType.normal,
                  initialCameraPosition: _kGooglePlex,
                  onMapCreated: (GoogleMapController controller) {
                    _controller.complete(controller);
                  },
                )
              : Center(child: CircularProgressIndicator()),
        ));
      },
    );
  }
}
