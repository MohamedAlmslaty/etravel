import 'package:e_travel/app/modules/booking/controllers/booking_controller.dart';
import 'package:e_travel/app/modules/booking/controllers/edit_flight_controller.dart';
import 'package:e_travel/app/modules/booking/controllers/flight_details_controller.dart';
import 'package:get/get.dart';

class BookingBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<BookingController>(
      () => BookingController(),
    );
    Get.lazyPut<FlightDetailsController>(
      () => FlightDetailsController(),
    );
    Get.lazyPut<EditFlightController>(
      () => EditFlightController(),
    );
  }
}
