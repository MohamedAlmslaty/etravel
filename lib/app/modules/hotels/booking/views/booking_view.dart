
import 'package:e_travel/app/modules/hotels/booking/controllers/booking_controller.dart';
import 'package:e_travel/app/modules/widgets/custom_text.dart';
import 'package:e_travel/app/modules/widgets/custom_widget.dart';
import 'package:e_travel/app/modules/widgets/permission_denied.dart';
import 'package:e_travel/app/util/consts/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class BookingView extends GetView<BookingController> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<BookingController>(
        init: BookingController(),
        initState: (_) {},
        builder: (_con) =>
            Scaffold(
              body: CustomWidget(
                  title: 'myBooking'.tr,
                  image: 'assets/icons/booking.svg',
                  main: true,
                  child: Padding(
                      padding:
                      EdgeInsets.only(top: 20.h, right: 20.w, left: 20.w),
                      child: Get.find<GetStorage>().hasData('token')
                          ? Column(
                        children: [
                          Container(
                            height: Get.height / 1.4,
                            padding: EdgeInsets.symmetric(
                                horizontal: 30.w, vertical: 10.h),
                            decoration: BoxDecoration(
                              color: whiteColor,
                              borderRadius:
                              BorderRadius.all(Radius.circular(20.r)),
                            ),
                            child: Column(
                              crossAxisAlignment:
                              CrossAxisAlignment.start,
                              mainAxisSize: MainAxisSize.min,
                              children: [

                                SizedBox(
                                  height: 16,
                                ),
                                Expanded(
                                  child: Obx(
                                        () =>
                                    _con.loading.value
                                        ? _con.hotelBooking.isEmpty
                                        ? Center(
                                        child: CustomText(
                                          text: 'no flights'.tr,
                                          maxLines: 3,
                                          fontSize: 18.sp,
                                          textAlign:
                                          TextAlign.center,
                                          color: blackColor,
                                        ))
                                        : ListView.separated(
                                        padding: EdgeInsets.zero,
                                        itemBuilder: (BuildContext
                                        context,
                                            int index) =>
                                           Row(
                                             children: [
                                               ClipRRect(
                                                 borderRadius:
                                                 BorderRadius.circular(20.0),
                                                 // Adjust the radius here
                                                 child: Image.asset(
                                                   'assets/images/hotel1.jpg',
                                                   width: 70.h,
                                                   height: 70.h,
                                                   fit: BoxFit.fill,
                                                 ),
                                               ),
                                               SizedBox(width: 10.w,),
                                               Column(
                                                 crossAxisAlignment: CrossAxisAlignment.start,
                                                 children: [
                                                   CustomText(text:'hotel'.tr+' : '+ _con.hotelBooking[index].hotel!.nameAr.toString(),color: blackColor,),
                                                   CustomText(text:'roomNo'.tr+' : '+ _con.hotelBooking[index].hotelRoom!.roomNo.toString(),color: blackColor,),

                                                 ],
                                               )
                                               ],
                                           ),
                                        separatorBuilder:
                                            (BuildContext context,
                                            int index) =>
                                            Divider(),
                                        itemCount: _con
                                            .hotelBooking.length)
                                        : Center(
                                        child:
                                        CircularProgressIndicator()),
                                  ),
                                )
                              ],
                            ),
                          ),
                        ],
                      )
                          : PermissionDenied())),
            ));
  }
}
