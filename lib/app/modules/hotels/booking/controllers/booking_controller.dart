import 'package:e_travel/app/data/models/hotel_booking.dart';
import 'package:e_travel/app/data/repositories/hotel_booking_repository.dart';
import 'package:get/get.dart';

class BookingController extends GetxController {
  final _hotelBookingRepo = HotelBookingRepository.instance;
  final loading = false.obs;
  RxList<HotelBooking> hotelBooking = <HotelBooking>[].obs;

  @override
  void onInit() {
    super.onInit();
    loadHotelBooking();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}

  Future<void> loadHotelBooking() async {
    final response = await _hotelBookingRepo.getHotelBookings();
    hotelBooking.assignAll(response);
    loading.value = true;
  }


}
