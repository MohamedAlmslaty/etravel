import 'package:e_travel/app/data/models/available_room_types.dart';
import 'package:e_travel/app/modules/hotels/hotel/views/hotel_view.dart';
import 'package:e_travel/app/modules/widgets/custom_button.dart';
import 'package:e_travel/app/modules/widgets/custom_text.dart';
import 'package:e_travel/app/modules/widgets/custom_widget.dart';
import 'package:e_travel/app/routes/app_pages.dart';
import 'package:e_travel/app/util/consts/base.dart';
import 'package:e_travel/app/util/consts/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_spinbox/cupertino.dart';
import 'package:flutter_svg/flutter_svg.dart';

import 'package:get/get.dart';

import '../controllers/hotel_details_controller.dart';

class HotelDetailsView extends GetView<HotelDetailsController> {
  const HotelDetailsView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<HotelDetailsController>(
        init: HotelDetailsController(),
        builder: (_con) => Scaffold(
            body: CustomWidget(
                title: 'Residence details'.tr,
                child: Padding(
                  padding: EdgeInsets.all(20),
                  child: SingleChildScrollView(
                    child: Column(children: [
                      SizedBox(
                        height: 20.h,
                      ),
                      Card(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                        color: Colors.white,
                       // elevation: 4.0,
                        child: Padding(
                            padding: const EdgeInsets.all(16.0),
                            child: Column(children: [
                              Row(
                                children: [
                                  ClipRRect(
                                    borderRadius: BorderRadius.circular(
                                        25.0), // Adjust the radius here
                                    child: Image.network(
                                      imageUrl(_con.hotel.value.imageFileUrl
                                          .toString()),
                                      width: 120.h,
                                      height: 120.h,
                                      fit: BoxFit.fill,
                                    ),
                                  ),
                                  SizedBox(
                                    width: 10.w,
                                  ),
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      StarDisplay(value: 4),
                                      CustomText(
                                        text:
                                            _con.hotel.value.nameAr.toString(),
                                        fontSize: 18.0,
                                        fontWeight: FontWeight.bold,
                                        color: primaryColor,
                                      ),
                                      SizedBox(
                                        width: 170.w,
                                        child: CustomText(
                                          text: _con.hotel.value.detailsAr
                                              .toString(),
                                          maxLines: 9,
                                          fontSize: 10.0,
                                          color: primaryColor,
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                              SizedBox(
                                height: 30.h,
                              ),
                              ListView.separated(
                                padding: EdgeInsets.zero,
                                  shrinkWrap: true,
                                  physics: NeverScrollableScrollPhysics(),
                                  itemBuilder: (context, index) {
                                    final room = _con
                                        .hotel.value.availableRoomTypes![index];
                                    return  Row(
                                          children: [
                                            SizedBox(
                                                width: 40.w,
                                                height: 40.w,
                                                child: Obx(() => Checkbox(
                                                    activeColor:
                                                        MaterialStateColor
                                                            .resolveWith(
                                                                (states) =>
                                                                    primaryColor),
                                                    value: room.checked.value,
                                                    onChanged: (value) {
                                                      room.checked.value =
                                                          value!; //selected value
                                                      if (_con.rooms
                                                          .contains(room)) {
                                                        _con.rooms.remove(room);
                                                      } else
                                                        _con.rooms.add(
                                                            room); //selected value
                                                    }))),
                                            ClipRRect(
                                              borderRadius:
                                                  BorderRadius.circular(20.0),
                                              // Adjust the radius here
                                              child: Image.network(
                                                imageUrl(room.imageFileUrl!),
                                                width: 100.h,
                                                height: 100.h,
                                                fit: BoxFit.fill,
                                              ),
                                            ),
                                            SizedBox(width: 10.w,),
                                            Column(
                                              children: [
                                                CustomText(
                                                  text: room.nameAr.toString(),
                                                  color: primaryColor,
                                                  fontSize: 18.sp,
                                                ),
                                                Row(children: [
                                                SvgPicture.asset('assets/icons/user_p.svg',width: 18.w,height: 18.w,),
                                               SizedBox(width: 5.w,),
                                                CustomText(
                                                  text: room.maxOccupancy.toString(),
                                                  color: primaryColor,
                                                  fontSize: 18.sp,
                                                ),
                                                  SizedBox(width: 20.w,),
                                                  SvgPicture.asset('assets/icons/bed.svg',width: 15.w,height: 15.w,),

                                                  // CustomText(
                                                  //   text: room.bedTypes.toString(),
                                                  //   color: primaryColor,
                                                  //   fontSize: 18.sp,
                                                  // ),
                                                ],),



                                                Row(
                                                  children: [
                                                    IconButton(
                                                      onPressed: () {
                                                        room.quantity.value++;
                                                        room.requestedNumberOfRooms!.value=room.quantity.value;
                                                      },
                                                      icon: Icon(
                                                        Icons.add_circle_outline,
                                                        color: secondaryColor,
                                                      ),
                                                    ),
                                                    SizedBox(
                                                      width: 5.w,
                                                    ),
                                                    Obx(() => CustomText(
                                                      text: room.quantity.value.toString(),
                                                      fontWeight: FontWeight.w500,
                                                      fontSize: 18.sp,
                                                      color: primaryColor,
                                                    )),
                                                    SizedBox(
                                                      width: 5.w,
                                                    ),
                                                    IconButton(
                                                      onPressed: () {
                                                        if(room.quantity.value!=1)
                                                        room.quantity.value--;
                                                        room.requestedNumberOfRooms!.value= room.quantity.value;

                                                      },
                                                      icon: Icon(
                                                        Icons.remove_circle_outline,
                                                        color: secondaryColor,
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                                Container(
                                                  padding: EdgeInsets.symmetric(
                                                      horizontal: 10.w,
                                                      vertical: 4.h),
                                                  decoration: BoxDecoration(
                                                      color: Colors.green,
                                                      borderRadius:
                                                      BorderRadius.circular(
                                                          20.r)),
                                                  child: Obx(()=>CustomText(
                                                    text:
                                                    '${room.price!*room.quantity.value}' +
                                                        ' ' +
                                                        'LY'.tr,
                                                    fontSize: 14.0,
                                                    fontWeight: FontWeight.bold,
                                                    color: whiteColor,
                                                  ),
                                                ),
                                                ),
                                              ],
                                            ),

                                          ],
                                        );
                                  },
                                  separatorBuilder: (context, index) =>
                                      SizedBox(
                                        height: 10.h,
                                      ),
                                  itemCount: _con
                                      .hotel.value.availableRoomTypes!.length),
                      SizedBox(
                          height: 20.h),
                              CustomButton(text: 'choose'.tr,onClick: ()=>_con.chooseRooms(),)
                            ])),
                      ),
                    ]),
                  ),
                ))));
  }
}
