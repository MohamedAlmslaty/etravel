import 'package:e_travel/app/data/models/available_room_types.dart';
import 'package:e_travel/app/data/models/hotel.dart';
import 'package:e_travel/app/routes/app_pages.dart';
import 'package:e_travel/app/util/consts/style.dart';
import 'package:get/get.dart';

class HotelDetailsController extends GetxController {
  final hotel = Hotel().obs;
  final selectedRoom = false.obs;
  RxList<AvailableRoomTypes> rooms = <AvailableRoomTypes>[].obs;

  @override
  void onInit() {
    super.onInit();
    if(Get.arguments!=null)
      hotel.value=Get.arguments;
  }
chooseRooms(){
    if(rooms.length==0)
      showSnackBar(title: 'error'.tr, message: 'please select room'.tr);
      else{
      Get.toNamed(
          Routes.HOTEL_CONFIRM,
          arguments: {
            'hotel': hotel.value,
            'rooms': rooms
          });
    }


}

}
