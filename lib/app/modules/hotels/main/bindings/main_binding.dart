import 'package:e_travel/app/modules/main/controllers/main_controller.dart';
import 'package:get/get.dart';

class MainHotelBinding extends Bindings {
  @override
  void dependencies() {
    Get.put<MainController>(
      MainController(),
    );
  }
}
