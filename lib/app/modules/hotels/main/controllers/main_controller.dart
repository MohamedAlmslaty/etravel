
import 'package:e_travel/app/data/repositories/wallet_repository.dart';
import 'package:e_travel/app/modules/account/bindings/account_binding.dart';
import 'package:e_travel/app/modules/account/views/account_view.dart';
import 'package:e_travel/app/modules/hotels/booking/bindings/booking_binding.dart';
import 'package:e_travel/app/modules/hotels/booking/views/booking_view.dart';
import 'package:e_travel/app/modules/hotels/home/bindings/home_binding.dart';
import 'package:e_travel/app/modules/hotels/home/views/home_view.dart';
import 'package:e_travel/app/modules/wallet/controllers/wallet_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class MainHotelController extends GetxController {
  var currentIndex = 0.obs;
  late PageController pageController;
  final _walletRepo = WalletRepository.instance;
  final balance = 0.0.obs;
  List pageList = [
    HomeView(),
    BookingView(),
    AccountView(),
  ];
  late DateTime currentBackPressTime;

  void changeCurrentIndex(index) {
    currentIndex.value = index;
    pageController.jumpToPage(index);
    Get.put(WalletController()).loadBalance();
  }

  @override
  void onInit() async {
    pageController = PageController(initialPage: currentIndex.value);
    if (Get.find<GetStorage>().hasData('token')) {
      loadBalance();
    }
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }


  Future<void> loadBalance() async {
    try {
      loadingBalance.value = true;
      balance.value = await _walletRepo.getWalletBalance();
      update();
      if (balance.value != null && balance.value != "") {
        Get.find<GetStorage>().write('wallet_amount', balance.value);
        if (Get.find<GetStorage>().hasData('wallet_amount')) walletBalance.value = Get.find<GetStorage>().read('wallet_amount');
      }
    } catch (e) {
      print(e.toString());
    } finally {
      loadingBalance.value = false;
    }
  }

  bindView(int index) {
    if (index == 0) AccountBinding().dependencies();
    if (index == 1) BookingBinding().dependencies();
    if (index == 2) HomeBinding().dependencies();

  }

  Future<bool> onWillPop() {
    SystemChannels.platform.invokeMethod('SystemNavigator.pop');
    return Future.value(true);
  }
  @override
  void onClose() {}
}
