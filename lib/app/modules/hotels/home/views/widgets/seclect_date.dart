import 'package:e_travel/app/data/models/select_date.dart';
import 'package:e_travel/app/modules/book_flight/views/widgets/cart_done_booking_widget.dart';
import 'package:e_travel/app/util/consts/colors.dart';
import 'package:e_travel/app/modules/widgets/custom_text.dart';
import 'package:e_travel/app/util/consts/style.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:intl/intl.dart';
import 'package:scrollable_clean_calendar/controllers/clean_calendar_controller.dart';
import 'package:scrollable_clean_calendar/scrollable_clean_calendar.dart';
import 'package:scrollable_clean_calendar/utils/enums.dart';

class SelectDateView extends StatelessWidget {
  SelectDate selectDate=new SelectDate();
  @override
  Widget build(BuildContext context) {
    String local=Get.find<GetStorage>().read('language');
          final calendarController = CleanCalendarController(
            minDate: DateTime.now(),
            maxDate: DateTime.now().add(const Duration(days: 365)),
            onRangeSelected: (firstDate, secondDate) {
              selectDate.departureDate.value =
                  DateFormat('yyyy-MM-dd').format(firstDate).toString();
              Get.arguments == 1 ?
              selectDate.returnDate.value = DateFormat('yyyy-MM-dd')
                  .format(secondDate ?? firstDate)
                  .toString()
              :null;
            },
            onDayTapped: (date) {},
            onPreviousMinDateTapped: (date) {},
            onAfterMaxDateTapped: (date) {},
            rangeMode: Get.arguments == 1 ?true:false,
            weekdayStart: DateTime.saturday,
            initialDateSelected: selectDate.departureDate.value.isNotEmpty
                ? DateTime.tryParse(selectDate.departureDate.value)
                : null,
            endDateSelected: selectDate.returnDate.value.isNotEmpty
                ? DateTime.tryParse(selectDate.returnDate.value)
                : null,
          );
        return Scaffold(
            bottomNavigationBar: Obx(
              () => CartDaneBookingWidget(
                departure: 'assets/icons/departure.svg',
                destination: 'assets/icons/destination.svg',
                rightText: selectDate.departureDate.value,
                leftText: selectDate.returnDate.value,
                btnText: "chosen".tr,
                onClick: () {
                  if (Get.arguments == 0 && selectDate.departureDate.value.isEmpty) {
                    showSnackBar(title:'error'.tr,message: 'Please select a departure date'.tr);
                  } else if (Get.arguments == 1 &&
                      selectDate.departureDate.value.isEmpty &&
                      selectDate.returnDate.value.isEmpty) {
                    showSnackBar(title:'error'.tr,message: 'Please select a departure and return date'.tr);
                  } else
                    Get.back(result: selectDate);
                },
              ),
            ),
            body: Container(
                color: primaryColor,
                child: Column(children: <Widget>[
                  SizedBox(
                    height: 42.h,
                  ),
                  Row(
                    children: [
                      IconButton(
                        icon: Icon(
                          Icons.arrow_back,
                          color: whiteColor,
                          size: 16.1.w,
                        ),
                        onPressed: () => Get.back(),
                      ),
                      CustomText(text: 'Choose the date'.tr),
                    ],
                  ),
                  Expanded(
                      child:
                          ListView(padding: EdgeInsets.zero, children: <Widget>[
                    Container(
                      height: Get.height,
                      child: Card(
                          child: Container(
                        color: whiteColor,
                        child: ScrollableCleanCalendar(
                          calendarController: calendarController,
                          layout: Layout.BEAUTY,
                          calendarCrossAxisSpacing: 0,
                          // dayBuilder: (context, values) {
                          //   final date = values.day;
                          //   final events = values.text;
                          //   final isSelected = values.isSelected;
                          //
                          //   // Customize the appearance of the day cell based on your requirements.
                          //   return Container(
                          //     margin: EdgeInsets.all(4),
                          //     decoration: BoxDecoration(
                          //       shape: BoxShape.circle,
                          //       color: isSelected ? Colors.blue : Colors.transparent,
                          //     ),
                          //     child: Center(
                          //       child: Text(
                          //         date.day.toString(),
                          //         style: TextStyle(
                          //           color: isSelected ? Colors.white : Colors.black,
                          //           fontWeight: FontWeight.bold,
                          //         ),
                          //       ),
                          //     ),
                          //   );
                          // },
                          locale: local,
                          daySelectedBackgroundColor: primaryColor,
                          dayTextStyle: TextStyle(
                            color: blackColor
                          ),
                          dayDisableColor: Colors.red,
                          dayRadius: 0,
                        ),
                      )),
                    )
                  ]))
                ]))
          );
  }

 }
