import 'package:e_travel/app/modules/hotels/home/controllers/city_controller.dart';
import 'package:e_travel/app/modules/widgets/custom_text.dart';
import 'package:e_travel/app/modules/widgets/custom_widget.dart';
import 'package:e_travel/app/util/consts/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class SelectCities extends GetView<CityController> {
  bool local = Get.find<GetStorage>().read('language') == 'ar';

  @override
  Widget build(BuildContext context) {
    return GetBuilder<CityController>(
        init: CityController(),
        initState: (_) {},
        builder: (_) => Scaffold(
                body: SingleChildScrollView(
                    child: CustomWidget(
              title: 'Choose the city'.tr,
              height: Get.height / 1.2,
              child: _.loading.value
                  ? Obx(() => Container(
                      padding: EdgeInsets.symmetric(
                          horizontal: 30.w, vertical: 10.h),
                      decoration: BoxDecoration(
                        color: whiteColor,
                        borderRadius: BorderRadius.all(Radius.circular(20.r)),
                      ),
                      child: Column(
                        children: [
                          Container(
                            margin: EdgeInsets.fromLTRB(16, 16, 10, 10),
                            child: TextField(
                              controller: _.Controller,
                              decoration: InputDecoration(
                                prefixIcon: Icon(
                                  Icons.search,
                                  color: primaryColor,
                                ),
                                suffixIcon: InkWell(
                                    onTap: () {
                                      _.citiesItems.clear();
                                      _.citiesItems.assignAll(_.cities);
                                      _.Controller.clear();
                                    },
                                    child: Icon(
                                      Icons.cancel_outlined,
                                      color: Colors.grey,
                                    )),
                                hintText: 'Choose the city'.tr,
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(20),
                                  borderSide: BorderSide(color: secondaryColor),
                                ),
                                focusedBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(20),
                                  borderSide: BorderSide(color: primaryColor),
                                ),
                                enabledBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(20),
                                  borderSide: BorderSide(color: secondaryColor),
                                ),
                              ),
                              onChanged: _.getSuggestions,
                            ),
                          ),
                          Expanded(
                              child: Scrollbar(
                                  controller: _.scrollController,
                                  thickness: 5,
                                  radius: Radius.circular(50),
                                  trackVisibility: true,
                                  thumbVisibility: true,
                                  child: ListView.builder(
                                      controller: _.scrollController,
                                      padding: EdgeInsets.zero,
                                      itemCount: _.citiesItems.length,
                                      itemBuilder: (context, index) {
                                        final city = _.citiesItems[index];
                                        return Container(
                                            color: _.Controller.text ==
                                                    (local
                                                        ? city.nameAr
                                                        : city.nameEn)
                                                ? primaryColor.withOpacity(0.5)
                                                : Colors.transparent,
                                            child: ListTile(
                                              onTap: () {
                                                _.cityId= city.id.toString();
                                                _.Controller.text = local
                                                    ? city.nameAr.toString()
                                                    : city.nameEn.toString();
                                                _.airlineScrees.cityName.value =
                                                    _.Controller.text;
                                                Get.back(
                                                    result: _.airlineScrees);
                                                _.Controller.clear();
                                              },
                                              leading: SvgPicture.asset(
                                                'assets/icons/departure.svg',
                                                width: 20.w,
                                                height: 20.h,
                                              ),
                                              title: CustomText(
                                                text: local
                                                    ? city.nameAr.toString()
                                                    : city.nameEn.toString(),
                                                fontWeight: FontWeight.bold,
                                                color: blackColor,
                                              ),
                                              subtitle: city.country != null
                                                  ? Text(local
                                                      ? city.country!.nameAr
                                                          .toString()
                                                      : city.country!.nameEn
                                                          .toString())
                                                  : SizedBox(),
                                            ));
                                      })))
                        ],
                      )))
                  : Center(child: CircularProgressIndicator()),
            ))));
  }
}
