import 'package:e_travel/app/data/models/airline_screen.dart';
import 'package:e_travel/app/data/models/select_date.dart';
import 'package:e_travel/app/modules/hotels/home/controllers/home_controller.dart';
import 'package:e_travel/app/modules/widgets/custom_button.dart';
import 'package:e_travel/app/modules/widgets/custom_row.dart';
import 'package:e_travel/app/modules/widgets/custom_text.dart';
import 'package:e_travel/app/routes/app_pages.dart';
import 'package:e_travel/app/util/consts/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_spinbox/flutter_spinbox.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class OneWayView extends StatelessWidget {
  OneWayView();

  @override
  Widget build(BuildContext context) {
    String local = Get.find<GetStorage>().read('language');
    return GetBuilder<HomeController>(
        init: HomeController(),
        initState: (_) {},
        builder: (_con) => SingleChildScrollView(
                child: Column(
              children: [
                SizedBox(
                  height: 20.h,
                ),
                SearchPlace(
                  title: 'Click here to select the country and city'.tr,
                  text: 'Departure'.tr,
                  image: 'assets/icons/departure.svg',
                  roundTrip: false,
                  con: _con,
                ),
                SizedBox(
                  height: 10.h,
                ),
                Obx(() =>  CustomRow(
                    width: 115.w,
                    title: 'DepartureDate'.tr,
                    text: _con.checkInDate.value.isEmpty
                        ? 'DepartureDate'.tr
                        : _con.checkInDate.value,
                    reTitle: 'ReturnDate'.tr,
                    reText: _con.checkoutDate.value.isEmpty
                        ? 'ReturnDate'.tr
                        : _con.checkoutDate.value,
                    image: 'assets/icons/chooseDate.svg',
                    onClick: () async {
                      SelectDate result = await Get.toNamed(
                          Routes.SELECT_DATE,
                          arguments: 1);
                      if (result.departureDate.value.isNotEmpty &&
                          result.returnDate.value.isNotEmpty) {
                        _con.checkInDate.value =
                            result.departureDate.value;
                        _con.checkoutDate.value = result.returnDate.value;
                      }
                    })
                   ),
                                SizedBox(
                  height: 10.h,
                ),
                Obx(() => CustomRow(
                      title: 'Search options'.tr,
                      text: 'adult'.tr +
                          '  ${_con.adult.value} , ' +
                          'child'.tr +
                          '  ${_con.children.value}',
                      image: 'assets/icons/searchOptions.svg',
                      onClick: () => Passengers(_con),
                    )),
                SizedBox(
                  height: 20.h,
                ),
                CustomButton(
                    text: 'SearchResidence'.tr,
                    fontSize:12.sp,
                    width: 150.w,
                    onClick: () => _con.loadHotels()),

              ],
            )));
  }


  void Passengers(_con) {
    Get.bottomSheet(

        Container(
          height: Get.height /2,
          child: Padding(
            padding: const EdgeInsets.all(28.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        child: Text(
                          'Search options'.tr,
                          style: TextStyle(
                            fontSize: 20.sp,
                            color: blackColor,
                          ),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(
                          top: 10.h,
                          bottom: 10.h,
                        ),
                        child: Column(
                          children: <Widget>[
                            Container(
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Container(
                                    child: Row(
                                      children: <Widget>[
                                        Container(
                                          child: Icon(
                                            Icons.person,
                                            color: primaryColor,
                                            size: 25,
                                          ),
                                        ),
                                        SizedBox(
                                          width: 10.w,
                                        ),
                                        Container(
                                          padding: EdgeInsets.only(
                                            left: 20.w,
                                          ),
                                          child: CustomText(
                                            text: 'adult'.tr,
                                            fontSize: 18.sp,
                                            color: primaryColor,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Container(
                                      width: 150.w,
                                      child: CupertinoSpinBox(
                                        incrementIcon: Icon(
                                          Icons.add_circle_outline,
                                          color: secondaryColor,
                                        ),
                                        decrementIcon: Icon(
                                          Icons.remove_circle_outline,
                                          color: secondaryColor,
                                        ),
                                        decoration: BoxDecoration(
                                          color: whiteColor,
                                        ),
                                        textStyle: TextStyle(
                                          color: primaryColor,
                                        ),
                                        min: 1,
                                        max: 10,
                                        value: _con.adult.value.toDouble(),
                                        onChanged: (value) {
                                          _con.adult.value = value.toInt();
                                        },
                                      )),
                                ],
                              ),
                            ),
                            Container(
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Container(
                                    child: Row(
                                      children: <Widget>[
                                        Container(
                                          child: Icon(
                                            Icons.boy,
                                            size: 20,
                                            color: primaryColor,
                                          ),
                                        ),
                                        SizedBox(
                                          width: 10.w,
                                        ),
                                        Container(
                                          padding: EdgeInsets.only(
                                            left: 20.w,
                                          ),
                                          child: CustomText(
                                            text: 'child'.tr,
                                            fontSize: 18.sp,
                                            color: primaryColor,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Container(
                                    width: 150.w,
                                    child: CupertinoSpinBox(
                                      incrementIcon: Icon(
                                        Icons.add_circle_outline,
                                        color: secondaryColor,
                                      ),
                                      decrementIcon: Icon(
                                        Icons.remove_circle_outline,
                                        color: secondaryColor,
                                      ),
                                      decoration: BoxDecoration(
                                        color: whiteColor,
                                      ),
                                      textStyle: TextStyle(
                                        color: primaryColor,
                                      ),
                                      min: 0,
                                      max: 10,
                                      value: _con.children.value.toDouble(),
                                      onChanged: (value) {
                                        _con.children.value = value.toInt();
                                      },
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Container(
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Container(
                                    child: Row(
                                      children: <Widget>[
                                        Container(
                                          child: Icon(
                                            Icons.child_friendly_sharp,
                                            size: 20,
                                            color: primaryColor,
                                          ),
                                        ),
                                        SizedBox(
                                          width: 10.w,
                                        ),
                                        Container(
                                          padding: EdgeInsets.only(
                                            left: 20.w,
                                          ),
                                          child: CustomText(
                                            text: 'infant'.tr,
                                            fontSize: 18.sp,
                                            color: primaryColor,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Container(
                                    width: 150.w,
                                    child: CupertinoSpinBox(
                                      incrementIcon: Icon(
                                        Icons.add_circle_outline,
                                        color: secondaryColor,
                                      ),
                                      decrementIcon: Icon(
                                        Icons.remove_circle_outline,
                                        color: secondaryColor,
                                      ),
                                      decoration: BoxDecoration(
                                        color: whiteColor,
                                      ),
                                      textStyle: TextStyle(
                                        color: primaryColor,
                                      ),
                                      min: 0,
                                      max: 10,
                                      value: _con.children.value.toDouble(),
                                      onChanged: (value) {
                                        _con.children.value = value.toInt();
                                      },
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
        elevation: 20.0,
        enableDrag: false,
        backgroundColor: whiteColor,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
          topLeft: Radius.circular(30.0),
          topRight: Radius.circular(30.0),
        )));
  }

  Widget SearchPlace(
      {required String title,
      required String text,
      required String image,
      required bool roundTrip,
      required HomeController con}) {
    return Obx(
      () => CustomRow(
        title: title,
        text:
            '${con.cityName.value}',
        image: image,
        onClick: () async {
          await con.loadCities();
          AirlineScrees result =
              await Get.toNamed(Routes.SELECT_CITIES, arguments: {
            'cities': con.cities,
            'citiesItems': con.citiesItems,
            'citiesName': con.citiesName,
            'cityId': con.cityId.value,
            'loading': con.load.value,
            'cityName': con.cityName.value,
          });
          if (result.cityName.value.isNotEmpty) {
            con.cityName.value = result.cityName.value;
            con.cityId.value = result.cityId.value;
          }
        },
      ),
    );
  }


}
