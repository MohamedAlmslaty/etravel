import 'package:e_travel/app/modules/hotels/home/controllers/home_controller.dart';
import 'package:e_travel/app/modules/hotels/home/views/widgets/one_way_view.dart';
import 'package:e_travel/app/modules/widgets/custom_widget.dart';
import 'package:e_travel/app/util/consts/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

class HomeView extends GetView<HomeController> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<HomeController>(
      init: HomeController(),
      builder: (_con) => Scaffold(
        backgroundColor: primaryColor,
        body: CustomWidget(
        title: 'ResidenceSearch'.tr,
        image: 'assets/icons/search.svg',
        main: true,
        child: Padding(
          padding: EdgeInsets.only(top: 20.h, right: 20.w, left: 20.w),
          child: Column(
                children: [
                  OneWayView(),
                                ],
              )),
        ),
      ),
    );
  }
}
