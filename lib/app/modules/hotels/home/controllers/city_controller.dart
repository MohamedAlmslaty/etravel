import 'package:e_travel/app/data/models/airline_screen.dart';
import 'package:e_travel/app/data/models/city.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class CityController extends GetxController {
  final loading = false.obs;
  String cityName = '';
  String cityId = '';
  String airlineText = '';
  RxList<City> cities = <City>[].obs;
  RxList<City> citiesItems = <City>[].obs;
  RxList<City> dummyCities = <City>[].obs;
  List<String> citiesName = [];
  final TextEditingController Controller = TextEditingController();
  final ScrollController scrollController = ScrollController();
  AirlineScrees airlineScrees = new AirlineScrees();

  @override
  void onInit() {
    super.onInit();
    if (Get.arguments != null) {
      cities = Get.arguments['cities'];
      citiesName = Get.arguments['citiesName'];
      citiesItems = Get.arguments['citiesItems'];
      loading.value = Get.arguments['loading'];
      cityName = Get.arguments['cityName'];
      cityId = Get.arguments['cityId'];
      Controller.text = cityName;
    }
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    Controller.dispose();
    citiesItems.clear();
    cities.clear();
    citiesName.clear();
  }

  void getSuggestions(String query) {
    if (query.isNotEmpty) {
      dummyCities.clear();
      dummyCities.addAll(cities);
      dummyCities.retainWhere((s) => Get.find<GetStorage>().read('language') ==
              'ar'
          ? s.nameAr.toString().toLowerCase().contains(query.toLowerCase()) ||
              s.country!.nameAr
                  .toString()
                  .toLowerCase()
                  .contains(query.toLowerCase())
          : s.nameEn.toString().toLowerCase().contains(query.toLowerCase()) ||
              s.country!.nameEn
                  .toString()
                  .toLowerCase()
                  .contains(query.toLowerCase()));
      citiesItems.clear();
      citiesItems.assignAll(dummyCities);
    } else {
      citiesItems.clear();
      citiesItems.assignAll(cities);
    }
  }
}
