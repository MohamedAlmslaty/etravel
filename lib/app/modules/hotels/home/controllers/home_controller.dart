import 'dart:ffi';

import 'package:e_travel/app/data/models/city.dart';
import 'package:e_travel/app/data/models/hotel.dart';
import 'package:e_travel/app/data/repositories/city_repository.dart';
import 'package:e_travel/app/data/repositories/hotel_repository.dart';
import 'package:e_travel/app/routes/app_pages.dart';
import 'package:e_travel/app/util/consts/style.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class HomeController extends GetxController {
  final _cityRepo = CityRepository.instance;
  final _HotelRepo = HotelRepository.instance;
  final cityName = ''.obs;
  final cityId = ''.obs;
  final checkInDate = ''.obs;
  final checkoutDate = ''.obs;
  final adult = 1.obs;
  final children = 0.obs;
  final load = false.obs;
  RxList<Hotel> hotels = <Hotel>[].obs;
  RxList<City> cities = <City>[].obs;
  RxList<City> citiesItems = <City>[].obs;
  RxList<City> cities1 = <City>[].obs;
  List<String> citiesName = [];

  @override
  void onInit() {
    super.onInit();
    loadCities();
  }

  @override
  void onReady() {
    super.onReady();
  }

  Future<void> loadCities() async {
    if (cities.isEmpty) {
      final response = await _cityRepo.getCities();
      cities.assignAll(response);
      if (cities.isNotEmpty) citiesItems.assignAll(cities);
      for (int i = 0; i < cities.length; i++) {
        citiesName.add(Get.find<GetStorage>().read('language') == 'ar'
            ? cities[i].nameAr.toString()
            : cities[i].nameEn.toString());
      }
      update();
      load.value = true;
    } else if (cities.isNotEmpty) {
      final response = await _cityRepo.getCities();
      cities1.assignAll(response);
      if (cities.isNotEmpty && cities.length != cities1.length) {
        for (int i = 0; i < cities1.length; i++) {
          citiesName.add(Get.find<GetStorage>().read('language') == 'ar'
              ? cities[i].nameAr.toString()
              : cities[i].nameEn.toString());
        }
        update();
        load.value = true;
      } else if (cities.isNotEmpty) {
        for (int i = 0; i < cities.length; i++) {
          citiesName.add(Get.find<GetStorage>().read('language') == 'ar'
              ? cities[i].nameAr.toString()
              : cities[i].nameEn.toString());
        }
        update();
      }
    }
  }

  Future<void> loadHotels() async {
    if(checkInDate.isNotEmpty&&checkoutDate.isNotEmpty){
    final response = await _HotelRepo.getHotels(
        adults: adult.value,
        cityId: '1',
        checkInDate: checkInDate.value,
        checkoutDate: checkoutDate.value,
        children: children.value);
    hotels.assignAll(response);
    Get.toNamed(Routes.HOTEL, arguments: hotels);
    Get.find<GetStorage>().write('adults',adult.value);
    Get.find<GetStorage>().write('checkInDate',checkInDate.value);
    Get.find<GetStorage>().write('checkoutDate',checkoutDate.value);
    Get.find<GetStorage>().write('children',children.value);
    load.value = true;
  }else  showSnackBar(
        title: 'error'.tr, message: 'Please fill in all fields'.tr);
  }
}
