import 'package:e_travel/app/data/models/flight_booking.dart';
import 'package:e_travel/app/modules/widgets/custom_button.dart';
import 'package:e_travel/app/modules/widgets/custom_text.dart';
import 'package:e_travel/app/modules/widgets/custom_widget.dart';
import 'package:e_travel/app/routes/app_pages.dart';
import 'package:e_travel/app/util/consts/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

class SuccessHotel extends StatelessWidget {
  SuccessHotel();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: CustomWidget(
            title: 'ConfirmBooking'.tr,
            child: Padding(
                padding: EdgeInsets.all(20),
                child: Card(
                shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0),
                ),
                color: whiteColor,
                elevation: 4.0,
                child: Padding(
                padding: const EdgeInsets.all(16.0),
                child:Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    CustomText(
                      text:
                          'Your reservation has been completed successfully'.tr,
                      color: primaryColor,
                      textAlign: TextAlign.center,
                      maxLines: 3,
                      fontSize: 20.sp,
                      fontWeight: FontWeight.bold,
                    ),
                    CustomText(
                      text: 'You can check the reservation list'.tr,
                      color: primaryColor,
                      textAlign: TextAlign.center,
                      maxLines: 3,
                      fontSize: 20.sp,
                      fontWeight: FontWeight.bold,
                    ),
                    Image(
                      image: AssetImage('assets/images/success.gif'),
                      height: 150.0.h,
                    ),
                    SizedBox(
                      height: 10.h,
                    ),
                    CustomButton(
                      text: 'Go to Home'.tr,
                      onClick: () => Get.offAllNamed(Routes.MAIN_HOTEL),
                    ),
                  ],
                ))))));
  }
}
