import 'package:e_travel/app/modules/widgets/custom_text.dart';
import 'package:e_travel/app/modules/widgets/custom_widget.dart';
import 'package:e_travel/app/modules/widgets/radius_widget.dart';
import 'package:e_travel/app/routes/app_pages.dart';
import 'package:e_travel/app/util/consts/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:form_field_validator/form_field_validator.dart';

import 'package:get/get.dart';

import '../controllers/hotel_confirm_controller.dart';

class HotelConfirmView extends GetView<HotelConfirmController> {
  const HotelConfirmView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<HotelConfirmController>(
        init: HotelConfirmController(),
        builder: (_con) => Scaffold(
            body: CustomWidget(
                title: 'ConfirmBooking'.tr,
                child: Padding(
                    padding: EdgeInsets.all(20),
                    child: SingleChildScrollView(child:Column(
                      children: [
                        SizedBox(
                          height: 20.h,
                        ),
                        InkWell(
                          onTap: () => _con.bookingHotel(),
                          child: Card(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10.0),
                            ),
                            color: whiteColor,
                            elevation: 4.0,
                            child: Padding(
                                padding: EdgeInsets.all(16.0),
                                child:  Column(
                                  children: [
                                    CustomText(
                                      text: 'تفاصيل الحجز',
                                      fontSize: 18.0,
                                      fontWeight: FontWeight.bold,
                                      color: primaryColor,
                                    ),
                                    SizedBox(height: 10.h),
                                    ListView.separated(
                                      shrinkWrap: true,
                                        physics: NeverScrollableScrollPhysics(),
                                        itemBuilder: (context, index) => Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                              children: [
                                                CustomText(
                                                  text: 'room type'.tr+_con.rooms[index].nameAr.toString(),
                                                  fontSize: 18.0,
                                                  color: primaryColor,
                                                ),
                                                CustomText(
                                                  text: 'count'.tr+_con.rooms[index].requestedNumberOfRooms.toString(),
                                                  fontSize: 18.0,
                                                  color: primaryColor,
                                                ),
                                                CustomText(
                                                  text: 'LY'.tr+'  ${_con.rooms[index].requestedNumberOfRooms!*_con.rooms[index].price!}',
                                                  fontSize: 18.0,
                                                  color: primaryColor,
                                                ),

                                              ],
                                            ),
                                        separatorBuilder: (context, index) =>
                                            SizedBox(height: 10.h),
                                        itemCount: _con.rooms.length),
                                    CustomText(
                                      text: 'بيانات الحجز',
                                      fontSize: 18.0,
                                      fontWeight: FontWeight.bold,
                                      color: primaryColor,
                                    ),
                                    SizedBox(height: 10.h),
                                    RadiusCircular(
                                        child: TextFormField(
                                      keyboardType: TextInputType.text,
                                      inputFormatters: [
                                        FilteringTextInputFormatter(
                                            RegExp("[a-zA-Z]"),
                                            allow: true)
                                      ],
                                      controller: _con.firstName,
                                      validator: RequiredValidator(
                                          errorText:
                                              "Please enter firstName".tr),
                                      decoration: InputDecoration(
                                        contentPadding: EdgeInsets.all(12),
                                        hintText: 'First Name'.tr,
                                        fillColor: whiteColor,
                                        filled: true,
                                        border: InputBorder.none,
                                        hintStyle: TextStyle(
                                            color:
                                                primaryColor.withOpacity(0.7)),
                                      ),
                                    )),
                                    SizedBox(height: 10.h),
                                    RadiusCircular(
                                        child: TextFormField(
                                      keyboardType: TextInputType.text,
                                      inputFormatters: [
                                        FilteringTextInputFormatter(
                                            RegExp("[a-zA-Z]"),
                                            allow: true)
                                      ],
                                      controller: _con.lastName,
                                      validator: RequiredValidator(
                                          errorText:
                                              "Please enter lastName".tr),
                                      decoration: InputDecoration(
                                        contentPadding: EdgeInsets.all(12),
                                        hintText: 'Last Name'.tr,
                                        fillColor: whiteColor,
                                        filled: true,
                                        border: InputBorder.none,
                                        hintStyle: TextStyle(
                                            color:
                                                primaryColor.withOpacity(0.7)),
                                      ),
                                    )),
                                    SizedBox(height: 10.h),
                                    SizedBox(
                                      height: 10.h,
                                    ),
                                    Container(
                                      padding: EdgeInsets.symmetric(
                                          horizontal: 10.w, vertical: 4.h),
                                      decoration: BoxDecoration(
                                          color: Colors.green,
                                          borderRadius:
                                              BorderRadius.circular(20.r)),
                                      child: CustomText(
                                        text: 'الدفع',
                                        fontSize: 14.0,
                                        fontWeight: FontWeight.bold,
                                        color: whiteColor,
                                      ),
                                    )
                                  ],
                                )),
                          ),
                          ),
                      ],
                    ))))));
  }
}
