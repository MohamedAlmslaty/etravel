import 'package:get/get.dart';

import '../controllers/hotel_confirm_controller.dart';

class HotelConfirmBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<HotelConfirmController>(
      () => HotelConfirmController(),
    );
  }
}
