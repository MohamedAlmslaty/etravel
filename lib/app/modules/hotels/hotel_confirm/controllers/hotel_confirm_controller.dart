import 'package:e_travel/app/data/models/available_room_types.dart';
import 'package:e_travel/app/data/models/hotel.dart';
import 'package:e_travel/app/data/models/hotel_request.dart';
import 'package:e_travel/app/data/models/passenger.dart';
import 'package:e_travel/app/data/repositories/hotel_repository.dart';
import 'package:e_travel/app/data/repositories/passenger_repository.dart';
import 'package:e_travel/app/routes/app_pages.dart';
import 'package:e_travel/app/util/consts/loading.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class HotelConfirmController extends GetxController {
  final _passengerRepo = PassengerRepository.instance;
  final _hotelRepo = HotelRepository.instance;
  RxList<Passenger> passengers = <Passenger>[].obs;
  List<Passenger> selectedPassengers = [];
  RxList<AvailableRoomTypes> rooms = <AvailableRoomTypes>[].obs;
  final hotel = Hotel().obs;
  final firstName=TextEditingController();
  final lastName=TextEditingController();

  @override
  void onInit() {
    super.onInit();
    loadPassengers();
    if (Get.arguments != null) {
      hotel.value = Get.arguments['hotel'];
      rooms.assignAll(Get.arguments['rooms']);
    }
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

  Future<void> loadPassengers() async {
    final response = await _passengerRepo.getPassengers(0, 1000);
    passengers.assignAll(response);
    selectedPassengers.clear();
  }

  Future<void> bookingHotel() async {
    loading();
    final HotelRequestBooking hotelRequestBooking = HotelRequestBooking(
      children:  Get.find<GetStorage>().read('children'),
      checkInDate:  Get.find<GetStorage>().read('checkInDate'),
      checkOutDate:  Get.find<GetStorage>().read('checkoutDate'),
      adults:  Get.find<GetStorage>().read('adults'),
      hotelId: hotel.value.id,
      hotelRoomTypes: rooms,
      personFirstName: firstName.text.trim(),
      personLastName: lastName.text.trim(),
    );
    _hotelRepo
        .hotelBooking(hotelRequestBooking: hotelRequestBooking)
        .then((value) {
      if (value) {
        EasyLoading.dismiss();
        Get.toNamed(Routes.SUCCESS_HOTELS);
      }
    });
  }
}
