import 'package:e_travel/app/modules/widgets/custom_text.dart';
import 'package:e_travel/app/modules/widgets/custom_widget.dart';
import 'package:e_travel/app/routes/app_pages.dart';
import 'package:e_travel/app/util/consts/base.dart';
import 'package:e_travel/app/util/consts/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:get/get.dart';

import '../controllers/hotel_controller.dart';

class HotelView extends GetView<HotelController> {
  const HotelView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<HotelController>(
        init: HotelController(),
        builder: (_con) => Scaffold(
            body: CustomWidget(
                title: 'search result'.tr,
                child: Padding(
                    padding: EdgeInsets.all(20),
                    child: Column(
                      children: [
                        SizedBox(
                          height: 20.h,
                        ),
                        Obx(() => _con.hotels.length == 0
                            ? SizedBox()
                            : Expanded(
                                child: ListView.separated(
                                    padding: EdgeInsets.zero,
                                    itemBuilder: (context, index) => InkWell(
                                          onTap: () =>
                                              Get.toNamed(Routes.HOTEL_DETAILS,arguments: _con.hotels[index]),
                                          child: Card(
                                            shape: RoundedRectangleBorder(
                                              borderRadius:
                                                  BorderRadius.circular(10.0),
                                            ),
                                            color: whiteColor,
                                            elevation: 4.0,
                                            child: Padding(
                                                padding:
                                                    const EdgeInsets.all(16.0),
                                                child: Row(
                                                  children: [
                                                    ClipRRect(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              25.0),
                                                      // Adjust the radius here
                                                      child:  Image.network(
                                                        imageUrl( _con.hotels[index].imageFileUrl.toString()),
                                                        width: 120.h,
                                                        height: 120.h,
                                                        fit: BoxFit.fill,
                                                      ),
                                                    ),
                                                    SizedBox(
                                                      width: 10.w,
                                                    ),
                                                    Column(
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .start,
                                                      children: <Widget>[
                                                        StarDisplay(value: 4),
                                                        CustomText(
                                                          text: _con.hotels[index].nameAr.toString(),
                                                          fontSize: 18.0,
                                                          fontWeight:
                                                              FontWeight.bold,
                                                          color: primaryColor,
                                                        ),
                                                        SizedBox(
                                                          width: 170,
                                                          child: CustomText(
                                                            text:
                                                            _con.hotels[index].detailsAr.toString(),
                                                            maxLines: 3,
                                                            fontSize: 10.0,
                                                            color: primaryColor,
                                                          ),
                                                        ),
                                                        Container(
                                                          padding: EdgeInsets
                                                              .symmetric(
                                                                  horizontal:
                                                                      10.w,
                                                                  vertical:
                                                                      4.h),
                                                          decoration: BoxDecoration(
                                                              color:
                                                                  Colors.green,
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          20.r)),
                                                          child: CustomText(
                                                            text: '2500 د.ل',
                                                            fontSize: 14.0,
                                                            fontWeight:
                                                                FontWeight.bold,
                                                            color: whiteColor,
                                                          ),
                                                        )
                                                      ],
                                                    ),
                                                  ],
                                                )),
                                          ),
                                        ),
                                    separatorBuilder: (context, index) =>
                                        SizedBox(
                                          height: 5,
                                        ),
                                    itemCount: _con.hotels.length)))
                      ],
                    )))));
  }
}

class StarDisplay extends StatelessWidget {
  final int value;

  const StarDisplay({Key? key, this.value = 0})
      : assert(value != null),
        assert(value >= 0),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: List.generate(5, (index) {
        return Icon(
          index < value ? Icons.star : Icons.star_border,
          color: Colors.amber,
        );
      }),
    );
  }
}
