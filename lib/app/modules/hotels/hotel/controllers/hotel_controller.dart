import 'package:e_travel/app/data/models/hotel.dart';
import 'package:get/get.dart';

class HotelController extends GetxController {
  RxList<Hotel> hotels = <Hotel>[].obs;

  @override
  void onInit() {
    super.onInit();
    if(Get.arguments!=null){
      hotels=Get.arguments;
    }
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

}
