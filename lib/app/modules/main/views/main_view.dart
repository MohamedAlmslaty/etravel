import 'package:e_travel/app/modules/main/controllers/main_controller.dart';
import 'package:e_travel/app/util/consts/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';

class MainView extends GetView<MainController> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<MainController>(
      init: MainController(),
      initState: (_) {},
      builder: (_) =>  Scaffold(
          body: PageView.builder(
            controller: _.pageController,
            physics: NeverScrollableScrollPhysics(),
            itemBuilder: (context, index) {
              return _.pageList[index];
            },
            onPageChanged: (index) {
              _.currentIndex.value = index;
              _.selectedImage.value = true;
            },
          ),
          bottomNavigationBar: new Theme(
            data: Theme.of(context).copyWith(
                canvasColor: primaryColor,
                textTheme: Theme.of(context)
                    .textTheme
                    .copyWith(caption: new TextStyle(color: secondaryColor))),
            child: Obx(
              () =>  BottomNavigationBar(
                showSelectedLabels: true,
                showUnselectedLabels: true,
                unselectedItemColor: whiteColor,
                selectedItemColor: secondaryColor,
                onTap: _.changeCurrentIndex,
                currentIndex: _.currentIndex.value,
                items: [
                  BottomNavigationBarItem(
                    icon: SvgPicture.asset(
                      "assets/icons/search.svg",
                      width: 20.w,
                      height: 20.w,
                      color: whiteColor,
                    ),
                    activeIcon: SvgPicture.asset(
                      "assets/icons/search.svg",
                      width: 20.w,
                      height: 20.w,
                      color: secondaryColor,
                    ),
                    label: 'findFlight'.tr,
                  ),
                  BottomNavigationBarItem(
                    icon: SvgPicture.asset(
                      "assets/icons/linksHandWhite.svg",
                      width: 30.w,
                      height: 30.w,
                      color: whiteColor,
                    ),
                    activeIcon: SvgPicture.asset(
                      "assets/icons/linksHandWhite.svg",
                      width: 30.w,
                      height: 30.w,
                      color: secondaryColor,
                    ),
                    label: 'Services'.tr,
                  ),
                  BottomNavigationBarItem(
                    icon: SvgPicture.asset(
                      "assets/icons/booking.svg",
                      width: 20.w,
                      height: 20.w,
                      color: whiteColor,
                    ),
                    activeIcon: SvgPicture.asset(
                      "assets/icons/booking.svg",
                      width: 20.w,
                      height: 20.w,
                      color: secondaryColor,
                    ),
                    label: 'myBooking'.tr,
                  ),
                  BottomNavigationBarItem(
                    icon: SvgPicture.asset(
                      "assets/icons/packges.svg",
                      width: 20.w,
                      height: 20.w,
                      color: whiteColor,
                    ),
                    activeIcon: SvgPicture.asset(
                      "assets/icons/packges.svg",
                      width: 20.w,
                      height: 20.w,
                      color: secondaryColor,
                    ),
                    label: 'packages'.tr,
                  ),
                  BottomNavigationBarItem(
                    icon: SvgPicture.asset(
                      "assets/icons/account.svg",
                      width: 20.w,
                      height: 20.w,
                      color: whiteColor,
                    ),
                    activeIcon: SvgPicture.asset(
                      "assets/icons/account.svg",
                      width: 20.w,
                      height: 20.w,
                      color: secondaryColor,
                    ),
                    label: 'account'.tr,
                  ),
                ],
              ),
            ),
          ),
        ),
    );
  }
}
