import 'dart:io';

import 'package:e_travel/app/data/models/customer.dart';
import 'package:e_travel/app/data/repositories/user_repository.dart';
import 'package:e_travel/app/data/repositories/wallet_repository.dart';
import 'package:e_travel/app/modules/account/bindings/account_binding.dart';
import 'package:e_travel/app/modules/account/views/account_view.dart';
import 'package:e_travel/app/modules/booking/bindings/booking_binding.dart';
import 'package:e_travel/app/modules/booking/views/booking_view.dart';
import 'package:e_travel/app/modules/home/bindings/home_binding.dart';
import 'package:e_travel/app/modules/home/views/home_view.dart';
import 'package:e_travel/app/modules/package/bindings/package_binding.dart';
import 'package:e_travel/app/modules/package/views/package_view.dart';
import 'package:e_travel/app/modules/services/bindings/services_binding.dart';
import 'package:e_travel/app/modules/services/views/services_view.dart';
import 'package:e_travel/app/modules/wallet/controllers/wallet_controller.dart';
import 'package:e_travel/app/modules/widgets/custom_text.dart';
import 'package:e_travel/app/util/consts/colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:e_travel/app/data/repositories/setting_repository.dart';

class MainController extends GetxController {
  final _userRepo = UserRepository.instance;
  final _settingRepo = SettingRepository.instance;
  final user = Customer().obs;
  var currentIndex = 0.obs;
  final selectedImage = false.obs;
  final lastBuildCode = ''.obs;
  late PageController pageController;
  final _walletRepo = WalletRepository.instance;
  final balance = 0.0.obs;
  List pageList = [
    HomeView(),
    ServicesView(),
    BookingView(),
    PackageView(),
    AccountView(),
  ];
  late DateTime currentBackPressTime;

  void changeCurrentIndex(index) {
    currentIndex.value = index;
    pageController.jumpToPage(index);
    Get.put(WalletController()).loadBalance();
  }

  @override
  void onInit() async {
    pageController = PageController(initialPage: currentIndex.value);
    getSettingByKey();
    if (Get.find<GetStorage>().hasData('token')) {
      await getUser();
      loadBalance();
    }
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  Future<Customer> getUser() async {
    user.value = await _userRepo.getCustomerAccount();
    update();
    return user.value;
  }

  Future<void> loadBalance() async {
    try {
      loadingBalance.value = true;
      balance.value = await _walletRepo.getWalletBalance();
      update();
      if (balance.value != null && balance.value != "") {
        Get.find<GetStorage>().write('wallet_amount', balance.value);
        if (Get.find<GetStorage>().hasData('wallet_amount')) walletBalance.value = Get.find<GetStorage>().read('wallet_amount');
      }
    } catch (e) {
      print(e.toString());
    } finally {
      loadingBalance.value = false;
    }
  }

  bindView(int index) {
    if (index == 0) AccountBinding().dependencies();
    if (index == 1) PackageBinding().dependencies();
    if (index == 2) BookingBinding().dependencies();
    if (index == 3) ServiceBinding().dependencies();
    if (index == 4) HomeBinding().dependencies();
  }

  Future<bool> onWillPop() {
    SystemChannels.platform.invokeMethod('SystemNavigator.pop');
    return Future.value(true);
  }
  Future<void> getSettingByKey() async {
    await _settingRepo
        .getSetting('lastBuildCode')
        .then((value) {
      if (value.id != null) {
        checkForUpdate(value.value!);
      }
    });
  }
  Future<void> checkForUpdate(String lastBuildCode) async {
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    String currentVersion = packageInfo.version;
    print('currentVersion : $currentVersion');
    String latestVersion = lastBuildCode; // Implement this function to get the latest version from your server.

    if (latestVersion != null && latestVersion != currentVersion) {
      Get.defaultDialog(
        barrierDismissible: false,
        title: 'Update app'.tr,
        titleStyle: TextStyle(fontWeight: FontWeight.bold),
        backgroundColor: Color(0xffFEF200),
        content: CustomText(
            text:'Update app dec'.tr,color: blackColor,maxLines: 3,textAlign: TextAlign.center,),
        confirm: ElevatedButton(
          onPressed: () async {
            Platform.isAndroid
           ?launchUrl(Uri.parse('https://play.google.com/store/apps/details?id=ly.etravel.app.e_travel'))
           :launchUrl(Uri.parse('https://apps.apple.com/us/app/etravel/id1627862639'));
          },
          style: ElevatedButton.styleFrom(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20.0), // Custom border radius
              ),
              primary: primaryColor),
          child: CustomText(text:"Update".tr),
        ),
      );}
  }
  @override
  void onClose() {}
}
