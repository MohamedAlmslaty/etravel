import 'package:e_travel/app/data/models/airline.dart';
import 'package:e_travel/app/data/models/final_booking.dart';
import 'package:e_travel/app/data/models/fligh_rq.dart';
import 'package:e_travel/app/data/models/flight_req.dart';
import 'package:e_travel/app/data/models/itineraries.dart';
import 'package:e_travel/app/data/models/passenger.dart';
import 'package:e_travel/app/data/models/slider.dart';
import 'package:e_travel/app/data/repositories/airline_repository.dart';
import 'package:e_travel/app/data/repositories/flight_booking_repository.dart';
import 'package:e_travel/app/data/repositories/itineraries_repository.dart';
import 'package:e_travel/app/data/repositories/slider_repository.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';

class FlightController extends GetxController {
  final _itinerariesRepo = ItinerariesRepository.instance;
  final _airlineRepo = AirlineRepository.instance;
  final _sliderRepo = SliderRepository.instance;
  RxList<Slider> sliders = <Slider>[].obs;
  RxList<Slider> sliders2 = <Slider>[].obs;
  final loading = false.obs;
  final loaded = false.obs;
  final loadingAirline = false.obs;
  final checked = false.obs;
  final search = false.obs;
  final edit = false.obs;
  final returnFlight = false.obs;
  final pnr = ''.obs;
  final result = ''.obs;
  RxDouble total = 0.0.obs;
  final _bookingRepo = FlightBookingRepository.instance;
  late FlightRequest flightRequest;
  RxList<FlightRq> flightRq = <FlightRq>[].obs;
  RxList<FlightRq> flightRqEmpty = <FlightRq>[].obs;
  RxList<FlightRq> searchFlight = <FlightRq>[].obs;
  RxList<Airline> airlines = <Airline>[].obs;
  RxList<Airline> selectAirlines = <Airline>[].obs;
  late DateTime departureDate;
  late DateTime returnDate;
  List<Passenger> selectedPassengers = [];
  @override
  void onInit() {
    super.onInit();
    selectedPassengers.add(Passenger(id: 1));
    if (Get.arguments['flightRequest'] != null) {
      flightRequest = Get.arguments['flightRequest'];
      edit.value = Get.arguments['Edit'];
      edit.value = Get.arguments['Edit'];
      if (Get.arguments['returnFlight'] != null) {
        returnFlight.value = Get.arguments['returnFlight'];
      }
      if(Get.arguments['isMulti']){
        loadItinerariesMultiCity(flightRequest);
        flightRequest.multiCityFlights!.forEach((element) {
          result.value +=element.originLocationCode + ' '+ 'To'.tr+ ' ';
        });
        departureDate =
        DateTime.tryParse(flightRequest.multiCityFlights![0].departureDate.toString())!;
      }else {
        loadItineraries(flightRequest);
        departureDate =
        DateTime.tryParse(flightRequest.departureDate.toString())!;
        if (flightRequest.returnDate!.isNotEmpty) returnDate = DateTime.tryParse(flightRequest.returnDate.toString())!;

      }

      //TODO :: TO TEST
      // loadItinerariesStream(new FlightRequest());

         }
    if (Get.arguments['pnr'] != null) {
      pnr.value = Get.arguments['pnr'];
    }
    loadAirlines();
    loadSliders();
    loadSliders2();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}
  Future<void> loadSliders() async {
    final response = await _sliderRepo.getSliders('SEARCH_SCREEN');
    sliders.assignAll(response);
    update();
    loading.value = true;
  }
  Future<void> loadSliders2() async {
    final response = await _sliderRepo.getSliders('SEARCH_SCREEN2');
    sliders2.assignAll(response);
    update();
    loading.value = true;
  }
  Future<void> loadItineraries(FlightRequest flightRequest) async {
    loaded.value = true;
    try {
      final response = await _itinerariesRepo.getItineraries(flightRequest);
      flightRq.assignAll(response);
      if(flightRq.isNotEmpty){
        var toBeMoved = flightRq.where((flight) =>
            flight.itineraries!.any((itinerary) => itinerary.seatNo == "0")).toList();
        flightRqEmpty.addAll(toBeMoved);
        flightRq.removeWhere((flight) => toBeMoved.contains(flight));

      }
    } catch (e) {
      print(e.toString());
    } finally {
      loaded.value = false;
    }
  }
  Future<void> loadItinerariesMultiCity(FlightRequest flightRequest) async {
    loaded.value = true;
    try {
      final response = await _itinerariesRepo.getItinerariesMultiCity(flightRequest);
      flightRq.assignAll(response);
      if(flightRq.isNotEmpty){
        var toBeMoved = flightRq.where((flight) =>
            flight.itineraries!.any((itinerary) => itinerary.seatNo == "0")).toList();
        flightRqEmpty.addAll(toBeMoved);
        flightRq.removeWhere((flight) => toBeMoved.contains(flight));

      }
    } catch (e) {
      print(e.toString());
    } finally {
      loaded.value = false;
    }
  }

  //TODO :: WILL BE USED TO IMPROVE PERFORMANCE OF THE REQUEST
  Future<void> loadItinerariesStream(FlightRequest flightRequest) async {
    loaded.value = true;
    try {
      final stream = _itinerariesRepo.getItinerariesStream(flightRequest);

      stream.listen(
        (FlightRq updatedFlight) {
          // Process individual FlightRq object
          //print("from stream " + updatedFlight.toString());
          // Add updatedFlight to a List if you want to maintain a list
          flightRq.add(updatedFlight);
          print("Stream2    ------------ >>>> " + updatedFlight.toString());

          //TODO :: FIX THIS TO KEEP THE RABBIT RUNNING WHEN LOADING
          loaded.value = false;

          // update();  // This will update the UI
        },
        onError: (error) {
          print(error.toString());
        },
        onDone: () {
          loaded.value = false;
        },
      );
    } catch (e) {
      print(e.toString());
      loaded.value = false;
    }
  }

  Future<void> getPrice(FlightRq flightRq, onWay) async {
    loading();
    flightRequest.flightNumber = flightRq.itineraries![0].flightNo;
    flightRequest.departureTicketClass = flightRq.itineraries![0].ticketClass;

    //TODO :: TO FIX MJI TIP (will not work in multi stop becuse it only takes itineraries[0])
    flightRequest.originLocationCode = flightRq.itineraries![0].departureAirport!.iataCode;
    flightRequest.destinationLocationCode = flightRq.itineraries![0].arrivalAirport!.iataCode;

    flightRequest.returnTicketClass = onWay ? "" : flightRq.itineraries![1].ticketClass;
    flightRequest.returnFlightNumber = onWay ? "" : flightRq.itineraries![1].flightNo;
    FinalBooking finalBooking = FinalBooking(flightRequest: flightRequest, passengers: selectedPassengers);
    _bookingRepo.getTotalBooking(finalBooking).then((value) {
      if (value != null) {
        EasyLoading.dismiss();
        total.value = double.tryParse(value)!;
        flightRq.total = total.value;
      }
    });
  }

  Future<void> loadAirlines() async {
    loadingAirline.value = true;
    try {
      final response = await _airlineRepo.getAirlines();
      airlines.assignAll(response);
      update();
    } catch (e) {
      print(e.toString());
    } finally {
      loadingAirline.value = false;
    }
  }

  getFlightById() {
    if (selectAirlines.isEmpty || selectAirlines.length == 0) {
      search.value = false;
    } else {
      search.value = true;
      List<FlightRq> matches = [];
      matches.addAll(flightRq);
      searchFlight.clear();

      matches.forEach((e) {
        selectAirlines.forEach((element) {
          if (element.id == e.itineraries![0].airline!.id) {
            print(' e ${element.id}  :  ${e.itineraries![0].airline!.id}');
            searchFlight.add(e);
            print(searchFlight.length);
          } else {
            print('not e ${element.id}  :  ${e.itineraries![0].airline!.id}');
          }
        });
      });
      print(searchFlight.length);
    }
  }
}
