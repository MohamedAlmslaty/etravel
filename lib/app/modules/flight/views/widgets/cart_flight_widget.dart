import 'package:e_travel/app/data/models/fligh_rq.dart';
import 'package:e_travel/app/data/models/flight_req.dart';
import 'package:intl/intl.dart';
import 'package:e_travel/app/modules/flight/views/widgets/row_flight_widget.dart';
import 'package:e_travel/app/modules/widgets/custom_text.dart';
import 'package:e_travel/app/routes/app_pages.dart';
import 'package:e_travel/app/util/consts/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class CartFlightWidget extends StatelessWidget {
  FlightRq flightRq;
  FlightRequest flightRequest;
  bool onWay;
  bool returnFlight;
  bool edit;
  bool isMultiCity;
  bool unavailable;
  String pnr;
  Function getPrice;
  RxDouble total;

  CartFlightWidget(
      {required this.flightRq,
      required this.flightRequest,
      required this.onWay,
      required this.unavailable,
      required this.returnFlight,
      required this.pnr,
      required this.isMultiCity,
      required this.total,
      required this.getPrice,
      required this.edit});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
            padding: EdgeInsets.symmetric(horizontal: 20),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                CustomText(
                  text: flightRq.itineraries![0].airline!.nameAr.toString(),
                  color: primaryColor,
                  fontSize: 16.sp,
                  fontWeight: FontWeight.bold,
                  textAlign: TextAlign.right,
                ),
                CustomText(
                  text: flightRq.itineraries![0].airline!.nameEn.toString(),
                  color: primaryColor,
                  fontSize: 16.sp,
                  fontWeight: FontWeight.bold,
                  textAlign: TextAlign.left,
                ),
              ],
            )),
        Container(
            width: Get.width,
            height:flightRq.itineraries!.length*85.h,
            // flightRq.isMultiCity!
            //     ? 175.h
            //     : flightRq.goingStops == 0 && flightRq.returnStops == 0
            //         ? onWay
            //             ? 85.h
            //             : 175.h
            //         : flightRq.goingStops! >= 1 && flightRq.returnStops! >= 1
            //             ? ((flightRq.goingStops! + 1) *
            //                     (flightRq.returnStops! + 1) *
            //                     85)
            //                 .h
            //             : flightRq.goingStops! >= 1
            //                 ? ((flightRq.goingStops! + 1) * 85).h
            //                 : flightRq.returnStops! >= 1
            //                     ? ((flightRq.returnStops! + 1) * 85).h
            //                     : 85.h,
            child: ListView.separated(
              physics: const NeverScrollableScrollPhysics(),
              padding: EdgeInsets.only(top: 0),
              separatorBuilder: (BuildContext context, int index) => SizedBox(
                height: 0.w,
              ),
              scrollDirection: Axis.vertical,
              itemCount: flightRq.itineraries!.length,
              itemBuilder: (BuildContext context, int index) => Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                      height: 85.h,
                      child: Column(
                        children: [
                          flightRq.itineraries![index].isSuggestion!
                                  ? Container(
                                      width: Get.width,
                                      height: 80.h,
                                      padding: EdgeInsets.only(top: 5.h),
                                      decoration: BoxDecoration(
                                        color: Color(0xffD79C02),
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(50.r)),
                                      ),
                                      child: Column(
                                        children: [
                                          SizedBox(
                                            width: 11.w,
                                          ),
                                          CustomText(
                                              text: 'Earliest flight date'.tr,
                                              color: blackColor,
                                              fontSize: 16.sp),
                                          SizedBox(
                                            width: 40.h,
                                          ),
                                          CustomText(
                                            text: DateFormat('yyyy / MM / dd')
                                                .format(flightRq
                                                    .itineraries![index]
                                                    .arrivalDateTime!)
                                                .toString(),
                                            color: blackColor,
                                            fontWeight: FontWeight.bold,
                                            fontSize: 16.sp,
                                          ),
                                        ],
                                      ))
                                  : RowFlightWidget(
                                          itineraries:
                                              flightRq.itineraries![index],
                                          multi: false,
                                        )


                          // flightRq.isMultiCity!
                          //     ? RowFlightWidget(
                          //         itineraries: flightRq.itineraries![index],
                          //         multi: false,
                          //       )
                          //      :flightRq.itineraries![index].isSuggestion!
                          //         ? Container(
                          //             width: Get.width,
                          //             height: 80.h,
                          //             padding: EdgeInsets.only(top: 5.h),
                          //             decoration: BoxDecoration(
                          //               color: Color(0xffD79C02),
                          //               borderRadius: BorderRadius.all(
                          //                   Radius.circular(50.r)),
                          //             ),
                          //             child: Column(
                          //               children: [
                          //                 SizedBox(
                          //                   width: 11.w,
                          //                 ),
                          //                 CustomText(
                          //                     text: 'Earliest flight date'.tr,
                          //                     color: blackColor,
                          //                     fontSize: 16.sp),
                          //                 SizedBox(
                          //                   width: 40.h,
                          //                 ),
                          //                 CustomText(
                          //                   text: DateFormat('yyyy / MM / dd')
                          //                       .format(flightRq
                          //                           .itineraries![index]
                          //                           .arrivalDateTime!)
                          //                       .toString(),
                          //                   color: blackColor,
                          //                   fontWeight: FontWeight.bold,
                          //                   fontSize: 16.sp,
                          //                 ),
                          //               ],
                          //             ))
                          //         : flightRq.itineraries![index].segment == 1 &&
                          //                 flightRq.goingStops == 0
                          //             ? RowFlightWidget(
                          //                 itineraries:
                          //                     flightRq.itineraries![index],
                          //                 multi: false,
                          //               )
                          //             : flightRq.itineraries![index].segment ==
                          //                         1 &&
                          //                     flightRq.goingStops! >= 1
                          //                 ? RowFlightWidget(
                          //                     itineraries:
                          //                         flightRq.itineraries![index],
                          //                     multi: true,
                          //                     stops: flightRq.goingStops,
                          //                   )
                          //                 : flightRq.itineraries![index]
                          //                                 .segment ==
                          //                             2 &&
                          //                         flightRq.returnStops == 0
                          //                     ? RowFlightWidget(
                          //                         itineraries: flightRq
                          //                             .itineraries![index],
                          //                         multi: false,
                          //                       )
                          //                     : RowFlightWidget(
                          //                         itineraries: flightRq
                          //                             .itineraries![index],
                          //                         multi: true,
                          //                         stops: flightRq.returnStops,
                          //                       ),
                        ],
                      )),
                ],
              ),
            )),
        unavailable
            ?Container(
                height: 60.h,
                padding: EdgeInsets.symmetric(
                  horizontal: 50.w,
                ),
                decoration: BoxDecoration(
                  color: primaryColor.withOpacity(0.82),
                  borderRadius: BorderRadius.all(Radius.circular(50.r)),
                ),
                child: Row(children: <Widget>[
                  SvgPicture.asset('assets/icons/booking_icon.svg'),
                  SizedBox(
                    width: 10,
                  ),
                  CustomText(
                      text: 'unavailable'.tr,
                      color: whiteColor,
                      fontWeight: FontWeight.bold,
                      fontSize: 14.sp),
                  Spacer(),
                  flightRq.total == 0.0
                      ? InkWell(
                      onTap: () => getPrice(),
                      child: Obx(() => total.value != 0.0 &&
                          flightRq.total != 0.0
                          ? CustomText(
                          text:
                          '${flightRq.total} ${'LY'.tr}',
                          color: whiteColor,
                          fontWeight: FontWeight.bold,
                          fontSize: 14.sp)
                          : Container(
                          height: 40.h,
                          width: 120.w,
                          decoration: BoxDecoration(
                            color: secondaryColor,
                            borderRadius: BorderRadius.all(
                                Radius.circular(50.r)),
                          ),
                          child: Center(
                            child: CustomText(
                                text: 'show price'.tr,
                                color: primaryColor,
                                fontWeight: FontWeight.bold,
                                fontSize: 12.sp),
                          ))))
                      : CustomText(
                      text: '${flightRq.total} ${'LY'.tr}',
                      color: whiteColor,
                      fontWeight: FontWeight.bold,
                      fontSize: 14.sp),
                ])):
        flightRq.containsSuggestion!
            ? InkWell(
                onTap: () => Get.back(),
                child: Container(
                    height: 60.h,
                    width: Get.width,
                    padding: EdgeInsets.symmetric(
                      horizontal: 50.w,
                    ),
                    decoration: BoxDecoration(
                      color: primaryColor,
                      borderRadius: BorderRadius.all(Radius.circular(50.r)),
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.search,
                          color: secondaryColor,
                        ),
                        CustomText(
                          text: 'Re-search'.tr,
                          color: Colors.white,
                        )
                      ],
                    )))
            : InkWell(
                onTap: () => Get.find<GetStorage>().hasData('token')
                    ? edit
                        ? Get.toNamed(Routes.CONFIRM_BOOKING, arguments: {
                            'flightRq': flightRq,
                            'flightRequest': flightRequest,
                            'onWay': onWay,
                            'Edit': edit,
                            'pnr': pnr,
                            'returnFlight': returnFlight,
                          })
                        : Get.toNamed(Routes.BOOK_FLIGHT, arguments: {
                            'flightRq': flightRq,
                            'flightRequest': flightRequest,
                            'onWay': onWay
                          })
                    : Get.toNamed(Routes.AUTH),
                child: Container(
                    height: 60.h,
                    padding: EdgeInsets.symmetric(
                      horizontal: 50.w,
                    ),
                    decoration: BoxDecoration(
                      color: primaryColor,
                      borderRadius: BorderRadius.all(Radius.circular(50.r)),
                    ),
                    child: edit
                        ? Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                                SvgPicture.asset(
                                    'assets/icons/booking_icon.svg'),
                                SizedBox(
                                  width: 10,
                                ),
                                CustomText(
                                    text: 'choose'.tr,
                                    color: whiteColor,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 14.sp),
                              ])
                        : Row(children: <Widget>[
                            SvgPicture.asset('assets/icons/booking_icon.svg'),
                            SizedBox(
                              width: 10,
                            ),
                            CustomText(
                                text: 'Book now'.tr,
                                color: whiteColor,
                                fontWeight: FontWeight.bold,
                                fontSize: 14.sp),
                            Spacer(),
                            flightRq.total == 0.0
                                ? InkWell(
                                    onTap: () => getPrice(),
                                    child: Obx(() => total.value != 0.0 &&
                                            flightRq.total != 0.0
                                        ? CustomText(
                                            text:
                                                '${flightRq.total} ${'LY'.tr}',
                                            color: whiteColor,
                                            fontWeight: FontWeight.bold,
                                            fontSize: 14.sp)
                                        : Container(
                                            height: 40.h,
                                            width: 120.w,
                                            decoration: BoxDecoration(
                                              color: secondaryColor,
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(50.r)),
                                            ),
                                            child: Center(
                                              child: CustomText(
                                                  text: 'show price'.tr,
                                                  color: primaryColor,
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 12.sp),
                                            ))))
                                : CustomText(
                                    text: '${flightRq.total} ${'LY'.tr}',
                                    color: whiteColor,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 14.sp),
                          ]))),
      ],
    );
  }
}
