import 'package:e_travel/app/util/consts/colors.dart';
import 'package:e_travel/app/modules/widgets/custom_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';

class CategoryFlight extends StatelessWidget {
  CategoryFlight({Key? key,required this.text, this.first=false, this.icon = ""}): super(key: key);

  bool first;
  String text;
  String icon;

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.symmetric(
          horizontal: 10.w,
        ),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(10.r)),
            border: Border.all(
              color: textColor,
            )),
        child: Row(
          children: [
            first
                ? SvgPicture.asset(
                    icon,
                    width: 10.77.w,
                    height: 9.53.h,
                  )
                : SizedBox(),
            first
                ? SizedBox(
                    width: 9.2.w,
                  )
                : SizedBox(),
            CustomText(
              text: text,
            ),
          ],
        ));
  }
}
