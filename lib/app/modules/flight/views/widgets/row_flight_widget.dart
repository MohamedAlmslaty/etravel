import 'package:cached_network_image/cached_network_image.dart';
import 'package:e_travel/app/data/models/itineraries.dart';
import 'package:e_travel/app/modules/widgets/custom_text.dart' as c;
import 'package:e_travel/app/util/consts/base.dart';
import 'package:e_travel/app/util/consts/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:intl/intl.dart';

class RowFlightWidget extends StatelessWidget {
  Itineraries itineraries;
  bool multi = false;
  int? stops = 0;

  RowFlightWidget({required this.itineraries, required this.multi, this.stops});

  @override
  Widget build(BuildContext context) {
    bool local = Get.find<GetStorage>().read('language') == 'ar';

    return Container(
        width: Get.width,
        height: 80.h,
        padding: EdgeInsets.only(top: 5.h),
        decoration: BoxDecoration(
          color: whiteColor,
          borderRadius: BorderRadius.all(Radius.circular(50.r)),
        ),
        child: Row(
          children: [
            SizedBox(
              width: 11.w,
            ),
            CachedNetworkImage(
              imageUrl: imageUrl(itineraries.airline!.imageUrl),
              imageBuilder: (context, imageProvider) => Container(
                height: 50.h,
                width: 50.h,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(6.0),
                  image: DecorationImage(
                    image: imageProvider,
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              errorWidget: (context, url, error) => Icon(Icons.error),
            ),
            SizedBox(
              width: 23.6.w,
            ),
            Data(),
            SizedBox(
              width: 10.w,
            ),
            Column(
              children: [
                c.CustomText(
                  text: formatTimeDuration(itineraries.duration!),
                  color: primaryColor,
                  fontSize: 9.sp,
                ),
                Image.asset(
                  local
                      ? 'assets/icons/line_ar.png'
                      : 'assets/icons/line_en.png',
                  height: 25.h,
                ),
                multi
                    ? c.CustomText(
                        text: 'stops'.tr + " (" + stops.toString() + ")",
                        color: primaryColor,
                        fontSize: 9.sp)
                    : c.CustomText(
                        text: 'Seats'.tr +
                            " " +
                            itineraries.seatNo.toString() +
                            " (" +
                            itineraries.ticketClass.toString() +
                            ")",
                        color: primaryColor,
                        fontSize: 11.sp),
              ],
            ),
            SizedBox(
              width: 10.w,
            ),
            Data(d: true),
            SizedBox(
              width: 10.w,
            ),
            InkWell(
              child: Icon(
                Icons.info_outline,
                color: primaryColor,
              ),
              onTap: () {
                Get.defaultDialog(
                    title: "Details".tr,
                    backgroundColor: Color(0xffFEF200),
                    content: Container(
                      child: Text(itineraries.bags.toString() == ""
                          ? "TicketDetailsUnavailable".tr
                          : itineraries.bags.toString()),
                    ),
                cancel: ElevatedButton(
                onPressed: () {
                Get.back(); // Close the dialog when the Cancel button is pressed
                },
                style: ElevatedButton.styleFrom(
                primary: Colors.red,
                shape: RoundedRectangleBorder(
                borderRadius:
                BorderRadius.circular(20.0), // Custom border radius
                ), // Custom background color for the Cancel button
                ),
                child: c.CustomText(text:"cancel".tr),
                ),
               );
              },
            )
          ],
        ));
  }

  Widget Data({bool d = false}) {
    return Column(
      children: [
        d
            ? c.CustomText(
                text: DateFormat('HH:mm')
                    .format(itineraries.arrivalDateTime!)
                    .toString(),
                fontWeight: FontWeight.bold,
            fontSize: 10.sp,
                color: primaryColor)
            : c.CustomText(
                text: DateFormat('HH:mm')
                    .format(itineraries.departureDateTime!)
                    .toString(),
                fontWeight: FontWeight.bold,
            fontSize: 10.sp,
                color: primaryColor),
        d
            ? c.CustomText(
            text: DateFormat('yy-MM-dd')
                .format(itineraries.arrivalDateTime!)
                .toString(),
            fontWeight: FontWeight.bold,
            fontSize: 10.sp,
            color: primaryColor)
            : c.CustomText(
            text: DateFormat('yy-MM-dd')
                .format(itineraries.departureDateTime!)
                .toString(),
            fontWeight: FontWeight.bold,
            fontSize: 10.sp,
            color: primaryColor),

        d
            ? c.CustomText(
                text: itineraries.arrivalAirport!.iataCode.toString(),
                color: secondaryColor,
                fontSize: 10.sp)
            : c.CustomText(
                text: itineraries.departureAirport!.iataCode.toString(),
                color: secondaryColor,
                fontSize: 10.sp),
      ],
    );
  }

  String formatTimeDuration(String timeDuration) {
    List<int> numbers = [];
    RegExp regex = RegExp(r'\d+');
    Iterable<Match> matches = regex.allMatches(timeDuration);

    for (Match match in matches) {
      int number = int.parse(match.group(0)!);
      numbers.add(number);
    }

    print(numbers);

    return '${numbers[0]}' + 'h'.tr + ' ${numbers[1]}' + 'm'.tr;
  }
}
