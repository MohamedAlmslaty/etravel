import 'package:e_travel/app/data/models/fligh_rq.dart';
import 'package:e_travel/app/modules/flight/controllers/flight_controller.dart';
import 'package:e_travel/app/modules/flight/views/widgets/cart_flight_widget.dart';
import 'package:e_travel/app/modules/flight/views/widgets/category_flight_widget.dart';
import 'package:e_travel/app/modules/widgets/custom_button.dart';
import 'package:e_travel/app/modules/widgets/custom_text.dart';
import 'package:e_travel/app/modules/widgets/loading_widget.dart';
import 'package:e_travel/app/modules/widgets/slider.dart';
import 'package:e_travel/app/routes/app_pages.dart';
import 'package:e_travel/app/util/consts/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:intl/intl.dart';

class FlightView extends GetView<FlightController> {
  bool local = Get.find<GetStorage>().read('language') == 'ar';

  @override
  Widget build(BuildContext context) {
    return GetBuilder<FlightController>(
        init: FlightController(),
        initState: (_) {},
        builder: (_con) => Scaffold(
            backgroundColor: whiteColor,
            body: Stack(
              alignment: AlignmentDirectional.topCenter,
              children: <Widget>[
                Container(
                  width: 375.w,
                  height: 255.h,
                  decoration: BoxDecoration(
                    color: primaryColor,
                    image: DecorationImage(
                      image: AssetImage(
                        "assets/images/background.png",
                      ),
                      fit: BoxFit.fill,
                    ),
                  ),
                ),
                SingleChildScrollView(
                  child: Column(
                    children: [
                      SizedBox(
                        height: 42.h,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          IconButton(
                            icon: Icon(
                              Icons.arrow_back,
                              color: whiteColor,
                              size: 16.1.w,
                            ),
                            onPressed: () => Get.back(),
                          ),
                          _con.flightRequest.isMultiCity??false
                          ?Column(
                            children:_con.flightRequest.multiCityFlights!.map((e) => Row(children: [
                              CustomText(
                                  text: '${e.originLocationCode} ' +
                                      'To'.tr +
                                      ' ${e.destinationLocationCode}',
                                  fontWeight: FontWeight.bold,
                                  color: secondaryColor),
                              SizedBox(width: 10.w,),
                              CustomText(
                                text:  DateFormat('MMM  ', Get.locale.toString())
                                    .format( DateTime.tryParse(e.departureDate.value.toString())!)
                                    .toString() +
                                    DateFormat('dd')
                                        .format(DateTime.tryParse(e.departureDate.value.toString())!)
                                        .toString(),
                              ),
                            ],)).toList())
                          :CustomText(
                              text: '${_con.flightRequest.originLocationCode} ' +
                                  'To'.tr +
                                  ' ${_con.flightRequest.destinationLocationCode}',
                              fontWeight: FontWeight.bold,
                              color: secondaryColor),
                          Spacer(),
                          IconButton(
                            icon: SvgPicture.asset(
                              'assets/icons/notification.svg',
                              width: 17.78.w,
                              height: 22.2.h,
                              fit: BoxFit.fill,
                            ),
                            onPressed: () =>
                                Get.find<GetStorage>().hasData('token')
                                    ? Get.toNamed(Routes.NOTIFICATIONS)
                                    : Get.toNamed(Routes.AUTH),
                          ),
                        ],
                      ),
                      _con.flightRequest.isMultiCity??false
                          ?SizedBox()
                          :Row(
                        children: [
                          SizedBox(
                            width: 46.w,
                          ),
                          CustomText(
                            text: _con.flightRequest.returnDate!.isNotEmpty
                                ? DateFormat('MMM  ', Get.locale.toString())
                                        .format(_con.departureDate)
                                        .toString() +
                                    DateFormat('dd')
                                        .format(_con.departureDate)
                                        .toString() +
                                    ' - ' +
                                    DateFormat('MMM ', Get.locale.toString())
                                        .format(_con.returnDate)
                                        .toString() +
                                    DateFormat('dd')
                                        .format(_con.returnDate)
                                        .toString()
                                : DateFormat('MMM  ', Get.locale.toString())
                                        .format(_con.departureDate)
                                        .toString() +
                                    DateFormat('dd')
                                        .format(_con.departureDate)
                                        .toString(),
                          ),
                          SizedBox(
                            width: 13.3.w,
                          ),
                          _con.flightRequest.adults==0
                          ?SizedBox():Image.asset(
                            'assets/icons/user.png',
                            width: 13.73.w,
                            height: 15.09.h,
                            fit: BoxFit.fill,
                          ),
                          SizedBox(
                            width: 5.w,
                          ),
                          _con.flightRequest.adults==0
                              ?SizedBox(): CustomText(
                            text: _con.flightRequest.adults.toString(),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 20.h,
                      ),
                      Container(
                        height: 30.h,
                        margin: EdgeInsets.symmetric(horizontal: 16.w),
                        child: Row(
                          children: [
                            InkWell(
                                onTap: () => selectAirline(_con),
                                child: CategoryFlight(
                                    text: 'airline'.tr,
                                    first: true,
                                    icon: 'assets/icons/menu.svg')),
                            Spacer(),
                            Container(
                                padding: EdgeInsets.symmetric(
                                  horizontal: 10.w,
                                ),
                                decoration: BoxDecoration(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(10.r)),
                                    border: Border.all(
                                      color: textColor,
                                    )),
                                child: CustomText(
                                  text: '1point'.tr + ' = ' + '1LYD'.tr,
                                  fontSize: 12.sp,
                                  fontWeight: FontWeight.bold,
                                )),
                            SizedBox(
                              width: 20.w,
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 22.h,
                      ),
                      Obx(
                        () => Container(
                          height: Get.height - 170,
                          width: Get.width,
                          padding: EdgeInsets.symmetric(horizontal: 20.w),
                          decoration: BoxDecoration(
                            color: secondaryColor,
                            borderRadius:
                                BorderRadius.all(Radius.circular(50.r)),
                          ),
                          child: DefaultTabController(
                              length: 2,
                              child:Column(
                                children: [

                                                              TabBar(
                                    indicatorWeight: 5,
                                    labelColor: primaryColor,
                                    unselectedLabelColor: primaryColor.withOpacity(0.5),
                                    indicatorColor: Color(0xff312871),
                                    labelPadding: EdgeInsets.only(bottom: 10.h),
                                    tabs: [
                                      Text(
                                        'available'.tr + ' (${_con.flightRq.length})',
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold, fontSize: 18.sp),
                                      ),
                                      Text(
                                        'unavailable'.tr + ' (${_con.flightRqEmpty.length})',
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold, fontSize: 18.sp),
                                      ),
                                    ],
                                                              ),
                                  Expanded(
                                      child: TabBarView(
                                        children: [
                                          flightList(_con,_con.flightRq,false),
                                          flightList(_con,_con.flightRqEmpty,true),
                                        ],
                                      )),
                                ],
                              ))
                        ),
                      )
                    ],
                  ),
                )
              ],
            )));
  }
  Widget flightList(FlightController _con,RxList<FlightRq> flightList,bool unavailable){
  return  _con.loaded.value
        ? LoadingWidget()
        : flightList.isEmpty
        ? Column(
      mainAxisAlignment:
      MainAxisAlignment.center,
      children: [
        SvgPicture.asset(
          'assets/images/noflights.svg',
          width: 150.w,
          height: 140.h,
        ),
        CustomText(
          text: 'No flights found'.tr,
          fontSize: 20.sp,
          fontWeight: FontWeight.bold,
          color: blackColor,
        ),
        CustomText(
          text:
          'We did not find any flights that match your search.'
              .tr,
          fontSize: 20.sp,
          maxLines: 3,
          textAlign: TextAlign.center,
          color: blackColor,
        )
      ],
    )
        : ListView.separated(
        padding:
        EdgeInsets.only(top: 0, bottom: 20.h),
        separatorBuilder:
            (BuildContext context, int index) {
          if (index == 0 &&
              !_con.search.value &&
              _con.sliders.isNotEmpty) {
            return SliderImageWidget(
                sliders: _con.sliders);
          } else if (_con.sliders2.isNotEmpty &&
              !_con.search.value &&
              index ==
                  _con.flightRq.length ~/ 2 + 1) {
            return SliderImageWidget(
                sliders: _con.sliders2);
          } else
            return SizedBox(
              height: 10.w,
            );
        },
        scrollDirection: Axis.vertical,
        itemCount: _con.search.value
            ? _con.searchFlight.length
            : flightList.length,
        itemBuilder:
            (BuildContext context, int index) {
          return CartFlightWidget(
              onWay: _con.flightRequest
                  .returnDate!.isEmpty
                  ? true
                  : false,
              unavailable: unavailable,
              isMultiCity: _con.flightRequest.isMultiCity??false,
              returnFlight:
              _con.returnFlight.value,
              edit: _con.edit.value,
              pnr: _con.pnr.value,
              getPrice: () => _con.getPrice(
                  flightList[index],
                  _con.flightRequest.returnDate!
                      .isEmpty
                      ? true
                      : false),
              total: _con.total,
              flightRequest: _con.flightRequest,
              flightRq: _con.search.value
                  ? _con.searchFlight[index]
                  : flightList[index]);
        });
  }

  void selectAirline(FlightController _con) {
    Get.bottomSheet(
        Obx(() => _con.loadingAirline.value
            ? LoadingWidget()
            : Container(
                height: Get.height / 1.5,
                padding: EdgeInsets.all(20),
                child: SingleChildScrollView(
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        CustomText(
                          text: 'airline'.tr,
                          fontSize: 20,
                          color: blackColor,
                        ),
                        Container(
                          padding: EdgeInsets.all(20),
                          child: Column(
                            mainAxisSize: MainAxisSize.max,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: _con.airlines.map<Widget>(
                              (data) {
                                return Obx(
                                  () => Container(
                                    child: Column(
                                      mainAxisSize: MainAxisSize.min,
                                      children: <Widget>[
                                        CheckboxListTile(
                                          value: data.checked.value,
                                          title: Text(local
                                              ? data.nameAr
                                              : data.nameEn),
                                          controlAffinity:
                                              ListTileControlAffinity.leading,
                                          onChanged: (bool? val) {
                                            data.checked.value = val!;
                                            data.checked.value
                                                ? _con.selectAirlines.add(data)
                                                : _con.selectAirlines
                                                    .remove(data);
                                            print(_con.selectAirlines.length);
                                          },
                                        ),
                                      ],
                                    ),
                                  ),
                                );
                              },
                            ).toList(),
                          ),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            CustomButton(
                                text: 'Cancellation'.tr,
                                width: 100.w,
                                color: whiteColor,
                                colorText: primaryColor,
                                onClick: () => Get.back()),
                            CustomButton(
                                text: 'chosen'.tr,
                                width: 100.w,
                                onClick: () {
                                  Get.back();
                                  _con.getFlightById();
                                })
                          ],
                        )
                      ]),
                ),
              )),
        elevation: 20.0,
        enableDrag: false,
        backgroundColor: whiteColor,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
          topLeft: Radius.circular(30.0),
          topRight: Radius.circular(30.0),
        )));
  }
}
