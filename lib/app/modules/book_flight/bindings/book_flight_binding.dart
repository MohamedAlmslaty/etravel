import 'package:e_travel/app/modules/book_flight/controllers/book_flight_controller.dart';
import 'package:get/get.dart';

class BookFlightBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<BookFlightController>(
      () => BookFlightController(),
    );
  }
}
