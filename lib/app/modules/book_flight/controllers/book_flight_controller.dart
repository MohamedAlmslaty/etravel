import 'dart:convert';
import 'dart:typed_data';

import 'package:e_travel/app/data/models/attachment.dart';
import 'package:e_travel/app/data/models/booking_total.dart';
import 'package:e_travel/app/data/models/customer.dart';
import 'package:e_travel/app/data/models/final_booking.dart';
import 'package:e_travel/app/data/models/fligh_rq.dart';
import 'package:e_travel/app/data/models/flight_req.dart';
import 'package:e_travel/app/data/models/open_ticket.dart';
import 'package:e_travel/app/data/models/passenger.dart';
import 'package:e_travel/app/data/models/payment_method.dart';
import 'package:e_travel/app/data/repositories/customer_point_repository.dart';
import 'package:e_travel/app/data/repositories/flight_booking_repository.dart';
import 'package:e_travel/app/data/repositories/passenger_repository.dart';
import 'package:e_travel/app/data/repositories/user_repository.dart';
import 'package:e_travel/app/data/repositories/wallet_repository.dart';
import 'package:e_travel/app/modules/book_flight/views/widgets/success.dart';
import 'package:e_travel/app/modules/widgets/custom_text.dart';
import 'package:e_travel/app/routes/app_pages.dart';
import 'package:e_travel/app/util/consts/colors.dart';
import 'package:e_travel/app/util/consts/loading.dart';
import 'package:e_travel/app/util/consts/style.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:image_picker/image_picker.dart';
import 'package:mime/mime.dart';

class BookFlightController extends GetxController {
  late FlightRq flightRq;
  late FlightRequest flightRequest;
  late FlightRequestMulti flightRequestMulti = FlightRequestMulti();
  late bool onWay;
  RxString typeBooking = 'final booking'.obs;
  RxString openBooking = ''.obs;
  RxBool finalBook = true.obs;
  RxBool goingOpen = false.obs;
  RxBool returnOpen = false.obs;
  RxBool active30 = false.obs;
  RxBool active50 = false.obs;
  RxBool active100 = false.obs;
  RxBool edit = false.obs;
  RxBool returnFlight = false.obs;
  final _userRepo = UserRepository.instance;
  final _walletRepo = WalletRepository.instance;
  final _loyalRepo = LoyalRepository.instance;
  final _passengerRepo = PassengerRepository.instance;
  final _bookingRepo = FlightBookingRepository.instance;
  RxDouble total = 0.0.obs;
  RxInt discount = 0.obs;
  final Rx<Customer> _user = Customer().obs;
  RxList<PaymentMethods> payment = <PaymentMethods>[].obs;
  RxList<BookingTotalDetails> bookingTotal = <BookingTotalDetails>[].obs;
  RxList<Passenger> passengers = <Passenger>[].obs;
  List<Passenger> selectedPassengers = [];
  List<String> bookingType = ['final booking', 'Pre-booking'];
  late List<OpenTicket> openTickets;
  final balance = 0.0.obs;
  final balanceWallet = 0.0.obs;
  final numberOfImage = 0.obs;
  final loadData = false.obs;
  final pnr = ''.obs;
  final token = ''.obs;
  List<Attachment> attachments = <Attachment>[];
  Rx<Attachment> attachment = Attachment().obs;
  List<XFile> pickedImages = [];
  final ImagePicker _picker = ImagePicker();

  @override
  void onInit() {
    super.onInit();
    loadPassengers();
    loadUser();
    loadBalance();
    if (Get.arguments != null) {
      flightRq = Get.arguments['flightRq'];
      flightRequest = Get.arguments['flightRequest'];
      onWay = Get.arguments['onWay'];
      flightRequest.airlineId=flightRq.itineraries![0].airline!.id;
      if (Get.arguments['returnFlight'] != null) {
        returnFlight.value = Get.arguments['returnFlight'];
      }

      openTickets = OpenTicket.getOpenTicket(onWay);
    }
    if (Get.arguments['Edit'] != null) {
      edit.value = Get.arguments['Edit'];
    }
    if (Get.arguments['pnr'] != null) {
      pnr.value = Get.arguments['pnr'];
      getFees(pnr.value);
    }
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}

  void imgFromGallery() async {
    try {
      final List<XFile?> resultList = await _picker.pickMultiImage(
        imageQuality: 80, // Adjust the image quality as needed
      );

      if (resultList.isNotEmpty) {
        numberOfImage.value=resultList.length;
        showSnackBar(
            title: 'successful'.tr,
            message: 'Visa photos have been successfully selected'.tr);
        pickedImages = resultList.cast<XFile>();
        List<Attachment> imagePaths =  await Future.wait(
          pickedImages.map((image) async {
            String base64String = base64.encode(await image.readAsBytes());
            return Attachment(attachmentType: 'PASSPORT',
                details: image.name+image.mimeType.toString()+image.path,
                file: base64String,
                fileContentType: lookupMimeType(image.path));
          }),
        );

        attachments.addAll(imagePaths);

      } else
        showSnackBar(
            title: 'error'.tr, message: 'No visa photos were selected'.tr);
    } catch (e) {
      print('Error picking images: $e');
    }
  }

  Future<bool> loadUser([bool refresh = false]) async {
    _user.value = await _userRepo.getCustomerAccount();

    update();
    return true;
  }

  Future<void> loadWalletBalance() async {
    balanceWallet.value = await _walletRepo.getWalletBalance();
    update();
    if (balanceWallet.value != null && balanceWallet.value != "") {
      Get.find<GetStorage>().write('wallet_amount', balanceWallet.value);
      if (Get.find<GetStorage>().hasData('wallet_amount'))
        walletBalance.value = Get.find<GetStorage>().read('wallet_amount');
    }
  }

  Future<void> loadPassengers() async {
    final response = await _passengerRepo.getPassengers(0, 1000);
    passengers.assignAll(response);
    selectedPassengers.clear();
  }

  Future<bool> loadBalance() async {
    balance.value = await _loyalRepo.getCustomerPointsBalance();
    update();
    loadData.value = true;
    return true;
  }

  Future<void> totalBooking(bool isMultiStop) async {
    loading();
    if (isMultiStop) {
      flightRequestMulti.adults = flightRequest.adults!;
      flightRequestMulti.children = flightRequest.children!;
      flightRequestMulti.cabinClass = flightRequest.cabinClass!;
      flightRequestMulti.departureDate = flightRequest.departureDate!;
      flightRequestMulti.returnDate = flightRequest.returnDate!;
      flightRequestMulti.airlineId = flightRequest.airlineId!;
      flightRequestMulti.destinationLocationCode =
          flightRequest.destinationLocationCode!;
      flightRequestMulti.originLocationCode = flightRequest.originLocationCode!;
      flightRequestMulti.flightNumber = flightRq.itineraries![0].flightNo;
      flightRequestMulti.itineraries = flightRq.itineraries!;
      flightRequestMulti.isMultiStop = isMultiStop;
      flightRequestMulti.isMultiCity = isMultiStop;
      FinalBookingMulti finalBookingMulti = FinalBookingMulti(
          flightRequestMulti: flightRequestMulti,
          passengers: selectedPassengers,
          attachments: attachments);
      if (finalBook.value) {
        _bookingRepo
            .getTotalBookingMultiDetails(finalBookingMulti)
            .then((value) {
          if (value.isNotEmpty) {
            bookingTotal.assignAll(value);
            attachments.clear();
          }
        });
        _bookingRepo.getTotalBookingMulti(finalBookingMulti).then((value) {
          if (value != null) {
            EasyLoading.dismiss();
            total.value = double.tryParse(value)!;
          }
        });
      } else {
        _bookingRepo
            .getTotalInitialBookingMulti(finalBookingMulti)
            .then((value) {
          if (value != null) {
            EasyLoading.dismiss();
            total.value = double.tryParse(value)!;
          }
        });
      }
    } else {
      flightRequest.flightNumber = flightRq.itineraries![0].flightNo;
      flightRequest.airlineId = flightRq.itineraries![0].airline!.id;

      //TODO :: TO FIX MJI TIP
      flightRequest.originLocationCode =
          flightRq.itineraries![0].departureAirport!.iataCode;
      flightRequest.destinationLocationCode =
          flightRq.itineraries![0].arrivalAirport!.iataCode;
      flightRequest.departureTicketClass = flightRq.itineraries![0].ticketClass;
      flightRequest.returnTicketClass =
          onWay ? "" : flightRq.itineraries![1].ticketClass;
      flightRequest.returnFlightNumber =
          onWay ? "" : flightRq.itineraries![1].flightNo;
      FinalBooking finalBooking = FinalBooking(
          flightRequest: flightRequest, passengers: selectedPassengers);
      if (finalBook.value) {
        _bookingRepo.getTotalBookingDetails(finalBooking).then((value) {
          if (value.isNotEmpty) {
            bookingTotal.assignAll(value);
          }
        });
        _bookingRepo.getTotalBooking(finalBooking).then((value) {
          if (value != null) {
            EasyLoading.dismiss();
            total.value = double.tryParse(value)!;
          }
        });
      } else {
        _bookingRepo.getTotalInitialBooking(finalBooking).then((value) {
          if (value != null) {
            EasyLoading.dismiss();
            total.value = double.tryParse(value)!;
          }
        });
      }
    }
  }

  Future<void> initBooking(bool isMultiStop) async {
    loading();
    if (isMultiStop) {
      flightRequestMulti.adults = flightRequest.adults!;
      flightRequestMulti.children = flightRequest.children!;
      flightRequestMulti.cabinClass = flightRequest.cabinClass!;
      flightRequestMulti.departureDate = flightRequest.departureDate!;
      flightRequestMulti.returnDate = flightRequest.returnDate!;
      flightRequestMulti.airlineId = flightRequest.airlineId!;
      flightRequestMulti.destinationLocationCode =
          flightRequest.destinationLocationCode!;
      flightRequestMulti.originLocationCode = flightRequest.originLocationCode!;
      flightRequestMulti.flightNumber = flightRq.itineraries![0].flightNo;
      flightRequestMulti.itineraries = flightRq.itineraries!;
      flightRequestMulti.isMultiStop = isMultiStop;
      FinalBookingMulti finalBookingMulti = FinalBookingMulti(
          flightRequestMulti: flightRequestMulti,
          passengers: selectedPassengers,
          attachments: attachments);
      _bookingRepo.initBookingMulti(finalBookingMulti).then((value) {
        if (value.id != null) {
          EasyLoading.dismiss();
          attachments.clear();
          Get.to(() => Success(
                flightBooking: value,
                edit: false,
              ));
        }
      });
    } else {
      flightRequest.flightNumber = flightRq.itineraries![0].flightNo;
      flightRequest.airlineId = flightRq.itineraries![0].airline!.id;
      flightRequest.departureTicketClass = flightRq.itineraries![0].ticketClass;
      flightRequest.returnTicketClass =
          onWay ? "" : flightRq.itineraries![0].ticketClass;
      flightRequest.returnFlightNumber =
          onWay ? "" : flightRq.itineraries![1].flightNo;
      FinalBooking finalBooking = FinalBooking(
          flightRequest: flightRequest,
          passengers: selectedPassengers,
          attachments: attachments);
      _bookingRepo.initBooking(finalBooking).then((value) {
        if (value.id != null) {
          EasyLoading.dismiss();
          attachments.clear();
          Get.to(() => Success(
                flightBooking: value,
                edit: false,
              ));
        }
      });
    }
  }

  Future<void> Booking(bool isMultiStop) async {
    loading();
    if (isMultiStop) {
      flightRequestMulti.adults = flightRequest.adults!;
      flightRequestMulti.airlineId = flightRequest.airlineId!;
      flightRequestMulti.children = flightRequest.children!;
      flightRequestMulti.cabinClass = flightRequest.cabinClass!;
      flightRequestMulti.departureDate = flightRequest.departureDate!;
      flightRequestMulti.returnDate = flightRequest.returnDate!;
      flightRequestMulti.destinationLocationCode =
          flightRequest.destinationLocationCode!;
      flightRequestMulti.originLocationCode = flightRequest.originLocationCode!;
      flightRequestMulti.flightNumber = flightRq.itineraries![0].flightNo;
      flightRequestMulti.itineraries = flightRq.itineraries!;
      flightRequestMulti.isMultiStop = isMultiStop;
      flightRequestMulti.isMultiCity = isMultiStop;
      FinalBookingMulti finalBookingMulti = FinalBookingMulti(
          flightRequestMulti: flightRequestMulti,
          passengers: selectedPassengers,
          attachments: attachments);
      _bookingRepo.flightBookingMulti(finalBookingMulti).then((value) {
        if (value.id != null) {
          EasyLoading.dismiss();
          attachments.clear();
          if (active50.value || active100.value)
            confirmRedeemDiscount(value.id!, token.value);
          Get.to(() => Success(
                flightBooking: value,
                edit: false,
              ));
        } else if (active50.value || active100.value) {
          EasyLoading.dismiss();
          cancelRedeem(token.value);
        }
      });
    } else {
      flightRequest.goingOpen = goingOpen.value;
      flightRequest.returnOpen = returnOpen.value;
      flightRequest.originLocationCode =
          flightRq.itineraries![0].departureAirport!.iataCode;
      flightRequest.destinationLocationCode =
          flightRq.itineraries![0].arrivalAirport!.iataCode;

      flightRequest.departureTicketClass = flightRq.itineraries![0].ticketClass;
      flightRequest.airlineId = flightRq.itineraries![0].airline!.id;
      flightRequest.returnTicketClass =
          onWay ? "" : flightRq.itineraries![1].ticketClass;
      flightRequest.flightNumber = flightRq.itineraries![0].flightNo;
      flightRequest.returnFlightNumber =
          onWay ? null : flightRq.itineraries![1].flightNo;

      FinalBooking finalBooking = FinalBooking(
          flightRequest: flightRequest,
          passengers: selectedPassengers,
          attachments: attachments);
      _bookingRepo.flightBooking(finalBooking).then((value) {
        if (value.id != null) {
          EasyLoading.dismiss();
          attachments.clear();
          if (active50.value || active100.value)
            confirmRedeemDiscount(value.id!, token.value);
          Get.to(() => Success(
                flightBooking: value,
                edit: false,
              ));
        } else if (active50.value || active100.value) {
          EasyLoading.dismiss();
          cancelRedeem(token.value);
        }
      });
    }
  }

  Future<void> finalBooking(bool isMultiStop) async {

    if(walletBalance.value>=total.value){

    if (active50.value) {
      discount.value = 50;
      redeem(_user.value.id!, discount.value, total.value.toInt(), isMultiStop);
    } else if (active100.value) {
      discount.value = 100;
      redeem(_user.value.id!, discount.value, total.value.toInt(), isMultiStop);
    } else {
      Booking(isMultiStop);
    }
  }else{
      double rest=total.value-walletBalance.value;
      Get.toNamed(Routes.DEPOSIT,arguments: rest);
    }}

  Future<void> getFees(pnr) async {
    loading();
    flightRequest.airlineId = flightRq.itineraries![0].airline!.id;
    flightRequest.departureTicketClass = flightRq.itineraries![0].ticketClass;
    flightRequest.returnTicketClass =
        onWay ? null : flightRq.itineraries![0].ticketClass;
    flightRequest.flightNumber =
        returnFlight.value ? null : flightRq.itineraries![0].flightNo;
    flightRequest.returnFlightNumber =
        returnFlight.value ? flightRq.itineraries![0].flightNo : null;

    FinalBooking finalBooking = FinalBooking(
        flightRequest: flightRequest,
        passengers: selectedPassengers,
        attachments: attachments);

    await _bookingRepo.getEditFee(pnr, finalBooking).then((value) {
      if (value[0].total != null) {
        EasyLoading.dismiss();
        attachments.clear();
        total.value = value[0].total!;
      }
    });
  }

  Future<void> editBooking() async {
    Get.defaultDialog(
      title: 'confirmation'.tr,
      backgroundColor: Color(0xffFEF200),
      content: Column(
        children: [
          Text('Are you sure to edit the flight?'.tr),
          CustomText(
            text: '${total.value}  ' + 'point'.tr + ' ' + 'will be deducted'.tr,
            textAlign: TextAlign.center,
            color: Colors.black,
            maxLines: 3,
          ),
        ],
      ),
      cancel: ElevatedButton(
        onPressed: () {
          Get.back(); // Close the dialog when the Cancel button is pressed
        },
        style: ElevatedButton.styleFrom(
          primary: Colors.red,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(20.0), // Custom border radius
          ), // Custom background color for the Cancel button
        ),
        child: CustomText(text:"cancel".tr),
      ),
      confirm: ElevatedButton(
        onPressed: () async {
          loading();
          flightRequest.airlineId =
              flightRq.itineraries![0].airline!.id;
          flightRequest.departureTicketClass =
              flightRq.itineraries![0].ticketClass;
          flightRequest.returnTicketClass =
              onWay ? "" : flightRq.itineraries![0].ticketClass;
          flightRequest.flightNumber =
              returnFlight.value ? null : flightRq.itineraries![0].flightNo;
          flightRequest.returnFlightNumber =
              returnFlight.value ? flightRq.itineraries![0].flightNo : null;

          FinalBooking finalBooking = FinalBooking(
              flightRequest: flightRequest,
              passengers: selectedPassengers,
              attachments: attachments);
          _bookingRepo.editBooking(pnr.value, finalBooking).then((value) {
            if (value.id != null) {
              EasyLoading.dismiss();
              attachments.clear();
              Get.to(() => Success(
                    flightBooking: value,
                    edit: true,
                  ));
            }
          });
        },
        style: ElevatedButton.styleFrom(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20.0), // Custom border radius
            ),
            primary: primaryColor),
        child: CustomText(text:"OK".tr),
      ),
    );
  }

  Future<void> redeem(int id, int discount, int total, bool isMultiStop) async {
    loading();
    await _loyalRepo.redeem(id, discount, total).then((value) {
      if (value != null) {
        token.value = value;
        EasyLoading.dismiss();
        Booking(isMultiStop);
        loadWalletBalance();
        loadBalance();
      }
    });
  }

  Future<void> confirmRedeemDiscount(int id, String discount) async {
    await _loyalRepo.confirmRedeemDiscount(id, discount);
  }

  Future<void> cancelRedeem(String token) async {
    loading();
    await _loyalRepo.redeemCancellation(token).then((value) {
      if (value) {
        EasyLoading.dismiss();
        loadWalletBalance();
        loadBalance();
      }
    });
  }
}
