import 'package:cached_network_image/cached_network_image.dart';
import 'package:e_travel/app/data/models/booking_total.dart';
import 'package:e_travel/app/data/models/itineraries.dart';
import 'package:e_travel/app/modules/book_flight/controllers/book_flight_controller.dart';
import 'package:e_travel/app/modules/book_flight/views/widgets/cart_done_booking_widget.dart';
import 'package:e_travel/app/modules/widgets/custom_text.dart';
import 'package:e_travel/app/util/consts/base.dart';
import 'package:e_travel/app/util/consts/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:intl/intl.dart';

class ConfirmBooking extends GetView<BookFlightController> {
  bool local = Get.find<GetStorage>().read('language') == 'ar';

  @override
  Widget build(BuildContext context) {
    return GetBuilder<BookFlightController>(
      init: BookFlightController(),
      builder: (_con) {
        return Scaffold(
            backgroundColor: whiteColor,
            appBar: AppBar(
              title: Text(
                  _con.edit.value ? 'editBooking'.tr : 'ConfirmBooking'.tr),
            ),
            bottomNavigationBar: Obx(
              () => CartDaneBookingWidget(
                onClick: () => _con.edit.value
                    ? _con.editBooking()
                    : _con.finalBook.value
                        ?  _con.flightRq.isMultiCity!
                    ?_con.finalBooking(_con.flightRq.isMultiCity!):_con.finalBooking(!(_con.flightRq.goingStops == 0 &&
                            _con.flightRq.returnStops == 0))
                        :  _con.flightRq.isMultiCity!
                    ?_con.initBooking(_con.flightRq.isMultiCity!):_con.initBooking(!(_con.flightRq.goingStops == 0 &&
                            _con.flightRq.returnStops == 0)),
                btnText:
                    _con.edit.value ? 'editBooking'.tr : 'ConfirmBooking'.tr,
                leftText: '${_con.total.value.toStringAsFixed(2)}  ${'LY'.tr}',
                rightText: "Total".tr,
              ),
            ),
            body: SingleChildScrollView(
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 20.w, vertical: 10.h),
                child: Column(
                  children: [
                    !_con.edit.value
                        ? Container(
                            width: 150.w,
                            padding: EdgeInsets.symmetric(
                                horizontal: 10, vertical: 2),
                            decoration: BoxDecoration(
                                color: _con.finalBook.value
                                    ? Colors.green
                                    : Colors.yellow,
                                borderRadius: BorderRadius.circular(24)),
                            child: CustomText(
                              text: _con.finalBook.value
                                  ? 'final booking'.tr
                                  : 'Pre-booking'.tr,
                              textAlign: TextAlign.center,
                            ),
                          )
                        : SizedBox(),
                    Container(
                        width: Get.width,
                        height: _con.flightRq.itineraries!.length*150,
                        child: ListView.separated(
                          physics: const NeverScrollableScrollPhysics(),
                          padding: EdgeInsets.only(top: 0),
                          separatorBuilder: (BuildContext context, int index) =>
                              SizedBox(
                            height: 0.w,
                          ),
                          scrollDirection: Axis.vertical,
                          itemCount: _con.flightRq.itineraries!.length,
                          itemBuilder: (BuildContext context, int index) =>
                              departureWidget(
                                  _con.flightRq.itineraries![index],
                                  _con.flightRq.itineraries![index].segment == 1 ||  _con.flightRq.isMultiCity!
                                      ? 'departure'.tr
                                      : 'return'.tr),
                        )),
                    Divider(),
                    !_con.edit.value
                        ? CustomText(
                            text: 'Passengers'.tr,
                            fontWeight: FontWeight.bold,
                            fontSize: 18.sp,
                            color: blackColor,
                          )
                        : SizedBox(),
                    Obx(() =>ListView.separated(
                      itemCount: _con.bookingTotal.length,
                      physics: const NeverScrollableScrollPhysics(),
                      shrinkWrap: true,
                      separatorBuilder: (BuildContext context, index) =>
                          SizedBox(
                        height: 10.h,
                      ),
                      itemBuilder: (BuildContext context, index) =>
                          PassengerWidget(_con.bookingTotal[index], index + 1)),
                    ),
                    Divider(),
                    _con.flightRq.itineraries![0].cabinClass == 'BUSINESS' &&
                            !_con.edit.value &&
                            _con.balance.value >= 300 &&
                            _con.finalBook.value
                        ? Obx(() => CheckboxListTile(
                              value: controller.active30.value,
                              controlAffinity: ListTileControlAffinity.leading,
                              onChanged: (value) {
                                controller.active30.value = value!;
                                controller.active50.value = !value;
                                controller.active100.value = !value;
                              },
                              title: InkWell(
                                  onTap: () => {},
                                  child: CustomText(
                                    text:
                                        'Redeem 300 points for a Business Class booking for an Economy Class Y price'
                                            .tr,
                                    color: Colors.black,
                                    maxLines: 2,
                                  )),
                            ))
                        : SizedBox(),
                    !_con.edit.value &&
                            _con.balance.value >= 500 &&
                            _con.finalBook.value
                        ? Obx(
                            () => CheckboxListTile(
                              value: controller.active50.value,
                              controlAffinity: ListTileControlAffinity.leading,
                              onChanged: (value) {
                                controller.active50.value = value!;
                                controller.active30.value = !value;
                                controller.active100.value = !value;
                              },
                              title: InkWell(
                                  onTap: () => {},
                                  child: CustomText(
                                    text:
                                        'Redeem 500 Tres Points for a 50% discount'
                                            .tr,
                                    color: Colors.black,
                                    maxLines: 2,
                                  )),
                            ),
                          )
                        : SizedBox(),
                    !_con.edit.value &&
                            _con.balance.value >= 1000 &&
                            _con.finalBook.value
                        ? Obx(
                            () => CheckboxListTile(
                              value: controller.active100.value,
                              controlAffinity: ListTileControlAffinity.leading,
                              onChanged: (value) {
                                controller.active100.value = value!;
                                controller.active30.value = !value;
                                controller.active50.value = !value;
                              },
                              title: InkWell(
                                  onTap: () => {},
                                  child: CustomText(
                                    text: 'Redeem 1000 points for a free ticket'
                                        .tr,
                                    color: Colors.black,
                                    maxLines: 2,
                                  )),
                            ),
                          )
                        : SizedBox(),
                  ],
                ),
              ),
            ));
      },
    );
  }

  departureWidget(Itineraries itineraries, text) {
    return Column(
      children: [
        Row(
          children: [
            CachedNetworkImage(
              imageUrl: imageUrl(itineraries.airline!.imageUrl),
              imageBuilder: (context, imageProvider) => Container(
                height: 50.h,
                width: 50.h,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(6.0),
                  image: DecorationImage(
                    image: imageProvider,
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              errorWidget: (context, url, error) => Icon(Icons.error),
            ),
            SizedBox(
              width: 10.w,
            ),
            CustomText(
              text: text + " - " + itineraries.flightNo + " ",
              color: blackColor,
              fontSize: 14.sp,
              fontWeight: FontWeight.bold,
            ),
            Spacer(),
            CustomText(
              text: DateFormat('MMM , dd', Get.locale.toString())
                  .format(itineraries.departureDateTime!)
                  .toString(),
              color: textColor,
              fontSize: 14.sp,
              fontWeight: FontWeight.bold,
            ),
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            CustomText(
              text: DateFormat('HH:mm')
                  .format(itineraries.departureDateTime!)
                  .toString(),
              color: blackColor,
              fontSize: 14.sp,
              fontWeight: FontWeight.bold,
            ),
            Column(
              children: [
                CustomText(
                  text: 'ticketClass'.tr + "  " + itineraries.ticketClass!,
                  fontSize: 14.sp,
                  fontWeight: FontWeight.bold,
                  color: blackColor,
                ),
                SvgPicture.asset(
                  'assets/icons/line.svg'
                      ,
                  width: Get.width / 2,
                ),
                CustomText(
                  text: itineraries.duration!,
                  fontSize: 14.sp,
                  fontWeight: FontWeight.bold,
                ),
              ],
            ),
            CustomText(
              text: DateFormat('HH:mm')
                  .format(itineraries.arrivalDateTime!)
                  .toString(),
              color: blackColor,
              fontSize: 14.sp,
              fontWeight: FontWeight.bold,
            ),
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            CustomText(
              text: local
                  ? " (" + itineraries.departureAirport!.iataCode.toString() + ")"
                  : " (" + itineraries.departureAirport!.iataCode.toString() + ")",
              color: textColor,
              fontSize: 12.sp,
              fontWeight: FontWeight.bold,
            ),
            CustomText(
              text: local
                  ? " (" + itineraries.arrivalAirport!.iataCode.toString() + ")"
                  : " (" + itineraries.arrivalAirport!.iataCode.toString() + ")",
              color: textColor,
              fontSize: 12.sp,
              fontWeight: FontWeight.bold,
            ),
          ],
        ),
        Divider(),
      ],
    );
  }

  PassengerWidget(BookingTotalDetails booking, int number) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        CustomText(
          text:
              '$number - ${booking.passenger!.firstName}  ${booking.passenger!.lastName}',
          color: blackColor,
        ),
        CustomText(
          text: booking.total!.toStringAsFixed(2) + 'LYD'.tr,
          color: blackColor,
        ),
      ],
    );
  }
}
