import 'package:e_travel/app/data/models/flight_booking.dart';
import 'package:e_travel/app/modules/widgets/custom_button.dart';
import 'package:e_travel/app/modules/widgets/custom_text.dart';
import 'package:e_travel/app/routes/app_pages.dart';
import 'package:e_travel/app/util/consts/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

class Success extends StatelessWidget {
  FlightBooking flightBooking;
  bool edit;

  Success({required this.flightBooking, required this.edit});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: whiteColor,
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          edit
              ? CustomText(
                  text: 'Your ticket has been modified successfully'.tr,
                  color: primaryColor,
                  textAlign: TextAlign.center,
                  maxLines: 3,
                  fontSize: 20.sp,
                )
              : CustomText(
                  text:
                      'Your ticket has been successfully booked. The ticket will be sent to your email'
                          .tr,
                  color: primaryColor,
                  textAlign: TextAlign.center,
                  maxLines: 3,
                  fontSize: 20.sp,
                ),
          Image(
            image: AssetImage('assets/images/success.gif'),
            height: 150.0.h,
          ),
          CustomText(
            text: 'Your payment was done successfully'.tr,
            color: primaryColor,
            fontSize: 20.sp,
          ),
          SizedBox(height:10.h ,),
          CustomText(
            text: 'Your reservation number is'.tr + flightBooking.pnr.toString(),
            color: primaryColor,
            fontSize: 20.sp,
          ),
          SizedBox(height:10.h ,),
          CustomButton(
            text: 'Go to Home'.tr,
            onClick: () =>  Get.offAllNamed(Routes.MAIN),
          ),
        ],
      ),
    );
  }
}