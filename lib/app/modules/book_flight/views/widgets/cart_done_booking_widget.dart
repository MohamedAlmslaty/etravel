import 'package:e_travel/app/util/consts/colors.dart';
import 'package:e_travel/app/modules/widgets/custom_button.dart';
import 'package:e_travel/app/modules/widgets/custom_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';

class CartDaneBookingWidget extends StatelessWidget {
  CartDaneBookingWidget(
      {required this.btnText,
      this.destination='',
      this.departure='',
      required this.leftText,
      required this.rightText,
      this.onClick});

  String btnText, leftText, rightText;
  String destination, departure;
  Function? onClick;

  @override
  Widget build(BuildContext context) {
    return Container(
        width: 375.w,
        height: 110.h,
        padding: EdgeInsets.only(top: 10.h, right: 58.w, left: 46.w),
        color: whiteColor,
        child: Column(
          children: [
            Row(
              mainAxisAlignment: leftText.isNotEmpty
              ?MainAxisAlignment.spaceBetween
              :MainAxisAlignment.center,
              children: [
                departure.isNotEmpty && rightText.isNotEmpty
                    ? Row(
                        children: [
                          SvgPicture.asset(
                            departure,
                            width: 20.w,
                            height: 20.h,
                          ),
                          SizedBox(
                            width: 5.w,
                          ),
                          CustomText(
                              text: rightText,
                              fontSize: 16.sp,
                              fontWeight: FontWeight.bold,
                              color: primaryColor),
                        ],
                      )
                    : CustomText(
                        text: rightText,
                        fontSize: 16.sp,
                        fontWeight: FontWeight.bold,
                        color: primaryColor),
                destination.isNotEmpty && leftText.isNotEmpty
                    ? Row(
                        children: [
                          SvgPicture.asset(
                            destination,
                            width: 20.w,
                            height: 20.h,
                          ),
                          SizedBox(
                            width: 5.w,
                          ),
                          CustomText(
                              text: leftText,
                              fontSize: 16.sp,
                              fontWeight: FontWeight.bold,
                              color: primaryColor),
                        ],
                      )
                    : CustomText(
                        text: leftText,
                        fontSize: 16.sp,
                        fontWeight: FontWeight.bold,
                        color: primaryColor),
              ],
            ),
            SizedBox(
              height: 15.h,
            ),
            CustomButton(
              onClick: () => onClick!(),
              text: btnText,
              height: 40.w,
              width: 283.w,
              color: primaryColor,
              colorText: whiteColor,
              radiusBottomLeft: 10.0.r,
              radiusBottomRight: 10.0.r,
              radiusTopLeft: 10.0.r,
              radiusTopRight: 10.0.r,
            ),
          ],
        ));
  }
}
