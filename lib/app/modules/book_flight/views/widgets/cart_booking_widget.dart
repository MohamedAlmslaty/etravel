import 'package:e_travel/app/data/models/fligh_rq.dart';
import 'package:e_travel/app/modules/flight/views/widgets/row_flight_widget.dart';
import 'package:e_travel/app/modules/widgets/custom_text.dart';
import 'package:e_travel/app/util/consts/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class CartBookWidget extends StatelessWidget {
  FlightRq flightRq;
  bool onWay;
  CartBookWidget({required this.flightRq,required this.onWay});
  @override
  Widget build(BuildContext context) {
    return Container(
        width: 343.w,
        height:  flightRq.goingStops == 0 && flightRq.returnStops == 0
            ? onWay ? 85.h : 165.h
            : flightRq.goingStops! >= 1
            ? ((flightRq.goingStops! + 1) * 85).h
            : flightRq.returnStops! >= 1
            ? ((flightRq.returnStops! + 1) * 85).h
            : flightRq.goingStops! >= 1 &&
            flightRq.returnStops! >= 1
            ? ((flightRq.goingStops! + 1) *
            (flightRq.returnStops! + 1) *
            85)
            .h :85.h,
        child:  Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
                padding: EdgeInsets.symmetric(horizontal: 20),
                child:Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    CustomText(
                      text: flightRq.itineraries![0].airline!.nameAr.toString(),
                      color: primaryColor,
                      fontSize: 16.sp,
                      fontWeight: FontWeight.bold,
                      textAlign: TextAlign.right,
                    ),
                    CustomText(
                      text: flightRq.itineraries![0].airline!.nameEn.toString(),
                      color: primaryColor,
                      fontSize: 16.sp,
                      fontWeight: FontWeight.bold,
                      textAlign: TextAlign.left,
                    ),
                  ],
                )
            ),
            Expanded(child: ListView.separated(
              padding: EdgeInsets.only(top: 10.h),
                separatorBuilder: (BuildContext context, int index) => SizedBox(
                  height: 5.h,
                ),
                scrollDirection: Axis.vertical,
                itemCount: flightRq.itineraries!.length,
                itemBuilder: (BuildContext context, int index) => Container(
                    height: 85.h,
                    child: Column(
                      children: [
                        flightRq.itineraries![index].segment == 1 &&
                                flightRq.goingStops == 0
                            ? RowFlightWidget(
                                itineraries: flightRq.itineraries![index],
                                multi: false,
                              )
                            : flightRq.itineraries![index].segment == 1 &&
                                    flightRq.goingStops! >= 1
                                ? RowFlightWidget(
                                    itineraries: flightRq.itineraries![index],
                                    multi: true,
                                    stops: flightRq.goingStops,
                                  )
                                : flightRq.itineraries![index].segment == 2 &&
                                        flightRq.returnStops == 0
                                    ? RowFlightWidget(
                                        itineraries:
                                            flightRq.itineraries![index],
                                        multi: false,
                                      )
                                    : RowFlightWidget(
                                        itineraries:
                                            flightRq.itineraries![index],
                                        multi: true,
                                        stops: flightRq.returnStops,
                                      ),
                      ],
                    )),
              ),
            ),
          ],
        ));
  }
}
