import 'package:e_travel/app/data/models/passenger.dart';
import 'package:e_travel/app/modules/book_flight/controllers/book_flight_controller.dart';
import 'package:e_travel/app/modules/book_flight/views/widgets/cart_booking_widget.dart';
import 'package:e_travel/app/modules/book_flight/views/widgets/cart_done_booking_widget.dart';
import 'package:e_travel/app/modules/passenger/views/passenger_add.dart';
import 'package:e_travel/app/modules/widgets/custom_text.dart';
import 'package:e_travel/app/routes/app_pages.dart';
import 'package:e_travel/app/util/consts/colors.dart';
import 'package:e_travel/app/util/consts/style.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:multi_select_flutter/multi_select_flutter.dart';

class BookFlightView extends GetView<BookFlightController> {
  bool local = Get.find<GetStorage>().read('language') == 'ar';

  @override
  Widget build(BuildContext context) {
    return GetBuilder<BookFlightController>(
        init: BookFlightController(),
        initState: (_) {},
        builder: (_con) => Scaffold(
              bottomNavigationBar: CartDaneBookingWidget(
                onClick: () {
                  if (_con.selectedPassengers.isEmpty &&
                      _con.selectedPassengers.length == 0) {
                    showSnackBar(
                        title: 'error'.tr,
                        message: 'Please choose Flight and Passengers !'.tr);
                  } else if (_con.attachments.isEmpty &&
                      _con.attachments.length == 0 &&
                      _con.flightRq.itineraries![0].airline!
                          .requireDocumentsForBooking) {
                    showSnackBar(
                        title: 'error'.tr,
                        message: 'Please select a visa photo!'.tr);
                  } else {
                    _con.flightRq.isMultiCity!
                    ?_con.totalBooking(_con.flightRq.isMultiCity!)
                   : _con.totalBooking(!(_con.flightRq.goingStops == 0 &&
                        _con.flightRq.returnStops == 0));
                    Get.toNamed(Routes.CONFIRM_BOOKING);
                  }
                },
                btnText: 'booking'.tr,
                leftText: '',
                rightText: '',
              ),
              body: SingleChildScrollView(
                child: Column(
                  children: [
                    SizedBox(
                      height: 44.h,
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(
                          horizontal: 20.w, vertical: 10.h),
                      child: Row(
                        children: [
                          IconButton(
                            onPressed: () => Get.back(),
                            icon: Icon(
                              Icons.arrow_back,
                              color: secondaryColor,
                            ),
                          ),
                          SizedBox(
                            width: 10.w,
                          ),
                          CustomText(
                            text: 'Book flight'.tr,
                            fontSize: 20.sp,
                          ),
                        ],
                      ),
                    ),
                    Stack(
                      children: [
                        Container(
                            height: Get.height,
                            width: Get.width,
                            padding: EdgeInsets.symmetric(horizontal: 20.w),
                            decoration: BoxDecoration(
                              color: secondaryColor,
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(50.r),
                                  topRight: Radius.circular(50.r)),
                            ),
                            child: CartBookWidget(
                              flightRq: _con.flightRq,
                              onWay: _con.onWay,
                            )),
                        Positioned(
                           top: _con.flightRq.itineraries!.length!=1?_con.flightRq.itineraries!.length*110.h:_con.flightRq.itineraries!.length*130.h,
                           //_con.flightRq.isMultiCity!
                          //     ? 220.h
                          //     :_con.flightRq.goingStops == 0 &&
                          //         _con.flightRq.returnStops == 0
                          //     ? _con.onWay
                          //         ? 130.h
                          //         : 220.h
                          //     : _con.flightRq.goingStops! >= 1 &&
                          //             _con.flightRq.returnStops! >= 1
                          //         ? ((_con.flightRq.goingStops! + 1) *
                          //                 (_con.flightRq.returnStops! + 1) *
                          //                 105)
                          //             .h
                          //         : _con.flightRq.goingStops! >= 1
                          //             ? ((_con.flightRq.goingStops! + 1) * 105)
                          //                 .h
                          //             : _con.flightRq.returnStops! >= 1
                          //                 ? ((_con.flightRq.returnStops! + 1) *
                          //                         105)
                          //                     .h
                          //                 : 130.h,
                          child: Container(
                            width: Get.width,
                            height: Get.height,
                            padding: EdgeInsets.only(
                                top: 20.h, right: 25.w, left: 25.w),
                            decoration: BoxDecoration(
                                color: whiteColor,
                                borderRadius: BorderRadius.only(
                                    topRight: Radius.circular(50.r),
                                    topLeft: Radius.circular(50.r))),
                            child: Column(children: [
                              // Row(
                              //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              //   children: [
                              //     CustomText(text: 'Passenger data'.tr, fontSize: 16.sp, fontWeight: FontWeight.bold, color: primaryColor),
                              //     SvgPicture.asset('assets/images/user_data.svg'),
                              //   ],
                              // ),
                              SizedBox(
                                height: 20.h,
                              ),
                              if( _con.flightRq.itineraries![0].airline!
                                  .iataCode=='BM'&& _con.flightRequest.destinationLocationCode=='MLA'||_con.flightRq.itineraries![0].airline!
                                  .iataCode=='BM'&& _con.flightRequest.destinationLocationCode=='FCO')
                              CustomText(text: 'Midsky destinations alert'.tr,color: Colors.red,maxLines: 3,textAlign: TextAlign.center,),
                              SizedBox(
                                height: 20.h,
                              ),
                              Obx(
                                () => MultiSelectDialogField(
                                  title: CustomText(
                                    text: 'Choose travelers'.tr,
                                    color: primaryColor,
                                  ),
                                  buttonIcon: Icon(
                                    Icons.supervised_user_circle_sharp,
                                    color: secondaryColor,
                                  ),
                                  buttonText: Text(
                                    "Choose travelers".tr,
                                    style: TextStyle(
                                      color: secondaryColor,
                                      fontSize: 16.sp,
                                    ),
                                  ),
                                  confirmText: Text(
                                    "OK".tr,
                                    style: TextStyle(
                                      color: primaryColor,
                                      fontSize: 16.sp,
                                    ),
                                  ),
                                  cancelText: Text(
                                    "Cancellation".tr,
                                    style: TextStyle(
                                      color: primaryColor,
                                      fontSize: 16.sp,
                                    ),
                                  ),
                                  items: _con.passengers
                                      .map((e) => MultiSelectItem(
                                          e, '${e.firstName} ${e.lastName}'))
                                      .toList(),
                                  listType: MultiSelectListType.CHIP,
                                  onConfirm: (List<Passenger> values) {
                                    _con.selectedPassengers.clear();
                                    _con.selectedPassengers = values;
                                    // _con.total.value=_con.flightRq.total! * _con.selectedPassengers.length;
                                  },
                                ),
                              ),
                              SizedBox(
                                height: 20.h,
                              ),
                              Container(
                                  height: 50.h,
                                  width: Get.width,
                                  decoration: BoxDecoration(
                                      border: Border.all(color: primaryColor),
                                      borderRadius:
                                          BorderRadius.circular(10.r)),
                                  child: TextButton.icon(
                                      onPressed: () =>
                                          Get.to(() => PassengerAdd()),
                                      icon: Icon(
                                        Icons.add_circle_outline,
                                        color: whiteColor,
                                        size: 20,
                                      ),
                                      style: TextButton.styleFrom(
                                        backgroundColor: primaryColor,
                                      ),
                                      label: CustomText(
                                          text: 'Add Passenger'.tr,
                                          fontWeight: FontWeight.bold,
                                          fontSize: 14.sp))),
                              SizedBox(
                                height: 20.h,
                              ),
                              _con.flightRq.itineraries![0].airline!
                                      .requireDocumentsForBooking
                                  ? Column(
                                      children: [
                                        Obx(() => CustomText(
                                              text: 'The number of images chosen is:'
                                                      .tr +
                                                  ' ${_con.numberOfImage.value}',
                                              color: blackColor,
                                              fontWeight: FontWeight.bold,
                                            )),
                                        SizedBox(
                                          height: 10.h,
                                        ),
                                        Container(
                                            height: 50.h,
                                            width: Get.width,
                                            decoration: BoxDecoration(
                                                border: Border.all(
                                                    color: primaryColor),
                                                borderRadius:
                                                    BorderRadius.circular(
                                                        10.r)),
                                            child: TextButton.icon(
                                                onPressed: () =>
                                                    _con.imgFromGallery(),
                                                icon: Icon(
                                                  Icons.upload_file,
                                                  color: whiteColor,
                                                  size: 20,
                                                ),
                                                style: TextButton.styleFrom(
                                                  backgroundColor: primaryColor,
                                                ),
                                                label: CustomText(
                                                    text:
                                                        'Upload a visa or residence'
                                                            .tr,
                                                    fontWeight: FontWeight.bold,
                                                    fontSize: 14.sp)))
                                      ],
                                    )
                                  : SizedBox(),
                              SizedBox(
                                height: 20.h,
                              ),
                              //  CustomText(text: 'bookingType'.tr, fontWeight: FontWeight.bold, color: blackColor, fontSize: 20.sp),
                              Column(
                                children: List.generate(_con.bookingType.length,
                                    (index) {
                                  var _type = _con.bookingType.elementAt(index);
                                  return Obx(
                                    () => RadioListTile(
                                      activeColor: secondaryColor,
                                      value: _type,
                                      groupValue: _con.typeBooking.value,
                                      onChanged: (value) {
                                        _con.typeBooking.value =
                                            value.toString();
                                        if (value == 'final booking') {
                                          _con.finalBook.value = true;
                                        } else
                                          _con.finalBook.value = false;
                                      },
                                      title: CustomText(
                                        text: _type.tr,
                                        fontSize: 14.sp,
                                        color: blackColor,
                                      ),
                                    ),
                                  );
                                }).toList(),
                              ),
                              Obx(
                                () => _con.finalBook.value
                                    ? Column(
                                        mainAxisSize: MainAxisSize.min,
                                        children:
                                            _con.openTickets.map((ticket) {
                                          return CheckboxListTile(
                                              value: ticket.isCheck.value,
                                              title: Text('${ticket.name}'.tr),
                                              controlAffinity:
                                                  ListTileControlAffinity
                                                      .leading,
                                              onChanged: (newValue) {
                                                ticket.isCheck.value =
                                                    newValue!;
                                                if (ticket.name ==
                                                    'goingOpen') {
                                                  _con.goingOpen.value =
                                                      ticket.isCheck.value;
                                                } else if (ticket.name ==
                                                    'returnOpen') {
                                                  _con.returnOpen.value =
                                                      ticket.isCheck.value;
                                                }
                                              });
                                        }).toList())
                                    : SizedBox(),
                              ),
                            ]),
                          ),
                        )
                      ],
                    ),
                  ],
                ),
              ),
            ));
  }
}
