import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:introduction_screen/introduction_screen.dart';

class OnboardController extends GetxController {
  final introKey = GlobalKey<IntroductionScreenState>();
}
