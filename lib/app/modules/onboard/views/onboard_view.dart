import 'package:e_travel/app/modules/onboard/controllers/onboard_controller.dart';
import 'package:e_travel/app/routes/app_pages.dart';
import 'package:e_travel/app/util/consts/colors.dart';
import 'package:e_travel/app/modules/widgets/custom_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:introduction_screen/introduction_screen.dart';

class OnboardView extends StatelessWidget {
  final _controller = OnboardController();
  String local=Get.find<GetStorage>().read('language');
  @override
  Widget build(BuildContext context) {
    var pageDecoration = PageDecoration(
      fullScreen: true,
      titleTextStyle: TextStyle(fontSize: 28.sp, color: blackColor),
      bodyTextStyle: TextStyle(
        fontSize: 17.sp,
        color: blackColor,
      ),
      bodyPadding: EdgeInsets.fromLTRB(16.0, 0.0, 16.0, 16.0),
      pageColor: Colors.transparent,
      imagePadding: EdgeInsets.zero,
    );

    return IntroductionScreen(
     globalBackgroundColor: whiteColor,
      key: _controller.introKey,
      pages: [
      PageViewModel(
        title: '',
        body: '',
        image: Image.asset(local=='ar'?'assets/images/onboard1.png':'assets/images/onboard1en.png', width: Get.width,height:Get.height,fit: BoxFit.fill,),
        decoration: pageDecoration,
      ),
        PageViewModel(
          title: '',
          body: '',
          image: Image.asset(local=='ar'?'assets/images/onboard2.png':'assets/images/onboard2en.png', width: Get.width,height:Get.height,fit: BoxFit.fill,),
          decoration: pageDecoration,
        ),
        PageViewModel(
          title: '',
          body: '',
          image: Image.asset(local=='ar'?'assets/images/onboard3.png':'assets/images/onboard3en.png', width: Get.width,height:Get.height,fit: BoxFit.fill,),
          decoration: pageDecoration,
        ),
      ],
      onDone: () => Get.offAllNamed(Routes.MAIN),
      showSkipButton: true,
      skipOrBackFlex: 0,
      nextFlex: 0,
      skip: CustomText(
        text: 'Skip'.tr,
        color: primaryColor,
        fontSize: 18.sp,
      ),
      next: CustomText(
        text: 'next'.tr,
        color: primaryColor,
        fontSize: 18.sp,
      ),
      done: CustomText(
        text: 'start'.tr,
        color: primaryColor,
        fontWeight: FontWeight.bold,
        fontSize: 20.sp,
      ),
      dotsDecorator: DotsDecorator(
        size: Size(10.0, 10.0),
        color: greyColor.withOpacity(0.5),
        activeColor: secondaryColor,
        activeSize: Size(22.0, 10.0),
        activeShape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(25.r)),
        ),
      ),
    );
  }
}
