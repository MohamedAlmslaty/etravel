import 'dart:async';

import 'package:e_travel/app/data/models/customer.dart';
import 'package:e_travel/app/data/repositories/customer_point_repository.dart';
import 'package:e_travel/app/data/repositories/user_repository.dart';
import 'package:e_travel/app/data/repositories/wallet_repository.dart';
import 'package:e_travel/app/routes/app_pages.dart';
import 'package:e_travel/app/util/consts/loading.dart';
import 'package:e_travel/app/util/consts/style.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class CustomerPointController extends GetxController {
  final _loyalRepo = LoyalRepository.instance;
  final _userRepo = UserRepository.instance;
  final _walletRepo = WalletRepository.instance;
  final Rx<Customer> _user = Customer().obs;
  final balance = 0.0.obs;
  final libyanaOTP = ''.obs;
  final libyanaPoints = '0'.obs;
  final balanceWallet = 0.0.obs;
  final load = false.obs;
  RxBool active = false.obs;
  final libyanaPoint = 100.obs;

  Customer get user => _user.value;

  GlobalKey<FormState> phoneForm = new GlobalKey<FormState>();
  GlobalKey<FormState> libyanaOTPForm = new GlobalKey<FormState>();
  TextEditingController phone = new TextEditingController();
  TextEditingController otp = new TextEditingController();
  TextEditingController otpController = new TextEditingController();

  ///Timer
  RxInt counter = 120.obs;
  Timer? _timer;

  Timer get timer => _timer!;

  void startTimer() {
    const oneSec = const Duration(seconds: 1);
    _timer = new Timer.periodic(
      oneSec,
      (Timer timer) {
        if (counter.value == 0) {
          timer.cancel();
          update();
        } else {
          counter.value--;
        }
      },
    );
  }

  ///

  @override
  void onInit() {
    if (Get.find<GetStorage>().hasData('token')) loadUser();
    loadBalance();
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}

  Future<bool> loadUser([bool refresh = false]) async {
    _user.value = await _userRepo.getCustomerAccount();

    update();
    return true;
  }

  Future<void> loadWalletBalance() async {
    balanceWallet.value = await _walletRepo.getWalletBalance();
    update();
    if (balanceWallet.value != null && balanceWallet.value != "") {
      Get.find<GetStorage>().write('wallet_amount', balanceWallet.value);
      if (Get.find<GetStorage>().hasData('wallet_amount'))
        walletBalance.value = Get.find<GetStorage>().read('wallet_amount');
    }
  }

  Future<bool> loadBalance() async {
    balance.value = await _loyalRepo.getCustomerPointsBalance();
    update();
    load.value = true;
    return true;
  }

  Future<void> resendOTP() async {
    await _loyalRepo.libyanaOTP(phone.text.trim());
    counter.value = 120;
    startTimer();
  }

  Future<void> goToOTP() async {
    if (phoneForm.currentState!.validate()) {
      phoneForm.currentState!.save();
      loading();
      await _loyalRepo.libyanaOTP(phone.text.trim()).then((value) {
        if (value != null) {
          libyanaOTP.value = value;
          EasyLoading.dismiss();
          startTimer();
          Get.toNamed(Routes.LIBYANA_OTP);
        }
      });
    }
  }

  Future<void> OTPLibyana() async {
    if (libyanaOTP.value == otp.text.trim()) {
      loading();
      await _loyalRepo.getLibyanaPoint(phone.text.trim()).then((value) {
        if (value != null) {
          EasyLoading.dismiss();
          libyanaPoints.value=value;
          Get.toNamed(Routes.REPLACE);
          phone.clear();
          otp.clear();
        }
      });
    } else {
      showSnackBar(title: 'error'.tr, message: 'the data enter not valid'.tr);
    }
  }

  Future<void> redeem100() async {
    loading();
    await _loyalRepo.redeem(user.id!, 20, 0).then((value) {
      if (value != null) {
        EasyLoading.dismiss();
        loadWalletBalance();
        loadBalance();
        showSnackBar(
            title: 'successful'.tr,
            message: 'Points have been redeemed successfully'.tr);
      }
    });
  }
}
