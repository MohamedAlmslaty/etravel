import 'package:get/get.dart';

import '../controllers/customer_points_controller.dart';

class CustomerPointBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<CustomerPointController>(
      () => CustomerPointController(),
    );
  }
}
