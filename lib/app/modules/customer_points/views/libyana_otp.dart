import 'package:e_travel/app/modules/customer_points/controllers/customer_points_controller.dart';
import 'package:e_travel/app/modules/widgets/custom_button.dart';
import 'package:e_travel/app/modules/widgets/custom_text.dart';
import 'package:e_travel/app/modules/widgets/custom_widget.dart';
import 'package:e_travel/app/util/consts/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:get/get.dart';

class LibyanaOTPView extends GetView<CustomerPointController> {
  const LibyanaOTPView({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScopeNode currentFocus = FocusScope.of(context);
        if (!currentFocus.hasPrimaryFocus) currentFocus.unfocus();
      },
      child: Scaffold(
        body: SingleChildScrollView(
          child: CustomWidget(
            title: 'loyal'.tr,
            child: GetBuilder<CustomerPointController>(
              init: CustomerPointController(),
              builder: (_) {
                return Padding(
                  padding: EdgeInsets.symmetric(horizontal: 20.w),
                  child: Column(
                    children: [
                      SizedBox(
                        height: 49.h,
                      ),
                      SvgPicture.asset(
                        'assets/images/account.svg',
                        width: 81.w,
                        height: 81.w,
                        fit: BoxFit.fill,
                        color: primaryColor,
                      ),
                      Form(
                        key: _.libyanaOTPForm,
                        child: Column(
                          children: [
                            CustomText(
                              text: 'Please Enter verification code'.tr,
                              color: primaryColor,
                              fontSize: 20.sp,
                              fontWeight: FontWeight.bold,
                            ),
                            SizedBox(
                              height: 20.h,
                            ),
                            TextFormField(
                              keyboardType: TextInputType.number,
                              maxLength: 4,
                              controller: controller.otp,
                              validator: RequiredValidator(
                                  errorText:
                                      'Please Enter verification code'.tr),
                              decoration: InputDecoration(
                                contentPadding: const EdgeInsets.all(12),
                                fillColor: whiteColor,
                                filled: true,
                                hintText: 'XXXX',
                                hintStyle: TextStyle(
                                    color: textColor.withOpacity(0.7)),
                                prefixIcon:
                                    Icon(Icons.pin, color: primaryColor),
                                border: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color: textColor.withOpacity(0.2))),
                                focusedBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color: textColor.withOpacity(0.5))),
                                enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color: textColor.withOpacity(0.2))),
                              ),
                            ),
                            Obx(
                              () => controller.counter.value != 0
                                  ? Obx(() => Center(
                                          child: CustomText(
                                        text:
                                            'You can request to re-send the verification code after'
                                                    .tr +
                                                ' ' +
                                                _.counter.value.toString() +
                                                ' ' +
                                                'second'.tr,
                                        color: primaryColor,
                                        maxLines: 3,
                                        fontWeight: FontWeight.bold,
                                        fontSize: 14.sp,
                                      )))
                                  : CustomButton(
                                      text: 'resend'.tr,
                                      width: Get.width / 4,
                                      height: 40.h,
                                      onClick: () {
                                        _.resendOTP();

                                      }),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            CustomButton(
                              text: 'send'.tr,
                              onClick: () => _.OTPLibyana(),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                );
              },
            ),
          ),
        ),
      ),
    );
  }
}
