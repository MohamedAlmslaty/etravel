import 'package:e_travel/app/modules/widgets/custom_button.dart';
import 'package:e_travel/app/modules/widgets/custom_text.dart';
import 'package:e_travel/app/modules/widgets/custom_widget.dart';
import 'package:e_travel/app/routes/app_pages.dart';
import 'package:e_travel/app/util/consts/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:percent_indicator/percent_indicator.dart';

import '../controllers/customer_points_controller.dart';

class CustomerPointView extends GetView<CustomerPointController> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<CustomerPointController>(
      init: CustomerPointController(),
      initState: (_) {},
      builder: (_) => Scaffold(
        resizeToAvoidBottomInset: false,
        body: CustomWidget(
          title: 'loyal'.tr,
          child: Padding(
            padding: EdgeInsets.only(top: 20.h, right: 20.w, left: 20.w),
            child: _.load.value
                ? RefreshIndicator(
                    onRefresh: () async {
                      _.loadBalance();
                    },
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox(
                          height: 20.h,
                        ),
                        Image.asset(
                          'assets/images/Libyana.png',
                          width: 150.w,
                          height: 30.w,
                        ),
                        CustomButton(
                          text: 'add points'.tr,
                          onClick: () => Get.toNamed(Routes.PHONE),
                          height: 25.w,
                          width: 100.h,
                          fontSize: 13.sp,
                        ),
                        SizedBox(
                          height: 20.h,
                        ),
                        Center(
                            child: Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                              Container(
                                width: 100.w,
                                height: 100.w,
                                child: SvgPicture.asset(
                                  'assets/images/loyalty.svg',
                                  fit: BoxFit.fill,
                                  color: primaryColor,
                                ),
                                decoration: BoxDecoration(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(100.r)),
                                    border: Border.all(
                                        color: primaryColor, width: 5)),
                              ),
                              SizedBox(height: 10.w,),
                              Container(
                                  width: Get.width,
                                  height: 45.h,
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(50.r)),
                                      color: primaryColor),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,

                                    children: [
                                     SvgPicture.asset(
                                          'assets/icons/profileIcon.svg',
                                          width: 30.w,
                                          height: 30.w,
                                          color: secondaryColor,
                                        ),

                                      SizedBox(width: 10.w,),
                                      Obx(
                                        () => CustomText(
                                          text: _.user.name.toString(),
                                          fontWeight: FontWeight.bold,
                                          fontSize: 14.sp,
                                          color: whiteColor,
                                        ),
                                      ),
                                    ],
                                  )),
                              SizedBox(
                                height: 10.h,
                              ),
                              CustomText(
                                text: 'your points'.tr +
                                    ' : ' +
                                    _.balance.value.toString() +
                                    ' ' +
                                    'point'.tr,
                                fontWeight: FontWeight.bold,
                                fontSize: 21.sp,
                                color: primaryColor,
                              ),
                              SizedBox(
                                height: 10.h,
                              ),
                              _.balance.value >= 100
                                  ? CustomButton(
                                      text:
                                          'replacing'.tr + ' 100 ' + 'point'.tr,
                                      onClick: () => _.redeem100(),
                                      height: 25.w,
                                      width: 200.h,
                                      fontSize: 13.sp,
                                    )
                                  : SizedBox(),
                              SizedBox(
                                height: 100.h,
                              ),
                              CustomButton(
                                text: 'collect points'.tr,
                                onClick: () => {
                                  Get.dialog(
                                    AlertDialog(
                                      title: CustomText(
                                        text: 'policy redeem title'.tr,
                                        color: primaryColor,
                                        maxLines: 2,
                                        fontSize: 12.sp,
                                        fontWeight: FontWeight.bold,
                                      ),
                                      content: SizedBox(
                                        height: Get.height / 2.5.h,
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            CustomText(
                                              text: 'policy redeem desc'.tr,
                                              color: primaryColor,
                                              maxLines: 4,
                                              fontSize: 12.sp,
                                            ),
                                            SizedBox(
                                              height: 15.h,
                                            ),
                                            CustomText(
                                              text: 'policy redeem points'.tr,
                                              color: primaryColor,
                                              maxLines: 2,
                                              fontWeight: FontWeight.bold,
                                              fontSize: 13.sp,
                                            ),
                                            SizedBox(
                                              height: 15.h,
                                            ),
                                            CustomText(
                                              text: '* ' +
                                                  'policy redeem point1'.tr,
                                              color: primaryColor,
                                              maxLines: 2,
                                              fontSize: 12.sp,
                                            ),
                                            SizedBox(
                                              height: 5.h,
                                            ),
                                            CustomText(
                                              text: '* ' +
                                                  'policy redeem point2'.tr,
                                              color: primaryColor,
                                              maxLines: 2,
                                              fontSize: 12.sp,
                                            ),
                                            SizedBox(
                                              height: 5.h,
                                            ),
                                            CustomText(
                                              text: '* ' +
                                                  'policy redeem point3'.tr,
                                              color: primaryColor,
                                              maxLines: 2,
                                              fontSize: 12.sp,
                                            ),
                                            SizedBox(
                                              height: 5.h,
                                            ),
                                            CustomText(
                                              text: '* ' +
                                                  'policy redeem point4'.tr,
                                              color: primaryColor,
                                              maxLines: 2,
                                              fontSize: 12.sp,
                                            ),
                                          ],
                                        ),
                                      ),
                                      actions: [
                                        CustomButton(
                                          text: 'I understood, thank you'.tr,
                                          onClick: () => Get.back(),
                                          color: Colors.green,
                                        ),
                                      ],
                                    ),
                                  ),
                                },
                              ),
                              SizedBox(
                                height: 10.h,
                              ),
                            ]))
                      ],
                    ))
                : Center(child: CircularProgressIndicator()),
          ),
        ),
      ),
    );
  }
}
