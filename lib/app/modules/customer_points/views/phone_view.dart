import 'package:e_travel/app/modules/widgets/custom_button.dart';
import 'package:e_travel/app/modules/widgets/custom_text.dart';
import 'package:e_travel/app/modules/widgets/custom_widget.dart';
import 'package:e_travel/app/util/consts/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:get/get.dart';

import '../controllers/customer_points_controller.dart';

class PhoneView extends GetView<CustomerPointController> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<CustomerPointController>(
      init: CustomerPointController(),
      initState: (_) {},
      builder: (_) => Scaffold(
        resizeToAvoidBottomInset: false,
        body: CustomWidget(
          title: 'loyal'.tr,
          child: Padding(
            padding: EdgeInsets.only(top: 20.h, right: 20.w, left: 20.w),
            child: _.load.value
                ?  Column(children: [
                      SizedBox(
                        height: 49.h,
                      ),
                      SvgPicture.asset(
                        'assets/images/account.svg',
                        width: 81.w,
                        height: 81.w,
                        fit: BoxFit.fill,
                        color: primaryColor,
                      ),
                      Obx(
                        () => CustomText(
                          text: _.user.name.toString(),
                          fontWeight: FontWeight.bold,
                          fontSize: 21.sp,
                          color: primaryColor,
                        ),
                      ),
                      Form(
                          key: _.phoneForm,
                          child: Column(
                            children: [
                              SizedBox(
                                height: 10.h,
                              ),
                              CustomText(
                                text: 'Enter Libyana number'.tr,
                                fontWeight: FontWeight.bold,
                                fontSize: 21.sp,
                                color: primaryColor,
                              ),
                              SizedBox(
                                height: 10.h,
                              ),
                              TextFormField(
                                keyboardType: TextInputType.number,
                                maxLength: 9,
                                controller: controller.phone,
                                validator: MultiValidator([
                                  RequiredValidator(
                                      errorText: 'Please enter phone'.tr),
                                  MinLengthValidator(9,
                                      errorText:
                                          'Please Enter Valid phone number'.tr),
                                  PatternValidator(
                                    r'^(9?((2|4)[0-9]{7}))$',
                                    errorText:
                                        'Please Enter Valid phone number'.tr,
                                  )
                                ]),
                                decoration: InputDecoration(
                                  contentPadding: const EdgeInsets.all(12),
                                  fillColor: whiteColor,
                                  filled: true,
                                  hintText: 'phone_ex1'.tr,
                                  hintStyle: TextStyle(
                                      color: textColor.withOpacity(0.7)),
                                  prefixIcon:
                                      Icon(Icons.phone, color: primaryColor),
                                  border: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color: textColor.withOpacity(0.2))),
                                  focusedBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color: textColor.withOpacity(0.5))),
                                  enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color: textColor.withOpacity(0.2))),
                                ),
                              ),
                              SizedBox(
                                height: 10.h,
                              ),
                              CustomButton(
                                text: 'Enquiry'.tr,
                                onClick: () => _.goToOTP(),
                              ),
                              SizedBox(
                                height: 10.h,
                              ),
                            ],
                          ))
                    ])
                : Center(child: CircularProgressIndicator()),
          ),
        ),
        ),
    );
  }
}
