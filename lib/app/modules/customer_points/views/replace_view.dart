import 'package:e_travel/app/modules/widgets/custom_button.dart';
import 'package:e_travel/app/modules/widgets/custom_text.dart';
import 'package:e_travel/app/modules/widgets/custom_widget.dart';
import 'package:e_travel/app/routes/app_pages.dart';
import 'package:e_travel/app/util/consts/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_spinbox/cupertino.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';

import '../controllers/customer_points_controller.dart';

class ReplaceView extends GetView<CustomerPointController> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<CustomerPointController>(
      init: CustomerPointController(),
      initState: (_) {},
      builder: (_) => Scaffold(
        resizeToAvoidBottomInset: false,
        body: CustomWidget(
          title: 'loyal'.tr,
          child: Padding(
            padding: EdgeInsets.only(top: 20.h, right: 20.w, left: 20.w),
            child: _.load.value
                ? RefreshIndicator(
                    onRefresh: () async {
                      _.loadBalance();
                    },
                    child: Column(children: [
                      Obx(() => CustomText(
                        text: 'Libyana points'.tr + _.libyanaPoints.value +" " +'point'.tr,
                        fontWeight: FontWeight.bold,
                        fontSize: 18.sp,
                        color: primaryColor,
                      ),
                      ),

                      SizedBox(
                        height: 49.h,
                      ),
                      SvgPicture.asset(
                        'assets/images/account.svg',
                        width: 81.w,
                        height: 81.w,
                        fit: BoxFit.fill,
                        color: primaryColor,
                      ),
                      Obx(
                        () => CustomText(
                          text: _.user.name.toString(),
                          fontWeight: FontWeight.bold,
                          fontSize: 21.sp,
                          color: primaryColor,
                        ),
                      ),
                      SizedBox(
                        height: 10.h,
                      ),
                      Container(
                        color: whiteColor,
                        width: 350.h,
                        height: 200.h,
                        child: Padding(
                          padding: EdgeInsets.all(20),
                          child: Column(
                            children: [
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  CustomText(
                                    text: 'replacing'.tr,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 21.sp,
                                    color: primaryColor,
                                  ),
                                  Container(
                                      width: 160.w,
                                      child: CupertinoSpinBox(
                                        incrementIcon: Icon(
                                          Icons.add_circle_outline,
                                          color: secondaryColor,
                                        ),
                                        decrementIcon: Icon(
                                          Icons.remove_circle_outline,
                                          color: secondaryColor,
                                        ),
                                        decoration: BoxDecoration(
                                          color: whiteColor,
                                        ),
                                        textStyle: TextStyle(
                                          color: primaryColor,
                                        ),
                                        min: 100,
                                        max: 1000,
                                        step: 100,
                                        value: _.libyanaPoint.value.toDouble(),
                                        onChanged: (value) {
                                          _.libyanaPoint.value = value.toInt();
                                        },
                                      )),
                                ],
                              ),
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  CustomText(
                                    text: 'loyal'.tr,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 21.sp,
                                    color: primaryColor,
                                  ),
                                 Obx(() =>  CustomText(
                                    text: '${_.libyanaPoint.value * 0.2}',
                                    fontWeight: FontWeight.bold,
                                    fontSize: 21.sp,
                                    color: primaryColor,
                                  ),
                                  ),
                                ],
                              )
                            ],
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 10.h,
                      ),
                      CustomButton(
                        text: 'replacing'.tr,
                        onClick: () => Get.toNamed(Routes.SUCCESS_REPLACE),
                      ),
                      SizedBox(
                        height: 10.h,
                      ),
                    ]))
                : Center(child: CircularProgressIndicator()),
          ),
        ),
      ),
    );
  }
}
