import 'package:e_travel/app/modules/widgets/custom_button.dart';
import 'package:e_travel/app/modules/widgets/custom_text.dart';
import 'package:e_travel/app/modules/widgets/custom_widget.dart';
import 'package:e_travel/app/routes/app_pages.dart';
import 'package:e_travel/app/util/consts/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:get/get.dart';

import '../controllers/customer_points_controller.dart';

class OTPView extends GetView<CustomerPointController> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<CustomerPointController>(
      init: CustomerPointController(),
      initState: (_) {},
      builder: (_) => Scaffold(
        resizeToAvoidBottomInset: false,
        body: CustomWidget(
          title: 'loyal'.tr,
          child: Padding(
            padding: EdgeInsets.only(top: 20.h, right: 20.w, left: 20.w),
            child: _.load.value
                ? RefreshIndicator(
                    onRefresh: () async {
                      _.loadBalance();
                    },
                    child: Column(children: [
                      SizedBox(
                        height: 49.h,
                      ),
                      SvgPicture.asset(
                        'assets/images/account.svg',
                        width: 81.w,
                        height: 81.w,
                        fit: BoxFit.fill,
                        color: primaryColor,
                      ),
                      Obx(
                        () => CustomText(
                          text: _.user.name.toString(),
                          fontWeight: FontWeight.bold,
                          fontSize: 21.sp,
                          color: primaryColor,
                        ),
                      ),
                      SizedBox(
                        height: 10.h,
                      ),
                      Container(
                        color: whiteColor,
                        width: 350.h,
                        height: 240.h,
                        child: Padding(
                          padding: EdgeInsets.all(20),
                          child: Column(
                            children: [
                              CustomText(
                                text: 'will be replaced'.tr +
                                    ' 000 ' +
                                    'Libyana point to'.tr +
                                    ' 00 ' +
                                    'Trace points for confirmation Enter the code that you will receive on your phone'
                                        .tr,
                                maxLines: 3,
                                fontSize: 18.sp,
                                color: primaryColor,
                              ),
                              Row(
                                children: [
                                  Container(
                                    width: 150.h,
                                    child: SizedBox(
                                      height: 60.h,
                                      child: TextFormField(
                                        keyboardType: TextInputType.number,
                                        decoration: InputDecoration(
                                          contentPadding:
                                              const EdgeInsets.all(12),
                                          fillColor: Colors.grey,
                                          filled: true,
                                          // hintText: 'otp'.tr,
                                          // hintStyle:
                                          // TextStyle(color: textColor.withOpacity(0.7)),
                                          border: OutlineInputBorder(
                                              borderSide: BorderSide(
                                                  color: textColor
                                                      .withOpacity(0.2))),
                                          focusedBorder: OutlineInputBorder(
                                              borderSide: BorderSide(
                                                  color: textColor
                                                      .withOpacity(0.5))),
                                          enabledBorder: OutlineInputBorder(
                                              borderSide: BorderSide(
                                                  color: textColor
                                                      .withOpacity(0.2))),
                                        ),
                                        validator: RequiredValidator(
                                            errorText:
                                                'Please Enter verification code'
                                                    .tr),
                                        maxLength: 4,
                                        controller: controller.otpController,
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                    width: 5.w,
                                  ),
                                  Obx(
                                    () => controller.counter.value != 0
                                        ? Obx(() => Center(
                                                child: CustomText(
                                              text: controller.counter.value
                                                      .toString() +
                                                  ' ' +
                                                  'second'.tr,
                                              color: blackColor,
                                              fontWeight: FontWeight.bold,
                                              fontSize: 18.sp,
                                            )))
                                        : CustomButton(
                                            text: 'resend'.tr,
                                            width: Get.width / 4,
                                            height: 40.h,
                                            onClick: () {
                                              //controller.sendOTP();
                                              controller.counter.value = 120;
                                              controller.startTimer();
                                            }),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 10.h,
                      ),
                      CustomButton(
                        text: 'send'.tr,
                        onClick: () => Get.toNamed(Routes.SUCCESS_REPLACE),
                      ),
                      SizedBox(
                        height: 10.h,
                      ),
                    ]))
                : Center(child: CircularProgressIndicator()),
          ),
        ),
      ),
    );
  }
}
