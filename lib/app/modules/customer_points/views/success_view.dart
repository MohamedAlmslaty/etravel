import 'package:e_travel/app/modules/widgets/custom_button.dart';
import 'package:e_travel/app/modules/widgets/custom_text.dart';
import 'package:e_travel/app/modules/widgets/custom_widget.dart';
import 'package:e_travel/app/routes/app_pages.dart';
import 'package:e_travel/app/util/consts/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';

import '../controllers/customer_points_controller.dart';

class SuccessView extends GetView<CustomerPointController> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<CustomerPointController>(
      init: CustomerPointController(),
      initState: (_) {},
      builder: (_) => Scaffold(
        resizeToAvoidBottomInset: false,
        body: CustomWidget(
          title: 'loyal'.tr,
          child: Padding(
            padding: EdgeInsets.only(top: 20.h, right: 20.w, left: 20.w),
            child: _.load.value
                ? Column(children: [
                    SvgPicture.asset(
                      'assets/images/account.svg',
                      width: 81.w,
                      height: 81.w,
                      fit: BoxFit.fill,
                      color: primaryColor,
                    ),
                    Obx(
                      () => CustomText(
                        text: _.user.name.toString(),
                        fontWeight: FontWeight.bold,
                        fontSize: 21.sp,
                        color: primaryColor,
                      ),
                    ),
                    SizedBox(
                      height: 10.h,
                    ),
                    Container(
                      color: whiteColor,
                      width: 350.h,
                      height: 240.h,
                      child: Padding(
                        padding: EdgeInsets.all(20),
                        child: Center(
                          child:  CustomText(
                            text: 'Libyana Points Exchange is not available now, please try again later'.tr,
                            fontSize: 18.sp,
                            maxLines: 2,
                            color: primaryColor,
                          ),
                        )
                        // Column(
                        //   children: [
                        //     CustomText(
                        //       text: 'It has been successfully replaced'.tr,
                        //       fontSize: 18.sp,
                        //       maxLines: 2,
                        //       color: primaryColor,
                        //     ),
                        //     Image(
                        //       image: AssetImage('assets/images/success.gif'),
                        //       height: 150.0.h,
                        //     ),
                        //   ],
                        // ),
                      ),
                    ),
                    SizedBox(
                      height: 10.h,
                    ),
                    CustomButton(
                      text: 'Go to Home'.tr,
                      onClick: () => Get.offAllNamed(Routes.MAIN),
                    ),
                    SizedBox(
                      height: 10.h,
                    ),
                  ])
                : Center(child: CircularProgressIndicator()),
          ),
        ),
      ),
    );
  }
}
