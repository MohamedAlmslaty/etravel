import 'package:e_travel/app/modules/widgets/custom_text.dart';
import 'package:e_travel/app/routes/app_pages.dart';
import 'package:e_travel/app/util/consts/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

import '../controllers/permission_controller.dart';
import 'package:permission_handler/permission_handler.dart';

class PermissionView extends GetView<PermissionController> {
  const PermissionView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: whiteColor,
      appBar: AppBar(
        title: Text('Permissions Explanation'.tr),
      ),
      body: SingleChildScrollView(
        child: Padding(
        padding: EdgeInsets.all(20),
        child:  Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            CustomText(
              text:'To provide the best experience, this app requires the following permissions:'.tr,
            fontSize: 20.sp,
              color: blackColor,
              fontWeight: FontWeight.bold,
              maxLines: 5,
            ),
            SizedBox(height: 16),
            PermissionCard(
              icon: Icons.contacts,
              permission: Permission.contacts,
              text: 'Access Contacts'.tr,
              desc: 'Contact description'.tr,
            ),
            PermissionCard(
              icon: Icons.camera_alt,
              permission: Permission.camera,
              text: 'Access Camera'.tr,
              desc: 'Camera description'.tr,
            ),
            PermissionCard(
              icon: Icons.photo_library,
              permission: Permission.photos,
              text: 'Access Photo Library'.tr,
              desc: 'Photo description'.tr,
            ),
            ElevatedButton(
              onPressed: () {
                _requestPermissions(context);
              },
              child: Text('Grant Permissions'.tr),
            ),
          ],
        ),
        ),
        ),
    );
  }

  Future<void> _requestPermissions(BuildContext context) async {
    Map<Permission, PermissionStatus> statuses = await [
      Permission.contacts,
      Permission.camera,
      Permission.photos,

    ].request();

    bool allGranted = statuses.values.every((status) => status.isGranted);
    Get.find<GetStorage>().write('PERMISSION', allGranted);
    Get.toNamed(Routes.SPLASH);
  }
}

class PermissionCard extends StatelessWidget {
  final IconData icon;
  final Permission permission;
  final String text;
  final String desc;

  PermissionCard({
    required this.icon,
    required this.permission,
    required this.text,
    required this.desc,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Icon(icon),
            SizedBox(width: 8),
            CustomText(
              text:text,
              fontSize: 16.sp,
              color: blackColor,
              fontWeight: FontWeight.w500,
            ),
      ],
    ),
        CustomText(
          text:desc,
          fontSize: 14.sp,
          color: blackColor,
          maxLines: 5,
        ),
      ],
    );
  }
}
