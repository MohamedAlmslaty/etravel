import 'package:e_travel/app/modules/booking/controllers/booking_controller.dart';
import 'package:e_travel/app/modules/booking/views/widgets/row_booking_widget.dart';
import 'package:e_travel/app/modules/widgets/custom_text.dart';
import 'package:e_travel/app/modules/widgets/custom_widget.dart';
import 'package:e_travel/app/modules/widgets/permission_denied.dart';
import 'package:e_travel/app/routes/app_pages.dart';
import 'package:e_travel/app/util/consts/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:intl/intl.dart';

class BookingView extends GetView<BookingController> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<BookingController>(
        init: BookingController(),
        initState: (_) {},
        builder: (_con) =>
            Scaffold(
              body: CustomWidget(
                  title: 'myBooking'.tr,
                  image: 'assets/icons/booking.svg',
                  main: true,
                  child: Padding(
                      padding:
                      EdgeInsets.only(top: 20.h, right: 20.w, left: 20.w),
                      child: Get.find<GetStorage>().hasData('token')
                          ? Column(
                        children: [
                          Container(
                            height: Get.height / 1.4,
                            padding: EdgeInsets.symmetric(
                                horizontal: 30.w, vertical: 10.h),
                            decoration: BoxDecoration(
                              color: whiteColor,
                              borderRadius:
                              BorderRadius.all(Radius.circular(20.r)),
                            ),
                            child: Column(
                              crossAxisAlignment:
                              CrossAxisAlignment.start,
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                SizedBox(
                                  height: 8,
                                ),
                                CustomText(
                                  text: 'your travels'.tr,
                                  fontSize: 18.sp,
                                  fontWeight: FontWeight.bold,
                                  color: primaryColor,
                                ),
                                SizedBox(
                                  height: 16,
                                ),
                                Expanded(
                                  child: Obx(
                                        () =>
                                    _con.loading.value
                                        ? _con.flightBooking.isEmpty
                                        ? Center(
                                        child: CustomText(
                                          text: 'no flights'.tr,
                                          maxLines: 3,
                                          fontSize: 18.sp,
                                          textAlign:
                                          TextAlign.center,
                                          color: blackColor,
                                        ))
                                        : ListView.separated(
                                        padding: EdgeInsets.zero,
                                        itemBuilder: (BuildContext
                                        context,
                                            int index) =>
                                            RowBookingWidget(
                                              itineraries: _con
                                                  .flightBooking[
                                              index].itineraries!,
                                              oneWay: _con
                                                  .flightBooking[
                                              index]
                                                  .oneWay!,
                                              isMultiCity: _con
                                                  .flightBooking[
                                              index]
                                                  .flightType.toString()=='MULTI_CITY',
                                              pnr: _con
                                                  .flightBooking[
                                              index]
                                                  .pnr!,
                                              passengerName: _con
                                                  .flightBooking[
                                              index]
                                                  .passengerFirstName! + ' ' +
                                                  _con
                                                      .flightBooking[
                                                  index]
                                                      .passengerLastName!,
                                              back: _con
                                                  .flightBooking[
                                              index]
                                                  .itineraries![0]
                                                  .departureAirport!
                                                  .cityName
                                                  .toString(),
                                              go: _con
                                                  .flightBooking[
                                              index]
                                                  .itineraries![0]
                                                  .arrivalAirport!
                                                  .cityName
                                                  .toString(),
                                              image: _con
                                                  .flightBooking[
                                              index]
                                                  .itineraries![0]
                                                  .airline!
                                                  .imageUrl,
                                              text:
                                              '${!_con.flightBooking[index]
                                                  .oneWay! ? DateFormat(
                                                  'yyyy-MM-dd')
                                                  .format(
                                                  _con.flightBooking[index]
                                                      .itineraries![0]
                                                      .arrivalDateTime!)
                                                  .toString() + ' - ' +
                                                  DateFormat('yyyy-MM-dd')
                                                      .format(
                                                      _con.flightBooking[index]
                                                          .itineraries![1]
                                                          .departureDateTime!)
                                                      .toString() : DateFormat(
                                                  'yyyy-MM-dd')
                                                  .format(
                                                  _con.flightBooking[index]
                                                      .itineraries![0]
                                                      .departureDateTime!)
                                                  .toString()}',
                                              onClick: () =>
                                                  Get.toNamed(
                                                      Routes
                                                          .FLIGHT_DETAILS,
                                                      arguments:
                                                      _con.flightBooking[
                                                      index]),
                                            ),
                                        separatorBuilder:
                                            (BuildContext context,
                                            int index) =>
                                            Divider(),
                                        itemCount: _con
                                            .flightBooking.length)
                                        : Center(
                                        child:
                                        CircularProgressIndicator()),
                                  ),
                                )
                              ],
                            ),
                          ),
                        ],
                      )
                          : PermissionDenied())),
            ));
  }
}
