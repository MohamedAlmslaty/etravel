import 'package:cached_network_image/cached_network_image.dart';
import 'package:e_travel/app/data/models/flight_booking.dart';
import 'package:e_travel/app/data/models/itineraries.dart';
import 'package:e_travel/app/modules/booking/controllers/flight_details_controller.dart';
import 'package:e_travel/app/modules/booking/views/widgets/pdf_view.dart';
import 'package:e_travel/app/modules/widgets/custom_text.dart';
import 'package:e_travel/app/modules/widgets/custom_widget.dart';
import 'package:e_travel/app/routes/app_pages.dart';
import 'package:e_travel/app/util/consts/base.dart';
import 'package:e_travel/app/util/consts/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:intl/intl.dart';
import 'package:share_plus/share_plus.dart';

class FlightDetailsView extends GetView<FlightDetailsController> {
  String fileUrl = baseURL + "public/flight-bookings/invoice/";
  bool local = Get.find<GetStorage>().read('language') == 'ar';

  @override
  Widget build(BuildContext context) {
    return GetBuilder<FlightDetailsController>(
        init: FlightDetailsController(),
        builder: (_con) => Scaffold(
            body: CustomWidget(
                title: 'flight details'.tr,
                icon: true,
                onDownload: () => Get.to(PDFView(
                      pdfUrl: baseURL +
                          'public/flight-bookings/invoice/' +
                          _con.flightBooking.value.id.toString(),
                    )),
                onShare: () {
                  Share.share('Download Ticket From ' +
                      baseURL +
                      'public/flight-bookings/share-invoice/' +
                      ((_con.flightBooking.value.id ?? 0) + 978231421781)
                          .toString());
                },
                initialBooking:
                    _con.flightBooking.value.bookingType! == 'INITIAL_BOOKING',
                cancelText: _con.flightBooking.value.bookingType! == 'ONEWAY_USED' &&
                    !_con.flightBooking.value.oneWay!?'return cancel'.tr:'Booking cancel'.tr,
                showCancel: _con.flightBooking.value.bookingType! ==
                        'FINAL_BOOKING' ||
                    (_con.flightBooking.value.bookingType! == 'ONEWAY_USED' &&
                        !_con.flightBooking.value.oneWay!),
                showEdit: _con.flightBooking.value.bookingType! ==
                        'FINAL_BOOKING' &&
                    _con.flightBooking.value.gdsType == 'VIDECOM' ||
                    (_con.flightBooking.value.bookingType! == 'ONEWAY_USED' &&
                        _con.flightBooking.value.flightType=='NORMAL_RETURN')&&
                        _con.flightBooking.value.gdsType == 'VIDECOM',
                onEdit: () => Get.toNamed(Routes.EDIT_BOOKING),
                onCancel: () {
                  if (_con.flightBooking.value.bookingType! !=
                          'CANCELLED_FULL_REFUND' &&
                      _con.flightBooking.value.bookingType! !=
                          'CANCELLED_PENALTY_REFUND' &&
                      _con.flightBooking.value.bookingType! !=
                          'INITIAL_BOOKING' &&
                      _con.flightBooking.value.bookingType! != 'EXPIRED')
                    _con.getFees(_con.flightBooking.value.id!,_con.flightBooking.value.pnr!,_con.flightBooking.value.itineraries![0].airline!.requireDocumentsForRefund!);
                  else if (_con.flightBooking.value.bookingType! ==
                      'INITIAL_BOOKING')
                    _con.changeToFinalBooking(_con.flightBooking.value.id!);
                },
                dividePnr: () =>
                    _con.dividePnrTicket(_con.flightBooking.value.id!),
                showDividePnr:
                    _con.flightBooking.value.bookingType! == 'FINAL_BOOKING' &&
                        _con.flightBooking.value.paxNo! > 1,
                child: Container(
                  padding:
                      EdgeInsets.symmetric(horizontal: 30.w, vertical: 10.h),
                  decoration: BoxDecoration(
                    color: whiteColor,
                    borderRadius: BorderRadius.all(Radius.circular(20.r)),
                  ),
                  child: Column(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Obx(
                            () => Container(

                              padding: EdgeInsets.symmetric(
                                  horizontal: 10, vertical: 2),
                              decoration: BoxDecoration(
                                  color: checkBookingTypeColor(
                                      bookingType: _con
                                          .flightBooking.value.bookingType!),
                                  borderRadius: BorderRadius.circular(24)),
                              child: CustomText(
                                text: checkBookingTypeText(
                                    bookingType:
                                        _con.flightBooking.value.bookingType!),
                                textAlign: TextAlign.center,
                              ),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 5.h,
                      ),
                      ticketDetailsWidget(
                          'passengerName'.tr,
                          '${_con.flightBooking.value.passengerFirstName} ${_con.flightBooking.value.passengerLastName}',
                          'ticketNo'.tr,
                          '${_con.flightBooking.value.eTicketNo}'),
                      SizedBox(
                        height: 10.h,
                      ),
                      (_con
                                      .flightBooking.value.bookingType! ==
                                  'INITIAL_BOOKING' ||
                              _con
                                      .flightBooking.value.bookingType! ==
                                  'EXPIRED')
                          ? ticketDetailsWidget(
                              'Reservation ends on'.tr,
                              '${DateFormat('yyyy-MM-dd HH:mm').format(_con.flightBooking.value.initialBookingExpiration!)}'
                                  .tr,
                              'pnr'.tr,
                              '${_con.flightBooking.value.pnr}')
                          : ticketDetailsWidget(
                              'Ticket type'.tr,
                              '${_con.flightBooking.value.itineraries![0].cabinClass}'
                                  .tr,
                              'pnr'.tr,
                              '${_con.flightBooking.value.pnr}'),
                      SizedBox(
                        height: 10.h,
                      ),
                      ticketDetailsWidget(
                          'Number of people associated with the reservation'.tr,
                          '${_con.flightBooking.value.paxNo}',
                          'Total'.tr,
                          '${_con.flightBooking.value.total} ${'LY'.tr}'),
                      SizedBox(
                        height: 10.h,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          CustomText(
                            text: _con.flightBooking.value.itineraries![0]
                                .airline!.nameAr
                                .toString(),
                            color: primaryColor,
                            fontSize: 16.sp,
                            fontWeight: FontWeight.bold,
                            textAlign: TextAlign.right,
                          ),
                          CustomText(
                            text: _con.flightBooking.value.itineraries![0]
                                .airline!.nameEn
                                .toString(),
                            color: primaryColor,
                            fontSize: 16.sp,
                            fontWeight: FontWeight.bold,
                            textAlign: TextAlign.left,
                          ),
                        ],
                      ),
                      Expanded(
                        child: ListView.separated(
                          padding: EdgeInsets.only(top: 0),
                    separatorBuilder: (BuildContext context, int index) => SizedBox(height: 0.w),
                    scrollDirection: Axis.vertical,
                    itemCount: _con.flightBooking.value.itineraries!.length,
                    itemBuilder: (BuildContext context, int index) => departureWidget(
                      _con.flightBooking.value.itineraries![index],
                      mapStringToFlightType(
                        _con.flightBooking.value.flightType ?? 'NORMAL_RETURN',
                        _con.flightBooking.value.itineraries![index].segment!,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget ticketDetailsWidget(String firstTitle, String firstDesc,
      String secondTitle, String secondDesc) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(left: 25.w, right: 25.w),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              CustomText(
                text: firstTitle,
                fontWeight: FontWeight.bold,
                color: primaryColor.withOpacity(0.5),
              ),
              Padding(
                padding: EdgeInsets.only(top: 4.0.w),
                child: CustomText(
                    text: firstDesc,
                    fontWeight: FontWeight.bold,
                    fontSize: 14.sp,
                    color: primaryColor),
              )
            ],
          ),
        ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            CustomText(
                text: secondTitle,
                fontWeight: FontWeight.bold,
                color: primaryColor.withOpacity(0.5)),
            Padding(
              padding: EdgeInsets.only(top: 4.0.h),
              child: CustomText(
                  text: secondDesc,
                  fontWeight: FontWeight.bold,
                  fontSize: 14.sp,
                  color: primaryColor),
            )
          ],
        ),
      ],
    );
  }

  departureWidget(Itineraries itineraries, text) {
    return Column(
      children: [
        Row(
          children: [
            CachedNetworkImage(
              imageUrl: imageUrl(itineraries.airline!.imageUrl),
              imageBuilder: (context, imageProvider) => Container(
                height: 50.h,
                width: 50.h,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(6.0),
                  image: DecorationImage(
                    image: imageProvider,
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              errorWidget: (context, url, error) => Icon(Icons.error),
            ),
            SizedBox(
              width: 10.w,
            ),
            CustomText(
              text: text + " - " + itineraries.flightNo + " ",
              color: blackColor,
              fontSize: 14.sp,
              fontWeight: FontWeight.bold,
            ),
            Spacer(),
            CustomText(
              text: DateFormat('MMM , dd', Get.locale.toString())
                  .format(itineraries.departureDateTime!)
                  .toString(),
              color: textColor,
              fontSize: 14.sp,
              fontWeight: FontWeight.bold,
            ),
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            CustomText(
              text: DateFormat('HH:mm')
                  .format(itineraries.departureDateTime!)
                  .toString(),
              color: blackColor,
              fontSize: 14.sp,
              fontWeight: FontWeight.bold,
            ),
            Column(
              children: [
                CustomText(
                  text: 'ticketClass'.tr + "  " + itineraries.ticketClass!,
                  fontSize: 14.sp,
                  fontWeight: FontWeight.bold,
                  color: blackColor,
                ),
                SvgPicture.asset(
                 'assets/icons/line.svg',
                  width: Get.width / 2,
                ),
                CustomText(
                  text: itineraries.duration!,
                  fontSize: 14.sp,
                  fontWeight: FontWeight.bold,
                ),
              ],
            ),
            CustomText(
              text: DateFormat('HH:mm')
                  .format(itineraries.arrivalDateTime!)
                  .toString(),
              color: blackColor,
              fontSize: 14.sp,
              fontWeight: FontWeight.bold,
            ),
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            CustomText(
              text: " (" + itineraries.departureAirport!.iataCode.toString() + ")",
              color: textColor,
              fontSize: 12.sp,
              fontWeight: FontWeight.bold,
            ),
            CustomText(
              text: " (" + itineraries.arrivalAirport!.iataCode.toString() + ")",
              color: textColor,
              fontSize: 12.sp,
              fontWeight: FontWeight.bold,
            ),
          ],
        ),
        Divider(),
      ],
    );
  }

  checkBookingTypeText({required String bookingType}) {
    if (bookingType == 'FINAL_BOOKING') {
      return 'paid'.tr;
    } else
    if (bookingType == 'ONEWAY_USED') {
      return 'ONEWAY_USED'.tr;
    } else if (bookingType == 'TWO_WAY_USED') {
      return 'TWO_WAY_USED'.tr;
    } else if (bookingType == 'EXPIRED') {
      return 'Expired'.tr;
    } else if (bookingType == 'CANCELLED_FULL_REFUND') {
      return 'Booking canceled'.tr;
    } else if (bookingType == 'CANCELLED_PENALTY_REFUND') {
      return 'Booking canceled'.tr;
    } else if (bookingType == 'INITIAL_BOOKING') {
      return 'Pre-booking'.tr;
    }
  }

  checkBookingTypeColor({required String bookingType}) {
    if (bookingType == 'FINAL_BOOKING') {
      return Colors.green;
    } else  if (bookingType == 'ONEWAY_USED') {
      return primaryColor;
    } else if (bookingType == 'TWO_WAY_USED') {
      return primaryColor;
    } else if (bookingType == 'EXPIRED') {
      return Colors.grey;
    } else if (bookingType == 'CANCELLED_FULL_REFUND') {
      return Colors.red;
    } else if (bookingType == 'CANCELLED_PENALTY_REFUND') {
      return Colors.red;
    } else if (bookingType == 'INITIAL_BOOKING') {
      return Colors.orange;
    }
  }
}
