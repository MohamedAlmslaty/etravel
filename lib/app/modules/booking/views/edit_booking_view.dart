import 'package:e_travel/app/modules/booking/controllers/flight_details_controller.dart';
import 'package:e_travel/app/modules/widgets/custom_button.dart';
import 'package:e_travel/app/modules/widgets/custom_text.dart';
import 'package:e_travel/app/modules/widgets/custom_widget.dart';
import 'package:e_travel/app/util/consts/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:intl/intl.dart';

class EditBookingView extends GetView<FlightDetailsController> {
  bool local = Get.find<GetStorage>().read('language') == 'ar';

  @override
  Widget build(BuildContext context) {
    return GetBuilder<FlightDetailsController>(
        init: FlightDetailsController(),
        builder: (_con) => Scaffold(
            body: CustomWidget(
                title: 'Booking edit'.tr,
                child: Container(
                  padding:
                      EdgeInsets.symmetric(horizontal: 30.w, vertical: 10.h),
                  decoration: BoxDecoration(
                    color: whiteColor,
                    borderRadius: BorderRadius.all(Radius.circular(20.r)),
                  ),
                  child: Column(
                    children: [
                      SizedBox(
                        height: 20.h,
                      ),
                      CustomText(
                        text: 'Choose the flight you want to modify:'.tr,
                        color: primaryColor,
                        fontSize: 18.sp,
                        fontWeight: FontWeight.bold,
                      ),
                      SizedBox(
                        height: 20.h,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          CustomText(
                            text: _con.flightBooking.value.itineraries![0]
                                .airline!.nameAr
                                .toString(),
                            color: primaryColor,
                            fontSize: 16.sp,
                            fontWeight: FontWeight.bold,
                            textAlign: TextAlign.right,
                          ),
                          CustomText(
                            text: _con.flightBooking.value.itineraries![0]
                                .airline!.nameEn
                                .toString(),
                            color: primaryColor,
                            fontSize: 16.sp,
                            fontWeight: FontWeight.bold,
                            textAlign: TextAlign.left,
                          ),
                        ],
                      ),
                      _con.flightBooking.value.bookingType! == 'ONEWAY_USED' &&
                          !_con.flightBooking.value.oneWay!
                      ?SizedBox():departureWidget(_con, 'departure'.tr),
                      Divider(),
                      _con.flightBooking.value.oneWay!
                          ? SizedBox()
                          : returnWidget(_con, 'return'.tr),
                      SizedBox(
                        height: 20,
                      ),
                      CustomButton(
                          text: 'choose new date'.tr,
                          onClick: () => _con.goToChooseDate())
                    ],
                  ),
                ))));
  }

  departureWidget(FlightDetailsController _con, text) {
    return Column(
      children: [
        Row(
          children: [
            Obx(
              () => Checkbox(
                checkColor: secondaryColor,
                activeColor: primaryColor,
                value: _con.departureActive.value,
                onChanged: (bool? value) {
                  _con.departureActive.value = value!;
                  _con.returnActive.value = !value;
                },
              ),
            ),
            SizedBox(
              width: 10.w,
            ),
            CustomText(
              text: text +
                  " - " +
                  _con.flightBooking.value.itineraries![0].flightNo +
                  " ",
              color: blackColor,
              fontSize: 14.sp,
              fontWeight: FontWeight.bold,
            ),
            Spacer(),
            CustomText(
              text: DateFormat('MMM , dd', Get.locale.toString())
                  .format(_con
                      .flightBooking.value.itineraries![0].departureDateTime!)
                  .toString(),
              color: textColor,
              fontSize: 14.sp,
              fontWeight: FontWeight.bold,
            ),
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            CustomText(
              text: DateFormat('HH:mm')
                  .format(_con
                      .flightBooking.value.itineraries![0].departureDateTime!)
                  .toString(),
              color: blackColor,
              fontSize: 14.sp,
              fontWeight: FontWeight.bold,
            ),
            Column(
              children: [
                CustomText(
                  text: 'ticketClass'.tr +
                      "  " +
                      _con.flightBooking.value.itineraries![0].ticketClass!,
                  color: blackColor,
                  fontSize: 14.sp,
                  fontWeight: FontWeight.bold,
                ),
                SvgPicture.asset(
                  'assets/icons/line.svg',
                  width: Get.width / 2,
                ),
                CustomText(
                  text: _con.flightBooking.value.itineraries![0].duration!,
                  color: blackColor,
                  fontSize: 14.sp,
                  fontWeight: FontWeight.bold,
                ),
              ],
            ),
            CustomText(
              text: DateFormat('HH:mm')
                  .format(
                      _con.flightBooking.value.itineraries![0].arrivalDateTime!)
                  .toString(),
              color: blackColor,
              fontSize: 14.sp,
              fontWeight: FontWeight.bold,
            ),
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            CustomText(
              text: local
                  ?
                      " (" +
                      _con.flightBooking.value.itineraries![0].departureAirport!
                          .iataCode.toString() +
                      ")"
                  :
                      " (" +
                      _con.flightBooking.value.itineraries![0].departureAirport!
                          .iataCode.toString() +
                      ")",
              color: textColor,
              fontSize: 12.sp,
              fontWeight: FontWeight.bold,
            ),
            CustomText(
              text: local
                  ?
                      " (" +
                      _con.flightBooking.value.itineraries![0].arrivalAirport!
                          .iataCode.toString() +
                      ")"
                  :
                      " (" +
                      _con.flightBooking.value.itineraries![0].arrivalAirport!
                          .iataCode.toString() +
                      ")",
              color: textColor,
              fontSize: 12.sp,
              fontWeight: FontWeight.bold,
            ),
          ],
        ),
      ],
    );
  }

  returnWidget(FlightDetailsController _con, text) {
    return Column(
      children: [
        Row(
          children: [
            Obx(
              () => Checkbox(
                checkColor: secondaryColor,
                activeColor: primaryColor,
                value: _con.returnActive.value,
                onChanged: (bool? value) {
                  _con.returnActive.value = value!;
                  _con.departureActive.value = !value;
                },
              ),
            ),
            SizedBox(
              width: 10.w,
            ),
            CustomText(
              text: text +
                  " - " +
                  _con.flightBooking.value.itineraries![1].flightNo +
                  "  ",
              color: blackColor,
              fontSize: 14.sp,
              fontWeight: FontWeight.bold,
            ),
            Spacer(),
            CustomText(
              text: DateFormat('MMM , dd', Get.locale.toString())
                  .format(_con
                      .flightBooking.value.itineraries![1].departureDateTime!)
                  .toString(),
              color: textColor,
              fontSize: 14.sp,
              fontWeight: FontWeight.bold,
            ),
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            CustomText(
              text: DateFormat('HH:mm')
                  .format(_con
                      .flightBooking.value.itineraries![1].departureDateTime!)
                  .toString(),
              color: blackColor,
              fontSize: 14.sp,
              fontWeight: FontWeight.bold,
            ),
            Column(
              children: [
                CustomText(
                  text: 'ticketClass'.tr +
                      "  " +
                      _con.flightBooking.value.itineraries![1].ticketClass!,
                  color: blackColor,
                  fontSize: 14.sp,
                  fontWeight: FontWeight.bold,
                ),
                SvgPicture.asset(
                  'assets/icons/line.svg',
                  width: Get.width / 2,
                ),
                CustomText(
                  text: _con.flightBooking.value.itineraries![1].duration!,
                  color: blackColor,
                  fontSize: 14.sp,
                  fontWeight: FontWeight.bold,
                ),
              ],
            ),
            CustomText(
              text: DateFormat('HH:mm')
                  .format(
                      _con.flightBooking.value.itineraries![1].arrivalDateTime!)
                  .toString(),
              color: blackColor,
              fontSize: 14.sp,
              fontWeight: FontWeight.bold,
            ),
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            CustomText(
              text: local
                  ?
                      " (" +
                      _con.flightBooking.value.itineraries![1].departureAirport!
                          .iataCode.toString() +
                      ")"
                  :
                      " (" +
                      _con.flightBooking.value.itineraries![1].departureAirport!
                          .iataCode.toString() +
                      ")",
              color: textColor,
              fontSize: 12.sp,
              fontWeight: FontWeight.bold,
            ),
            CustomText(
              text: local
                  ?
                      " (" +
                      _con.flightBooking.value.itineraries![1].arrivalAirport!
                          .iataCode.toString() +
                      ")"
                  :
                      " (" +
                      _con.flightBooking.value.itineraries![1].arrivalAirport!
                          .iataCode.toString() +
                      ")",
              color: textColor,
              fontSize: 12.sp,
              fontWeight: FontWeight.bold,
            ),
          ],
        ),
      ],
    );
  }
}
