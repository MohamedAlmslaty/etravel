import 'package:cached_network_image/cached_network_image.dart';
import 'package:intl/intl.dart';
import 'package:e_travel/app/data/models/itineraries.dart';
import 'package:e_travel/app/util/consts/base.dart';
import 'package:e_travel/app/util/consts/colors.dart';
import 'package:e_travel/app/modules/widgets/custom_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

class RowBookingWidget extends StatelessWidget {
  RowBookingWidget(
      {required this.go,
      required this.back,
      required this.pnr,
      required this.passengerName,
      required this.oneWay,
      required this.isMultiCity,
      required this.image,
      required this.itineraries,
      required this.text,
      required this.onClick});

  String go, back, image, text, pnr, passengerName;
  bool oneWay, isMultiCity;
  List<Itineraries> itineraries;

  Function onClick;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => onClick(),
      child: Row(
        children: [
          SizedBox(
            width: 11.w,
          ),
          CachedNetworkImage(
            imageUrl: imageUrl(image),
            imageBuilder: (context, imageProvider) => Container(
              height: 50.h,
              width: 50.h,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(6.0),
                image: DecorationImage(
                  image: imageProvider,
                  fit: BoxFit.cover,
                ),
              ),
            ),
            errorWidget: (context, url, error) => Icon(Icons.error),
          ),
          SizedBox(
            width: 23.6.w,
          ),
          isMultiCity
              ? Column(
                  children: [
                    CustomText(
                        text: 'pnr'.tr + ' : $pnr',
                        fontWeight: FontWeight.bold,
                        color: primaryColor,
                        fontSize: 16.sp),
                    SizedBox(
                      width: 190.w,
                      child: ListView.separated(
                        padding: EdgeInsets.zero,
                        physics: NeverScrollableScrollPhysics(),
                        shrinkWrap: true,
                        separatorBuilder: (context, index) => SizedBox(height: 5,),
                        itemCount: itineraries.length,
                        itemBuilder: (context, index) {
                          return Row(mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                            CustomText(
                                text: '${itineraries[index].departureAirport!.cityName} ',
                                fontWeight: FontWeight.bold,
                                fontSize: 12,
                                color: primaryColor),
                              SizedBox(width: 3.w,),  Icon(
                                Icons.arrow_forward,
                                size: 12,
                              ), SizedBox(width: 3.w,),
                              CustomText(
                                  text: ' ${itineraries[index].arrivalAirport!.cityName}',
                                  fontWeight: FontWeight.bold,
                                  fontSize: 12,
                                  color: primaryColor),
                            SizedBox(width: 10.w,),
                            CustomText(
                            fontWeight: FontWeight.bold,
                            fontSize: 12,
                          color: primaryColor,
                              text: DateFormat('yy-MM-dd')
                                      .format(DateTime.tryParse(itineraries[index].departureDateTime.toString())!)
                                      .toString(),
                            ),
                          ],);

                        },
                      ),
                    ),
                    CustomText(
                        text: passengerName,
                        color: primaryColor,
                        fontSize: 12.sp),
                  ],
                )
              : oneWay
                  ? Column(
                      children: [
                        CustomText(
                            text: 'pnr'.tr + ' : $pnr',
                            fontWeight: FontWeight.bold,
                            color: primaryColor,
                            fontSize: 16.sp),
                        Row(
                          children: [
                            CustomText(
                                text: back,
                                fontWeight: FontWeight.bold,
                                color: primaryColor,
                                fontSize: 16.sp),
                            SizedBox(
                              width: 7.w,
                            ),
                            Icon(
                              Icons.arrow_forward,
                              size: 16,
                            ),
                            SizedBox(
                              width: 7.w,
                            ),
                            CustomText(
                                text: go,
                                fontWeight: FontWeight.bold,
                                color: primaryColor,
                                fontSize: 16.sp),
                          ],
                        ),
                        CustomText(
                            text: text, color: primaryColor, fontSize: 12.sp),
                        CustomText(
                            text: passengerName,
                            color: primaryColor,
                            fontSize: 12.sp),
                      ],
                    )
                  : Column(
                      children: [
                        CustomText(
                            text: 'pnr'.tr + ' : $pnr',
                            fontWeight: FontWeight.bold,
                            color: primaryColor,
                            fontSize: 16.sp),
                        Row(
                          children: [
                            CustomText(
                                text: back,
                                fontWeight: FontWeight.bold,
                                color: primaryColor,
                                fontSize: 14.sp),
                            SizedBox(
                              width: 5.w,
                            ),
                            Icon(
                              Icons.arrow_forward,
                              size: 12,
                            ),
                            SizedBox(
                              width: 5.w,
                            ),
                            CustomText(
                                text: go,
                                fontWeight: FontWeight.bold,
                                color: primaryColor,
                                fontSize: 14.sp),
                            SizedBox(
                              width: 5.w,
                            ),
                            Icon(
                              Icons.arrow_forward,
                              size: 12,
                            ),
                            SizedBox(
                              width: 5.w,
                            ),
                            CustomText(
                                text: back,
                                fontWeight: FontWeight.bold,
                                color: primaryColor,
                                fontSize: 14.sp),
                          ],
                        ),
                        CustomText(
                            text: text, color: primaryColor, fontSize: 12.sp),
                        CustomText(
                            text: passengerName,
                            color: primaryColor,
                            fontSize: 12.sp),
                      ],
                    ),
        ],
      ),
    );
  }
}
