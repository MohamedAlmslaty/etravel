import 'package:e_travel/app/data/models/cabin_class.dart';
import 'package:e_travel/app/data/models/select_date.dart';
import 'package:e_travel/app/modules/booking/controllers/edit_flight_controller.dart';
import 'package:e_travel/app/modules/widgets/custom_button.dart';
import 'package:e_travel/app/modules/widgets/custom_row.dart';
import 'package:e_travel/app/modules/widgets/custom_text.dart';
import 'package:e_travel/app/modules/widgets/custom_widget.dart';
import 'package:e_travel/app/routes/app_pages.dart';
import 'package:e_travel/app/util/consts/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

class EditDatesView extends GetView<EditFlightController> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<EditFlightController>(
      init: EditFlightController(),
      initState: (_) {},
      builder: (_con) => Scaffold(
        body: CustomWidget(
          title: 'Booking edit'.tr,
          child: Padding(
            padding: EdgeInsets.only(top: 20.h, right: 20.w, left: 20.w),
            child: Column(
              children: [
                SizedBox(
                  height: 20.h,
                ),
                Obx(() => CustomRow(
                    title: 'DepartureDate'.tr,
                    text: _con.departureDate.value,
                    image: 'assets/icons/chooseDate.svg',
                    onClick: () async {
                      SelectDate result =
                          await Get.toNamed(Routes.SELECT_DATE, arguments: 0);
                      if (result.departureDate.value.isNotEmpty) {
                        _con.departureDate.value = result.departureDate.value;
                      }
                    })),
                SizedBox(
                  height: 10.h,
                ),
                Obx(() => CustomRow(
                      title: 'CabinClass'.tr,
                      text: '${_con.enumName.value}'.tr,
                      image: 'assets/icons/cabinClass.svg',
                      onClick: () => CabinClass(_con),
                    )),
                SizedBox(
                  height: 10.h,
                ),
                CustomButton(
                    text: 'SearchFlights'.tr,
                    width: 200.w,
                    onClick: () =>
                        _con.searchFlight(roundTrip: _con.returnActive.value)),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void CabinClass(_con) {
    Get.bottomSheet(
        Container(
          height: Get.height / 2,
          child: Padding(
            padding: const EdgeInsets.all(28.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                    child: CustomText(
                  text: 'CabinClass'.tr,
                  fontSize: 20,
                  color: blackColor,
                )),
                Container(
                  padding: EdgeInsets.all(20),
                  child: Column(
                    children: List.generate(cabin.length, (index) {
                      var _lang = cabin.elementAt(index);
                      return Obx(() => RadioListTile(
                            value: _lang,
                            groupValue: _con.selectCabinClass.value,
                            onChanged: (dynamic value) {
                              _con.selectCabinClass.value = value;
                              _con.Class.value = value.name;
                              _con.enumName.value = value.enumName;
                              Get.back();
                            },
                            title: CustomText(
                              text: _lang.name!.tr,
                              fontWeight: FontWeight.bold,
                              fontSize: 16,
                              color: primaryColor,
                            ),
                          ));
                    }).toList(),
                  ),
                ),
              ],
            ),
          ),
        ),
        elevation: 20.0,
        enableDrag: false,
        backgroundColor: whiteColor,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
          topLeft: Radius.circular(30.0),
          topRight: Radius.circular(30.0),
        )));
  }
}
