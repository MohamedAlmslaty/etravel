import 'package:e_travel/app/data/models/cabin_class.dart' as c;
import 'package:e_travel/app/data/models/flight_booking.dart';
import 'package:e_travel/app/data/models/flight_req.dart';
import 'package:e_travel/app/routes/app_pages.dart';
import 'package:e_travel/app/util/consts/style.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

class EditFlightController extends GetxController {
  Rx<FlightBooking> flightBooking = FlightBooking().obs;
  final departureActive = false.obs;
  final returnActive = false.obs;
  final departureDate = ''.obs;
  final destinationLocationCode = ''.obs;
  final originLocationCode = ''.obs;
  final adult = 1.obs;
  final children = 0.obs;
  final bookingAt = DateTime.now().obs;
  final Rx<c.CabinClass> selectCabinClass = c.CabinClass().obs;
  final Class = ''.obs;
  final enumName = ''.obs;

  ///
  final loading = false.obs;

  @override
  void onInit() {
    super.onInit();
    if (Get.arguments['flightBooking'] != null) {
      flightBooking.value = Get.arguments['flightBooking'];
      departureActive.value = Get.arguments['departureActive'] ?? false;
      returnActive.value = Get.arguments['returnActive'] ?? false;
      if (c.cabin[0].enumName ==
          flightBooking.value.itineraries![0].cabinClass!) {
        selectCabinClass.value = c.cabin[0];
        enumName.value = c.cabin[0].enumName!;
      } else if (c.cabin[1].enumName ==
          flightBooking.value.itineraries![0].cabinClass!) {
        selectCabinClass.value = c.cabin[1];
        enumName.value = c.cabin[1].enumName!;
      } else if (c.cabin[2].enumName ==
          flightBooking.value.itineraries![0].cabinClass!) {
        selectCabinClass.value = c.cabin[2];
        enumName.value = c.cabin[2].enumName!;
      } else {
        selectCabinClass.value = c.cabin[0];
        enumName.value = c.cabin[0].enumName!;
      }
      departureDate.value = DateFormat('yyyy-MM-dd')
          .format(flightBooking.value.itineraries![0].departureDateTime!)
          .toString();
      returnActive.value
          ? departureDate.value = DateFormat('yyyy-MM-dd')
              .format(flightBooking.value.itineraries![1].departureDateTime!)
              .toString()
          : departureDate.value = DateFormat('yyyy-MM-dd')
              .format(flightBooking.value.itineraries![0].departureDateTime!)
              .toString();

      destinationLocationCode.value = flightBooking
          .value.itineraries![0].arrivalAirport!.iataCode
          .toString();
      originLocationCode.value = flightBooking
          .value.itineraries![0].departureAirport!.iataCode
          .toString();
    }
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}

  void searchFlight({required bool roundTrip}) {
    if (departureDate.isNotEmpty) {
      FlightRequest flightRequest = FlightRequest(
          adults: flightBooking.value.paxNo,
          searchForFlightsWithLuggage: false,
          searchForRefundable: true,
          searchForDirect: true,
          isMultiCity: false,
          cabinClass: enumName.value,
          children: children.value,
          departureDate: departureDate.value,
          returnDate: '',
          destinationLocationCode: returnActive.value
              ? originLocationCode.value
              : destinationLocationCode.value,
          originLocationCode: returnActive.value
              ? destinationLocationCode.value
              : originLocationCode.value,
          includedAirlineCodes:
              flightBooking.value.itineraries![0].airline!.iataCode,
          goingOpen: false,
          returnOpen: false);
      Get.toNamed(Routes.FLIGHT, arguments: {
        'flightRequest': flightRequest,
        'Edit': true,
        'isMulti':false,
        'returnFlight': returnActive.value,
        'pnr': flightBooking.value.pnr
      });
    } else
      showSnackBar(title: 'error'.tr, message: 'Please fill in all fields'.tr);
  }
}
