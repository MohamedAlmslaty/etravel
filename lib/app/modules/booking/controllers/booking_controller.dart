import 'package:e_travel/app/data/models/flight_booking.dart';
import 'package:e_travel/app/data/repositories/flight_booking_repository.dart';
import 'package:get/get.dart';

class BookingController extends GetxController {
  final _flightBookingRepo = FlightBookingRepository.instance;
  final loading = false.obs;
  RxList<FlightBooking> flightBooking = <FlightBooking>[].obs;

  @override
  void onInit() {
    super.onInit();
    loadFlightBooking();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}

  Future<void> loadFlightBooking() async {
    final response = await _flightBookingRepo.getFlightBookings();
    flightBooking.assignAll(response);
    loading.value = true;
  }


}
