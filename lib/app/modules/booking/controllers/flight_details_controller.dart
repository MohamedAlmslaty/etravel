import 'dart:convert';
import 'dart:typed_data';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:mime/mime.dart';
import 'package:e_travel/app/data/models/attachment.dart';
import 'package:e_travel/app/data/models/flight_booking.dart';
import 'package:e_travel/app/data/repositories/flight_booking_repository.dart';
import 'package:e_travel/app/data/repositories/attachment_repository.dart';
import 'package:e_travel/app/modules/booking/controllers/booking_controller.dart';
import 'package:e_travel/app/modules/wallet/controllers/wallet_controller.dart';
import 'package:e_travel/app/modules/widgets/custom_button.dart';
import 'package:e_travel/app/modules/widgets/custom_text.dart';
import 'package:e_travel/app/routes/app_pages.dart';
import 'package:e_travel/app/util/consts/colors.dart';
import 'package:e_travel/app/util/consts/loading.dart';
import 'package:e_travel/app/util/consts/style.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:url_launcher/url_launcher.dart';

class FlightDetailsController extends GetxController {
  Rx<FlightBooking> flightBooking = FlightBooking().obs;
  final _flightBookingRepo = FlightBookingRepository.instance;
  final _attachmentRepo = AttachmentRepository.instance;
  final departureActive = false.obs;
  final returnActive = false.obs;
  final numberOfImage = 0.obs;
  List<Attachment> attachments = <Attachment>[];
  Rx<Attachment> attachment = Attachment().obs;
  List<XFile> pickedImages = [];
  final ImagePicker _picker = ImagePicker();

  @override
  void onInit() {
    flightBooking.value = Get.arguments;
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}

  void imgFromGallery() async {
    try {
      final List<XFile?> resultList = await _picker.pickMultiImage(
        imageQuality: 80,
        // Adjust the image quality as needed
      );

      if (resultList.isNotEmpty) {
        numberOfImage.value = resultList.length;
        showSnackBar(
            title: 'successful'.tr,
            message: 'Passport photos have been successfully selected'.tr);
        pickedImages = resultList.cast<XFile>();
        List<Attachment> imagePaths = await Future.wait(
          pickedImages.map((image) async {
            String base64String = base64.encode(await image.readAsBytes());
            return Attachment(
                attachmentType: 'PASSPORT',
                details: image.name + image.mimeType.toString() + image.path,
                file: base64String,
                fileContentType: lookupMimeType(image.path));
          }),
        );

        attachments.addAll(imagePaths);
      } else
        showSnackBar(
            title: 'error'.tr, message: 'No Passport photos were selected'.tr);
    } catch (e) {
      print('Error picking images: $e');
    }
  }

  void dividePnrTicket(int id) {
    Get.defaultDialog(
      title: 'confirmation'.tr,
      backgroundColor: Color(0xffFEF200),
      content: Text('Are you sure to divide pnr you ticket?'.tr),
      cancel: ElevatedButton(
        onPressed: () {
          Get.back(); // Close the dialog when the Cancel button is pressed
        },
        style: ElevatedButton.styleFrom(
          primary: Colors.red,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(20.0), // Custom border radius
          ), // Custom background color for the Cancel button
        ),
        child: CustomText(text:"cancel".tr),
      ),
      confirm: ElevatedButton(
        onPressed: () async {
          loading();
          await _flightBookingRepo.dividePnr(id).then((value) {
            if (value) {
              EasyLoading.dismiss();
              Get.back();
              Get.back();
              Get.find<BookingController>().loadFlightBooking();
              showSnackBar(
                  title: 'successful'.tr,
                  message:
                      'Your ticket pnr has been separated successfully'.tr);
            } else {
              Get.back();
              EasyLoading.dismiss();
            }
          });
        },
        style: ElevatedButton.styleFrom(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20.0), // Custom border radius
            ),
            primary: primaryColor),
        child: CustomText(text:"OK".tr),
      ),
    );
  }

  void cancelFlightBooking(
      int id, total, String pnr, bool requireDocumentsForRefund) {
    if (requireDocumentsForRefund) {
      Get.defaultDialog(
        title: 'confirmation'.tr,
        backgroundColor: Color(0xffFEF200),
        content: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            CustomText(
              text: 'Are you sure to cancel the flight?'.tr,
              textAlign: TextAlign.center,
              color: Colors.black,
            ),
            // CustomText(
            //   text: '$total  ' + 'point'.tr + ' ' + 'will be deducted'.tr,
            //   textAlign: TextAlign.center,
            //   color: Colors.black,
            //   maxLines: 3,
            // ),
          ],
        ),
        cancel: ElevatedButton(
          onPressed: () {
            Get.back(); // Close the dialog when the Cancel button is pressed
          },
          style: ElevatedButton.styleFrom(
            primary: Colors.red,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20.0), // Custom border radius
            ), // Custom background color for the Cancel button
          ),
          child: CustomText(text:"cancel".tr),
        ),
        confirm: ElevatedButton(
          onPressed: () async {
            Get.defaultDialog(
              title: 'Please upload passport photos so you can cancel'.tr,
              backgroundColor: Color(0xffFEF200),
              content: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Obx(() => CustomText(
                        text: 'The number of images chosen is:'.tr +
                            ' ${numberOfImage.value}',
                        color: blackColor,
                        fontWeight: FontWeight.bold,
                      )),
                  SizedBox(height: 10.h,),
                  CustomButton(
                    text: 'uploadPassport'.tr,
                    color: primaryColor,
                    onClick: () => imgFromGallery(),
                  )
                ],
              ),
              cancel: ElevatedButton(
                onPressed: () {
                  Get.back(); // Close the dialog when the Cancel button is pressed
                },
                style: ElevatedButton.styleFrom(
                  primary: Colors.red,
                  shape: RoundedRectangleBorder(
                    borderRadius:
                        BorderRadius.circular(20.0), // Custom border radius
                  ), // Custom background color for the Cancel button
                ),
                child: CustomText(text:"cancel".tr),
              ),
              confirm: ElevatedButton(
                onPressed: () async {
                  if (attachments.isEmpty && attachments.length == 0) {
                    showSnackBar(
                        title: 'error'.tr,
                        message: 'Please select a passport photo!'.tr);
                  } else {
                    loading();
                    await _attachmentRepo
                        .addNewAttachment(attachments, pnr)
                        .then((value) async {
                      if (value) {
                        EasyLoading.dismiss();
                        attachments.clear();
                        Get.back();
                        loading();
                        await _flightBookingRepo
                            .deleteFlightBooking(id)
                            .then((value) {
                          if (value) {
                            EasyLoading.dismiss();
                            Get.back();
                            Get.back();
                            Get.find<BookingController>().loadFlightBooking();
                            Get.put(WalletController()).loadBalance();
                            showSnackBar(
                                title: 'successful'.tr,
                                message:
                                    'flight booking canceled successfully'.tr);
                          } else {
                            Get.back();
                            Get.back();
                            EasyLoading.dismiss();
                            Get.defaultDialog(
                              title: 'error'.tr,
                              backgroundColor: Color(0xffFEF200),
                              content: Column(
                                children: [
                                  Text(
                                      'To cancel please contact customer support!'
                                          .tr),
                                  SizedBox(
                                    height: 5,
                                  ),
                                  InkWell(
                                    onTap: () {
                                      launchUrl(Uri.parse(
                                          "https://fb.com/eTravelApp.ly"));
                                    },
                                    child: new Text(
                                      "https://fb.com/eTravelApp.ly",
                                      style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                    height: 5,
                                  ),
                                  InkWell(
                                    onTap: () {
                                      launchUrl(
                                          Uri.parse("tel://+218-900500000"));
                                    },
                                    child: Directionality(
                                      textDirection: TextDirection.ltr,
                                      child: new Text(
                                        "+218-900500000",
                                        style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              confirm: ElevatedButton(
                                onPressed: () async {
                                  Get.back();
                                  Get.back();
                                },
                                style: ElevatedButton.styleFrom(
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(
                                          20.0), // Custom border radius
                                    ),
                                    primary: primaryColor),
                                child: CustomText(text:"OK".tr),
                              ),
                            );
                          }
                        });
                      }
                    });
                  }
                },
                style: ElevatedButton.styleFrom(
                    shape: RoundedRectangleBorder(
                      borderRadius:
                          BorderRadius.circular(20.0), // Custom border radius
                    ),
                    primary: primaryColor),
                child: Text("OK".tr),
              ),
            );
          },
          style: ElevatedButton.styleFrom(
              shape: RoundedRectangleBorder(
                borderRadius:
                    BorderRadius.circular(20.0), // Custom border radius
              ),
              primary: primaryColor),
          child: Text("OK".tr),
        ),
      );
    } else {
      Get.defaultDialog(
        title: 'confirmation'.tr,
        backgroundColor: Color(0xffFEF200),
        content: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            CustomText(
              text: 'Are you sure to cancel the flight?'.tr,
              textAlign: TextAlign.center,
              color: Colors.black,
            ),
            // CustomText(
            //   text: '$total  ' + 'point'.tr + ' ' + 'will be deducted'.tr,
            //   textAlign: TextAlign.center,
            //   color: Colors.black,
            //   maxLines: 3,
            // ),
          ],
        ),
        cancel: ElevatedButton(
          onPressed: () {
            Get.back(); // Close the dialog when the Cancel button is pressed
          },
          style: ElevatedButton.styleFrom(
            primary: Colors.red,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20.0), // Custom border radius
            ), // Custom background color for the Cancel button
          ),
          child: CustomText(text:"cancel".tr),
        ),
        confirm: ElevatedButton(
          onPressed: () async {
            loading();
            await _flightBookingRepo.deleteFlightBooking(id).then((value) {
              if (value) {
                EasyLoading.dismiss();
                Get.back();
                Get.back();
                Get.find<BookingController>().loadFlightBooking();
                Get.put(WalletController()).loadBalance();
                showSnackBar(
                    title: 'successful'.tr,
                    message: 'flight booking canceled successfully'.tr);
              } else {
                Get.back();
                Get.back();
                EasyLoading.dismiss();
                Get.defaultDialog(
                  title: 'error'.tr,
                  backgroundColor: Color(0xffFEF200),
                  content: Column(
                    children: [
                      Text('To cancel please contact customer support!'.tr),
                      SizedBox(
                        height: 5,
                      ),
                      InkWell(
                        onTap: () {
                          launchUrl(Uri.parse("https://fb.com/eTravelApp.ly"));
                        },
                        child: new Text(
                          "https://fb.com/eTravelApp.ly",
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      InkWell(
                        onTap: () {
                          launchUrl(Uri.parse("tel://+218-900500000"));
                        },
                        child: Directionality(
                          textDirection: TextDirection.ltr,
                          child: new Text(
                            "+218-900500000",
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                  confirm: ElevatedButton(
                    onPressed: () async {
                      Get.back();
                      Get.back();
                    },
                    style: ElevatedButton.styleFrom(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(
                              20.0), // Custom border radius
                        ),
                        primary: primaryColor),
                    child: CustomText(text:"OK".tr),
                  ),
                );
              }
            });
          },
          style: ElevatedButton.styleFrom(
              shape: RoundedRectangleBorder(
                borderRadius:
                    BorderRadius.circular(20.0), // Custom border radius
              ),
              primary: primaryColor),
          child: Text("OK".tr),
        ),
      );
    }
  }

  Future<void> getFees(
      int id, String pnr, bool requireDocumentsForRefund) async {
    loading();
    await _flightBookingRepo.getCancelFee(id).then((value) {
      if (value.isNotEmpty) {
        double total=0;
        value.forEach((element) { total+=element.total!;});
        EasyLoading.dismiss();
        cancelFlightBooking(id, total, pnr, requireDocumentsForRefund);
      }
    });
  }

  void changeToFinalBooking(int id) {
    Get.defaultDialog(
      title: 'confirmation'.tr,
      backgroundColor: Color(0xffFEF200),
      content: Text(
          'Are you sure you want to change a reservation to permanent?'.tr),
      cancel: ElevatedButton(
        onPressed: () {
          Get.back(); // Close the dialog when the Cancel button is pressed
        },
        style: ElevatedButton.styleFrom(
          primary: Colors.red,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(20.0), // Custom border radius
          ), // Custom background color for the Cancel button
        ),
        child: Text("cancel".tr),
      ),
      confirm: ElevatedButton(
        onPressed: () async {
          loading();
          await _flightBookingRepo.changeTOFinalBooking(id).then((value) {
            if (value) {
              EasyLoading.dismiss();
              Get.back();
              Get.back();
              Get.find<BookingController>().loadFlightBooking();
              Get.put(WalletController()).loadBalance();
              showSnackBar(
                  title: 'successful'.tr,
                  message: 'Reservation changed successfully'.tr);
            } else {
              Get.back();
              Get.back();
              EasyLoading.dismiss();
              showSnackBar(
                  title: 'error'.tr, message: 'Internal server error'.tr);
            }
          });
        },
        style: ElevatedButton.styleFrom(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20.0), // Custom border radius
            ),
            primary: primaryColor),
        child: CustomText(text:"OK".tr),
      ),
    );
  }

  void goToChooseDate() {
    if (departureActive != false || returnActive != false) {
      Get.toNamed(Routes.EDIT_DATES, arguments: {
        'flightBooking': flightBooking.value,
        'departureActive': departureActive.value,
        'returnActive': returnActive.value,
      });
    } else {
      showSnackBar(
          title: 'warning'.tr,
          message: 'Please select the flight you want to change'.tr);
    }
  }
}
