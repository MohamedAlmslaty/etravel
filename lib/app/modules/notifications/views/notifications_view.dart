import 'package:e_travel/app/util/consts/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:e_travel/app/modules/widgets/custom_widget.dart';
import '../controllers/notifications_controller.dart';

class NotificationsView extends GetView<NotificationsController> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<NotificationsController>(
      init: NotificationsController(),
      initState: (_) {},
      builder: (_) {
        return Scaffold(
            body: SingleChildScrollView(
                child: CustomWidget(
                    title: 'Notifications'.tr,
                    height: Get.height / 1.2,
                    child: _.loading.value
                        ? RefreshIndicator(
                            onRefresh: () async {
                              await _.getNotifications();
                            },
                            child: Padding(
                                padding: EdgeInsets.symmetric(horizontal: 30.h),
                                child: ListView.separated(
                                  padding: EdgeInsets.symmetric(vertical: 20.h),
                                  separatorBuilder:
                                      (BuildContext context, int index) =>
                                          SizedBox(
                                    height: 10.h,
                                  ),
                                  itemCount: _.notifications.length,
                                  itemBuilder:
                                      (BuildContext context, int index) => Card(
                                    elevation: 0.5,
                                    clipBehavior: Clip.antiAlias,
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(8),
                                    ),
                                    child: ListTile(
                                      leading: Icon(
                                        Icons.notifications,
                                        color: primaryColor,
                                      ),
                                      title: Text(
                                        _.notifications[index].title,
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold),
                                      ),
                                      subtitle: Text(
                                          _.notifications[index].description),
                                    ),
                                  ),
                                )),
                          )
                        : Center(child: CircularProgressIndicator()))));
      },
    );
  }
}
