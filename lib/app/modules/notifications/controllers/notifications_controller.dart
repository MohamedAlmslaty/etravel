import 'package:e_travel/app/data/models/notification.dart';
import 'package:e_travel/app/data/repositories/notification_repository.dart';
import 'package:get/get.dart';

class NotificationsController extends GetxController {
  final _notificationRepo = NotificationRepository.instance;
  final notifications = <Notification>[].obs;
  final loading = false.obs;

  @override
  void onInit() {
    getNotifications();
    super.onInit();
  }

  Future<void> getNotifications() async {
    final response = await _notificationRepo.getNotifications();
    notifications.assignAll(response);
    loading.value = true;
    update();
  }
}
