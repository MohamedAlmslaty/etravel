import 'package:e_travel/app/data/models/customer.dart';
import 'package:e_travel/app/data/repositories/user_repository.dart';
import 'package:e_travel/app/modules/widgets/custom_text.dart';
import 'package:e_travel/app/routes/app_pages.dart';
import 'package:e_travel/app/util/consts/colors.dart';
import 'package:e_travel/app/util/consts/style.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class ProfileController extends GetxController {
  final _userRepo = UserRepository.instance;
  final Rx<Customer> _user = Customer().obs;
  final newPassword = TextEditingController();
  final currentPassword = TextEditingController();
  final loading = false.obs;
  final editForm = GlobalKey<FormState>();

  Customer get user => _user.value;

  @override
  void onInit() {
    if (Get.find<GetStorage>().hasData('token')) loadUser();
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}

  Future<bool> loadUser([bool refresh = false]) async {
    _user.value = await _userRepo.getCustomerAccount();

    update();
    return true;
  }

  changePass() {
    Get.defaultDialog(
      title: "change password".tr,
      backgroundColor: Color(0xffFEF200),
      barrierDismissible: false,
      content: Form(
        key: editForm,
        child: Container(
          height: 250.h,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 15, vertical: 10),
                child: TextFormField(
                    decoration: dialogInputStyle.copyWith(
                      hintText: 'currentPassword'.tr,
                      filled: true,
                      fillColor: Colors.white,

                    ),
                    validator: RequiredValidator(
                        errorText: 'Please enter the currentPassword'.tr),
                    controller: currentPassword,
                  ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15),
                child: TextFormField(
                    decoration: dialogInputStyle.copyWith(
                      hintText: 'newPassword'.tr,
                      filled: true,
                      fillColor: Colors.white,

                    ),
                    validator: RequiredValidator(
                        errorText: 'Please enter the newPassword'.tr),
                    controller: newPassword,
                  ),
                ),

            ],
          ),
        ),
      ),
      cancel: ElevatedButton(
        onPressed: () {
          Get.back();
          currentPassword.clear();
          newPassword.clear();
        },
        style: ElevatedButton.styleFrom(
          primary: Colors.red,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(20.0), // Custom border radius
          ), // Custom background color for the Cancel button
        ),
        child: CustomText(text: "cancel".tr),
      ),
      confirm: ElevatedButton(
        onPressed: () async {
          loading();
          if (editForm.currentState!.validate()) {
            editForm.currentState!.save();
            _userRepo
                .changePassword(
                    currentPassword.text.trim(), newPassword.text.trim())
                .then((value) {
              if (value) {
                currentPassword.clear();
                newPassword.clear();
                EasyLoading.dismiss();
                Get.back();
                Get.back();
                showSnackBar(
                    title: 'successful'.tr,
                    message: 'Password changed successfully'.tr);
              } else {
                Get.back();
                Get.back();
                EasyLoading.dismiss();
                showSnackBar(
                    title: 'error'.tr, message: 'Old Password is wrong!'.tr);
              }
            });
          }
        },
        style: ElevatedButton.styleFrom(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20.0), // Custom border radius
            ),
            primary: primaryColor),
        child: CustomText(text:"Save".tr),
      ),
    );
  }

  logout() async {
    Get.find<GetStorage>().remove('token');
    Get.find<GetStorage>().remove('wallet_amount');
    Get.back();
    Get.offAllNamed(Routes.MAIN);
  }

  requestDelete() {
    Get.defaultDialog(
      title: 'warning'.tr,
      backgroundColor: Color(0xffFEF200),
      content: Text(
          'Are you sure you want to request your account to be deleted? \n Your data cannot be restored one we finish the deletion process!'
              .tr),
      cancel: ElevatedButton(
        onPressed: () {
          Get.back();
        },
        style: ElevatedButton.styleFrom(
          primary: Colors.red,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(20.0), // Custom border radius
          ), // Custom background color for the Cancel button
        ),
        child: CustomText(text:"cancel".tr),
      ),
      confirm: ElevatedButton(
        onPressed: () async {
          loading();
          await _userRepo.deleteAccount().then((value) {
            if (value) {
              EasyLoading.dismiss();
              Get.back();
              Get.back();
              showSnackBar(
                  title: 'successful'.tr,
                  message: 'account delete requested successfully'.tr);
            } else {
              Get.back();
              Get.back();
              EasyLoading.dismiss();
              showSnackBar(
                  title: 'error'.tr,
                  message: 'account delete request failed!'.tr);
            }
          });
        },
        style: ElevatedButton.styleFrom(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20.0), // Custom border radius
            ),
            primary: primaryColor),
        child: CustomText(text:"Save".tr),
      ),
    );
  }
}
