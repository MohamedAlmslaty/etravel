import 'package:e_travel/app/modules/widgets/custom_text.dart';
import 'package:e_travel/app/modules/widgets/custom_widget.dart';
import 'package:e_travel/app/util/consts/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import '../controllers/profile_controller.dart';

class ProfileView extends GetView<ProfileController> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<ProfileController>(
      init: ProfileController(),
      initState: (_) {},
      builder: (_) => Scaffold(
        resizeToAvoidBottomInset: false,
        body: CustomWidget(
          title: 'profile'.tr,
          child: SingleChildScrollView(
            child: Obx(
              () => Column(
                children: [
                  SizedBox(
                    height: 49.h,
                  ),
                  SvgPicture.asset(
                    'assets/images/account.svg',
                    width: 81.w,
                    height: 81.w,
                    fit: BoxFit.fill,
                  ),
                  CustomText(
                    text: controller.user.name ?? "",
                    fontWeight: FontWeight.bold,
                    fontSize: 16.sp,
                    color: primaryColor,
                  ),
                  CustomText(
                    text: controller.user.email ?? "",
                    fontWeight: FontWeight.bold,
                    fontSize: 16.sp,
                    color: primaryColor,
                  ),
                  Directionality(
                    textDirection: TextDirection.ltr,
                    child: CustomText(
                      text: controller.user.mobileNo ?? "",
                      fontWeight: FontWeight.bold,
                      fontSize: 16.sp,
                      color: primaryColor,
                    ),
                  ),
                  CustomText(
                    text: 'welcome'.tr,
                    fontWeight: FontWeight.bold,
                    color: primaryColor,
                  ),
                  if (controller.user.appleId.toString().contains("DELETE"))
                    CustomText(
                      text: "YOUR ACCOUNT REQUESTED TO BE DELETED!".tr,
                      fontWeight: FontWeight.bold,
                      fontSize: 14.sp,
                      color: Colors.red,
                    ),
                  if (controller.user.appleId.toString().contains("DELETE"))
                    CustomText(
                      text: "We will send you an email in 48 hours once account is deleted!".tr,
                      fontWeight: FontWeight.bold,
                      fontSize: 11.sp,
                      color: Colors.red,
                    ),
                  SizedBox(
                    height: 10,
                  ),
                  Container(
                    width: 343.w,
                    padding: EdgeInsets.only(right: 10.w, left: 17.w, top: 10.h, bottom: 10.h),
                    decoration: BoxDecoration(
                      color: whiteColor,
                      borderRadius: BorderRadius.all(Radius.circular(10.r)),
                    ),
                    child: Column(
                      children: [
                        ListTile(
                          leading: SvgPicture.asset(
                            'assets/images/Lock.svg',
                            color: primaryColor,
                            height: 20.h,
                          ),
                          title: CustomText(text: 'Edit password'.tr, fontWeight: FontWeight.bold, color: blackColor, fontSize: 18.sp),
                          onTap: () => _.changePass(),
                        ),
                        Divider(),
                        ListTile(
                          leading: Padding(
                            padding: const EdgeInsets.only(left: 10),
                            child: Icon(
                              Icons.delete_outline,
                              size: 28,
                              color: primaryColor,
                            ),
                          ),
                          title: CustomText(text: 'Request Account Deletion'.tr, fontWeight: FontWeight.bold, color: blackColor, fontSize: 18.sp),
                          onTap: () => _.requestDelete(),
                        ),
                        Divider(),
                        ListTile(
                          leading: SvgPicture.asset(
                            'assets/images/logout.svg',
                            color: primaryColor,
                            height: 20.h,
                          ),
                          title: CustomText(text: 'logout'.tr, fontWeight: FontWeight.bold, color: blackColor, fontSize: 18.sp),
                          onTap: () => _.logout(),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
