import 'package:e_travel/app/routes/app_pages.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:e_travel/app/modules/widgets/custom_widget.dart';
import 'package:get_storage/get_storage.dart';
import '../controllers/package_subscription_controller.dart';

class PackageSubscriptionView extends GetView<PackageSubscriptionController> {
  bool local=Get.find<GetStorage>().read('language')=='ar';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomWidget(
      title: 'PackageSubscription'.tr,
    child:GetBuilder<PackageSubscriptionController>(
      init: PackageSubscriptionController(),
      initState: (_) {},
      builder: (_) =>_.loading.value
        ?Column(
        children: [
           Expanded(
                child:  Padding(
                        padding: EdgeInsets.symmetric(horizontal: 12.h),
                        child: ListView.separated(
                          padding: EdgeInsets.only(top: 20.h),
                          separatorBuilder: (BuildContext context, int index) => SizedBox(
                            width: 10,
                          ),
                          itemCount: _.packageSubscriptions.length,
                          itemBuilder: (BuildContext context, int index) => Card(
                            elevation: 0.5,
                            clipBehavior: Clip.antiAlias,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(8),
                            ),
                            child: ListTile(
                              trailing: Text(_.packageSubscriptions[index].totalPrice.toString() + ' د.ل ', style: TextStyle(fontWeight: FontWeight.bold)),
                              leading: Text(
                                _.packageSubscriptions[index].id.toString() + "#",
                                style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold),
                              ),
                              title: Text(local
                               ? _.packageSubscriptions[index].touristPackage!.nameAr
                               : _.packageSubscriptions[index].touristPackage!.nameEn,
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                              subtitle: Text('Valid'.tr + _.packageSubscriptions[index].activeUntil!),
                              onTap: () {
                                Get.toNamed(Routes.PACKAGE_SUBSCRIPTION_DETAILS, arguments: _.packageSubscriptions[index].id);
                              },
                            ),
                          ),
                        ))
                    ),

    ] ) : Center(child: CircularProgressIndicator()),

      ),
      ),
    );
  }
}
