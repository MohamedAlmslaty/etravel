import 'package:e_travel/app/data/models/package_subscription.dart';
import 'package:e_travel/app/data/repositories/package_subscription_repository.dart';
import 'package:get/get.dart';

class PackageSubscriptionController extends GetxController {
  final _Repo = PackageSubscriptionRepository.instance;
  final loading = false.obs;
  RxList<PackageSubscription> packageSubscriptions = <PackageSubscription>[].obs;

  @override
  void onInit() {
    super.onInit();
    loadPackageSubscription();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}

  Future<void> loadPackageSubscription() async {
    final response = await _Repo.getPackageSubscriptions();
    packageSubscriptions.assignAll(response);
    loading.value = true;
    update();
  }

  Future<PackageSubscription> findPackageSubscriptionById(id) async {
    final response = await _Repo.findPackageSubscriptionById(id);
    loading.value = true;
    update();
    return response;
  }
}
