import 'package:e_travel/app/modules/account/controllers/account_controller.dart';
import 'package:e_travel/app/modules/widgets/custom_row.dart';
import 'package:e_travel/app/modules/widgets/custom_text.dart';
import 'package:e_travel/app/modules/widgets/custom_widget.dart';
import 'package:e_travel/app/routes/app_pages.dart';
import 'package:e_travel/app/util/consts/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class AccountView extends GetView<AccountController> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<AccountController>(
      init: AccountController(),
      initState: (_) {},
      builder: (_con) => Scaffold(
        body: CustomWidget(
          title: 'account'.tr,
          image: 'assets/icons/account.svg',
          main: true,
          child: SingleChildScrollView(
              child: Column(
            children: [
              Container(
                height: 130.h,
                width: Get.width,
                padding: EdgeInsets.symmetric(vertical: 5.h),
                margin: EdgeInsets.only(right: 20.w, left: 20.w, top: 40.h),
                decoration: BoxDecoration(
                  color: primaryColor,
                  borderRadius: BorderRadius.all(Radius.circular(30.r)),
                  image: DecorationImage(
                    image: AssetImage('assets/images/users.png'),
                    fit: BoxFit.cover,
                  ),
                ),
                child: Column(
                  children: [
                    SizedBox(
                      height: 10.h,
                    ),
                    Get.find<GetStorage>().hasData('token')
                        ? SizedBox(
                      width: 120.w,
                      child: CustomText(
                            text: controller.user.name ?? "",
                            fontWeight: FontWeight.bold,
                            fontSize: 14.sp,
                            textAlign: TextAlign.center,
                            maxLines: 3,
                            color: whiteColor,
                          )
                          )
                        : SizedBox(),
                    SizedBox(
                      height: 15.h,
                    ),
                    CustomText(
                      text: 'hello'.tr,
                      fontWeight: FontWeight.bold,
                      color: secondaryColor,
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 10.h,
              ),
              Get.find<GetStorage>().hasData('token')
                  ? Container(
                      margin: EdgeInsets.symmetric(horizontal: 20.w, vertical: 20.h),
                      child: Column(
                        children: [
                          CustomRow(
                            onClick: () => Get.toNamed(Routes.PROFILE),
                            image: 'assets/icons/user_p.svg',
                            title: 'profile'.tr,
                            home: false,
                            height: 50.h,
                          ),
                          SizedBox(
                            height: 10.h,
                          ),
                          CustomRow(
                            onClick: () => Get.toNamed(Routes.REFERRAL),
                            image: 'assets/icons/referral.svg',
                            title: 'Invite friends'.tr,
                            home: false,
                            height: 50.h,
                          ),
                          SizedBox(
                            height: 10.h,
                          ),
                          CustomRow(
                            onClick: () => Get.toNamed(Routes.WALLET),
                            image: 'assets/icons/currncy.svg',
                            title: 'wallet'.tr,
                            home: false,
                            height: 50.h,
                          ),
                          SizedBox(
                            height: 10.h,
                          ),
                          CustomRow(
                            onClick: () => Get.toNamed(Routes.PACKAGE_SUBSCRIPTION),
                            image: 'assets/icons/cabinClass.svg',
                            title: 'PackageSubscription'.tr,
                            home: false,
                            height: 50.h,
                          ),
                          SizedBox(
                            height: 10.h,
                          ),
                          CustomRow(
                            onClick: () => Get.toNamed(Routes.PASSENGER),
                            image: 'assets/icons/passengers.svg',
                            title: 'Passengers'.tr,
                            home: false,
                            height: 50.h,
                          ),
                          SizedBox(
                            height: 10.h,
                          ),
                          CustomRow(
                            onClick: () => Get.toNamed(Routes.NOTIFICATIONS),
                            image: 'assets/icons/not.svg',
                            title: 'Notifications'.tr,
                            home: false,
                            height: 50.h,
                          ),
                          SizedBox(
                            height: 10.h,
                          ),
                          CustomRow(
                            onClick: () => Get.toNamed(Routes.LANGUAGE),
                            image: 'assets/icons/language.svg',
                            title: 'Languages'.tr,
                            height: 50.h,
                            home: false,
                          ),
                          SizedBox(
                            height: 10.h,
                          ),
                          CustomRow(
                            onClick: () => Get.toNamed(Routes.VOUCHER_DEALER),
                            image: 'assets/icons/passengers.svg',
                            title: 'VoucherDealer'.tr,
                            home: false,
                            height: 50.h,
                          ),
                          SizedBox(
                            height: 10.h,
                          ),
                          CustomRow(
                            image: 'assets/icons/trees.svg',
                            title: 'loyal'.tr,
                            height: 50.h,
                            onClick: () => Get.toNamed(Routes.LOYAL),
                            home: false,
                          ),
                          SizedBox(
                            height: 10.h,
                          ),
                          CustomRow(
                            image: 'assets/icons/contact.svg',
                            title: 'Contact us'.tr,
                            height: 50.h,
                            onClick: () => Get.toNamed(Routes.CONTACTUS),
                            home: false,
                          ),
                          SizedBox(
                            height: 10.h,
                          ),
                          CustomRow(
                            image: 'assets/icons/about.svg',
                            title: 'About'.tr,
                            height: 50.h,
                            onClick: () => Get.toNamed(Routes.ABOUT),
                            home: false,
                          ),
                          SizedBox(
                            height: 10.h,
                          ),
                          CustomRow(
                            onClick: () => _con.logout(),
                            image: 'assets/icons/exit.svg',
                            title: 'logout'.tr,
                            home: false,
                            height: 50.h,
                          )
                        ],
                      ))
                  : Container(
                      margin: EdgeInsets.symmetric(horizontal: 20.w, vertical: 40.h),
                      child: Column(children: [
                        CustomRow(
                          onClick: () => Get.toNamed(Routes.AUTH),
                          image: 'assets/icons/user_p.svg',
                          title: 'login'.tr,
                          home: false,
                          height: 50.h,
                        ),
                        SizedBox(
                          height: 10.h,
                        ),
                        CustomRow(
                          onClick: () => Get.toNamed(Routes.LANGUAGE),
                          image: 'assets/icons/language.svg',
                          title: 'Languages'.tr,
                          height: 50.h,
                          home: false,
                        ),
                        SizedBox(
                          height: 10.h,
                        ),
                        CustomRow(
                          onClick: () => Get.toNamed(Routes.VOUCHER_DEALER),
                          image: 'assets/icons/passengers.svg',
                          title: 'VoucherDealer'.tr,
                          home: false,
                          height: 50.h,
                        ),
                        SizedBox(
                          height: 10.h,
                        ),
                        CustomRow(
                          image: 'assets/icons/contact.svg',
                          title: 'Contact us'.tr,
                          height: 50.h,
                          onClick: () => Get.toNamed(Routes.CONTACTUS),
                          home: false,
                        ),
                        SizedBox(
                          height: 10.h,
                        ),
                        CustomRow(
                          image: 'assets/icons/about.svg',
                          title: 'About'.tr,
                          height: 50.h,
                          onClick: () => Get.toNamed(Routes.ABOUT),
                          home: false,
                        ),
                      ]),
                    ),
              SizedBox(
                height: 30.h,
              ),
            ],
          )),
        ),
      ),
    );
  }
}
