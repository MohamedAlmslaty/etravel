import 'package:e_travel/app/data/models/customer.dart';
import 'package:e_travel/app/data/repositories/user_repository.dart';
import 'package:e_travel/app/routes/app_pages.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class AccountController extends GetxController {
  final _userRepo = UserRepository.instance;
  final Rx<Customer> _user = Customer().obs;

  Customer get user => _user.value;

  @override
  void onInit() {
    if (Get.find<GetStorage>().hasData('token')) loadUser();
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}

  Future<bool> loadUser([bool refresh = false]) async {
    _user.value = await _userRepo.getCustomerAccount();

    update();
    return true;
  }

  logout() async {
    Get.find<GetStorage>().remove('token');
    Get.find<GetStorage>().remove('wallet_amount');
    Get.offAllNamed(Routes.MAIN);
  }
}
