import 'package:e_travel/app/data/models/fligths_show.dart';
import 'package:e_travel/app/data/repositories/services_repository.dart';
import 'package:get/get.dart';

class ShowFlightsController extends GetxController {
  final _servicesRepo = ServicesRepository.instance;
  RxList<InternationalArrival> international = <InternationalArrival>[].obs;
  RxList<InternationalArrival> domestic = <InternationalArrival>[].obs;
  final _loading = false.obs;
  final airport = ''.obs;
  @override
  void onInit() {
    super.onInit();
    if(Get.arguments!=null){
      airport.value=Get.arguments;
      loadDomesticDeparture(Get.arguments,Get.locale.toString().substring(0,2).toUpperCase());
      loadDomesticArrival(Get.arguments,Get.locale.toString().substring(0,2).toUpperCase());
      loadInternationalDeparture(Get.arguments,Get.locale.toString().substring(0,2).toUpperCase());
      loadInternationalArrival(Get.arguments,Get.locale.toString().substring(0,2).toUpperCase());
    }

  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

  Future<void> loadInternationalArrival(String airport,String lang) async {
    final response = await _servicesRepo.getInternationalArrival(airport,lang);
    international.addAll(response);
    _loading.value = true;
    update();
  }
  Future<void> loadInternationalDeparture(String airport,String lang) async {
    final response = await _servicesRepo.getInternationalDeparture(airport,lang);
    domestic.addAll(response);
    _loading.value = true;
    update();
  }
  Future<void> loadDomesticArrival(String airport,String lang) async {
    final response = await _servicesRepo.getDomesticArrival(airport,lang);
    international.addAll(response);
    _loading.value = true;

    update();
  }
  Future<void> loadDomesticDeparture(String airport,String lang) async {
    final response = await _servicesRepo.getDomesticDeparture(airport,lang);
    domestic.addAll(response);
       _loading.value = true;
    update();
  }
}
