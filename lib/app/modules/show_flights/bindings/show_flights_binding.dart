import 'package:get/get.dart';

import '../controllers/show_flights_controller.dart';

class ShowFlightsBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<ShowFlightsController>(
      () => ShowFlightsController(),
    );
  }
}
