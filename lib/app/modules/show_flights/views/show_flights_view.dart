import 'package:e_travel/app/modules/show_flights/views/widgets/datatable.dart';
import 'package:e_travel/app/modules/widgets/custom_widget.dart';
import 'package:e_travel/app/util/consts/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';

import '../controllers/show_flights_controller.dart';

class ShowFlightsView extends GetView<ShowFlightsController> {

  ShowFlightsView({Key? key}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    return GetBuilder<ShowFlightsController>(
        init: ShowFlightsController(),
        builder: (_con) {
          if (_con.international.isNotEmpty) {
            _con.international.sort((a, b) => a.scheduledTime!.compareTo(b.scheduledTime!));
          }
          if (_con.domestic.isNotEmpty) {
            _con.domestic
                .sort((a, b) => a.scheduledTime!.compareTo(b.scheduledTime!));
          }
          return Scaffold(
            body: CustomWidget(
              title: 'Services'.tr,
              //image: 'assets/icons/service.svg',
              child: Padding(
                padding: EdgeInsets.only(
                    top: 20.h, right: 5.w, left: 5.w, bottom: 34.h),
                child: DefaultTabController(
                    length: 2,
                    child: Column(
                      children: [
                        TabBar(
                          indicatorWeight: 5,
                          labelColor: primaryColor,
                          unselectedLabelColor: primaryColor.withOpacity(0.5),
                          indicatorColor: Color(0xff312871),
                          labelPadding: EdgeInsets.only(bottom: 10.h),
                          tabs: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Text(
                                  'leaving'.tr,
                                  textScaleFactor: 1.0,
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 18.sp),
                                ),
                                SizedBox(
                                  width: 10.w,
                                ),
                                SvgPicture.asset('assets/icons/leaving.svg'),
                              ],
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Text(
                                  'arrival'.tr,
                                  textScaleFactor: 1.0,
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 18.sp),
                                ),
                                SizedBox(
                                  width: 10.w,
                                ),
                                SvgPicture.asset('assets/icons/arrival.svg'),
                              ],
                            ),
                          ],
                        ),
                        Expanded(
                            child: TabBarView(
                          children: [
                            DataTableWidget(flights: _con.domestic,airport: _con.airport.value,arrival: false),
                            DataTableWidget(flights: _con.international,airport: _con.airport.value,arrival: true),
                          ],
                        )),
                      ],
                    )),
              ),
            ),
          );
        });
  }
}
