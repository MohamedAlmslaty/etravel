import 'package:e_travel/app/data/models/fligths_show.dart';
import 'package:e_travel/app/modules/widgets/custom_text.dart';
import 'package:e_travel/app/util/consts/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

class DataTableWidget extends StatelessWidget {
  List<InternationalArrival> flights;
  String airport;
  bool arrival;

  DataTableWidget({Key? key, required this.flights,required this.airport,required this.arrival}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
        child: Column(
      children: [
        SizedBox(
          height: 30.h,
        ),
        CustomText(
          text: airport=='MRA'?'Flights screen from Misurata Airport'.tr:'Flights screen from Mitiga Airport'.tr,
          color: blackColor,
          fontWeight: FontWeight.bold,
          fontSize: 16.h,
          maxLines: 2,
        ),
        SizedBox(
          height: 15.h,
        ),
        DataTable(
          headingRowColor: MaterialStateProperty.all<Color>(primaryColor),
          columnSpacing: 10.0,
          headingRowHeight: 40.h,
          dataRowHeight: 50.h,
          columns: [
            DataColumn(
              label: CustomText(text: 'Flight Number'.tr, fontSize: 11.sp),
            ),
            DataColumn(
              label: CustomText(
                text: 'airline'.tr,
                fontSize: 11.sp,
              ),
            ),
            DataColumn(
              label: CustomText(text: 'the Destination'.tr, fontSize: 11.sp),
            ),
            DataColumn(
              label: CustomText(text: arrival?'the arrival'.tr:'the Departure'.tr, fontSize: 11.sp),
            ),
            DataColumn(
              label: CustomText(text: 'Flight status'.tr, fontSize: 11.sp),
            ),
          ],
          rows: flights.map((rowData) {
            return DataRow(
                color: rowData.flightType=='DD' ||rowData.flightType=='DA'?MaterialStateProperty.all<Color>(greyColor.withOpacity(0.4)):MaterialStateProperty.all<Color>(whiteColor.withOpacity(0.4)),
                cells: [
              DataCell(CustomText(
                  text: rowData.iata!+rowData.flightNumber!,
                  fontSize: 11.sp,
                  color: blackColor)),
              DataCell(SizedBox(
                width:80.w,
                  child:CustomText(
                maxLines: 2,
                  text: rowData.airlineName ?? '',
                  fontSize: 11.sp,
                  color: blackColor))),
              DataCell(CustomText(
                  text: rowData.cityName ?? '',
                  fontSize: 11.sp,
                  color: blackColor)),
              DataCell(CustomText(
                  text: rowData.scheduledTime!.substring(0, 2) + ":" + rowData.scheduledTime!.substring(2) ?? '',
                  fontSize: 11.sp,
                  color: blackColor)),
              DataCell(CustomText(
                  text: rowData.remark ?? '',
                  fontSize: 11.sp,
                  color: blackColor)),
            ]);
          }).toList(),
        ),
      ],
    ));
  }
}
