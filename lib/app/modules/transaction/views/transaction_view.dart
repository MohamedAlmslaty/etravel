import 'package:e_travel/app/modules/widgets/custom_text.dart';
import 'package:e_travel/app/modules/widgets/custom_widget.dart';
import 'package:e_travel/app/util/consts/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../controllers/transaction_controller.dart';

class TransactionsView extends GetView<TransactionsController> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<TransactionsController>(
      init: TransactionsController(),
      initState: (_) {},
      builder: (_) {
        return Scaffold(
            body: SingleChildScrollView(
                child: CustomWidget(
                    title: 'Transaction'.tr,
                    height: Get.height / 1.2,
                    child: _.loading.value
                        ? RefreshIndicator(
                            onRefresh: () async {
                              await _.getTransactions();
                            },
                            child: Padding(
                                padding: EdgeInsets.symmetric(horizontal: 30.h),
                                child: ListView.separated(
                                  padding: EdgeInsets.symmetric(vertical: 20.h),
                                  separatorBuilder: (BuildContext context, int index) => SizedBox(
                                    height: 10.h,
                                  ),
                                  itemCount: _.transactions.length,
                                  itemBuilder: (BuildContext context, int index) => Card(
                                    elevation: 0.5,
                                    clipBehavior: Clip.antiAlias,
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(8),
                                    ),
                                    child: ListTile(
                                      leading: CircleAvatar(
                                        backgroundColor: primaryColor,
                                        child: CustomText(
                                          text: '${_.transactions[index].id!}'.tr,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                      trailing: Text('${_.transactions[index].total} ' + 'LY'.tr),
                                      title: Text(
                                        '${_.transactions[index].paymentType!}'.tr + ' - ' + '${_.transactions[index].transactionStatus!}'.tr,
                                        style: TextStyle(fontWeight: FontWeight.bold),
                                      ),
                                      subtitle: Text(
                                        _.transactions[index].notes != null ? '${_.transactions[index].notes!}' : "",
                                        style: TextStyle(fontWeight: FontWeight.bold),
                                      ),
                                    ),
                                  ),
                                )),
                          )
                        : Center(child: CircularProgressIndicator()))));
      },
    );
  }
}
