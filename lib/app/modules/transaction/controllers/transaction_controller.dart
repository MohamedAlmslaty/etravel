import 'package:e_travel/app/data/models/transactions.dart';
import 'package:e_travel/app/data/repositories/transaction_repository.dart';
import 'package:get/get.dart';

class TransactionsController extends GetxController {
  final _transactionsRepo = TransactionsRepository.instance;
  final transactions = <Transactions>[].obs;
  final loading = false.obs;

  @override
  void onInit() {
    getTransactions();
    super.onInit();
  }

  Future<void> getTransactions() async {
    final response = await _transactionsRepo.getTransactions();
    transactions.assignAll(response);
    loading.value = true;
    update();
  }
}
