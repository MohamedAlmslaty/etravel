import 'package:e_travel/app/modules/transaction/controllers/transaction_controller.dart';
import 'package:get/get.dart';

class TransactionsBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<TransactionsController>(
      () => TransactionsController(),
    );
  }
}
