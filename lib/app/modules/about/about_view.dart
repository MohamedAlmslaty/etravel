import 'dart:io';

import 'package:e_travel/app/modules/widgets/custom_text.dart';
import 'package:e_travel/app/modules/widgets/custom_widget.dart';
import 'package:e_travel/app/util/consts/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:share_plus/share_plus.dart';

class AboutView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: CustomWidget(
            title: 'About'.tr,
            child: Column(
              children: [
                SizedBox(
                  height: 20.h,
                ),
                SvgPicture.asset(
                  'assets/images/logo.svg',
                  width: 100.h,
                  height: 100.h,
                  fit: BoxFit.fill,
                ),
                SizedBox(
                  height: 10.h,
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    children: <Widget>[
                      SizedBox(
                        height: 8.h,
                      ),
                      CustomText(
                        text: 'Welcome to the ETravel app'.tr,
                        fontWeight: FontWeight.bold,
                        color: primaryColor,
                      ),
                      SizedBox(
                        height: 8.h,
                      ),
                      CustomText(
                          color: primaryColor,
                          maxLines: 3,
                          textAlign: TextAlign.center,
                          text:
                              'eTravel is an application for booking airline tickets and everything related to travel.'
                                  .tr),
                      SizedBox(
                        height: 28.h,
                      ),
                      CustomText(
                          color: primaryColor, text: 'All rights reserved'.tr),
                      SizedBox(
                        height: 8.h,
                      ),
                      CustomText(
                          text: 'Copyright © 2023 Etravel'.tr,
                          color: primaryColor,
                          fontWeight: FontWeight.bold),
                      SizedBox(
                        height: 200.h,
                      ),
                      ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          foregroundColor:
                              Theme.of(context).colorScheme.onPrimary,
                          backgroundColor:primaryColor,
                        ),
                        onPressed: ()  async {
                          String url='';
                          Platform.isAndroid
                              ?url='Etravel app https://play.google.com/store/apps/details?id=ly.etravel.app.e_travel'
                          :url='Etravel app https://apps.apple.com/us/app/etravel/id1627862639';
                          final result = await Share.shareWithResult(url);

                          if (result.status == ShareResultStatus.success) {
                            print('Thank you for sharing Etravel!');
                          }
                        },
                        child:  Text('Share app'.tr),
                      ),
                    ],
                  ),
                ),
              ],
            )));
  }
}
