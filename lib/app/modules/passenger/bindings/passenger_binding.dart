import 'package:e_travel/app/modules/book_flight/controllers/book_flight_controller.dart';
import 'package:e_travel/app/modules/passenger/controllers/passenger_controller.dart';
import 'package:get/get.dart';

class PassengerBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<PassengerController>(
      () => PassengerController(),
    );Get.lazyPut<BookFlightController>(
      () => BookFlightController(),
    );
  }
}
