import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';

import 'package:e_travel/app/data/models/country.dart';
import 'package:e_travel/app/data/models/passenger.dart';
import 'package:e_travel/app/data/repositories/country_repository.dart';
import 'package:e_travel/app/data/repositories/passenger_repository.dart';
import 'package:e_travel/app/modules/book_flight/controllers/book_flight_controller.dart';
import 'package:e_travel/app/modules/widgets/custom_button.dart';
import 'package:e_travel/app/modules/widgets/custom_text.dart';
import 'package:e_travel/app/util/consts/colors.dart';
import 'package:e_travel/app/util/consts/loading.dart';
import 'package:e_travel/app/util/consts/style.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:mime/mime.dart';
class PassengerController extends GetxController {
  final _passengerRepo = PassengerRepository.instance;
  final _countryRepo = CountryRepository.instance;
  RxList<Passenger> passengers = <Passenger>[].obs;
  RxList<Country> countries = <Country>[].obs;
  RxString selectedCountry = ''.obs;
  RxString selectedNationality = ''.obs;

  Rx<Passenger> passenger = Passenger().obs;
  final _loading = false.obs;
  final addingNewPassenger = false.obs;
  final selectedTitle = ''.obs;

  bool get loaded => _loading.value;
  final GlobalKey<FormState> _passengerFormKey = GlobalKey<FormState>();

  get passengerFormKey => _passengerFormKey;

  final scrollController = ScrollController();
  final name = TextEditingController();
  final details = TextEditingController();
  final phone = TextEditingController();
  RxBool isForeign = false.obs;
  RxString setDate = ''.obs;
  RxString setDateExpiry = ''.obs;

  final ImagePicker picker = ImagePicker();
  late Rxn<File> image = Rxn();

  GlobalKey tutorialButton1 = GlobalKey();
  GlobalKey tutorialButton2 = GlobalKey();
  GlobalKey tutorialButton3 = GlobalKey();


  @override
  void onInit() {
    passenger.value.birthday = null;
    setDate.value = '';
    super.onInit();
    loadPassengers();
    loadCountries();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    passenger.value.birthday = null;
    setDate.value = '';
  }

  void defaultAddress() async {
    isForeign.value = !isForeign.value;
    passenger.value.isForeign = isForeign.value;
  }

  void back() {
    passenger.value.birthday = null;
    setDate.value = '';
    print('Date' + setDate.value);
    Get.back();
  }

  selectBirthdayDate(BuildContext context) async {
    showModalBottomSheet(
      context: context,
      builder: (BuildContext builder) {
        return Container(
            height: 400.0,
            child: Column(
              children: [
                SizedBox(height: 10.h,),
                CustomText(text: 'Set your Birthday'.tr,color: blackColor,fontSize: 20.sp,),
                SizedBox(height: 10.h,),
                Expanded(child:
                CupertinoDatePicker(
                  mode: CupertinoDatePickerMode.date,
                  initialDateTime: DateTime.now(),
                  dateOrder: DatePickerDateOrder.dmy,
                  onDateTimeChanged: (DateTime newDateTime) {
                    // Handle the selected date
                    setDate.value =
                        DateFormat('yyyy-MM-dd').format(newDateTime).toString();
                  },
                ),
                ),
                SizedBox(
                  height: 20.h,
                ),
                CustomButton(
                    text: 'choose'.tr,
                    color: primaryColor,
                    onClick: () {
                      passenger.value.birthday = setDate.value;
                      Get.back();
                    }),
                SizedBox(
                  height: 20.h,
                ),
              ],
            ));
      },
    );
  }
  selectExpiryDate(BuildContext context) async {
    showModalBottomSheet(
      context: context,
      builder: (BuildContext builder) {
        return Container(
            height: 400.0,
            child: Column(
              children: [
                SizedBox(height: 10.h,),
                CustomText(text: 'Passport expiry date'.tr,color: blackColor,fontSize: 20.sp,),
                SizedBox(height: 10.h,),
                Expanded(child:
                CupertinoDatePicker(
                  mode: CupertinoDatePickerMode.date,
                 // initialDateTime: edit?DateTime.parse(birthday??DateTime.now()):DateTime.now(),
                  initialDateTime: DateTime.now(),
                  dateOrder: DatePickerDateOrder.dmy,
                  onDateTimeChanged: (DateTime newDateTime) {
                    // Handle the selected date
                    setDateExpiry.value =
                        DateFormat('yyyy-MM-dd').format(newDateTime).toString();
                  },
                ),
                ),
                SizedBox(
                  height: 20.h,
                ),
                CustomButton(
                    text: 'choose'.tr,
                    color: primaryColor,
                    onClick: () {
                      passenger.value.passportExpiryDate = setDateExpiry.value;
                      Get.back();
                    }),
                SizedBox(
                  height: 20.h,
                ),
              ],
            ));
      },
    );
  }
  editBirthdayDate(BuildContext context,birthday) async {
    showModalBottomSheet(
      context: context,
      builder: (BuildContext builder) {
        return Container(
            height: 400.0,
            child: Column(
              children: [
                SizedBox(height: 10.h,),
                CustomText(text: 'Set your Birthday'.tr,color: blackColor,fontSize: 20.sp,),
                SizedBox(height: 10.h,),
                Expanded(child:
                CupertinoDatePicker(
                  mode: CupertinoDatePickerMode.date,
                  initialDateTime: DateTime.now(),
                  dateOrder: DatePickerDateOrder.dmy,
                  onDateTimeChanged: (DateTime newDateTime) {
                    // Handle the selected date
                    setDate.value =
                        DateFormat('yyyy-MM-dd').format(newDateTime).toString();
                  },
                ),
                ),
                SizedBox(
                  height: 20.h,
                ),
                CustomButton(
                    text: 'choose'.tr,
                    color: primaryColor,
                    onClick: () {
                      passenger.value.birthday = setDate.value;
                      Get.back();
                    }),
                SizedBox(
                  height: 20.h,
                ),
              ],
            ));
      },
    );
  }
  editExpiryDate(BuildContext context,birthday) async {
    showModalBottomSheet(
      context: context,
      builder: (BuildContext builder) {
        return Container(
            height: 400.0,
            child: Column(
              children: [
                SizedBox(height: 10.h,),
                CustomText(text: 'Passport expiry date'.tr,color: blackColor,fontSize: 20.sp,),
                SizedBox(height: 10.h,),
                Expanded(child:
                CupertinoDatePicker(
                  mode: CupertinoDatePickerMode.date,
                  // initialDateTime: edit?DateTime.parse(birthday??DateTime.now()):DateTime.now(),
                  initialDateTime: DateTime.now(),
                  dateOrder: DatePickerDateOrder.dmy,
                  onDateTimeChanged: (DateTime newDateTime) {
                    // Handle the selected date
                    setDateExpiry.value =
                        DateFormat('yyyy-MM-dd').format(newDateTime).toString();
                  },
                ),
                ),
                SizedBox(
                  height: 20.h,
                ),
                CustomButton(
                    text: 'choose'.tr,
                    color: primaryColor,
                    onClick: () {
                      passenger.value.passportExpiryDate = setDateExpiry.value;
                      Get.back();
                    }),
                SizedBox(
                  height: 20.h,
                ),
              ],
            ));
      },
    );
  }
  void imgFromCamera() async {
    try {
      final pickedFile = await picker.pickImage(
        source: ImageSource.camera,
      );

      if (pickedFile != null) {
         image.value = File(pickedFile.path);
        final bytes = await pickedFile.readAsBytes();
         passenger.value.passport = base64.encode(bytes);
         passenger.value.passportContentType = lookupMimeType(pickedFile.path);
      }
    } catch (e) {}
  }

  void imgFromGallery() async {
    try {
      final pickedFile = await picker.pickImage(
        source: ImageSource.gallery,
        maxWidth: 1800,
        maxHeight: 1800,
      );

      if (pickedFile != null) {
         image.value = File(pickedFile.path);
        Uint8List imagebytes = await pickedFile.readAsBytes();
       passenger.value.passport = base64.encode(imagebytes);
       passenger.value.passportContentType = lookupMimeType(pickedFile.path);
      }
    } catch (e) {}
  }

  void showPicker(context, bool visa) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return SafeArea(
            child: Container(
              child: Wrap(
                children: <Widget>[
                  ListTile(
                      leading: Icon(Icons.photo_library),
                      title: Text(
                        'Gallery'.tr,
                      ),
                      onTap: () {
                        imgFromGallery();
                        Get.back();
                      }),
                  ListTile(
                    leading: Icon(Icons.photo_camera),
                    title: Text(
                      'Camera'.tr,
                    ),
                    onTap: () {
                      imgFromCamera();
                      Get.back();
                    },
                  ),
                ],
              ),
            ),
          );
        });
  }

  Future<void> loadPassengers() async {
    final response = await _passengerRepo.getPassengers(0,1000);
    passengers.assignAll(response);
    _loading.value = true;
    update();
  }
  Future<void> loadCountries() async {
    final response = await _countryRepo.getCountries();
    countries.assignAll(response);
    _loading.value = true;
    update();
  }

  addPassenger() {
    if (setDate.isEmpty) {
      showSnackBar(title: 'error'.tr, message: 'Please enter birthdate!'.tr);
      EasyLoading.dismiss();
      return;
    }if (setDateExpiry.isEmpty) {
      showSnackBar(title: 'error'.tr, message: 'Please enter expiry date!'.tr);
      EasyLoading.dismiss();
      return;
    }
    if (selectedTitle.value.isEmpty || selectedTitle.value == null) {
      showSnackBar(title: 'error'.tr, message: 'Please choose title!'.tr);
      EasyLoading.dismiss();
      return;
    }
    if (isForeign.value) {
      if (image.value != null) {
        if (passengerFormKey.currentState!.validate()) {
          passengerFormKey.currentState!.save();
          loading();
          passenger.value.title = selectedTitle.value;
          passenger.value.nationalityCode = selectedNationality.value;
          passenger.value.passportIssueCode = selectedCountry.value;
          _passengerRepo.addNewPassenger(passenger.value).then((value) {
            EasyLoading.dismiss();
            showSnackBar(
                title: 'successful'.tr,
                message: 'Passenger added successfully'.tr);
            passenger.value.birthday = null;
            setDate.value = '';
            setDateExpiry.value = '';
            selectedCountry.value = '';
            selectedNationality.value = '';
            selectedTitle.value = '';
            isForeign.value = false;
            image.value = null;
            loadPassengers();
            Get.find<BookFlightController>().loadPassengers();
            Get.back();
          });
        }
      } else {
        showSnackBar(
            title: 'error'.tr, message: 'Please upload a passport photo'.tr);
      }
    } else {
      if (passengerFormKey.currentState!.validate()) {
        passengerFormKey.currentState!.save();
        loading();
        passenger.value.title = selectedTitle.value;
        passenger.value.nationalityCode = selectedNationality.value;
        passenger.value.passportIssueCode = selectedCountry.value;
        _passengerRepo.addNewPassenger(passenger.value).then((value) {
          EasyLoading.dismiss();
          showSnackBar(
              title: 'successful'.tr,
              message: 'Passenger added successfully'.tr);
          passenger.value.birthday = null;
          setDate.value = '';
          setDateExpiry.value = '';
          selectedCountry.value = '';
          selectedNationality.value = '';
          selectedTitle.value = '';
          isForeign.value = false;
          image.value = null;
          loadPassengers();
          Get.find<BookFlightController>().loadPassengers();
          Get.back();
        });
      }
    }
  }

  editPassenger(int id) {
    if (passengerFormKey.currentState!.validate()) {
      passengerFormKey.currentState!.save();
      loading();
      passenger.value.id = id;
      passenger.value.title = selectedTitle.value;
      passenger.value.birthday = setDate.value;
      passenger.value.passportExpiryDate = setDateExpiry.value;
      passenger.value.nationalityCode = selectedNationality.value;
      passenger.value.passportIssueCode = selectedCountry.value;
      _passengerRepo.updatePassenger(id, passenger.value).then((value) {
        EasyLoading.dismiss();
        selectedTitle.value = '';
        setDate.value = '';
        setDateExpiry.value = '';
        selectedCountry.value = '';
        selectedNationality.value = '';
        showSnackBar(
            title: 'successful'.tr,
            message: 'Passenger updated successfully'.tr);
        loadPassengers();
      });
    }
  }

  void removePassenger(int id, BuildContext context) {
    Get.defaultDialog(
        title: 'warning'.tr,
        backgroundColor: Color(0xffFEF200),
        content: Text('Are you sure to remove the passenger?'.tr),
        cancel: ElevatedButton(
          onPressed: () {
            Get.back(); // Close the dialog when the Cancel button is pressed
          },
          style: ElevatedButton.styleFrom(
            primary: Colors.red,
            shape: RoundedRectangleBorder(
              borderRadius:
              BorderRadius.circular(20.0), // Custom border radius
            ), // Custom background color for the Cancel button
          ),
          child: CustomText(text:"cancel".tr),
        ),
        confirm: ElevatedButton(
          onPressed: () async {
            await _passengerRepo.deletePassenger(id).then((value) {
              if (value) {
                loadPassengers();
                update();
                Get.back();
                Get.back();
                showSnackBar(
                    title: 'successful'.tr,
                    message: 'Passenger removed successfully'.tr);
              } else {
                Get.back();
                Get.back();
                showSnackBar(
                    title: 'error'.tr,
                    message: 'Passenger already used in a booking!'.tr);
              }
            });
          },
          style: ElevatedButton.styleFrom(
              shape: RoundedRectangleBorder(
                borderRadius:
                BorderRadius.circular(20.0), // Custom border radius
              ),
              primary: primaryColor),
          child: CustomText(text:"OK".tr),
        ),
      );
  }
}
