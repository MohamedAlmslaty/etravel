import 'package:cached_network_image/cached_network_image.dart';
import 'package:e_travel/app/data/models/passenger.dart';
import 'package:e_travel/app/modules/passenger/controllers/passenger_controller.dart';
import 'package:e_travel/app/modules/widgets/custom_button.dart';
import 'package:e_travel/app/modules/widgets/custom_text.dart';
import 'package:e_travel/app/modules/widgets/radius_widget.dart';
import 'package:e_travel/app/util/consts/base.dart';
import 'package:e_travel/app/util/consts/colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:get/get.dart';

class PassengerEdit extends GetView<PassengerController> {
  final Passenger passenger;

  const PassengerEdit({Key? key, required this.passenger}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScopeNode currentFocus = FocusScope.of(context);
        if (!currentFocus.hasPrimaryFocus) currentFocus.unfocus();
      },
      child: Scaffold(
        backgroundColor: secondaryColor,
        body: SingleChildScrollView(
            child: GetBuilder<PassengerController>(
          init: PassengerController(),
          builder: (_) {
            _.selectedTitle.value=passenger.title??'';
            _.selectedNationality.value=passenger.nationalityCode??'';
            _.selectedCountry.value=passenger.passportIssueCode??'';
            _.setDate.value=passenger.birthday??'';
            _.setDateExpiry.value=passenger.passportExpiryDate??'';
            return Column(children: [
              SizedBox(
                height: 44.h,
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 20.w, vertical: 10.h),
                child: Row(
                  children: [
                    IconButton(
                        onPressed: () => _.back(),
                        icon: Icon(
                          Icons.arrow_back,
                          color: primaryColor,
                        )),
                    SizedBox(
                      width: 10.w,
                    ),
                    CustomText(
                      text: 'Edit Passenger'.tr,
                      fontSize: 20.sp,
                      color: primaryColor,
                    ),
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 20.w),
                child: Column(
                  children: [
                    SizedBox(
                      height: 45.h,
                    ),
                    Form(
                      key: _.passengerFormKey,
                      child: Column(
                        children: [
                          Row(
                            children: [
                              CustomText(
                                text: 'title'.tr,
                                fontSize: 20.sp,
                                color: blackColor,
                              ),
                              SizedBox(
                                width: 15.w,
                              ),
                              Obx(
                                () => Container(
                                  height: 40.h,
                                  padding: EdgeInsets.symmetric(horizontal: 5.w),
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: BorderRadius.all(Radius.circular(10.r))
                                  ),
                                  child: DropdownButton<String>(

                                    value: _.selectedTitle.value.isNotEmpty
                                        ? _.selectedTitle.value
                                        : null,

                                    items: <String>['MR', 'MRS', 'MISS']
                                        .map((String value) {
                                      return DropdownMenuItem<String>(
                                        value: value,
                                        child: Text(value),
                                      );
                                    }).toList(),
                                    onChanged: (String? newValue) {
                                      _.selectedTitle.value = newValue!;
                                    },
                                    // add extra sugar..
                                    icon: Icon(Icons.arrow_drop_down),
                                    iconSize: 28,
                                    underline: SizedBox(),
                                  ),
                                ),
                              ),
                            ],
                          ),

                          SizedBox(height: 10.h),
                          RadiusCircular(child:TextFormField(
                            keyboardType: TextInputType.text,
                            initialValue: passenger.firstName,
                            inputFormatters: [
                              FilteringTextInputFormatter(RegExp("[a-zA-Z]"),
                                  allow: true)
                            ],
                            onSaved: (input) =>
                                _.passenger.value.firstName = input!.trim(),
                            validator: RequiredValidator(
                                errorText: "Please enter firstName".tr),
                            decoration: InputDecoration(
                              contentPadding: EdgeInsets.all(12),
                              hintText: 'First Name'.tr,
                              fillColor: whiteColor,
                              filled: true,
                              border: InputBorder.none,
                              hintStyle: TextStyle(
                                  color: primaryColor.withOpacity(0.7)),
                            ),
                          )),
                          SizedBox(height: 10.h),
                          RadiusCircular(child: TextFormField(
                            keyboardType: TextInputType.text,
                            inputFormatters: [
                              FilteringTextInputFormatter(RegExp("[a-zA-Z]"),
                                  allow: true)
                            ],
                            initialValue: passenger.lastName,
                            onSaved: (input) =>
                                _.passenger.value.lastName = input!.trim(),
                            validator: RequiredValidator(
                                errorText: "Please enter lastName".tr),
                            decoration: InputDecoration(
                              contentPadding: EdgeInsets.all(12),
                              hintText: 'Last Name'.tr,
                              fillColor: whiteColor,
                              filled: true,
                              border: InputBorder.none,
                              hintStyle: TextStyle(
                                  color: primaryColor.withOpacity(0.7)),
                            ),
                          )),
                          SizedBox(height: 10.h),
                          InkWell(
                            onTap: () =>  _.editBirthdayDate(context,
                                    passenger.birthday),
                            child: Container(
                              height: 45.h,
                              decoration: BoxDecoration(
                                  color: whiteColor,
                                  borderRadius: BorderRadius.all(Radius.circular(10.r))
                              ),
                              padding: EdgeInsets.symmetric(
                                  horizontal: 10.w, vertical: 5.h),
                              width: Get.width,
                              child: Obx(
                                () => _.setDate.value.isEmpty
                                    ? CustomText(
                                        text: 'birthday'.tr,
                                        color: textColor,
                                        fontSize: 16.sp,
                                      )
                                    : CustomText(
                                        text: _.setDate.value,
                                        color: blackColor,
                                        fontSize: 16.sp,
                                      ),
                              ),
                            ),
                          ),
                          SizedBox(height: 10.h),

                          RadiusCircular(child:  Obx(() =>DropdownButtonHideUnderline(
                              child: ButtonTheme(
                                alignedDropdown: true,
                                child:  DropdownButton<String>(
                                  value: _.selectedNationality.value.isNotEmpty ? _.selectedNationality.value : null,
                                  items: _.countries.map((country) {
                                    return DropdownMenuItem<String>(
                                      value: country.iso3,
                                      child: SizedBox(width: 200.w,child:  CustomText(text:country.nameEn!+' - '+country.iso3!,color: Colors.black,maxLines: 2,)),
                                    );
                                  }).toList(),
                                  onChanged: (newValue) {
                                    _.selectedNationality.value = newValue!;
                                  },
                                  hint: Text('Choose nationality'.tr),
                                  icon: Icon(Icons.arrow_drop_down),
                                  iconSize: 28,
                                ),
                              ),
                            ),
                            ),
                          ),
                          SizedBox(height: 10.h),

                          RadiusCircular(child:TextFormField(
                            keyboardType: TextInputType.text,
                            inputFormatters: [
                              FilteringTextInputFormatter(RegExp(r'^[a-zA-Z0-9]*$'),allow: true),
                            ],
                            initialValue: passenger.passportNo,
                            onSaved: (input) =>
                                _.passenger.value.passportNo = input!.trim(),
                            validator: RequiredValidator(
                                errorText: "Please enter passportNo".tr),
                            decoration: InputDecoration(

                              hintText: 'Passport No'.tr,
                              fillColor: whiteColor,
                              filled: true,

                              border: InputBorder.none,
                              hintStyle: TextStyle(
                                  color: primaryColor.withOpacity(0.7)),
                            ),
                          )),
                          SizedBox(height: 10.h),
                          RadiusCircular(child:  Obx(() =>DropdownButtonHideUnderline(
                              child: ButtonTheme(
                                alignedDropdown: true,
                                child: DropdownButton<String>(
                                  value: _.selectedCountry.value.isNotEmpty ? _.selectedCountry.value : null,
                                  items: _.countries.map((country) {
                                    return DropdownMenuItem<String>(
                                      value: country.iso3,
                                      child: SizedBox(width: 200.w,child:  CustomText(text:country.nameEn!+' - '+country.iso3!,color: Colors.black,maxLines: 2,)),
                                    );
                                  }).toList(),
                                  onChanged: (newValue) {
                                    _.selectedCountry.value = newValue!;
                                  },
                                  hint:
                                  Text('Choose the country of issue'.tr),
                                  icon: Icon(Icons.arrow_drop_down),
                                  iconSize: 28,
                                  underline: SizedBox(),
                                ),
                              ),
                            ),
                            ),
                          ),
                          SizedBox(height: 10.h),
                          InkWell(
                            onTap: () =>  _.editExpiryDate(
                                    context,
                                    passenger.passportExpiryDate),
                            child: Container(
                              height: 45.h,
                              decoration: BoxDecoration(
                                  color: whiteColor,
                                  borderRadius: BorderRadius.all(Radius.circular(10.r))
                              ),
                              padding: EdgeInsets.symmetric(
                                  horizontal: 10.w, vertical: 5.h),
                              width: Get.width,
                              child: Obx(
                                () => _.setDateExpiry.value.isEmpty
                                    ? CustomText(
                                        text: 'Passport expiry date'.tr,
                                        color: textColor,
                                        fontSize: 16.sp,
                                      )
                                    : CustomText(
                                        text: _.setDateExpiry.value,
                                        color: blackColor,
                                        fontSize: 16.sp,
                                      ),
                              ),
                            ),
                          ),

                          SizedBox(height: 10.h),
                          CustomButton(
                            text: 'Save'.tr,
                            color: primaryColor,
                            onClick: () =>  _.editPassenger(passenger.id!),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ]);
          },
        )),
      ),
    );
  }
}
