import 'package:e_travel/app/data/models/passenger.dart';
import 'package:e_travel/app/modules/passenger/views/passenger_add.dart';
import 'package:e_travel/app/modules/passenger/views/passenger_edit.dart';
import 'package:e_travel/app/util/consts/colors.dart';
import 'package:e_travel/app/modules/widgets/custom_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:e_travel/app/modules/widgets/custom_widget.dart';
import '../controllers/passenger_controller.dart';

class PassengersView extends GetView<PassengerController> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<PassengerController>(
      init: PassengerController(),
      builder: (_) {
        return Scaffold(
            floatingActionButton: FloatingActionButton(
              backgroundColor: secondaryColor,
              child: Icon(
                Icons.add,
                color: primaryColor,
              ),
              onPressed: () {
                _.setDate.value='';
                Get.to(() => PassengerAdd(
                ));
              }
            ),
            body:CustomWidget(
        title: 'Passengers'.tr,
        child: Column(
              children: [
                SizedBox(height: 30.h,),
                _.loaded
                    ? _.passengers.isNotEmpty
                        ? Expanded(
                            child: RefreshIndicator(
                              onRefresh: () => _.loadPassengers(),
                              child: Obx(()=>ListView.builder(
                                padding: EdgeInsets.only(top: 10.h),
                                itemCount: _.passengers.length,
                                itemBuilder: (BuildContext context, int index) {
                                  final passenger = _.passengers[index];
                                  return Card(
                                    elevation: 1,
                                    clipBehavior: Clip.hardEdge,
                                    margin: EdgeInsets.symmetric(horizontal: 15.w, vertical: 5.h),
                                    child: Container(
                                      color: Colors.white,
                                      child: ListTile(
                                        onTap: ()=>Get.to(() => PassengerEdit(
                                          passenger: passenger,
                                        )),
                                        leading: Container(
                                          padding: EdgeInsets.all(15),
                                          decoration: BoxDecoration(color: primaryColor, borderRadius: BorderRadius.circular(15)),
                                          child: SvgPicture.asset('assets/images/user_data.svg'),
                                        ),
                                        title: CustomText(text: '${passenger.firstName}  ${passenger.lastName}', fontSize: 18.sp, color: Colors.black),
                                        subtitle: CustomText(text: '${passenger.passportNo}', fontSize: 14.sp, color: Colors.black),
                                         trailing: IconButton(
                                           icon: Icon(
                                             Icons.delete,
                                             color: Colors.red,
                                           ),
                                           onPressed: ()=>_.removePassenger(passenger.id!,context),
                                         )

                                      ),
                                    ),
                                  );
                                },
                              ),
                            ),
                            ),
                          )
                        : Expanded(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                CustomText(text: 'No passengers have been added'.tr, color: primaryColor, fontSize: 20),
                              ],
                            ),
                          )
                    : Center(child: CircularProgressIndicator()),
              ],
            )
            )
        );
      },
    );
  }
}
