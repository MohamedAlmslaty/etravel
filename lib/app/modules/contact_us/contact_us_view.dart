import 'package:e_travel/app/modules/widgets/custom_widget.dart';
import 'package:e_travel/app/util/consts/colors.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:url_launcher/url_launcher.dart';

class ContactUsView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomWidget(
        title: 'Contact us'.tr,
        child: Column(
          children: [
            SizedBox(
              height: 20,
            ),
            ListTile(
              leading: Icon(
                Icons.map,
                color: primaryColor,
              ),
              title: Text(
                "Tripoli, Libya, Nasr Street",
                style: TextStyle(
                  color: primaryColor,
                ),
              ),
            ),
            InkWell(
              onTap: () {
                launch("mailto:info@etravel.ly");
              },
              child: ListTile(
                leading: Icon(
                  Icons.email,
                  color: primaryColor,
                ),
                title: Text(
                  "info@etravel.ly",
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: primaryColor,
                  ),
                ),
              ),
            ),
            InkWell(
              onTap: () {
                launch("https://www.facebook.com/eTravelApp.ly");
              },
              child: ListTile(
                leading: Icon(
                  Icons.facebook,
                  color: primaryColor,
                ),
                title: Text(
                  "https://www.facebook.com/eTravelApp.ly",
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: primaryColor,
                  ),
                ),
              ),
            ),
            InkWell(
              onTap: () {
                launch("tel://+218-900500000");
              },
              child: ListTile(
                leading: Icon(
                  Icons.phone,
                  color: primaryColor,
                ),
                title: Directionality(
                  textDirection: TextDirection.ltr,
                  child: Text(
                    "+218-900500000",
                    style: TextStyle(
                      color: primaryColor,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
