import 'package:e_travel/app/data/models/referral.dart';
import 'package:e_travel/app/modules/widgets/custom_button.dart';
import 'package:e_travel/app/modules/widgets/custom_text.dart';
import 'package:e_travel/app/util/consts/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:share_plus/share_plus.dart';

class DataTableWidget extends StatelessWidget {
  List<Referral> referrals;

  DataTableWidget({Key? key, required this.referrals}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
        child: Column(
      children: [
        SizedBox(
          height: 30.h,
        ),
        DataTable(
          headingRowColor: MaterialStateProperty.all<Color>(primaryColor),
          columnSpacing: 13.0,
          headingRowHeight: 30.h,
          dataRowHeight: 50.h,
          columns: [
            DataColumn(
              label: CustomText(text: 'code'.tr, fontSize: 11.sp),
            ),
            DataColumn(
              label: CustomText(
                text: 'statue'.tr,
                fontSize: 11.sp,
              ),
            ),
            DataColumn(
              label: CustomText(text: 'customer'.tr, fontSize: 11.sp),
            ),
            DataColumn(
              label: CustomText(text: 'Share'.tr, fontSize: 11.sp),
            ),
          ],
          rows: referrals.map((rowData) {
            return DataRow(
                color: MaterialStateProperty.all<Color>(
                    whiteColor.withOpacity(0.4)),
                cells: [
                  DataCell(CustomText(
                      text: rowData.referralCode.toString(),
                      fontSize: 11.sp,
                      color: blackColor)),
                  DataCell(CustomText(
                      text: rowData.referralStatus.toString().tr,
                      fontSize: 11.sp,
                      color: blackColor)),
                  DataCell(CustomText(
                      text: rowData.referredCustomer!.name.toString() ?? '',
                      fontSize: 11.sp,
                      color: blackColor)),
                  DataCell(ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      foregroundColor:
                      Theme.of(context).colorScheme.onPrimary,
                      backgroundColor:primaryColor,
                    ),
                    onPressed: () async {
                      final result = await Share.shareWithResult('مرحبا \nتمت دعوتك من (${rowData.referrerCustomer!.name}) للتسجيل في تطبيق إي تراڤل لحجز تذاكر السفر حمل التطبيق و سجل عن طريق كود الدعوة التالي: (${rowData.referralCode}) و سافر مع إي تراڤل بكل سهولة.\nرابط التطبيق: https://onelink.to/kg62gk');

                      if (result.status == ShareResultStatus.success) {
                        print('Thank you for sharing Etravel!');
                      }
                    },
                    child:  Text('Share'.tr,textScaleFactor: 1.0,),
                  ),),
                ]);
          }).toList(),
        ),
      ],
    ));
  }
}
