import 'package:e_travel/app/modules/referral/views/widgets/datatable.dart';
import 'package:e_travel/app/modules/widgets/custom_button.dart';
import 'package:e_travel/app/modules/widgets/custom_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import '../controllers/referral_controller.dart';

class ReferralView extends GetView<ReferralController> {
  const ReferralView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<ReferralController>(
        init: ReferralController(),
        builder: (_con) => Scaffold(
            body:  CustomWidget(
                title: 'referralCode'.tr,
                child: SingleChildScrollView(child:Column(
                  children: [
                    SizedBox(
                      height: 20.h,
                    ),
                   CustomButton(text: 'Generate Referral code'.tr,onClick: ()=>_con.generateReferral(),),
              SizedBox(
              height: 20.h,
            ),

                    DataTableWidget(referrals: _con.referrals),
                  ],
                )))));
  }
}
