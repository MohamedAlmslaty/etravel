import 'package:e_travel/app/data/models/referral.dart';
import 'package:e_travel/app/data/repositories/referral_repository.dart';
import 'package:e_travel/app/util/consts/loading.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';

class ReferralController extends GetxController {
  final _referralRepo = ReferralRepository.instance;
  RxList<Referral> referrals = <Referral>[].obs;
  final referral = Referral().obs;
  final load = false.obs;
  @override
  void onInit() {
    super.onInit();
    loadReferrals();
  }

  @override
  void onReady() {
    super.onReady();
  }
  Future<void> loadReferrals() async {
    final response = await _referralRepo.getReferrals();
    referrals.assignAll(response);
    update();
    load.value = true;
  }
  Future<void> generateReferral() async {
    loading();
    final response = await _referralRepo.generateReferral();
    if(response!=null){
      EasyLoading.dismiss();
      referral.value=response;
      update();
      load.value = true;
      loadReferrals();
    }
  }

}
