import 'package:e_travel/app/modules/services/controllers/services_controller.dart';
import 'package:get/get.dart';


class ServiceBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<ServicesController>(
          () => ServicesController(),
    );

  }
}
