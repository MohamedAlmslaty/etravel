import 'package:e_travel/app/modules/widgets/custom_text.dart';
import 'package:e_travel/app/modules/widgets/custom_widget.dart';
import 'package:e_travel/app/modules/widgets/permission_denied.dart';
import 'package:e_travel/app/modules/widgets/slider.dart';
import 'package:e_travel/app/routes/app_pages.dart';
import 'package:e_travel/app/util/consts/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import '../controllers/services_controller.dart';

class ServicesView extends GetView<ServicesController> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<ServicesController>(
      init: ServicesController(),
      builder: (_con) => CustomWidget(
        title: 'Services'.tr,
        image: 'assets/icons/service.svg',
        main: true,
        child: FutureBuilder<bool>(
            future: _con.getSettingByKey(),
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.waiting) {
                return Center(child: CircularProgressIndicator(color: primaryColor,),); // Show a loading indicator while waiting for data.
              } else if (snapshot.hasError) {
                return Text('Error: ${snapshot.error}');
              } else {
                bool data = snapshot.data!;
                // Build your UI with the fetched data
                return data
                    ? Get.find<GetStorage>().hasData('token')
                        ? Padding(
                            padding: EdgeInsets.only(
                                top: 20.h, right: 20.w, left: 20.w),
                            child: DefaultTabController(
                                length: 2,
                                child: Column(
                                  children: [
                                    _con.sliders.isNotEmpty
                                        ? SliderImageWidget(
                                            sliders: _con.sliders)
                                        : SizedBox(),
                                    SizedBox(
                                      height: 40.h,
                                    ),
                                    InkWell(
                                        onTap: () => Get.toNamed(
                                            Routes.SHOW_FLIGHTS,
                                            arguments: 'MJI'),
                                        child: Container(
                                            width: Get.width,
                                            height: 45.h,
                                            decoration: BoxDecoration(
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(50.r)),
                                                color: primaryColor),
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.center,
                                              children: [
                                                SvgPicture.asset(
                                                  'assets/icons/departure.svg',
                                                  width: 24.w,
                                                  height: 24.w,
                                                  fit: BoxFit.fill,
                                                ),
                                                SizedBox(
                                                  width: 10.w,
                                                ),
                                                CustomText(
                                                  text:
                                                      'Flights screen from Mitiga Airport'
                                                          .tr,
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 14.sp,
                                                ),
                                              ],
                                            ))),
                                    SizedBox(
                                      height: 20.h,
                                    ),
                                    InkWell(
                                        onTap: () => Get.toNamed(
                                            Routes.SHOW_FLIGHTS,
                                            arguments: 'MRA'),
                                        child: Container(
                                            width: Get.width,
                                            height: 45.h,
                                            decoration: BoxDecoration(
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(50.r)),
                                                color: primaryColor),
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.center,
                                              children: [
                                                SvgPicture.asset(
                                                  'assets/icons/departure.svg',
                                                  width: 24.w,
                                                  height: 24.w,
                                                  fit: BoxFit.fill,
                                                ),
                                                SizedBox(
                                                  width: 10.w,
                                                ),
                                                CustomText(
                                                  text:
                                                      'Flights screen from Misurata Airport'
                                                          .tr,
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 14.sp,
                                                ),
                                              ],
                                            ))),
                                  ],
                                )),
                          )
                        : PermissionDenied()
                    : Padding(
                        padding:
                            EdgeInsets.only(top: 20.h, right: 20.w, left: 20.w),
                        child: DefaultTabController(
                            length: 2,
                            child: Column(
                              children: [
                                _con.sliders.isNotEmpty
                                    ? SliderImageWidget(sliders: _con.sliders)
                                    : SizedBox(),
                                SizedBox(
                                  height: 40.h,
                                ),
                                InkWell(
                                    onTap: () => Get.toNamed(
                                        Routes.SHOW_FLIGHTS,
                                        arguments: 'MJI'),
                                    child: Container(
                                        width: Get.width,
                                        height: 45.h,
                                        decoration: BoxDecoration(
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(50.r)),
                                            color: primaryColor),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: [
                                            SvgPicture.asset(
                                              'assets/icons/departure.svg',
                                              width: 24.w,
                                              height: 24.w,
                                              fit: BoxFit.fill,
                                            ),
                                            SizedBox(
                                              width: 10.w,
                                            ),
                                            CustomText(
                                              text:
                                                  'Flights screen from Mitiga Airport'
                                                      .tr,
                                              fontWeight: FontWeight.bold,
                                              fontSize: 14.sp,
                                            ),
                                          ],
                                        ))),
                                SizedBox(
                                  height: 20.h,
                                ),
                                InkWell(
                                    onTap: () => Get.toNamed(
                                        Routes.SHOW_FLIGHTS,
                                        arguments: 'MRA'),
                                    child: Container(
                                        width: Get.width,
                                        height: 45.h,
                                        decoration: BoxDecoration(
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(50.r)),
                                            color: primaryColor),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: [
                                            SvgPicture.asset(
                                              'assets/icons/departure.svg',
                                              width: 24.w,
                                              height: 24.w,
                                              fit: BoxFit.fill,
                                            ),
                                            SizedBox(
                                              width: 10.w,
                                            ),
                                            CustomText(
                                              text:
                                                  'Flights screen from Misurata Airport'
                                                      .tr,
                                              fontWeight: FontWeight.bold,
                                              fontSize: 14.sp,
                                            ),
                                          ],
                                        ))),
                              ],
                            )),
                      );
              }
            }),
      ),
    );
  }
}
