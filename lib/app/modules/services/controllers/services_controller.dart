import 'package:e_travel/app/data/models/slider.dart';
import 'package:e_travel/app/data/repositories/setting_repository.dart';
import 'package:e_travel/app/data/repositories/slider_repository.dart';
import 'package:e_travel/app/util/consts/loading.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';

class ServicesController extends GetxController {
  final _sliderRepo = SliderRepository.instance;
  final _settingRepo = SettingRepository.instance;
  RxList<Slider> sliders = <Slider>[].obs;
  final load = false.obs;
  final nonRegistered = false.obs;
  @override
  void onInit() {
    super.onInit();
    loadSliders();
    getSettingByKey();
  }
  Future<void> loadSliders() async {
    final response = await _sliderRepo.getSliders('SERVICES_SCREEN');
    sliders.assignAll(response);
    update();
    load.value = true;
  }
  Future<bool> getSettingByKey() async {
    await _settingRepo
        .getSetting('services_to_non_registered')
        .then((value) {
      if (value.id != null) {
        nonRegistered.value = value.value!.toLowerCase() != "false";
      }
    });
    return nonRegistered.value;
  }
}
