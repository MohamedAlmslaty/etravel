import 'package:e_travel/app/modules/package/controllers/package_controller.dart';
import 'package:get/get.dart';

class PackageBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<PackageController>(
      () => PackageController(),
    );
  }
}
