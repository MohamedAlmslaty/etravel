import 'package:cached_network_image/cached_network_image.dart';
import 'package:e_travel/app/modules/package/controllers/package_controller.dart';
import 'package:e_travel/app/modules/widgets/custom_text.dart';
import 'package:e_travel/app/routes/app_pages.dart';
import 'package:e_travel/app/util/consts/base.dart';
import 'package:e_travel/app/util/consts/colors.dart';
import 'package:e_travel/app/modules/widgets/custom_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

class PackageView extends GetView<PackageController> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<PackageController>(
      init: PackageController(),
      initState: (_) {},
      builder: (_con) => Scaffold(
        body: CustomWidget(
          title: 'packages'.tr,
          image: 'assets/icons/packges.svg',
          main: true,
          child: Padding(
            padding: EdgeInsets.only(top: 20.h, right: 20.w, left: 20.w),
            child: _con.loading.value
                ?_con.packages.isEmpty
          ? Center(
          child: CustomText(
            text: 'no packages'.tr,
            maxLines: 3,
            fontSize: 18.sp,
            textAlign:
            TextAlign.center,
            color: blackColor,
          ))
              :ListView.separated(
                    padding: EdgeInsets.zero,
                    itemBuilder: (BuildContext context, int index) => InkWell(
                          onTap: () {
                            Get.toNamed(Routes.PACKAGE_DETAILS,
                                arguments: _con.packages[index].id);
                          },
                          child: Card(
                            margin: const EdgeInsets.fromLTRB(16, 10, 16, 0),
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(6.0)),
                            child: Stack(
                              children: <Widget>[
                                CachedNetworkImage(
                                  imageUrl:
                                      imageUrl(_con.packages[index].imageUrl),
                                  imageBuilder: (context, imageProvider) =>
                                      Container(
                                    height: 120.h,
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(6.0),
                                      image: DecorationImage(
                                        image: imageProvider,
                                        fit: BoxFit.cover,
                                      ),
                                    ),
                                  ),
                                  errorWidget: (context, url, error) =>
                                      Icon(Icons.error),
                                ),
                                Positioned(
                                  bottom: 0,
                                  left: 0,
                                  right: 0,
                                  child: Container(
                                    padding:
                                        const EdgeInsets.symmetric(vertical: 2),
                                    child: Center(
                                        child: Text(Get.locale.toString()=='ar'
                                      ?_con.packages[index].nameAr
                                      :_con.packages[index].nameEn,
                                      overflow: TextOverflow.fade,
                                      softWrap: false,
                                      maxLines: 1,
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontWeight: FontWeight.bold),
                                    )),
                                    decoration: BoxDecoration(
                                      color: primaryColor,
                                      borderRadius: BorderRadius.only(
                                          topLeft: Radius.circular(20.r),
                                          topRight: Radius.circular(20.r)),
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                    separatorBuilder: (BuildContext context, int index) =>
                        SizedBox(
                          height: 10.h,
                        ),
                    itemCount: _con.packages.length)
                : Center(
                    child: CircularProgressIndicator(),
                  ),
          ),
        ),
      ),
    );
  }
}
