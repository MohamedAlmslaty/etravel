import 'package:e_travel/app/data/models/tourist_packages.dart';
import 'package:e_travel/app/data/repositories/tourist_packages_repository.dart';
import 'package:get/get.dart';

class PackageController extends GetxController {
  final _packageRepo = TouristPackagesRepository.instance;
  final loading = false.obs;
  RxList<TouristPackages> packages =<TouristPackages>[].obs ;

  @override
  void onInit() {
    super.onInit();
    loadPackages();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}

  Future<void> loadPackages() async {
    final response = await _packageRepo.getTouristPackages();
    packages.assignAll(response);
    loading.value = true;
    update();
  }
}
