import 'package:e_travel/app/modules/widgets/custom_text.dart';
import 'package:e_travel/app/modules/widgets/custom_widget.dart';
import 'package:e_travel/app/routes/app_pages.dart';
import 'package:e_travel/app/util/consts/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:qr_flutter/qr_flutter.dart';

import '../controllers/wallet_controller.dart';

class WalletView extends GetView<WalletController> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<WalletController>(
        init: WalletController(),
        initState: (_) {},
        builder: (_) => Scaffold(
            resizeToAvoidBottomInset: false,
            body: CustomWidget(
                title: 'wallet'.tr,
                child: Padding(
                  padding: EdgeInsets.only(top: 20.h, right: 20.w, left: 20.w),
                  child: _.load.value
                      ? RefreshIndicator(
                          onRefresh: () async {
                            await _.loadWalletTransactions();
                            _.loadBalance();
                          },
                          child: Column(children: [
                            Container(
                              height: Get.height / 4.5,
                              decoration: BoxDecoration(
                                color: whiteColor,
                                borderRadius:
                                    BorderRadius.all(Radius.circular(20.r)),
                              ),
                              child: QrImageView(
                                padding: EdgeInsets.all(20),
                                data: _.user.value.walletPublicKey!,
                                version: QrVersions.auto,
                                size: Get.height / 4.5,
                                gapless: false,
                              ),
                            ),
                            SizedBox(
                              height: 10.h,
                            ),
                            Container(
                                height: Get.height / 2.1,
                                padding: EdgeInsets.symmetric(
                                    horizontal: 10.w, vertical: 10.h),
                                decoration: BoxDecoration(
                                  color: whiteColor,
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(20.r)),
                                ),
                                child: Column(
                                  children: [
                                    Obx(() => CustomText(
                                          text: _.balance.toString() + 'LY'.tr,
                                          fontSize: 20.sp,
                                          fontWeight: FontWeight.bold,
                                          color: primaryColor,
                                        )),
                                    SizedBox(
                                      height: 10.h,
                                    ),
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        InkWell(
                                          onTap: () =>
                                              Get.toNamed(Routes.DEPOSIT),
                                          child: buttonWidget(
                                            icon: Icons.download_rounded,
                                            color: Colors.green,
                                            text: 'deposit'.tr,
                                          ),
                                        ),
                                        InkWell(
                                            onTap: () => _.transferChoose(),
                                            child: buttonWidget(
                                              icon: Icons.upload_rounded,
                                              color: Colors.red,
                                              text: 'transfer'.tr,
                                            )),
                                        InkWell(
                                          onTap: () =>
                                              Get.toNamed(Routes.TRANSACTIONS),
                                          child: buttonWidget(
                                            icon: Icons.list,
                                            color: Colors.black,
                                            text: 'record'.tr,
                                          ),
                                        ),
                                      ],
                                    ),
                                    SizedBox(
                                      height: 20.h,
                                    ),
                                    Expanded(
                                        child: Obx(
                                      () => _.transactions.isNotEmpty
                                          ? ListView.separated(
                                              padding: EdgeInsets.zero,
                                              separatorBuilder:
                                                  (BuildContext context,
                                                          int index) =>
                                                      SizedBox(
                                                        height: 10.h,
                                                      ),
                                              itemCount: _.transactions.length,
                                              itemBuilder:
                                                  (BuildContext context,
                                                      int index) {
                                                final _transaction =
                                                    _.transactions[index];
                                                List<String> splitList = _transaction.notes.split("**");
                                                String modifiedNote = splitList.last;
                                                return Column(
                                                  children: [
                                                    Row(
                                                      children: [
                                                        _transaction.walletAction ==
                                                                'DEPOSIT'
                                                            ? Icon(
                                                                Icons
                                                                    .download_rounded,
                                                                color: Colors
                                                                    .green,
                                                              )
                                                            : Icon(
                                                                Icons
                                                                    .upload_rounded,
                                                                color:
                                                                    Colors.red,
                                                              ),
                                                        CustomText(
                                                          text: _transaction
                                                                      .walletAction ==
                                                                  'DEPOSIT'
                                                              ? 'credit'.tr
                                                              : 'debit'.tr,
                                                          fontSize: 16.sp,
                                                          color: primaryColor,
                                                        ),
                                                        Spacer(),
                                                        CustomText(
                                                          text: _transaction
                                                                  .amount
                                                                  .toString() +
                                                              'LY'.tr,
                                                          fontSize: 16.sp,
                                                          color: primaryColor,
                                                        ),
                                                      ],
                                                    ),
                                                    Row(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .spaceBetween,
                                                        children: [
                                                          CustomText(
                                                            text: DateFormat(
                                                                    'yyyy-MM-dd',
                                                                    'en')
                                                                .format(_transaction
                                                                    .createdDate)
                                                                .toString(),
                                                            fontSize: 14.sp,
                                                            color: blackColor
                                                                .withOpacity(
                                                                    0.5),
                                                          ),
                                                          Container(
                                                            width: 200.w,
                                                            child: CustomText(
                                                              text: modifiedNote,
                                                              color: blackColor
                                                                  .withOpacity(
                                                                      0.5),
                                                              maxLines: 3,
                                                              textAlign:
                                                                  TextAlign.end,
                                                            ),
                                                          ),
                                                        ]),
                                                  ],
                                                );
                                              })
                                          : SizedBox(),
                                    ))
                                  ],
                                ))
                          ]))
                      : Center(child: CircularProgressIndicator()),
                ))));
  }

  Widget buttonWidget({icon, color, text}) {
    return Container(
      width: 100.w,
      height: 35.h,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20.r), color: secondaryColor),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Icon(
            icon,
            color: color,
          ),
          CustomText(
            text: text,
            fontSize: 16.sp,
            color: blackColor,
          ),
        ],
      ),
    );
  }
}
