import 'package:e_travel/app/data/models/customer.dart';
import 'package:e_travel/app/modules/wallet/controllers/wallet_controller.dart';
import 'package:e_travel/app/modules/widgets/custom_button.dart';
import 'package:e_travel/app/modules/widgets/custom_text.dart';
import 'package:e_travel/app/modules/widgets/custom_widget.dart';
import 'package:e_travel/app/util/consts/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:get/get.dart';

class TransferView extends GetView<WalletController> {
  final Customer customer;

  const TransferView({Key? key, required this.customer}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScopeNode currentFocus = FocusScope.of(context);
        if (!currentFocus.hasPrimaryFocus) currentFocus.unfocus();
      },
      child: Scaffold(
        body: SingleChildScrollView(
          child: CustomWidget(
            title: 'Transfer to another wallet'.tr,
            child: GetBuilder<WalletController>(
              init: WalletController(),
              builder: (_) {
                return Padding(
                  padding: EdgeInsets.symmetric(horizontal: 20.w),
                  child: Column(
                    children: [
                      SizedBox(
                        height: 45.h,
                      ),
                      Container(
                        height: 350.h,
                        width: Get.width,
                        padding: EdgeInsets.symmetric(horizontal: 20.w),
                        decoration: BoxDecoration(
                          color: whiteColor,
                          borderRadius: BorderRadius.all(Radius.circular(20.r)),
                        ),
                        child: Form(
                          key: _.transferForm,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              CustomText(
                                text: 'Transfer to'.tr + customer.name!,
                                color: blackColor,
                                fontSize: 20.sp,
                                fontWeight: FontWeight.bold,
                              ),
                              SizedBox(
                                height: 20.h,
                              ),
                              Row(
                                children: [
                                  Container(
                                    width: Get.width / 2,
                                    child: TextFormField(
                                      keyboardType: TextInputType.number,
                                      controller: _.otpController,
                                      maxLength: 4,
                                      validator: RequiredValidator(errorText: 'Please Enter verification code'.tr),
                                      decoration: InputDecoration(
                                        contentPadding: EdgeInsets.all(12),
                                        hintText: 'Please Enter verification code'.tr,
                                        fillColor: whiteColor,
                                        filled: true,
                                        hintStyle: TextStyle(color: primaryColor.withOpacity(0.7)),
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                    width: 5.w,
                                  ),
                                  Obx(
                                    () => controller.counter.value != 0
                                        ? Obx(() => Center(
                                                child: CustomText(
                                              text: controller.counter.value.toString() + ' ' + 'second'.tr,
                                              color: blackColor,
                                              fontWeight: FontWeight.bold,
                                              fontSize: 18.sp,
                                            )))
                                        : CustomButton(
                                            text: 'resend'.tr,
                                            width: Get.width / 4,
                                            height: 40.h,
                                            onClick: () {
                                              controller.sendOTP();
                                              controller.counter.value = 120;
                                              controller.startTimer();
                                            }),
                                  ),
                                ],
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              TextFormField(
                                keyboardType: TextInputType.number,
                                controller: _.amount,
                                maxLength: 5,
                                validator: RequiredValidator(errorText: 'Please enter the value'.tr),
                                decoration: InputDecoration(
                                  contentPadding: EdgeInsets.all(12),
                                  hintText: 'the value'.tr,
                                  fillColor: whiteColor,
                                  filled: true,
                                  hintStyle: TextStyle(color: primaryColor.withOpacity(0.7)),
                                ),
                              ),
                              SizedBox(height: 10.h),
                              CustomButton(
                                text: 'send'.tr,
                                color: primaryColor,
                                onClick: () =>
                                    _.getFeesByQR(customer.walletPublicKey!),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                );
              },
            ),
          ),
        ),
      ),
    );
  }
}
