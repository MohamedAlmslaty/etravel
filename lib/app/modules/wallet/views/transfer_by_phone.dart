import 'package:e_travel/app/modules/wallet/controllers/wallet_controller.dart';
import 'package:e_travel/app/modules/widgets/custom_button.dart';
import 'package:e_travel/app/modules/widgets/custom_text.dart';
import 'package:e_travel/app/util/consts/colors.dart';
import 'package:e_travel/app/util/consts/style.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:get/get.dart';
import 'package:intl_phone_field/intl_phone_field.dart';

class TransferByPhone extends GetView<WalletController> {
  const TransferByPhone({Key? key}) : super(key: key);

  List<Step> stepList() => [
        Step(
          title: CustomText(
            text: 'Enter a phone number'.tr,
            fontWeight: FontWeight.bold,
            fontSize: 20.sp,
            color: blackColor,
          ),
          subtitle: CustomText(
            text: 'Must be registered with the service'.tr,
            color: textColor,
          ),
          content: Form(
            key: controller.redeemForm,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                TextButton(
                    onPressed: () => controller.getPhoneNumber(),
                    style: TextButton.styleFrom(
                      fixedSize: Size.fromHeight(25),
                      padding: EdgeInsets.symmetric(horizontal: 10.w, vertical: 5.h),
                      backgroundColor: primaryColor,
                    ),
                    child: CustomText(
                      text: 'select phone'.tr,
                    )),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 15.w),
                  child: Directionality(
                    // add this
                    textDirection: TextDirection.ltr, // set this property
                    child: IntlPhoneField(
                      decoration: dialogInputStyle.copyWith(hintText: 'xxxxxxxx'.tr),
                      initialCountryCode: 'LY',
                      controller: controller.phoneNumber,
                      searchText: 'Search Country/region'.tr,
                      onSaved: (phone) {
                        controller.phoneNumberAll = phone!.completeNumber;
                        controller.phoneNumber.text = phone.number;
                      },
                    ),
                  ),
                ),
              ],
            ),
          ),
          isActive: controller.currentStep.value >= 0,
          state: controller.currentStep.value == 0 ? StepState.editing : StepState.complete,
        ),
        Step(
          title: CustomText(
            text: 'Enter the verification code'.tr,
            fontWeight: FontWeight.bold,
            fontSize: 20.sp,
            color: blackColor,
          ),
          subtitle: CustomText(
            text: 'You will receive an SMS'.tr,
            color: textColor,
          ),
          content: Form(
              key: controller.smsFormKey,
              child: Column(
                children: [
                  controller.customer.value.name != null
                      ? CustomText(
                          text: 'Transfer to'.tr + controller.customer.value.name!,
                          color: blackColor,
                          fontSize: 20.sp,
                          fontWeight: FontWeight.bold,
                        )
                      : SizedBox(),
                  SizedBox(
                    height: 20.h,
                  ),
                  Row(
                    children: [
                      Container(
                        width: Get.width / 2,
                        child: SizedBox(
                          height: Get.height * .095,
                          child: TextFormField(
                            keyboardType: TextInputType.number,
                            decoration: dialogInputStyle.copyWith(
                              hintText: 'Please Enter verification code'.tr,
                            ),
                            validator: RequiredValidator(errorText: 'Please Enter verification code'.tr),
                            maxLength: 4,
                            controller: controller.otpController,
                          ),
                        ),
                      ),
                      SizedBox(
                        width: 5.w,
                      ),
                      Obx(
                        () => controller.counter.value != 0
                            ? Obx(() => Center(
                                    child: CustomText(
                                  text: controller.counter.value.toString() + ' ' + 'second'.tr,
                                  color: blackColor,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 18.sp,
                                )))
                            : CustomButton(
                                text: 'resend'.tr,
                                width: Get.width / 4,
                                height: 40.h,
                                onClick: () {
                                  controller.sendOTP();
                                  controller.counter.value = 120;
                                  controller.startTimer();
                                }),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 10.h,
                  ),
                  SizedBox(
                    height: Get.height * .095,
                    child: TextFormField(
                      keyboardType: TextInputType.number,
                      decoration: dialogInputStyle.copyWith(
                        hintText: 'the value'.tr,
                      ),
                      maxLength: 5,
                      validator: RequiredValidator(errorText: 'Please enter the value'.tr),
                      controller: controller.amount,
                    ),
                  ),
                ],
              )),
          isActive: controller.currentStep.value >= 0,
          state: controller.currentStep.value == 0 ? StepState.editing : StepState.complete,
        ),
      ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: whiteColor,
        appBar: AppBar(
          centerTitle: true,
          backgroundColor: primaryColor,
          title: CustomText(
            text: 'Transfer to another wallet'.tr,
            color: whiteColor,
            fontSize: 22.sp,
          ),
          leading: IconButton(
            onPressed: () {
              Get.back();
              controller.currentStep.value = 0;
              // controller.timer.cancel();
              controller.phoneNumber.clear();
              controller.otpController.clear();
              controller.amount.clear();

            },
            icon: Icon(
              Icons.arrow_back,
              color: whiteColor,
            ),
          ),
        ),
        body: Theme(
          data: ThemeData(colorScheme: ColorScheme.light(primary: primaryColor)),
          child: Obx(
            () => Stepper(
                currentStep: controller.currentStep.value,
                steps: stepList(),
                controlsBuilder: (BuildContext context, ControlsDetails details) {
                  return Row(
                    children: <Widget>[
                      CustomButton(text: 'Confirm'.tr, width: 200.w, onClick: details.onStepContinue),
                    ],
                  );
                },
                onStepContinue: () {
                  if (controller.currentStep.value == 0) {
                    if (controller.redeemForm.currentState!.validate()) {
                      controller.redeemForm.currentState!.save();
                      controller.getCustomer();
                    }
                  } else {
                    if (controller.smsFormKey.currentState!.validate()) {
                      controller.smsFormKey.currentState!.save();
                      controller.getFeesByPhone();
                      // controller.transferByPhoneNumber();
                    }
                  }
                }),
          ),
        ));
  }
}
