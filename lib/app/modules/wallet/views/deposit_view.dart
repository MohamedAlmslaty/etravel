import 'package:e_travel/app/modules/payment/controllers/payment_controller.dart';
import 'package:e_travel/app/modules/payment/views/tadawal.dart';
import 'package:e_travel/app/modules/widgets/custom_button.dart';
import 'package:e_travel/app/modules/widgets/custom_text.dart';
import 'package:e_travel/app/routes/app_pages.dart';
import 'package:e_travel/app/util/consts/base.dart';
import 'package:e_travel/app/util/consts/colors.dart';
import 'package:e_travel/app/util/consts/style.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class DepositView extends GetView<PaymentController> {
  bool local = Get.find<GetStorage>().read('language') == 'ar';

  @override
  Widget build(BuildContext context) {
    return GetBuilder<PaymentController>(
      init: PaymentController(),
      initState: (_) {},
      builder: (_) {
        return GestureDetector(onTap:()=>FocusScope.of(context).unfocus(),child:Scaffold(
          backgroundColor: whiteColor,
          appBar: AppBar(
            title: Text('charging wallet'.tr),
          ),
          bottomNavigationBar: Container(
              width: Get.width,
              height: 70.h,
              color: whiteColor,
              child: Row(
                children: [
                  Container(
                      width: Get.width / 2,
                      height: 70.h,
                      padding: EdgeInsets.only(top: 10.h),
                      color: whiteColor,
                      child: Obx(() => _.selectPayment.value.id != null &&
                              _.selectPayment.value.paymentType == 'MASTERCARD'
                          ? CustomText(
                              text: 'Total'.tr +
                                  ' : ' +
                                  (((_.selectPayment.value.feePercentage! /
                                                  100 *
                                                  _.money.value) +
                                              _.money.value) /
                                          _.exchangeRate.value)
                                      .toStringAsFixed(3) +
                                  '\$'.tr,
                              fontWeight: FontWeight.bold,
                              fontSize: 18.sp,
                              textAlign: TextAlign.center,
                              color: blackColor,
                            )
                          : CustomText(
                              text: 'Total'.tr +
                                  ' : ' +
                                  '${(_.selectPayment.value.feePercentage! / 100 * _.money.value) + _.money.value}' +
                                  'LYD'.tr,
                              fontWeight: FontWeight.bold,
                              fontSize: 18.sp,
                              textAlign: TextAlign.center,
                              color: blackColor,
                            ))),
                  CustomButton(
                    onClick: () {
                      if (controller.amountFormKey.currentState!.validate()) {
                        controller.amountFormKey.currentState!.save();
                        if (_.selectPayment.value.id != null) {
                          if (_.selectPayment.value.paymentType == 'MOAMALAT') {
                            _.initPayMoamalat();
                          } else if (_.selectPayment.value.paymentType ==
                              'MASTERCARD') {
                            _.initPayMasterCard();
                          } else if (_.selectPayment.value.paymentType ==
                              'MIZA') {
                            Get.toNamed(Routes.UNIPAY);
                          } else if (_.selectPayment.value.paymentType ==
                              'SADAD') {
                            Get.toNamed(Routes.SADAD_PHONE);
                          } else if (_.selectPayment.value.paymentType ==
                              'TADAWUL') {
                            Get.to(TadawalView(type: true));
                          } else if (_.selectPayment.value.paymentType ==
                              'MOBICASH') {
                            Get.to(TadawalView(type: false));
                          } else
                            Get.toNamed(Routes.PAYMENT_METHOD);
                        } else {
                          showSnackBar(
                              title: 'warning'.tr,
                              message: 'Please choose a payment method'.tr);
                        }
                      } else {
                        showSnackBar(
                            title: 'warning'.tr, message: 'the value'.tr);
                      }
                    },
                    text: 'chosen'.tr,
                    height: 70.h,
                    width: Get.width / 2,
                    color: primaryColor,
                    colorText: whiteColor,
                    radiusBottomLeft: 0.r,
                    radiusBottomRight: 0.r,
                    radiusTopLeft: 0.r,
                    radiusTopRight: 0.r,
                  ),
                ],
              )),
          body: Padding(
            padding: EdgeInsets.symmetric(horizontal: 10.w),
            child: Column(
              children: [
                SizedBox(
                  height: 15.h,
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    CustomText(
                          text: 'POINTS'.tr,
                          color: blackColor,
                          textAlign: TextAlign.center,
                          fontSize: 20.sp,
                        ),

                    SizedBox(
                      width: 10.w,
                    ),
                    Expanded(
                        child: Form(
                            key: _.amountFormKey,
                            child: TextFormField(
                              keyboardType: TextInputType.number,
                                inputFormatters: [FilteringTextInputFormatter.digitsOnly],
                              maxLength: 5,
                              controller: controller.amount,
                              onChanged: (va) =>
                                  _.money.value = double.tryParse(va)!,
                              validator: RequiredValidator(
                                  errorText: 'Please enter the value'.tr),
                              decoration: InputDecoration(
                                contentPadding: const EdgeInsets.all(12),
                                counterText: "",
                                hintText:
                                    'Please enter the value of the points to be charged'
                                        .tr,
                                hintStyle: TextStyle(
                                    color: textColor.withOpacity(0.7),
                                    fontSize: 12.sp),
                                border: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color: textColor.withOpacity(0.2))),
                                focusedBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color: textColor.withOpacity(0.5))),
                                enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color: textColor.withOpacity(0.2))),
                              ),
                            )))
                  ],
                ),
                CustomText(
                  text:
                      'Please select the payment method you want to charge your wallet through'
                          .tr,
                  color: blackColor,
                  maxLines: 3,
                  fontSize: 20,
                  textAlign: TextAlign.center,
                ),
                Expanded(
                  child: _.load.value
                      ? ListView.builder(
                    keyboardDismissBehavior: ScrollViewKeyboardDismissBehavior.onDrag,
                          padding: EdgeInsets.only(top: 10.h),
                          itemCount: _.payment.length,
                          itemBuilder: (BuildContext context, int index) =>
                              Obx(() => InkWell(
                                    onTap: () {
                                      _.selectIndex.value = index;
                                      _.selectPayment.value = _.payment[index];
                                    },
                                    child: Card(
                                      elevation: 1,
                                      child: Container(
                                        color: _.selectIndex.value == index
                                            ? primaryColor.withOpacity(0.5)
                                            : Colors.white,
                                        child: ListTile(
                                          leading: Image.network(
                                            imageUrl(
                                                _.payment[index].imageUrl!),
                                            fit: BoxFit.scaleDown,
                                            width: 60,
                                          ),
                                          title: CustomText(
                                              text: local
                                                  ? '${_.payment[index].nameAr}'
                                                  : '${_.payment[index].nameEn}',
                                              fontSize: 18.sp,
                                              color: Colors.black),
                                          subtitle: CustomText(
                                              text: '${'fee'.tr} : '
                                                  '${_.payment[index].feePercentage} %',
                                              fontSize: 14.sp,
                                              color: Colors.grey),
                                          trailing: _.selectIndex.value == index
                                              ? Icon(
                                                  Icons.check,
                                                  color: secondaryColor,
                                                )
                                              : SizedBox(),
                                        ),
                                      ),
                                    ),
                                  )))
                      : Center(child: CircularProgressIndicator()),
                ),
              ],
            ),
          ),
        ));
      },
    );
  }
}
