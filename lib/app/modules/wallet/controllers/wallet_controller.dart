import 'dart:async';

import 'package:e_travel/app/data/models/customer.dart';
import 'package:e_travel/app/data/models/wallet.dart';
import 'package:e_travel/app/data/repositories/user_repository.dart';
import 'package:e_travel/app/data/repositories/wallet_repository.dart';
import 'package:e_travel/app/modules/wallet/views/transfer_view.dart';
import 'package:e_travel/app/modules/widgets/custom_text.dart';
import 'package:e_travel/app/routes/app_pages.dart';
import 'package:e_travel/app/util/consts/colors.dart';
import 'package:e_travel/app/util/consts/loading.dart';
import 'package:e_travel/app/util/consts/style.dart';
import 'package:flutter/material.dart';
import 'package:flutter_barcode_scanner/flutter_barcode_scanner.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fluttercontactpicker/fluttercontactpicker.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class WalletController extends GetxController {
  final _walletRepo = WalletRepository.instance;
  final transactions = <Wallet>[].obs;
  final load = false.obs;

  final _userRepo = UserRepository.instance;
  final user = Customer().obs;
  final customer = Customer().obs;
  final balance = 0.0.obs;
  final opa = 0.0.obs;
  final total = 0.0.obs;
  final _cartCount = 0.obs;
  RxInt currentStep = 0.obs;

  int get cartCount => _cartCount.value;

  late String qr;
  final amount = TextEditingController();
  final phoneNumber = TextEditingController();
  var phoneNumberAll = "";
  final redeemCode = TextEditingController();
  GlobalKey<FormState> redeemForm = new GlobalKey<FormState>();
  GlobalKey<FormState> transferForm = new GlobalKey<FormState>();
  final smsFormKey = new GlobalKey<FormState>();
  TextEditingController otpController = new TextEditingController();

  ///Timer
  RxInt counter = 120.obs;
  Timer? _timer;

  Timer get timer => _timer!;

  void startTimer() {
    const oneSec = const Duration(seconds: 1);
    _timer = new Timer.periodic(
      oneSec,
      (Timer timer) {
        if (counter.value == 0) {
          timer.cancel();
          update();
        } else {
          counter.value--;
        }
      },
    );
  }

  ///

  @override
  void onInit() async {
    currentStep = 0.obs;
    loadWalletTransactions();
    loadBalance();
    if (Get.find<GetStorage>().hasData('token')) {
      await getUser();
    }
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    currentStep = 0.obs;
    _timer!.cancel();
  }

  Future<Customer> getUser() async {
    user.value = await _userRepo.getCustomerAccount();
    update();
    return user.value;
  }

  Future<void> loadBalance() async {
    balance.value = await _walletRepo.getWalletBalance();
    update();
    if (balance.value != null && balance.value != "") {
      Get.find<GetStorage>().write('wallet_amount', balance.value);
      if (Get.find<GetStorage>().hasData('wallet_amount'))
        walletBalance.value = Get.find<GetStorage>().read('wallet_amount');
    }
  }

  qrScanner() async {
    qr = await FlutterBarcodeScanner.scanBarcode(
        "#004297", "cancel".tr, true, ScanMode.QR);

    if (qr != "-1") {
      getUserInfo(qr.toString());
    } else {
      return showSnackBar(title: 'error'.tr, message: 'Qr not Scanner'.tr);
    }
  }

  Future<void> getUserInfo(String qrCode) async {
    try {
      counter.value = 120;
      loading();
      await _userRepo.getCustomerByWalletPublicKey(qrCode).then((value) {
        if (value != null) {
          EasyLoading.dismiss();
          _walletRepo.getOTP();
          startTimer();
          user.value = value;
          Get.to(
            () => TransferView(
              customer: user.value,
            ),
          );
        } else {
          return showSnackBar(title: 'error'.tr, message: 'Qr not Scanned!'.tr);
        }
      });
    } catch (e) {
      throw e;
    }
  }

  transferAnotherWallet(total, String walletPublicKey) {
    if (transferForm.currentState!.validate()) {
      transferForm.currentState!.save();
      Get.defaultDialog(
        title: 'confirmation'.tr,
        backgroundColor: Color(0xffFEF200),
        content: Column(
          children: [
            Text('Are you sure to Confirm the transfer?'.tr),
            Text('will be deducted'.tr + total.toString() + 'point'.tr),
          ],
        ),
        cancel: ElevatedButton(
          onPressed: () {
            Get.back();
          },
          style: ElevatedButton.styleFrom(
            primary: Colors.red,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20.0), // Custom border radius
            ), // Custom background color for the Cancel button
          ),
          child: CustomText(text:"cancel".tr),
        ),
        confirm: ElevatedButton(
          onPressed: () async {
            loading();
            _walletRepo
                .walletTransfer(double.parse(amount.text), walletPublicKey,
                    otpController.text.trim())
                .then((value) {
              if (value) {
                EasyLoading.dismiss();
                loadBalance();
                _timer!.cancel();
                loadWalletTransactions();
                amount.clear();
                otpController.clear();
                Get.back();
                Get.back();
                update();
                showSnackBar(
                    title: 'successful'.tr,
                    message:
                        'Successfully transferred from your wallet to the customer is wallet'
                            .tr);
              }
            });
          },
          style: ElevatedButton.styleFrom(
              shape: RoundedRectangleBorder(
                borderRadius:
                    BorderRadius.circular(20.0), // Custom border radius
              ),
              primary: primaryColor),
          child: CustomText(text:"Save".tr),
        ),
      );
    }
  }

  transferChoose() {
    Get.defaultDialog(
      backgroundColor: Color(0xffFEF200),
      title: '',
      titleStyle: TextStyle(fontSize: 0),
      content: Column(
        children: [
          Image.asset(
            'assets/icons/transfer.png',
            width: 45.w,
            height: 45.w,
          ),
          CustomText(
            text: "Transfer to another wallet using".tr,
            color: primaryColor,
            fontWeight: FontWeight.bold,
            fontSize: 16.sp,
          ),
          SizedBox(
            height: 10.h,
          ),
          InkWell(
            onTap: () {
              Get.back();
              Get.toNamed(Routes.TRANSFER_BY_PHONE);
            },
            child: Container(
              padding: EdgeInsets.all(5),
              decoration: BoxDecoration(
                color: Color(0xffFFFBB2),
                borderRadius: BorderRadius.all(Radius.circular(50)),
              ),
              child: Row(
                children: [
                  Icon(
                    Icons.phone,
                    color: Color(0xff4F7DA1),
                  ),
                  SizedBox(
                    width: 20.w,
                  ),
                  CustomText(
                    text: 'phone number'.tr,
                    color: Color(0xff4F7DA1),
                  ),
                ],
              ),
            ),
          ),
          SizedBox(
            height: 20.h,
          ),
          InkWell(
              onTap: () {
                Get.back();
                qrScanner();
              },
              child: Container(
                  padding: EdgeInsets.all(5),
                  decoration: BoxDecoration(
                    color: Color(0xffFFFBB2),
                    borderRadius: BorderRadius.all(Radius.circular(50)),
                  ),
                  child: Row(
                    children: [
                      Icon(
                        Icons.qr_code,
                        color: Color(0xff4F7DA1),
                      ),
                      SizedBox(
                        width: 20.w,
                      ),
                      CustomText(
                        text: 'QR'.tr,
                        color: Color(0xff4F7DA1),
                      ),
                    ],
                  )))
        ],
      ),
    );
  }

  Future<void> sendOTP() async {
    await _walletRepo.getOTP();
  }

  Future<void> getCustomer() async {
    if (redeemForm.currentState!.validate()) {
      redeemForm.currentState!.save();
      loading();
      counter.value = 120;
      await _userRepo.getCustomerByPhone(phoneNumberAll.trim()).then((value) {
        if (value != null) {
          EasyLoading.dismiss();
          sendOTP();
          startTimer();
          customer.value = value;
          currentStep.value < 1 ? currentStep.value += 1 : null;
        }
      });
    }
  }

  Future<void> getFeesByPhone() async {
    loading();
    await _walletRepo
        .getTransferFee(
      double.parse(amount.text),
    )
        .then((value) {
      if (value[0].total != null) {
        EasyLoading.dismiss();
        transferByPhoneNumber(value[0].total);
      }
    });
  }

  Future<void> getFeesByQR(String walletPublicKey) async {
    loading();
    await _walletRepo
        .getTransferFee(
      double.parse(amount.text),
    )
        .then((value) {
      if (value[0].total != null) {
        EasyLoading.dismiss();
        transferAnotherWallet(value[0].total, walletPublicKey);
      }
    });
  }

  Future<void> transferByPhoneNumber(total) async {
    if (redeemForm.currentState!.validate()) {
      redeemForm.currentState!.save();
      Get.defaultDialog(
        title: 'confirmation'.tr,
        backgroundColor: Color(0xffFEF200),
        content: Column(
          children: [
            Text('Are you sure to Confirm the transfer?'.tr),
            Text('${total}  ' + 'point'.tr + ' ' + 'will be deducted'.tr),
          ],
        ),
        cancel: ElevatedButton(
          onPressed: () {
            Get.back();
          },
          style: ElevatedButton.styleFrom(
            primary: Colors.red,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20.0), // Custom border radius
            ), // Custom background color for the Cancel button
          ),
          child: CustomText(text:"cancel".tr),
        ),
        confirm: ElevatedButton(
          onPressed: () async {
            loading();
            await _walletRepo
                .walletTransferByPhoneNumber(double.parse(amount.text),
                    phoneNumberAll.trim(), otpController.text.trim())
                .then((value) {
              if (value) {
                EasyLoading.dismiss();
                currentStep.value = 0;
                _timer!.cancel();
                phoneNumber.clear();
                otpController.clear();
                amount.clear();
                Get.back();
                Get.back();
                loadBalance();
                loadWalletTransactions();
                showSnackBar(
                    title: 'successful'.tr,
                    message:
                        'Successfully transferred from your wallet to the customer is wallet'
                            .tr);
              }
            });
          },
          style: ElevatedButton.styleFrom(
              shape: RoundedRectangleBorder(
                borderRadius:
                    BorderRadius.circular(20.0), // Custom border radius
              ),
              primary: primaryColor),
          child: CustomText(text:"Save".tr),
        ),
      );
    }
  }

  getPhoneNumber() async {
    String phoneNum;
    String phoneNum1;
    if (await FlutterContactPicker.requestPermission(force: true)) {
      final PhoneContact contact =
          await FlutterContactPicker.pickPhoneContact();
      phoneNum = contact.phoneNumber!.number!.replaceAll('-', '');
      phoneNum1 = phoneNum.removeAllWhitespace;
      phoneNumber.text = phoneNum1.substring(phoneNum1.length - 9);
      phoneNumberAll = phoneNum1.substring(phoneNum1.length - 9);
    } else
      print(await FlutterContactPicker.hasPermission());
  }

  Future<void> loadWalletTransactions() async {
    final response = await _walletRepo.getWalletTransactions();
    transactions.assignAll(response);
    load.value = true;
    update();
  }

  void resend() {
    counter.value = 120;
    startTimer();
    _walletRepo.getOTP();
    //TODO::SEND OTP
  }
}
