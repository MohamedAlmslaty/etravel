import 'dart:async';
import 'dart:developer';

import 'package:e_travel/app/data/models/edfali.dart';
import 'package:e_travel/app/data/models/payment_method.dart';
import 'package:e_travel/app/data/models/query_models/query.dart';
import 'package:e_travel/app/data/models/sadad.dart';
import 'package:e_travel/app/data/models/tadawal.dart';
import 'package:e_travel/app/data/models/uni_pay.dart';
import 'package:e_travel/app/data/repositories/payment_methods_repository.dart';
import 'package:e_travel/app/data/repositories/setting_repository.dart';
import 'package:e_travel/app/modules/payment/views/moamalat_webview.dart';
import 'package:e_travel/app/modules/payment/views/webview.dart';
import 'package:e_travel/app/modules/wallet/controllers/wallet_controller.dart';
import 'package:e_travel/app/routes/app_pages.dart';
import 'package:e_travel/app/util/consts/loading.dart';
import 'package:e_travel/app/util/consts/style.dart';
import 'package:flutter/material.dart';
import 'package:flutter_barcode_scanner/flutter_barcode_scanner.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';

class PaymentController extends GetxController {
  final _paymentRepo = PaymentMethodsRepository.instance;
  final uniPay = UniPay().obs;
  final load = false.obs;
  RxInt currentStep = 0.obs;
  RxDouble exchangeRate = 0.0.obs;
  final _settingRepo = SettingRepository.instance;
  RxList<PaymentMethods> payment = <PaymentMethods>[].obs;
  final checkOutCont = PageController();
  final phoneFormKey = new GlobalKey<FormState>();
  final emailFormKey = new GlobalKey<FormState>();
  final uniFormKey = new GlobalKey<FormState>();
  final smsFormKey = new GlobalKey<FormState>();
  final amountFormKey = new GlobalKey<FormState>();
  TextEditingController phone = new TextEditingController();
  TextEditingController email = new TextEditingController();
  TextEditingController birthYear = new TextEditingController();
  TextEditingController otp = new TextEditingController();
  late TextEditingController amount;
  TextEditingController card = new TextEditingController();
  Rx<PaymentMethods> selectPayment = PaymentMethods().obs;
  GlobalKey<FormState> sadadForm = new GlobalKey<FormState>();
  GlobalKey<FormState> sadadOTPForm = new GlobalKey<FormState>();
  RxInt selectIndex = 900.obs;
  RxInt tId = 0.obs;
  RxDouble money = 0.0.obs;
  RxDouble restOfMoney = 0.0.obs;
  RxString sessionID = ''.obs;
  late String qr;

  ///Timer
  RxInt counter = 120.obs;
  Timer? _timer;

  Timer get timer => _timer!;

  void startTimer() {
    const oneSec = const Duration(seconds: 1);
    _timer = new Timer.periodic(
      oneSec,
      (Timer timer) {
        if (counter.value == 0) {
          timer.cancel();
          update();
        } else {
          counter.value--;
        }
      },
    );
  }

  ///
  @override
  void onInit() {
    super.onInit();
   if(Get.arguments!=null){ restOfMoney.value=Get.arguments+1;
   money.value =double.tryParse(restOfMoney.value.toStringAsFixed(0))!;
    amount = new TextEditingController(text: restOfMoney.value.toStringAsFixed(0));}
   else  amount = new TextEditingController();
    loadPayment();
    getSettingByKey();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}

  Future<void> loadPayment() async {
    final response = await _paymentRepo.getAllPaymentMethodPublic(
        isActiveQuery: IsActiveQuery(equals: true));
    payment.assignAll(response);
    load.value = true;
    update();
  }

  Future<void> getSettingByKey() async {
    await _settingRepo.getSetting('exchange_rate').then((value) {
      if (value.id != null) {
        exchangeRate.value = double.tryParse(value.value!)!;
      }
    });
  }

  Future<void> sendOTP() async {
    await _paymentRepo.getOTP();
  }

  qrScanner() async {
    qr = await FlutterBarcodeScanner.scanBarcode(
        "#004297", "cancel".tr, true, ScanMode.QR);

    if (qr != "-1") {
      card.text = qr.toString().trim();
    } else {
      return showSnackBar(title: 'error'.tr, message: 'QR not Scanner'.tr);
    }
  }

  Future<void> OTPEdfali() async {
    loading();
    await _paymentRepo
        .getOTPEdfali(amount.text.trim(), phone.text.trim())
        .then((value) {
      EasyLoading.dismiss();
      if (value.isNotEmpty && value != null) {
        sessionID.value = value;
        currentStep.value < 1 ? currentStep.value += 1 : null;
      } else {
        showSnackBar(
            title: 'error'.tr, message: 'Please Enter Valid phone number'.tr);
      }
    });
  }

  Future<void> OTPSadad() async {
    if (sadadForm.currentState!.validate()) {
      sadadForm.currentState!.save();
      loading();
      await _paymentRepo
          .getOTPSadad(phone.text.trim(), birthYear.text.trim(),
              double.tryParse(amount.text.trim())!)
          .then((value) {
        EasyLoading.dismiss();
        if (value.statusCode == 0) {
          startTimer();
          tId.value = value.result!.transactionId!;
          Get.toNamed(Routes.SADAD_OTP);
        } else {
          showSnackBar(
              title: 'error'.tr, message: 'the data enter not valid'.tr);
        }
      });
    }
    }

    Future<void> uniPayMethod() async {
      if (uniFormKey.currentState!.validate()) uniFormKey.currentState!.save();
      uniPay.value.amountToConsume = int.tryParse(amount.text.trim());
      uniPay.value.rechargeCode = card.text.trim();
      loading();
      await _paymentRepo.payUni(uniPay.value).then((value) {
        if (value) {
          EasyLoading.dismiss();
          Get.back();
          Get.back();
          Get.put(WalletController()).loadBalance();
          Get.put(WalletController()).loadWalletTransactions();
          showSnackBar(
              title: 'successful'.tr,
              message: 'Credit has been successfully charged'.tr);
        }
      });
    }


  Future<void> initPayMoamalat() async {
    loading();
    await _paymentRepo
        .initPay(amount.text.trim(), selectPayment.value.paymentType!)
        .then((value) {
      EasyLoading.dismiss();
      if (value.id != null) {
        log(value.transactionReference.toString());
        Get.to(MoamalatWebView(
          transactionReference: value.transactionReference!,
        ));
      } else {
        EasyLoading.dismiss();
        showSnackBar(title: 'error'.tr, message: 'something went wrong'.tr);
      }
    });
  }

  Future<void> initPayMasterCard() async {
    loading();
    String total = (((selectPayment.value.feePercentage! / 100 * money.value) +
                money.value) /
            exchangeRate.value)
        .toStringAsFixed(3);
    await _paymentRepo.payMasterCard(total).then((value) {
      EasyLoading.dismiss();
      if (value.paymentLink != null) {
        log(value.paymentLink.toString());
        Get.to(WebViewPayment(
          URL: value.paymentLink!,
          title: 'MasterCard'.tr,
        ));
      } else {
        EasyLoading.dismiss();
        showSnackBar(title: 'error'.tr, message: 'something went wrong'.tr);
      }
    });
  }

  Future<void> edfaliPay() async {
    Edfali edfali = Edfali(
        mobileNumber: phone.text.trim(),
        pin: otp.text.trim(),
        amount: int.tryParse(amount.text.trim()),
        sessionID: sessionID.value);
    loading();
    await _paymentRepo.payEdfali(edfali).then((value) {
      if (value) {
        EasyLoading.dismiss();
        Get.back();
        Get.back();
        Get.put(WalletController()).loadBalance();
        Get.put(WalletController()).loadWalletTransactions();
        showSnackBar(
            title: 'successful'.tr,
            message: 'Balance has been recharged by Edfali successfully'.tr);
      }
    });
  }

  Future<void> sadadPay() async {
    Sadad sadad = Sadad(
      code: otp.text.trim(),
      sadadTransactionId: tId.value,
    );
    loading();
    await _paymentRepo.paySadad(sadad).then((value) {
      EasyLoading.dismiss();
      if (value) {
        Get.toNamed(Routes.SUCCESS_PAY);
      } else {
        showSnackBar(title: 'error'.tr, message: 'Please Enter Valid otp'.tr);
      }
    });
  }

  Future<void> goToWebView(bool type) async {
    if (phoneFormKey.currentState!.validate()) {
      phoneFormKey.currentState!.save();
      Tadawal tadawal = Tadawal(
        amount: double.tryParse(amount.text.trim()),
        customerEmail: email.text.trim(),
        customerPhone: phone.text.trim(),
      );
      loading();
      type
          ? await _paymentRepo.payTadawal(tadawal).then((value) {
              EasyLoading.dismiss();
              if (value != null) {
                Get.to(WebViewPayment(URL: value, title: 'tadawal online'.tr));
              } else {
                showSnackBar(
                    title: 'error'.tr, message: 'Please Enter Valid data'.tr);
              }
            })
          : await _paymentRepo.payMobicash(tadawal).then((value) {
              EasyLoading.dismiss();
              if (value != null) {
                Get.to(WebViewPayment(
                  URL: value,
                  title: 'mobiCash'.tr,
                ));
              } else {
                showSnackBar(
                    title: 'error'.tr, message: 'Please Enter Valid data'.tr);
              }
            });
    }
  }

  Future<void> GoToWallet() async {
    Get.put(WalletController()).loadBalance();
    Get.put(WalletController()).loadWalletTransactions();
    Get.offAllNamed(Routes.WALLET);
  }
}
