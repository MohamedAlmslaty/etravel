import 'package:e_travel/app/modules/payment/controllers/payment_controller.dart';
import 'package:e_travel/app/modules/widgets/custom_button.dart';
import 'package:e_travel/app/modules/widgets/custom_text.dart';
import 'package:e_travel/app/util/consts/colors.dart';
import 'package:flutter/material.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:get/get.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class PaymentMethod extends GetView<PaymentController> {
  const PaymentMethod({Key? key}) : super(key: key);

  List<Step> stepList() => [
        Step(
          title: CustomText(
            text: 'Enter a phone number'.tr,
            fontWeight: FontWeight.bold,
            fontSize: 20.sp,
            color: blackColor,
          ),
          subtitle: CustomText(
            text: 'Must be registered with the service'.tr,
            color: textColor,
          ),
          content: Form(
              key: controller.phoneFormKey,
              child: Column(
                children: [
                  TextFormField(
                    keyboardType: TextInputType.number,
                    maxLength: 10,
                    controller: controller.phone,
                    validator: MultiValidator([
                      RequiredValidator(errorText: 'Please enter phone'.tr),
                      MinLengthValidator(10,
                          errorText: 'Please Enter Valid phone number'.tr),
                      // PatternValidator(
                      //   r'^(9?(9[09]{8}))$',
                      //   errorText: 'Please Enter Valid phone number'.tr,
                      // )
                    ]),
                    decoration: InputDecoration(
                      contentPadding: const EdgeInsets.all(12),
                      hintText: 'phone_ex'.tr,
                      hintStyle: TextStyle(color: textColor.withOpacity(0.7)),
                      prefixIcon: Icon(Icons.phone, color: primaryColor),
                      border: OutlineInputBorder(
                          borderSide:
                              BorderSide(color: textColor.withOpacity(0.2))),
                      focusedBorder: OutlineInputBorder(
                          borderSide:
                              BorderSide(color: textColor.withOpacity(0.5))),
                      enabledBorder: OutlineInputBorder(
                          borderSide:
                              BorderSide(color: textColor.withOpacity(0.2))),
                    ),
                  ),
                ],
              )),
          isActive: controller.currentStep.value >= 0,
          state: controller.currentStep.value == 0
              ? StepState.editing
              : StepState.complete,
        ),
        Step(
          title: CustomText(
            text: 'Enter the verification code'.tr,
            fontWeight: FontWeight.bold,
            fontSize: 20.sp,
            color: blackColor,
          ),
          subtitle: CustomText(
            text: 'You will receive an SMS'.tr,
            color: textColor,
          ),
          content: Form(
              key: controller.smsFormKey,
              child: Column(
                children: [
                  TextFormField(
                    keyboardType: TextInputType.number,
                    maxLength: 6,
                    controller: controller.otp,
                    validator: RequiredValidator(
                        errorText: 'Please Enter verification code'.tr),
                    decoration: InputDecoration(
                      contentPadding: const EdgeInsets.all(12),
                      hintText: 'XXXXXX'.tr,
                      hintStyle: TextStyle(color: textColor.withOpacity(0.7)),
                      prefixIcon: Icon(Icons.pin, color: primaryColor),
                      border: OutlineInputBorder(
                          borderSide:
                              BorderSide(color: textColor.withOpacity(0.2))),
                      focusedBorder: OutlineInputBorder(
                          borderSide:
                              BorderSide(color: textColor.withOpacity(0.5))),
                      enabledBorder: OutlineInputBorder(
                          borderSide:
                              BorderSide(color: textColor.withOpacity(0.2))),
                    ),
                  ),
                ],
              )),
          isActive: controller.currentStep.value >= 0,
          state: controller.currentStep.value == 0
              ? StepState.editing
              : StepState.complete,
        ),
      ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: whiteColor,
        appBar: AppBar(
          centerTitle: true,
          backgroundColor: primaryColor,
          title: CustomText(
            text: 'edfali'.tr,
            color: whiteColor,
            fontSize: 22.sp,
          ),
        ),
        bottomNavigationBar: InkWell(
          onTap: () => help(controller),
          child: Container(
            width: Get.width,
            height: 100.h,
            child: CustomText(
              text: 'help'.tr,
              fontSize: 22.sp,
              fontWeight: FontWeight.bold,
              textAlign: TextAlign.center,
              color: blackColor,
            ),
          ),
        ),
        body: Theme(
          data:
              ThemeData(colorScheme: ColorScheme.light(primary: primaryColor)),
          child: Obx(
            () => Stepper(
                currentStep: controller.currentStep.value,
                steps: stepList(),
                controlsBuilder:
                    (BuildContext context, ControlsDetails details) {
                  return Row(
                    children: <Widget>[
                      CustomButton(
                          text: 'Confirm'.tr,
                          width: 200.w,
                          onClick: details.onStepContinue),
                    ],
                  );
                },
                onStepContinue: () {
                  if (controller.currentStep.value == 0) {
                    if (controller.phoneFormKey.currentState!.validate()) {
                      controller.phoneFormKey.currentState!.save();
                      controller.OTPEdfali();
                    }
                  } else {
                    if (controller.smsFormKey.currentState!.validate()) {
                      controller.smsFormKey.currentState!.save();
                      controller.edfaliPay();
                    }
                  }
                }),
          ),
        ));
  }

  void help(PaymentController _con) {
    Get.bottomSheet(
      Container(
        height: Get.height / 2,
        child: Padding(
          padding: const EdgeInsets.all(28.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              CustomText(
                text: 'Please follow the following steps'.tr,
                fontSize: 20.sp,
                fontWeight: FontWeight.bold,
                color: primaryColor,
              ),
              Divider(),
              CustomText(
                text: 'help_1'.tr,
                fontSize: 16.sp,
                maxLines: 3,
                color: primaryColor,
              ),
              CustomText(
                text: 'help2'.tr,
                fontSize: 16.sp,
                maxLines: 3,
                color: primaryColor,
              ),
              CustomText(
                text: 'help3'.tr,
                fontSize: 16.sp,
                maxLines: 3,
                color: primaryColor,
              ),
            ],
          ),
        ),
      ),
      elevation: 20.0,
      enableDrag: false,
      backgroundColor: whiteColor,
    );
  }
}
