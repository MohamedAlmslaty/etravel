import 'package:e_travel/app/modules/widgets/custom_button.dart';
import 'package:e_travel/app/util/consts/base.dart';
import 'package:e_travel/app/modules/widgets/custom_text.dart';
import 'package:e_travel/app/util/consts/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

import '../controllers/payment_controller.dart';

class PaymentView extends GetView<PaymentController> {
  bool local=Get.find<GetStorage>().read('language')=='ar';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: whiteColor,
      appBar: AppBar(
        title: Text('PaymentMethod'.tr),
      ),

      body: Column(
        children: [
          GetBuilder<PaymentController>(
            init: PaymentController(),
            initState: (_) {},
            builder: (_) {
              return Expanded(
                child: _.load.value
                    ? Padding(
                    padding: EdgeInsets.symmetric(horizontal: 6.h),
                    child: ListView.builder(
                        padding: EdgeInsets.only(top: 10.h),
                        itemCount: _.payment.length,
                        itemBuilder: (BuildContext context, int index) =>
                            Obx(() => InkWell(
                              onTap: () {
                                _.selectIndex.value = index;
                                _.selectPayment.value =
                                _.payment[index];
                              },
                              child: Card(
                                elevation: 1,
                                child: Container(
                                  color: _.selectIndex.value == index
                                      ? primaryColor.withOpacity(0.5)
                                      : Colors.white,
                                  child: ListTile(
                                    leading: Image.network(
                                      imageUrl(
                                          _.payment[index].imageUrl!),
                                      fit: BoxFit.scaleDown,
                                      width: 60,
                                    ),
                                    title: CustomText(
                                        text:local
                                        ?'${_.payment[index].nameAr} '
                                        :'${_.payment[index].nameEn} ',
                                        fontSize: 18.sp,
                                        color: Colors.black),
                                    trailing:
                                    _.selectIndex.value == index
                                        ? Icon(
                                      Icons.check,
                                      color: secondaryColor,
                                    )
                                        : SizedBox(),
                                  ),
                                ),
                              ),
                            ))))
                    : Center(child: CircularProgressIndicator()),
              );
            },
          )
        ],
      ),
    );
  }
}
