import 'package:e_travel/app/modules/payment/controllers/payment_controller.dart';
import 'package:e_travel/app/modules/widgets/custom_text.dart';
import 'package:e_travel/app/util/consts/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:webview_flutter/webview_flutter.dart';

import '../../../routes/app_pages.dart';

class WebViewPayment extends GetView<PaymentController> {
  const WebViewPayment({Key? key, required this.URL, required this.title})
      : super(key: key);
  final String URL;
  final String title;

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
        backgroundColor: whiteColor,
        appBar: AppBar(
          automaticallyImplyLeading: false,
          centerTitle: true,
          leading: IconButton(
            icon: Icon(Icons.arrow_back_ios_new),
            onPressed: () => Get.back(),
          ),
          backgroundColor: primaryColor,
          title: CustomText(
            text: title,
            color: whiteColor,
            fontSize: 22.sp,
          ),
        ),
        resizeToAvoidBottomInset: false,
        body: Container(
          decoration: BoxDecoration(
            color: whiteColor,
          ),
          child: SafeArea(
            child: WebView(
              javascriptMode: JavascriptMode.unrestricted,
              initialUrl: URL,
            ),
          ),
        ),
      ),
    );
  }
}
