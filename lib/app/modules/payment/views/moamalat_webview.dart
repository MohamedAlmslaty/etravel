import 'package:e_travel/app/modules/payment/controllers/payment_controller.dart';
import 'package:e_travel/app/modules/widgets/custom_text.dart';
import 'package:e_travel/app/util/consts/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:webview_flutter/webview_flutter.dart';

import '../../../routes/app_pages.dart';
import '../../../util/consts/base.dart';

class MoamalatWebView extends GetView<PaymentController> {
  const MoamalatWebView({Key? key, required this.transactionReference}) : super(key: key);
  final String transactionReference;

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
        backgroundColor: whiteColor,
        appBar: AppBar(
          automaticallyImplyLeading: false,
          centerTitle: true,
          backgroundColor: primaryColor,
          title: CustomText(
            text: 'MOAMALAT'.tr,
            color: whiteColor,
            fontSize: 22.sp,
          ),
          actions: [IconButton(onPressed: () => Get.offNamedUntil(Routes.WALLET, ModalRoute.withName('/main')), icon: Icon(Icons.check))],
        ),
        resizeToAvoidBottomInset: false,
        body: Container(
          decoration: BoxDecoration(
            color: whiteColor,
          ),
          child: SafeArea(
            child: WebView(
              javascriptMode: JavascriptMode.unrestricted,
              initialUrl: URL + '#/payment-method/moamalat-gateway?transactionReference=$transactionReference',
            ),
          ),
        ),
      ),
    );
  }
}
