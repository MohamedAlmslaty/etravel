import 'package:e_travel/app/modules/payment/controllers/payment_controller.dart';
import 'package:e_travel/app/modules/widgets/custom_button.dart';
import 'package:e_travel/app/modules/widgets/custom_text.dart';
import 'package:e_travel/app/util/consts/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:get/get.dart';

class UniPayView extends GetView<PaymentController> {
  const UniPayView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: whiteColor,
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: primaryColor,
        title: CustomText(
          text: 'unipay'.tr,
          color: whiteColor,
          fontSize: 22.sp,
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 8.0),
        child: Form(
            key: controller.uniFormKey,
            child: Column(
              children: [
                SizedBox(
                  height: 30,
                ),
                InkWell(
                    onTap: () {
                      controller.qrScanner();
                    },
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(Icons.qr_code),
                        SizedBox(
                          width: 20.w,
                        ),
                        CustomText(
                          text: 'QR'.tr,
                          color: blackColor,
                        ),
                      ],
                    )),
                TextFormField(
                  keyboardType: TextInputType.number,
                  maxLength: 14,
                  controller: controller.card,
                  //onSaved: (input) => controller.uniPay.value.rechargeCode = input!.trim(),
                  validator: MultiValidator([
                    RequiredValidator(
                        errorText: 'Please enter rechargeCode'.tr),
                    MinLengthValidator(14,
                        errorText: 'Please Enter Valid rechargeCode'.tr),
                  ]),
                  decoration: InputDecoration(
                    contentPadding: const EdgeInsets.all(12),
                    hintText: 'rechargeCode_ex'.tr,
                    hintStyle: TextStyle(color: textColor.withOpacity(0.7)),
                    prefixIcon: Icon(Icons.credit_card, color: primaryColor),
                    border: OutlineInputBorder(
                        borderSide:
                            BorderSide(color: textColor.withOpacity(0.2))),
                    focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: textColor.withOpacity(0.5))),
                    enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: textColor.withOpacity(0.2))),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                TextFormField(
                  keyboardType: TextInputType.number,
                  //maxLength: 6,
                  onSaved: (input) => controller.uniPay.value.codePin = int.tryParse(input!.trim()),
                  validator: MultiValidator([
                    RequiredValidator(errorText: 'Please enter codePin'.tr),
                    MaxLengthValidator(6, errorText: 'Please Enter Valid codePin'.tr),
                    MinLengthValidator(4, errorText: 'Please Enter Valid codePin'.tr),
                  ]),
                  decoration: InputDecoration(
                    contentPadding: const EdgeInsets.all(12),
                    hintText: 'codePin_ex'.tr,
                    hintStyle: TextStyle(color: textColor.withOpacity(0.7)),
                    prefixIcon: Icon(Icons.pin, color: primaryColor),
                    border: OutlineInputBorder(borderSide: BorderSide(color: textColor.withOpacity(0.2))),
                    focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: textColor.withOpacity(0.5))),
                    enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: textColor.withOpacity(0.2))),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                CustomButton(text: 'deposit'.tr, width: 200.w, onClick: () => controller.uniPayMethod()),
              ],
            )),
      ),
    );
  }
}
