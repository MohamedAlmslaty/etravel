import 'package:e_travel/app/modules/payment/controllers/payment_controller.dart';
import 'package:e_travel/app/modules/widgets/custom_button.dart';
import 'package:e_travel/app/modules/widgets/custom_text.dart';
import 'package:e_travel/app/util/consts/colors.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:e_travel/app/modules/widgets/custom_widget.dart';

class SuccessPayment extends GetView<PaymentController> {
  SuccessPayment();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: CustomWidget(
            title: 'sadad'.tr,
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 20.w,vertical: 45.h),
              child: Container(
              height: Get.height / 2,
              width: Get.width,
              padding: EdgeInsets.symmetric(horizontal: 20.w),
              decoration: BoxDecoration(
              color: whiteColor,
              borderRadius: BorderRadius.all(Radius.circular(20.r)),
              ),
              child:Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  CustomText(
                    text: 'Payment completed successfully'.tr,
                    color: primaryColor,
                    textAlign: TextAlign.center,
                    maxLines: 3,
                    fontSize: 18.sp,
                  ),
                  SizedBox(
                    height: 30.h,
                  ),
                  Image(
                    image: AssetImage('assets/images/success.gif'),
                    height: 150.0.h,
                  ),
                  SizedBox(
                    height: 30.h,
                  ),
                  CustomButton(
                    text: 'Go to Wallet'.tr,
                    color: primaryColor,
                    colorText: secondaryColor,
                    onClick: () => controller.GoToWallet(),
                  ),
                ],
              ),
            ))));
  }
}
