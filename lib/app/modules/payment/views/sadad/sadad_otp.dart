import 'package:e_travel/app/modules/payment/controllers/payment_controller.dart';
import 'package:e_travel/app/modules/widgets/custom_button.dart';
import 'package:e_travel/app/modules/widgets/custom_text.dart';
import 'package:e_travel/app/modules/widgets/custom_widget.dart';
import 'package:e_travel/app/util/consts/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:get/get.dart';

class SadadOTPView extends GetView<PaymentController> {
  const SadadOTPView({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScopeNode currentFocus = FocusScope.of(context);
        if (!currentFocus.hasPrimaryFocus) currentFocus.unfocus();
      },
      child: Scaffold(
        body: SingleChildScrollView(
          child: CustomWidget(
            title: 'sadad'.tr,
            child: GetBuilder<PaymentController>(
              init: PaymentController(),
              builder: (_) {
                return Padding(
                  padding: EdgeInsets.symmetric(horizontal: 20.w),
                  child: Column(
                    children: [
                      SizedBox(
                        height: 45.h,
                      ),
                      Container(
                        height: Get.height / 1.5,
                        width: Get.width,
                        padding: EdgeInsets.symmetric(horizontal: 20.w),
                        decoration: BoxDecoration(
                          color: whiteColor,
                          borderRadius: BorderRadius.all(Radius.circular(20.r)),
                        ),
                        child: Form(
                          key: _.sadadOTPForm,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              CustomText(
                                text: 'Payment details'.tr,
                                color: primaryColor,
                                fontSize: 20.sp,
                                fontWeight: FontWeight.bold,
                              ),
                              SizedBox(
                                height: 20.h,
                              ),
                              CustomText(
                                text: 'Store ID'.tr + ' : ' + '011831'.tr,
                                fontSize: 17.sp,
                                color: Colors.black,
                              ),
                              CustomText(
                                text: 'Store name'.tr + ' : ' + 'etravel'.tr,
                                fontSize: 17.sp,
                                color: Colors.black,
                              ),
                              CustomText(
                                text: 'amount'.tr + ' : ${_.money.value}' + 'LYD'.tr,
                                fontSize: 17.sp,
                                color: Colors.black,
                              ),
                              CustomText(
                                text: 'Total'.tr + ' : ${(_.selectPayment.value.feePercentage! / 100 * _.money.value) + _.money.value}' + 'LYD'.tr,
                                fontSize: 17.sp,
                                color: Colors.black,
                              ),
                              SizedBox(height: 10.h),
                              TextFormField(
                                keyboardType: TextInputType.number,
                                maxLength: 6,
                                controller: controller.otp,
                                validator: RequiredValidator(errorText: 'Please Enter verification code'.tr),
                                decoration: InputDecoration(
                                  contentPadding: const EdgeInsets.all(12),
                                  hintText: 'XXXXXX'.tr,
                                  hintStyle: TextStyle(color: textColor.withOpacity(0.7)),
                                  prefixIcon: Icon(Icons.pin, color: primaryColor),
                                  border: OutlineInputBorder(borderSide: BorderSide(color: textColor.withOpacity(0.2))),
                                  focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: textColor.withOpacity(0.5))),
                                  enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: textColor.withOpacity(0.2))),
                                ),
                              ),
                              Obx(
                                    () => controller.counter.value != 0
                                    ? Obx(() => Center(
                                            child: CustomText(
                                          text: 'You can request to re-send the verification code after'.tr + ' ' + _.counter.value.toString() + ' ' + 'second'.tr,
                                          color: primaryColor,
                                          maxLines: 3,
                                          fontWeight: FontWeight.bold,
                                          fontSize: 14.sp,
                                        )))
                                    : CustomButton(
                                        text: 'resend'.tr,
                                        width: Get.width / 4,
                                        height: 40.h,
                                        onClick: () {
                                          _.OTPSadad();
                                          _.counter.value = 120;
                                          _.startTimer();
                                        }),
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              CustomButton(
                                text: 'deposit'.tr,
                                color: Colors.green,
                                onClick: () => _.sadadPay(),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                );
              },
            ),
          ),
        ),
      ),
    );
  }
}
