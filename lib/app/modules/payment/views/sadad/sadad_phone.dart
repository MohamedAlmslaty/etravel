import 'package:e_travel/app/modules/payment/controllers/payment_controller.dart';
import 'package:e_travel/app/modules/widgets/custom_button.dart';
import 'package:e_travel/app/modules/widgets/custom_text.dart';
import 'package:e_travel/app/modules/widgets/custom_widget.dart';
import 'package:e_travel/app/util/consts/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:get/get.dart';

class SadadPhoneView extends GetView<PaymentController> {
  const SadadPhoneView({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScopeNode currentFocus = FocusScope.of(context);
        if (!currentFocus.hasPrimaryFocus) currentFocus.unfocus();
      },
      child: Scaffold(
        body: SingleChildScrollView(
          child: CustomWidget(
            title: 'sadad'.tr,
            child: GetBuilder<PaymentController>(
              init: PaymentController(),
              builder: (_) {
                return Padding(
                  padding: EdgeInsets.symmetric(horizontal: 20.w),
                  child: Column(
                    children: [
                      SizedBox(
                        height: 45.h,
                      ),
                      Container(
                        height: Get.height / 1.5,
                        width: Get.width,
                        padding: EdgeInsets.symmetric(horizontal: 20.w),
                        decoration: BoxDecoration(
                          color: whiteColor,
                          borderRadius: BorderRadius.all(Radius.circular(20.r)),
                        ),
                        child: Form(
                          key: _.sadadForm,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              CustomText(
                                text: 'Payment details'.tr,
                                color: primaryColor,
                                fontSize: 20.sp,
                                fontWeight: FontWeight.bold,
                              ),
                              SizedBox(
                                height: 20.h,
                              ),
                              CustomText(
                                text: 'Store ID'.tr + ' : ' + '011831'.tr,
                                fontSize: 17.sp,
                                color: Colors.black,
                              ),
                              CustomText(
                                text: 'Store name'.tr + ' : ' + 'etravel'.tr,
                                fontSize: 17.sp,
                                color: Colors.black,
                              ),
                              CustomText(
                                text: 'amount'.tr + ' : ${_.money.value}' + 'LYD'.tr,
                                fontSize: 17.sp,
                                color: Colors.black,
                              ),
                              CustomText(
                                text: 'Total'.tr + ' : ${(_.selectPayment.value.feePercentage! / 100 * _.money.value) + _.money.value}' + 'LYD'.tr,
                                fontSize: 17.sp,
                                color: Colors.black,
                              ),
                              SizedBox(height: 10.h),
                              CustomText(text: 'phone number'.tr,color: primaryColor,),
                              TextFormField(
                                keyboardType: TextInputType.number,
                                maxLength: 10,
                                controller: controller.phone,
                                validator: MultiValidator([
                                  RequiredValidator(
                                      errorText: 'Please enter phone'.tr),
                                  MinLengthValidator(10,
                                      errorText:
                                          'Please Enter Valid phone number'.tr),
                                  PatternValidator(
                                    r'^(091?([0-9]{7}))$',
                                    errorText:
                                        'Please Enter Valid phone number'.tr,
                                  )
                                ]),
                                decoration: InputDecoration(
                                  contentPadding: const EdgeInsets.all(12),
                                  hintText: '091xxxxxx'.tr,
                                  hintStyle: TextStyle(color: textColor.withOpacity(0.7)),
                                  prefixIcon: Icon(Icons.phone, color: primaryColor),
                                  border: OutlineInputBorder(borderSide: BorderSide(color: textColor.withOpacity(0.2))),
                                  focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: textColor.withOpacity(0.5))),
                                  enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: textColor.withOpacity(0.2))),
                                ),
                              ),
                              SizedBox(
                                width: 5.w,
                              ),
                              CustomText(text: 'Birth year'.tr,color: primaryColor,),

                              TextFormField(
                                keyboardType: TextInputType.number,
                                maxLength: 4,
                                controller: controller.birthYear,
                                validator: MultiValidator([
                                  RequiredValidator(errorText: 'Please Enter year of birth'.tr),
                                ]),
                                decoration: InputDecoration(
                                  contentPadding: const EdgeInsets.all(12),
                                  hintText: 'birth_ex'.tr,
                                  hintStyle: TextStyle(color: textColor.withOpacity(0.7)),
                                  prefixIcon: Icon(Icons.date_range_outlined, color: primaryColor),
                                  border: OutlineInputBorder(borderSide: BorderSide(color: textColor.withOpacity(0.2))),
                                  focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: textColor.withOpacity(0.5))),
                                  enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: textColor.withOpacity(0.2))),
                                ),
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              CustomButton(
                                text: 'send'.tr,
                                color: primaryColor,
                                onClick: () => _.OTPSadad(),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                );
              },
            ),
          ),
        ),
      ),
    );
  }
}
