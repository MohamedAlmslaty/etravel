import 'package:e_travel/app/modules/payment/controllers/payment_controller.dart';
import 'package:e_travel/app/modules/widgets/custom_button.dart';
import 'package:e_travel/app/modules/widgets/custom_text.dart';
import 'package:e_travel/app/modules/widgets/custom_widget.dart';
import 'package:e_travel/app/util/consts/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:get/get.dart';

class TadawalView extends GetView<PaymentController> {
  final bool type;

  const TadawalView({Key? key, required this.type}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<PaymentController>(
      init: PaymentController(),
      initState: (_) {},
      builder: (_) => Scaffold(
        resizeToAvoidBottomInset: false,
        body: CustomWidget(
          title: type?'tadawal online'.tr:'mobiCash'.tr,
          child: Padding(
            padding: EdgeInsets.only(top: 20.h, right: 20.w, left: 20.w),
            child: _.load.value
                ? Column(children: [
                    SizedBox(
                      height: 49.h,
                    ),
                    SvgPicture.asset(
                      'assets/images/account.svg',
                      width: 81.w,
                      height: 81.w,
                      fit: BoxFit.fill,
                      color: primaryColor,
                    ),
                    Form(
                        key: _.phoneFormKey,
                        child: Column(
                          children: [
                            SizedBox(
                              height: 10.h,
                            ),
                            CustomText(
                              text: type
                                  ? 'Please enter your account information in the online tadawal service'
                                      .tr
                                  : 'Please enter your account information in the Mobicash service'
                                      .tr,
                              fontWeight: FontWeight.bold,
                              fontSize: 21.sp,
                              maxLines: 3,
                              textAlign: TextAlign.center,
                              color: primaryColor,
                            ),
                            SizedBox(
                              height: 10.h,
                            ),
                            TextFormField(
                              keyboardType: TextInputType.number,
                              maxLength: 10,
                              controller: controller.phone,
                              validator: MultiValidator([
                                RequiredValidator(
                                    errorText: 'Please enter phone'.tr),
                                MinLengthValidator(10,
                                    errorText:
                                        'Please Enter Valid phone number'.tr),
                                PatternValidator(
                                  r'^(09?((1|2|4)[0-9]{7}))$',
                                  errorText:
                                      'Please Enter Valid phone number'.tr,
                                )
                              ]),
                              decoration: InputDecoration(
                                contentPadding: const EdgeInsets.all(12),
                                fillColor: whiteColor,
                                filled: true,
                                hintText: '09xxxxxxx'.tr,
                                hintStyle: TextStyle(
                                    color: textColor.withOpacity(0.7)),
                                prefixIcon:
                                    Icon(Icons.phone, color: primaryColor),
                                border: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color: textColor.withOpacity(0.2))),
                                focusedBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color: textColor.withOpacity(0.5))),
                                enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color: textColor.withOpacity(0.2))),
                              ),
                            ),
                            SizedBox(
                              height: 10.h,
                            ),
                            TextFormField(
                              keyboardType: TextInputType.emailAddress,
                              controller: controller.email,
                              validator: (value) {
                                bool emailValid = RegExp(
                                        r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
                                    .hasMatch(value!);
                                if (value == null || value.isEmpty) {
                                  return "Please enter the email".tr;
                                } else if (emailValid == false) {
                                  return "Please enter a valid email".tr;
                                }
                                return null;
                              },
                              inputFormatters: [
                                FilteringTextInputFormatter.deny(
                                    new RegExp(r"\s\b|\b\s"))
                              ],
                              decoration: InputDecoration(
                                contentPadding: const EdgeInsets.all(12),
                                fillColor: whiteColor,
                                filled: true,
                                hintText: "email".tr,
                                hintStyle: TextStyle(
                                    color: textColor.withOpacity(0.7)),
                                prefixIcon: Icon(Icons.alternate_email,
                                    color: primaryColor),
                                border: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color: textColor.withOpacity(0.2))),
                                focusedBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color: textColor.withOpacity(0.5))),
                                enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color: textColor.withOpacity(0.2))),
                              ),
                            ),
                            SizedBox(
                              height: 10.h,
                            ),
                            CustomButton(
                              text: 'Confirm'.tr,
                              onClick: () => _.goToWebView(type),
                            ),
                            SizedBox(
                              height: 10.h,
                            ),
                          ],
                        ))
                  ])
                : Center(child: CircularProgressIndicator()),
          ),
        ),
      ),
    );
  }
}
