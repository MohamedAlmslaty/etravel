import 'dart:async';
import 'dart:io';

import 'package:e_travel/app/data/models/customer.dart';
import 'package:e_travel/app/data/models/login.dart';
import 'package:e_travel/app/data/repositories/auth_repository.dart';
import 'package:e_travel/app/modules/auth/views/login_view.dart';
import 'package:e_travel/app/modules/auth/views/register_view.dart';
import 'package:e_travel/app/modules/widgets/custom_button.dart';
import 'package:e_travel/app/modules/widgets/custom_text.dart';
import 'package:e_travel/app/util/consts/loading.dart';
import 'package:e_travel/app/util/consts/style.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_facebook_auth/flutter_facebook_auth.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:get/get.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl_phone_field/intl_phone_field.dart';
import 'package:sign_in_with_apple/sign_in_with_apple.dart';

import '../../../util/consts/colors.dart';

class AuthController extends GetxController {
  final _authRepo = AuthRepository.instance;
  RxBool check = false.obs;
  List<String> ConfirmType = ['Confirm by email', 'Confirm by phone'];
  RxString typeConfirm = 'Confirm by email'.obs;
  RxBool ConfirmByEmail = true.obs;
  final otpFormKey = new GlobalKey<FormState>();
  TextEditingController otpController = new TextEditingController();

//login view
  late GlobalKey<FormState> _loginFormKey;
  late GlobalKey<FormState> _registerFormKey;
  GlobalKey<FormState> _passwordFormKey = new GlobalKey<FormState>();
  GlobalKey<FormState> _emailFormKey = new GlobalKey<FormState>();
  GlobalKey<FormState> _otpFormKey = new GlobalKey<FormState>();

  final tooltipKey = new GlobalKey();

  get loginFormKey => _loginFormKey;

  get formKey => _loginFormKey;
  final loginEmail = TextEditingController();
  final loginPassword = TextEditingController();
  final key = TextEditingController();
  final newPassword = TextEditingController();
  final _rememberMeCheck = false.obs;

  bool get rememberMeCheck => _rememberMeCheck.value;

//register view
  get registerFormKey => _registerFormKey;
  final registerEmail = TextEditingController();
  final registerPassword = TextEditingController();
  final registerReferralCode = TextEditingController();
  final registerFirstName = TextEditingController();
  final registerLastName = TextEditingController();
  final registerUserName = TextEditingController();
  final registerPhone = TextEditingController();
  final otp = TextEditingController();
  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging.instance;
  final RxString token = ''.obs;

  String verificationID = "";

  final emailNode = FocusNode(),
      passwordNode = FocusNode(),
      firstNameNode = FocusNode(),
      lastNameNode = FocusNode(),
      userNameNode = FocusNode(),
      phoneNode = FocusNode();
  late Widget selectedWidget;
  final selectedButton = true.obs;

  late String imageUrl;
  late File _image;

  get image => _image;

  String facebookMessage = 'Log in/out by pressing the buttons below.';

  ///Timer
  RxInt counter = 120.obs;
  Timer? _timer;

  void startTimer() {
    const oneSec = const Duration(seconds: 1);
    _timer = new Timer.periodic(
      oneSec,
      (Timer timer) {
        if (counter.value == 0) {
          timer.cancel();
          update();
        } else {
          counter.value--;
        }
      },
    );
  }

  ///
  @override
  void onInit() {
    _loginFormKey = new GlobalKey<FormState>();
    _registerFormKey = new GlobalKey<FormState>();
    selectedWidget = LoginWidget();
    if (Get.arguments != null) selectButton(Get.arguments);
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}

  void rememberMe() {
    _rememberMeCheck.value = !_rememberMeCheck.value;
    update();
  }

  login() async {
    if (loginFormKey.currentState.validate()) {
      loading();
      token.value = (await _firebaseMessaging.getToken())!;
      LoginVM user = LoginVM(
          username: loginEmail.text.trim(),
          password: loginPassword.text.trim(),
          rememberMe: rememberMeCheck,
          firebaseId: token.value);
      await _authRepo.authorize(user).then(
            (value) => EasyLoading.dismiss(),
          );
    }
  }

  chooseImage() async {
    final pickedFile =
        await ImagePicker().pickImage(source: ImageSource.gallery);
    if (pickedFile != null) {
      imageUrl = pickedFile.path;
      _image = File(pickedFile.path);
      update();
    }
  }

  register() {
    if (registerFormKey.currentState.validate()) {
      registerFormKey.currentState.save();
      if (check.value) {
        Get.defaultDialog(
          backgroundColor: Color(0xffFEF200),
          title: "Please verify your information before registering".tr,
          content: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              CustomText(
                text: "name".tr + ' : ' + registerFirstName.text.trim().toString(),
                color: blackColor,
                fontSize: 16.sp,
                fontWeight: FontWeight.bold,
              ),
              SizedBox(
                height: 10.h,
              ),
              CustomText(
                text: "email".tr + ' : ' + registerEmail.text.trim().toString(),
                color: blackColor,
                fontSize: 16.sp,
                fontWeight: FontWeight.bold,
              ),
              SizedBox(
                height: 10.h,
              ),
              CustomText(
                text: "phone number".tr + ' : ' + registerPhone.text.trim().toString(),
                color: blackColor,
                fontSize: 16.sp,
                fontWeight: FontWeight.bold,
              ),
              SizedBox(
                height: 10.h,
              ),
              CustomText(
                text: ConfirmByEmail.value
                    ? 'Confirm by email'.tr
                    : 'Confirm by phone'.tr,
                color: blackColor,
                fontSize: 16.sp,
                fontWeight: FontWeight.bold,
              ),
            ],
          ),
          cancel: ElevatedButton(
            onPressed: () {
              Get.back(); // Close the dialog when the Cancel button is pressed
            },
            style: ElevatedButton.styleFrom(
              primary: Colors.red,
              shape: RoundedRectangleBorder(
                borderRadius:
                    BorderRadius.circular(20.0), // Custom border radius
              ), // Custom background color for the Cancel button
            ),
            child: CustomText(text:"cancel".tr),
          ),
          confirm: ElevatedButton(
            onPressed: () async {
              loading();
              counter.value = 120;
              startTimer();
              _authRepo
                  .activationAccount(
                      ConfirmByEmail.value
                          ? registerEmail.text.trim()
                          : registerPhone.text.trim(),
                      ConfirmByEmail.value)
                  .then((value) {
                if (value) {
                  EasyLoading.dismiss();

                  Get.defaultDialog(
                    backgroundColor: Color(0xffFEF200),
                    title: "Enter the verification code".tr,
                    content: Form(
                      key: otpFormKey,
                      child: Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 15),
                            child: SizedBox(
                              height: Get.height * .095,
                              child: TextFormField(
                                keyboardType: TextInputType.number,
                                decoration: dialogInputStyle.copyWith(
                                    hintText:
                                        'Please Enter verification code'.tr,
                                    fillColor: whiteColor,
                                    filled: true),
                                validator: RequiredValidator(
                                    errorText:
                                        'Please Enter verification code'.tr),
                                maxLength: 4,
                                controller: otpController,
                              ),
                            ),
                          ),
                          Obx(
                            () => counter.value != 0
                                ? Obx(() => Center(
                                        child: CustomText(
                                      text: 'wait'.tr +
                                          ' ' +
                                          counter.value.toString() +
                                          ' ' +
                                          'second'.tr,
                                      color: blackColor,
                                    )))
                                : CustomButton(
                                    text: 'resend'.tr,
                                    width: 200.w,
                                    height: 40.h,
                                    onClick: () {
                                      _authRepo.activationAccount(
                                          ConfirmByEmail.value
                                              ? registerEmail.text.trim()
                                              : registerPhone.text.trim(),
                                          ConfirmByEmail.value);
                                      counter.value = 120;
                                      startTimer();
                                    },
                                  ),
                          ),
                        ],
                      ),
                    ),
                    cancel: ElevatedButton(
                      onPressed: () {
                        Get.back(); // Close the dialog when the Cancel button is pressed
                      },
                      style: ElevatedButton.styleFrom(
                        primary: Colors.red,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(
                              20.0), // Custom border radius
                        ), // Custom background color for the Cancel button
                      ),
                      child: CustomText(text:"cancel".tr),
                    ),
                    confirm: ElevatedButton(
                      onPressed: () async {
                        loading();
                        token.value = (await _firebaseMessaging.getToken())!;
                        Customer customer = Customer(
                            newPassword: registerPassword.text.trim(),
                            referralCode: registerReferralCode.text.trim(),
                            name: registerFirstName.text.trim(),
                            email: registerEmail.text.trim(),
                            mobileNo: registerPhone.text.trim(),
                            otp: otpController.text.trim(),
                            verifiedByEmail: ConfirmByEmail.value,
                            verifiedByMobileNo: !ConfirmByEmail.value,
                            userLogin: registerUserName.text.trim(),
                            firebaseId: token.value);
                        _authRepo.registerAccount(customer).then(
                          (value) {
                            if (value == true) {
                              EasyLoading.dismiss();
                            } else {
                              EasyLoading.dismiss();
                            }
                          },
                        );
                      },
                      style: ElevatedButton.styleFrom(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(
                                20.0), // Custom border radius
                          ),
                          primary: primaryColor),
                      child: CustomText(text:"send".tr),
                    ),
                  );
                }
              });
            },
            style: ElevatedButton.styleFrom(
                shape: RoundedRectangleBorder(
                  borderRadius:
                      BorderRadius.circular(20.0), // Custom border radius
                ),
                primary: primaryColor),
            child: CustomText(text:"OK".tr),
          ),
        );
      } else
        showSnackBar(
            title: 'error'.tr,
            message: 'Please agree to the terms and conditions'.tr);
    }
  }

  selectButton(bool value) {
    selectedButton.value = value;
    if (value) {
      selectedWidget = LoginWidget(
        key: ValueKey(0),
      );
    } else {
      selectedWidget = RigsterWidget(
        key: ValueKey(1),
      );
    }
    update();
  }

  focus(BuildContext context, FocusNode currentFocus, FocusNode nextFocus) {
    currentFocus.unfocus();
    FocusScope.of(context).requestFocus(nextFocus);
  }

  Future<void> facebookLogin() async {
    try {
      // Login with Facebook
      final LoginResult result = await FacebookAuth.instance.login();

      if (result.status == LoginStatus.success) {
        final AccessToken accessToken = result.accessToken!;
        // Authenticate with Firebase using Facebook access token
        final AuthCredential credential =
            FacebookAuthProvider.credential(accessToken.token);
        final UserCredential userCredential =
            await FirebaseAuth.instance.signInWithCredential(credential);
        final User? user = userCredential.user;
        _authRepo.getPhoneNamber(accessToken.token).then((value) async {
          if (value != null) {
            // Phone number retrieved successfully
            print('Phone number: $value');
            showSnackBar(title: 'Phone number:'.tr, message: '$value');
            await _authRepo.authorizeFacebook(accessToken.token);
          } else {
            // Phone number retrieval failed
            showSnackBar(
                title: 'Error'.tr, message: 'Failed to retrieve phone number');
            print('Failed to retrieve phone number');
          }
        });
      } else {
        print(result.status);
        print(result.message);
        showSnackBar(title: '${result.status}', message: '${result.message}');
      }
    } catch (err) {
      showSnackBar(title: 'error', message: '$err');
    }
  }

  Future<void> googleLogin() async {
    try {
      final GoogleSignIn googleSignIn = GoogleSignIn();

      // Start the Google sign-in process
      final GoogleSignInAccount? googleSignInAccount =
          await googleSignIn.signIn();

      if (googleSignInAccount != null) {
        // Google sign-in successful, authenticate with Firebase
        final GoogleSignInAuthentication googleAuth =
            await googleSignInAccount.authentication;
        final AuthCredential credential = GoogleAuthProvider.credential(
          accessToken: googleAuth.accessToken,
          idToken: googleAuth.idToken,
        );
        final UserCredential userCredential =
            await FirebaseAuth.instance.signInWithCredential(credential);
        final User? user = userCredential.user;

        if (user != null) {
          // Google login successful
          token.value = (await _firebaseMessaging.getToken())!;
          Customer customer = Customer(
              newPassword: user.uid,
              name: user.displayName,
              email: user.email,
              userLogin: user.email,
              googleId: user.uid,
              verifiedByEmail: false,
              verifiedByMobileNo: false,
              verifiedBySocialId: true,
              mobileNo: user.phoneNumber,
              firebaseId: token.value);
          _authRepo.checkEmail(customer).then((value) async {
            if (value) {
              loading();
              LoginVM user = LoginVM(
                  password: customer.newPassword!,
                  username: customer.email!,
                  firebaseId: customer.firebaseId!);
              await _authRepo.authorize(user);
              EasyLoading.dismiss();
            } else {
              Get.defaultDialog(
                backgroundColor: Color(0xffFEF200),
                title: "Please enter phone".tr,
                content: Form(
                  key: otpFormKey,
                  child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: 15.w),
                    child: Directionality(
                      // add this
                      textDirection: TextDirection.ltr,
                      // set this property
                      child: IntlPhoneField(
                        decoration: formInputStyle.copyWith(
                            hintText: 'phone_ex1'.tr,
                            contentPadding: EdgeInsets.all(12),
                            prefixIcon: Icon(Icons.lock, color: primaryColor),
                            fillColor: whiteColor,
                            filled: true),
                        initialCountryCode: 'LY',
                        onChanged: (phone) {
                          registerPhone.text = phone.completeNumber;
                        },
                      ),
                    ),
                  ),
                ),
                cancel: ElevatedButton(
                  onPressed: () {
                    Get.back(); // Close the dialog when the Cancel button is pressed
                  },
                  style: ElevatedButton.styleFrom(
                    primary: Colors.red,
                    shape: RoundedRectangleBorder(
                      borderRadius:
                          BorderRadius.circular(20.0), // Custom border radius
                    ), // Custom background color for the Cancel button
                  ),
                  child: CustomText(text:"cancel".tr),
                ),
                confirm: ElevatedButton(
                  onPressed: () async {
                    loading();
                    customer.mobileNo = registerPhone.text.trim();
                    _authRepo.registerAccount(customer);
                    EasyLoading.dismiss();
                  },
                  style: ElevatedButton.styleFrom(
                      shape: RoundedRectangleBorder(
                        borderRadius:
                            BorderRadius.circular(20.0), // Custom border radius
                      ),
                      primary: primaryColor),
                  child: CustomText(text:"send".tr),
                ),
              );
              //  Initialize Google sign-in
            }
          });
        } else {
          // Google login failed
          showSnackBar(title: 'failed', message: 'Google login failed');
        }
      } else {
        // Google sign-in failed
        print('Google sign-in failed');
        showSnackBar(title: 'failed', message: 'Google sign-in failed');
      }
    } catch (err) {
      showSnackBar(title: 'error', message: '$err');
      throw err;
    }
  }

  Future<void> appleLogin() async {
    try {
      // Perform the Apple Sign-In flow
      final credential = await SignInWithApple.getAppleIDCredential(
        scopes: [
          AppleIDAuthorizationScopes.email,
          AppleIDAuthorizationScopes.fullName
        ],
      );

      // Create a Firebase credential from the Apple credential
      final firebaseCredential = OAuthProvider('apple.com').credential(
        idToken: credential.identityToken,
        accessToken: credential.authorizationCode,
      );

      // Sign in to Firebase with the Firebase credential
      final userCredential =
          await FirebaseAuth.instance.signInWithCredential(firebaseCredential);
      final user = userCredential.user;
      if (user != null) {
        // apple login successful
        token.value = (await _firebaseMessaging.getToken())!;
        Customer customer = Customer(
            newPassword: user.uid,
            name: user.displayName,
            email: user.email,
            userLogin: user.email,
            googleId: user.uid,
            verifiedByEmail: false,
            verifiedByMobileNo: false,
            verifiedBySocialId: true,
            mobileNo: user.phoneNumber,
            firebaseId: token.value);
        _authRepo.checkEmail(customer).then((value) async {
          if (value) {
            loading();
            LoginVM user = LoginVM(
                username: loginEmail.text.trim(),
                password: loginPassword.text.trim(),
                rememberMe: rememberMeCheck,
                firebaseId: token.value!);
            await _authRepo.authorize(user);
            EasyLoading.dismiss();
          } else {
            Get.defaultDialog(
              backgroundColor: Color(0xffFEF200),
              title: "Please enter phone".tr,
              content: Form(
                key: otpFormKey,
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 15.w),
                  child: Directionality(
                    // add this
                    textDirection: TextDirection.ltr, // set this property
                    child: IntlPhoneField(
                      decoration: formInputStyle.copyWith(
                          hintText: 'phone_ex1'.tr,
                          contentPadding: EdgeInsets.all(12),
                          prefixIcon: Icon(Icons.lock, color: primaryColor),
                          fillColor: whiteColor,
                          filled: true),
                      initialCountryCode: 'LY',
                      onChanged: (phone) {
                        registerPhone.text = phone.completeNumber;
                      },
                    ),
                  ),
                ),
              ),
              cancel: ElevatedButton(
                onPressed: () {
                  Get.back(); // Close the dialog when the Cancel button is pressed
                },
                style: ElevatedButton.styleFrom(
                  primary: Colors.red,
                  shape: RoundedRectangleBorder(
                    borderRadius:
                        BorderRadius.circular(20.0), // Custom border radius
                  ), // Custom background color for the Cancel button
                ),
                child: CustomText(text:"cancel".tr),
              ),
              confirm: ElevatedButton(
                onPressed: () async {
                  loading();
                  customer.mobileNo = registerPhone.text.trim();
                  _authRepo.registerAccount(customer);
                  EasyLoading.dismiss();
                },
                style: ElevatedButton.styleFrom(
                    shape: RoundedRectangleBorder(
                      borderRadius:
                          BorderRadius.circular(20.0), // Custom border radius
                    ),
                    primary: primaryColor),
                child: CustomText(text:"send".tr),
              ),
            );
          }
        });
      } else {
        // apple login failed
        showSnackBar(title: 'failed', message: 'apple login failed');
      }
    } catch (err) {
      print(err.toString());
      throw err;
    }
  }

  forgetPassword() {
    Get.defaultDialog(
      backgroundColor: Color(0xffFEF200),
      title: '',
      titleStyle: TextStyle(fontSize: 0),
      content: Column(children: [
        Image.asset(
          'assets/icons/ask.png',
          width: 45.w,
          height: 45.w,
        ),
        CustomText(
          text: "forgot password?".tr,
          color: primaryColor,
          fontWeight: FontWeight.bold,
          fontSize: 16.sp,
        ),
        SizedBox(
          height: 10.h,
        ),
        Form(
          key: _emailFormKey,
          child: TextFormField(
            decoration: InputDecoration(
              hintText: 'email'.tr,
              filled: true,
              fillColor: Colors.white,
              labelStyle: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
              focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(50)),
                  borderSide: BorderSide(color: whiteColor)),
              enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(50)),
                  borderSide: BorderSide(color: whiteColor)),
              errorBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(50)),
                  borderSide: BorderSide(color: Colors.red)),
            ),
            validator: MultiValidator([
              RequiredValidator(errorText: "Please enter the email".tr),
              EmailValidator(errorText: "Please enter a valid email".tr),
            ]),
            controller: loginEmail,
          ),
        ),
      ]),
      cancel: ElevatedButton(
        onPressed: () {
          Get.back(); // Close the dialog when the Cancel button is pressed
        },
        style: ElevatedButton.styleFrom(
          primary: Colors.red,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(20.0), // Custom border radius
          ), // Custom background color for the Cancel button
        ),
        child: CustomText(text: "cancel".tr,),
      ),
      confirm: ElevatedButton(
        onPressed: () {
          if (_emailFormKey.currentState!.validate()) {
            _emailFormKey.currentState!.save();
            _authRepo
                .requestPasswordReset(loginEmail.text.trim())
                .then((value) {
              Get.defaultDialog(
                backgroundColor: Color(0xffFEF200),
                title: '',
                titleStyle: TextStyle(fontSize: 0),
                content: Column(children: [
                  Image.asset(
                    'assets/icons/ask.png',
                    width: 45.w,
                    height: 45.w,
                  ),
                  CustomText(
                    text: "An email has been sent to you".tr,
                    color: primaryColor,
                    fontWeight: FontWeight.bold,
                    fontSize: 16.sp,
                  ),
                  SizedBox(
                    height: 10.h,
                  ),
                  Form(
                    key: _passwordFormKey,
                    child: Container(
                      height: 150.h,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Padding(
                            padding: EdgeInsets.symmetric(
                                horizontal: 15.w, vertical: 10.h),
                            child: SizedBox(
                              height: 50.h,
                              child: TextFormField(
                                keyboardType: TextInputType.number,
                                decoration: InputDecoration(
                                  hintText: 'Enter the verification code'.tr,
                                  filled: true,
                                  fillColor: Colors.white,
                                  labelStyle: TextStyle(
                                      fontSize: 20,
                                      fontWeight: FontWeight.bold),
                                  focusedBorder: OutlineInputBorder(
                                      borderRadius:
                                          BorderRadius.all(Radius.circular(50)),
                                      borderSide:
                                          BorderSide(color: whiteColor)),
                                  enabledBorder: OutlineInputBorder(
                                      borderRadius:
                                          BorderRadius.all(Radius.circular(50)),
                                      borderSide:
                                          BorderSide(color: whiteColor)),
                                  errorBorder: OutlineInputBorder(
                                      borderRadius:
                                          BorderRadius.all(Radius.circular(50)),
                                      borderSide:
                                          BorderSide(color: Colors.red)),
                                ),
                                validator: RequiredValidator(
                                    errorText:
                                        'Please Enter verification code'.tr),
                                controller: key,
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 15),
                            child: SizedBox(
                              height: 50.h,
                              child: TextFormField(
                                decoration: InputDecoration(
                                  hintText: 'newPassword'.tr,
                                  filled: true,
                                  fillColor: Colors.white,
                                  labelStyle: TextStyle(
                                      fontSize: 20,
                                      fontWeight: FontWeight.bold),
                                  focusedBorder: OutlineInputBorder(
                                      borderRadius:
                                          BorderRadius.all(Radius.circular(50)),
                                      borderSide:
                                          BorderSide(color: whiteColor)),
                                  enabledBorder: OutlineInputBorder(
                                      borderRadius:
                                          BorderRadius.all(Radius.circular(50)),
                                      borderSide:
                                          BorderSide(color: whiteColor)),
                                  errorBorder: OutlineInputBorder(
                                      borderRadius:
                                          BorderRadius.all(Radius.circular(50)),
                                      borderSide:
                                          BorderSide(color: Colors.red)),
                                ),
                                validator: RequiredValidator(
                                    errorText:
                                        'Please enter the newPassword'.tr),
                                controller: newPassword,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ]),
                cancel: ElevatedButton(
                  onPressed: () {
                    Get.back(); // Close the dialog when the Cancel button is pressed
                  },
                  style: ElevatedButton.styleFrom(
                    primary: Colors.red,
                    shape: RoundedRectangleBorder(
                      borderRadius:
                          BorderRadius.circular(20.0), // Custom border radius
                    ), // Custom background color for the Cancel button
                  ),
                  child:  CustomText(text:"cancel".tr),
                ),
                confirm: ElevatedButton(
                  onPressed: () {
                    if (_passwordFormKey.currentState!.validate()) {
                      _passwordFormKey.currentState!.save();
                      _authRepo
                          .sendResetPassword(key.text.trim(),
                          newPassword.text.trim())
                          .then((value) {

                      });
                    }
                  },
                  style: ElevatedButton.styleFrom(
                      shape: RoundedRectangleBorder(
                        borderRadius:
                            BorderRadius.circular(20.0), // Custom border radius
                      ),
                      primary: primaryColor),
                  child:  CustomText(text:"send".tr),
                ),
              );
            });
          }
        },
        style: ElevatedButton.styleFrom(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20.0), // Custom border radius
            ),
            primary: primaryColor),
        child:  CustomText(text:"send".tr),
      ),
    );
  }
}
