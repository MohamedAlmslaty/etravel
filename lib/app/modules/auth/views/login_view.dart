import 'package:e_travel/app/modules/auth/controllers/auth_controller.dart';
import 'package:e_travel/app/modules/widgets/circule_button.dart';
import 'package:e_travel/app/modules/widgets/custom_button.dart';
import 'package:e_travel/app/modules/widgets/custom_text.dart';
import 'package:e_travel/app/util/consts/colors.dart';
import 'package:e_travel/app/util/consts/style.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class LoginWidget extends GetView<AuthController> {
  LoginWidget({
    Key? key,
  }) : super(key: key);
  bool local = Get.find<GetStorage>().read('language') == 'ar';

  @override
  Widget build(BuildContext context) {
    return GetBuilder<AuthController>(
      init: AuthController(),
      builder: (_) {
        return Form(
          key: _.loginFormKey,
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 15.w),
            child: AutofillGroup(
              child: Column(
                children: [
                  TextFormField(
                      style: TextStyle(
                        color: primaryColor,
                      ),
                      cursorColor: primaryColor,
                      decoration: formInputStyle.copyWith(
                          contentPadding: EdgeInsets.all(12),
                          prefixIcon:
                              Icon(Icons.alternate_email, color: primaryColor),
                          hintText: "email".tr,
                          hintStyle: TextStyle(color: primaryColor),
                          fillColor: whiteColor,
                          filled: true),
                      validator: (value) {
                        bool emailValid = RegExp(
                                r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
                            .hasMatch(value!);
                        if (value == null || value.isEmpty) {
                          return "Please enter the email".tr;
                        } else if (emailValid == false) {
                          return "Please enter a valid email".tr;
                        }
                        return null;
                      },
                      inputFormatters: [
                        FilteringTextInputFormatter.deny(
                            new RegExp(r"\s\b|\b\s"))
                      ],
                      controller: _.loginEmail,
                      autofillHints: [AutofillHints.email]),
                  SizedBox(
                    height: 10.h,
                  ),
                  TextFormField(
                    obscureText: true,
                    style: TextStyle(color: primaryColor),
                    cursorColor: primaryColor,
                    decoration: formInputStyle.copyWith(
                        hintText: 'password'.tr,
                        hintStyle: TextStyle(color: primaryColor),
                        contentPadding: EdgeInsets.all(12),
                        prefixIcon: Icon(Icons.lock, color: primaryColor),
                        fillColor: whiteColor,
                        filled: true),
                    validator: RequiredValidator(
                        errorText: "Please enter the password".tr),
                    controller: _.loginPassword,
                    autofillHints: [AutofillHints.password],
                    onEditingComplete: () => TextInput.finishAutofillContext(),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(vertical: Get.height * 0.03),
                    child: InkWell(
                      onTap: () => _.forgetPassword(),
                      child: Text(
                        'forgot password?'.tr,
                        style: TextStyle(
                          fontSize: Get.width * 0.04,
                          color: primaryColor,
                        ),
                      ),
                    ),
                  ),
                  CustomButton(
                      text: 'login'.tr, width: 200.w, onClick: () => _.login()),
                  SizedBox(
                    height: 10.h,
                  ),
                  CustomText(
                    text: 'or'.tr,
                    color: primaryColor,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      InkWell(
                        onTap: () => _.googleLogin(),
                        child: Image.asset(
                          local
                              ? 'assets/icons/google.png'
                              : 'assets/icons/google_en.png',
                          width: 150.w,
                          height: 100.h,
                        ),
                      ),
                      // CirculeButton(
                      //     asset: 'assets/images/fb.svg',
                      //     color: Colors.white,
                      //     onPressed: () => _.facebookLogin()),
                      GetPlatform.isIOS
                          ? CirculeButton(
                              asset: 'assets/images/apple.svg',
                              color: Colors.white,
                              onPressed: () => _.appleLogin())
                          : SizedBox(),
                    ],
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
