import 'package:e_travel/app/modules/auth/controllers/auth_controller.dart';
import 'package:e_travel/app/util/consts/colors.dart';
import 'package:e_travel/app/modules/widgets/custom_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';

class AuthView extends GetView<AuthController> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: () {
          FocusScopeNode currentFocus = FocusScope.of(context);
          if (!currentFocus.hasPrimaryFocus) currentFocus.unfocus();
        },
        child: Scaffold(
          backgroundColor: secondaryColor,
          body: GetBuilder<AuthController>(
            init: AuthController(),
            initState: (_) {},
            builder: (_) =>  SingleChildScrollView(
    child:Column(
                children: [
                  SizedBox(
                    height: 44.h,
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 20.w, vertical: 10.h),
                    child: Row(
                      children: [
                        IconButton(
                            onPressed: () => Get.back(),
                            icon: Icon(Icons.arrow_back,color: primaryColor,)
                        ),
                        SizedBox(width: 10.w,),
                        CustomText(
                          text: 'hello'.tr,
                          fontSize: 20.sp,
                          color: primaryColor,
                        ),
                      ],
                    ),
                  ),

                  Align(
                    alignment: Alignment.center,
                    child: Padding(
                      padding: EdgeInsets.symmetric(
                          horizontal: 15.w, vertical: 25.h),
                      child: SvgPicture.asset(
                        'assets/images/logo.svg',
                        width: 100.h,
                        height: 100.h,
                        fit: BoxFit.fill,
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                        left: 25.w, right: 25.w, bottom: 40.h, top: 8.h),
                    child: Row(
                      children: [
                        InkWell(
                          onTap: () => _.selectButton(true),
                          child: Container(
                            padding: EdgeInsets.symmetric(
                                horizontal: 20.w, vertical: 13.h),
                            child: CustomText(
                                text: 'login'.tr,
                                fontSize: 18.sp,
                                color: Colors.white,
                                fontWeight: FontWeight.bold),
                            decoration: BoxDecoration(
                                color: _.selectedButton.value
                                    ? primaryColor
                                    : primaryColor.withOpacity(0.7),
                                borderRadius: BorderRadius.circular(100.r)),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: 15.w),
                          child: CustomText(
                              text: 'or'.tr,
                              fontSize: 16.sp,
                              color: primaryColor,
                              fontWeight: FontWeight.bold),
                        ),
                        InkWell(
                          onTap: () => _.selectButton(false),
                          child: Container(
                            padding: EdgeInsets.symmetric(
                                horizontal: 15.w, vertical: 13.h),
                            child: CustomText(
                                text: 'new account'.tr,
                                fontSize: 18.sp,
                                color: Colors.white,
                                fontWeight: FontWeight.bold),
                            decoration: BoxDecoration(
                                color: _.selectedButton.value
                                    ? primaryColor.withOpacity(0.7)
                                    : primaryColor,
                                borderRadius: BorderRadius.circular(100.r)),
                          ),
                        )
                      ],
                    ),
                  ),
                  AnimatedSwitcher(
                    duration: Duration(milliseconds: 200),
                    child: _.selectedWidget,
                  ),

                ],
              ),
            ),
            ),

        ));
  }

}
