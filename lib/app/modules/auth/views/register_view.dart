import 'package:e_travel/app/modules/auth/controllers/auth_controller.dart';
import 'package:e_travel/app/modules/widgets/custom_button.dart';
import 'package:e_travel/app/modules/widgets/custom_text.dart';
import 'package:e_travel/app/util/consts/colors.dart';
import 'package:e_travel/app/util/consts/style.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:get/get.dart';
import 'package:intl_phone_field/intl_phone_field.dart';
import 'package:url_launcher/url_launcher.dart';

class RigsterWidget extends GetView<AuthController> {
  const RigsterWidget({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Form(
        key: controller.registerFormKey,
        child: Column(
          children: [
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 15.w),
              child: TextFormField(
                textInputAction: TextInputAction.next,
                focusNode: controller.firstNameNode,
                onFieldSubmitted: (value) {
                  controller.focus(context, controller.firstNameNode,
                      controller.lastNameNode);
                },
                style: TextStyle(
                  color: Colors.black,
                ),
                cursorColor: Colors.black,
                decoration: formInputStyle.copyWith(
                    hintText: "name".tr,
                    contentPadding: EdgeInsets.all(12),
                    prefixIcon:
                        Icon(Icons.account_circle_sharp, color: primaryColor),
                    fillColor: whiteColor,
                    filled: true),
                validator:
                    RequiredValidator(errorText: "Please enter the name".tr),
                controller: controller.registerFirstName,
              ),
            ),
            SizedBox(
              height: 10.h,
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 15.w),
              child: TextFormField(
                textInputAction: TextInputAction.next,
                focusNode: controller.emailNode,
                onFieldSubmitted: (value) {
                  controller.focus(
                      context, controller.emailNode, controller.phoneNode);
                },
                style: TextStyle(
                  color: Colors.black,
                ),
                cursorColor: Colors.black,
                decoration: formInputStyle.copyWith(
                    hintText: "email".tr,
                    contentPadding: EdgeInsets.all(12),
                    prefixIcon:
                        Icon(Icons.alternate_email, color: primaryColor),
                    fillColor: whiteColor,
                    filled: true),
                validator: (value) {
                  bool emailValid = RegExp(
                          r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
                      .hasMatch(value!);
                  if (value == null || value.isEmpty) {
                    return "Please enter the email".tr;
                  } else if (emailValid == false) {
                    return "Please enter a valid email".tr;
                  }
                  return null;
                },
                inputFormatters: [
                  FilteringTextInputFormatter.deny(new RegExp(r"\s\b|\b\s"))
                ],
                controller: controller.registerEmail,
              ),
            ),
            SizedBox(
              height: 10.w,
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 15.w),
              child: Directionality(
                // add this
                textDirection: TextDirection.ltr, // set this property
                child: IntlPhoneField(
                  decoration: formInputStyle.copyWith(
                      hintText: 'phone_ex1'.tr,
                      contentPadding: EdgeInsets.all(12),
                      prefixIcon: Icon(Icons.lock, color: primaryColor),
                      fillColor: whiteColor,
                      filled: true),
                  initialCountryCode: 'LY',
                  searchText: 'Search Country/region'.tr,
                  onChanged: (phone) {
                    controller.registerPhone.text = phone.completeNumber;
                  },
                ),
              ),
            ),
            SizedBox(
              height: 10.h,
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 15.w),
              child: TextFormField(
                textInputAction: TextInputAction.done,
                focusNode: controller.passwordNode,
                onFieldSubmitted: (value) {
                  controller.passwordNode.unfocus();
                },
                obscureText: true,
                style: TextStyle(color: Colors.black),
                cursorColor: Colors.black,
                decoration: formInputStyle.copyWith(
                    hintText: 'password'.tr,
                    contentPadding: EdgeInsets.all(12),
                    prefixIcon: Icon(Icons.lock, color: primaryColor),
                    fillColor: whiteColor,
                    filled: true),
                validator: RequiredValidator(
                    errorText: "Please enter the password".tr),
                controller: controller.registerPassword,
              ),
            ),
            SizedBox(
              height: 10.h,
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 15.w),
              child: TextFormField(
                textInputAction: TextInputAction.done,
                obscureText: true,
                style: TextStyle(color: Colors.black),
                cursorColor: Colors.black,
                decoration: formInputStyle.copyWith(
                    hintText: 'referralCode'.tr,
                    contentPadding: EdgeInsets.all(12),
                    prefixIcon: Icon(Icons.card_giftcard, color: primaryColor),
                    fillColor: whiteColor,
                    filled: true),
                controller: controller.registerReferralCode,
              ),
            ),
            Column(
              children: List.generate(controller.ConfirmType.length, (index) {
                var _type = controller.ConfirmType.elementAt(index);
                return Obx(
                  () => RadioListTile(
                    activeColor: primaryColor,
                    value: _type,
                    groupValue: controller.typeConfirm.value,
                    onChanged: (value) {
                      controller.typeConfirm.value = value.toString();
                      print(controller.typeConfirm.value);
                      print(controller.ConfirmByEmail.value);
                      if (value == 'Confirm by email')
                        controller.ConfirmByEmail.value = true;
                      else
                        controller.ConfirmByEmail.value = false;
                    },
                    title: CustomText(
                      text: _type.tr,
                      fontSize: 15.sp,
                      color: blackColor,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                );
              }).toList(),
            ),
            SizedBox(
              height: 10.h,
            ),
            Obx(
              () => CheckboxListTile(
                value: controller.check.value,
                controlAffinity: ListTileControlAffinity.leading,
                onChanged: (value) {
                  controller.check.value = value!;
                },
                title: InkWell(
                  onTap: () => _launchUrl(),
                  child: Text.rich(
                    TextSpan(
                      text: 'Agree to'.tr,
                      style: TextStyle(fontSize: 18),
                      children: <TextSpan>[
                        TextSpan(
                            text: 'Terms and conditions'.tr,
                            style: TextStyle(
                              decoration: TextDecoration.underline,
                              fontWeight: FontWeight.bold,
                            )),
                        // can add more TextSpans here...
                      ],
                    ),
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 20,
            ),
            CustomButton(
                text: 'signup'.tr,
                width: 200.w,
                onClick: () => controller.register()),
            SizedBox(
              height: 20,
            ),
          ],
        ),
      ),
    );
  }

  _launchUrl() async {
    final Uri url = Uri.parse('https://etravel.ly/terms-and-conditions');
    if (!await launchUrl(url)) throw 'Could not launch $url';
  }
}
