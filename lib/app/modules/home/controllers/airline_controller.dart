import 'package:e_travel/app/data/models/airline.dart';
import 'package:e_travel/app/data/models/airline_screen.dart';
import 'package:e_travel/app/data/models/airport.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class AirlineController extends GetxController {
  final loading = false.obs;
  bool roundTrip = false;
  String departureText = '';
  String destinationText = '';
  String airlineText = '';
  RxList<Airport> airports = <Airport>[].obs;
  RxList<Airline> airlines = <Airline>[].obs;
  RxList<Airport> airportsItems = <Airport>[].obs;
  RxList<Airline> airlinesItems = <Airline>[].obs;
  RxList<Airport> dummyAirports = <Airport>[].obs;
  RxList<Airline> dummyAirlines = <Airline>[].obs;
  List<String> airportName = [];
  List<String> airlineName = [];
  final TextEditingController Controller = TextEditingController();
  final ScrollController scrollController = ScrollController();
  AirlineScrees airlineScrees = new AirlineScrees();

  @override
  void onInit() {
    super.onInit();
    if (Get.arguments != null && Get.arguments['airport']) {
      airports = Get.arguments['airports'];
      airportName = Get.arguments['airportName'];
      airportsItems = Get.arguments['airportsItems'];
      loading.value = Get.arguments['loading'];
      roundTrip = Get.arguments['roundTrip'];
      departureText = Get.arguments['departureText'];
      destinationText = Get.arguments['destinationText'];
      Controller.text = roundTrip ? destinationText : departureText;
    } else if (Get.arguments != null && !Get.arguments['airport']) {
      airlines = Get.arguments['airlines'];
      airlineName = Get.arguments['airlineName'];
      airlinesItems = Get.arguments['airlinesItems'];
      loading.value = Get.arguments['loading'];
      airlineText = Get.arguments['airlineText'];
      Controller.text = airlineText;
    }
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    Controller.dispose();
    airportsItems.clear();
    airports.clear();
    airportName.clear();
    airlinesItems.clear();
    airlines.clear();
    airlineName.clear();
  }

  void getSuggestions(String query) {
    if (query.isNotEmpty) {
      dummyAirports.clear();
      dummyAirports.addAll(airports);
      dummyAirports.retainWhere((s) => Get.find<GetStorage>()
                  .read('language') ==
              'ar'
          ? s.nameAr.toString().toLowerCase().contains(query.toLowerCase()) ||
              s.iataCode
                  .toString()
                  .toLowerCase()
                  .contains(query.toLowerCase()) ||
              s.city!.nameAr
                  .toString()
                  .toLowerCase()
                  .contains(query.toLowerCase()) ||
              s.city!.country!.nameAr
                  .toString()
                  .toLowerCase()
                  .contains(query.toLowerCase())
          : s.nameEn.toString().toLowerCase().contains(query.toLowerCase()) ||
              s.iataCode
                  .toString()
                  .toLowerCase()
                  .contains(query.toLowerCase()) ||
              s.city!.nameEn
                  .toString()
                  .toLowerCase()
                  .contains(query.toLowerCase()) ||
              s.city!.country!.nameEn
                  .toString()
                  .toLowerCase()
                  .contains(query.toLowerCase()));
      airportsItems.clear();
      airportsItems.assignAll(dummyAirports);
    } else {
      airportsItems.clear();
      airportsItems.assignAll(airports);
    }
  }

  void getAirlines(String query) {
    if (query.isNotEmpty) {
      dummyAirlines.clear();
      dummyAirlines.addAll(airlines);
      dummyAirlines.retainWhere((s) =>
          Get.find<GetStorage>().read('language') == 'ar'
              ? s.nameAr.toString().toLowerCase().contains(query.toLowerCase())
              : s.nameEn
                  .toString()
                  .toLowerCase()
                  .contains(query.toLowerCase()));
      airlinesItems.clear();
      airlinesItems.assignAll(dummyAirlines);
    } else {
      airlinesItems.clear();
      airlinesItems.assignAll(airlines);
    }
  }
}
