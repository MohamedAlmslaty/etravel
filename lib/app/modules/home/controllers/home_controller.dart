import 'package:e_travel/app/data/models/airline.dart';
import 'package:e_travel/app/data/models/airport.dart';
import 'package:e_travel/app/data/models/cabin_class.dart' as c;
import 'package:e_travel/app/data/models/flight_req.dart';
import 'package:e_travel/app/data/models/multiCityFlights.dart';
import 'package:e_travel/app/data/models/slider.dart';
import 'package:e_travel/app/data/repositories/airline_repository.dart';
import 'package:e_travel/app/data/repositories/airprot_repository.dart';
import 'package:e_travel/app/data/repositories/setting_repository.dart';
import 'package:e_travel/app/data/repositories/slider_repository.dart';
import 'package:e_travel/app/routes/app_pages.dart';
import 'package:e_travel/app/util/consts/style.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class HomeController extends GetxController {
  final airlineText = ''.obs;
  final airlineCode = ''.obs;
  final airlineId = 0.obs;
  final departureText = ''.obs;
  final destinationText = ''.obs;
  final departureDate = ''.obs;
  final destinationLocationCode = ''.obs;
  final originLocationCode = ''.obs;
  final returnDate = ''.obs;
  final adult = 1.obs;
  final children = 0.obs;
  final bookingAt = DateTime.now().obs;
  final Rx<c.CabinClass> selectCabinClass = c.CabinClass().obs;
  final Class = ''.obs;
  final enumName = ''.obs;

  ///
  final _airportRepo = AirportRepository.instance;
  final _airlineRepo = AirlineRepository.instance;
  final _settingRepo = SettingRepository.instance;
  final _sliderRepo = SliderRepository.instance;
  final load = false.obs;
  final direct = false.obs;
  final refundable = false.obs;
  final searchAvailable = false.obs;
  RxList<Airport> airports = <Airport>[].obs;
  RxList<Airline> airlines = <Airline>[].obs;
  RxList<Airport> airportsItems = <Airport>[].obs;
  RxList<Airline> airlinesItems = <Airline>[].obs;
  RxList<Airline> airlines1 = <Airline>[].obs;
  RxList<Airport> airports1 = <Airport>[].obs;
  RxList<Slider> sliders = <Slider>[].obs;
  List<String> airportName = [];
  List<String> airlineName = [];

  ///Multi stop
  RxList<MultiCity> multiCityFlight = <MultiCity>[].obs;


  @override
  void onInit() {
    super.onInit();
    multiCityFlight.add(MultiCity());
    multiCityFlight.add(MultiCity());
    selectCabinClass.value = c.cabin[0];
    Class.value = c.cabin[0].name!;
    enumName.value = c.cabin[0].enumName!;
    loadSliders();
    loadAirlines();
    getSettingByKey();

    ///
  }

  @override
  void onReady() {
    super.onReady();
  }


  Future<void> loadSliders() async {
    final response = await _sliderRepo.getSliders('MAIN_SCREEN');
    sliders.assignAll(response);
    update();
    load.value = true;
  }

  Future<void> getSettingByKey() async {
    await _settingRepo
        .getSetting('flights_available_to_non_registered')
        .then((value) {
      if (value.id != null) {
        searchAvailable.value = value.value!.toLowerCase() != "false";
      }
    });
  }

  Future<void> loadAirports() async {
    if (airports.isEmpty) {
      final response = await _airportRepo.getAirports();
      airports.assignAll(response);
      if (airports.isNotEmpty) airportsItems.assignAll(airports);
      for (int i = 0; i < airports.length; i++) {
        airportName.add(Get.find<GetStorage>().read('language') == 'ar'
            ? airports[i].nameAr.toString()
            : airports[i].nameEn.toString());
      }
      update();
      load.value = true;
    } else if (airports.isNotEmpty) {
      final response = await _airportRepo.getAirports();
      airports1.assignAll(response);
      if (airports.isNotEmpty && airports.length != airports1.length) {
        for (int i = 0; i < airports1.length; i++) {
          airportName.add(Get.find<GetStorage>().read('language') == 'ar'
              ? airports[i].nameAr.toString()
              : airports[i].nameEn.toString());
        }
        update();
        load.value = true;
      } else if (airports.isNotEmpty) {
        for (int i = 0; i < airports.length; i++) {
          airportName.add(Get.find<GetStorage>().read('language') == 'ar'
              ? airports[i].nameAr.toString()
              : airports[i].nameEn.toString());
        }
        update();
      }
    }
  }

  Future<void> loadAirlines() async {
    if (airlines.isEmpty) {
      final response = await _airlineRepo.getAirlines();
      airlines.assignAll(response);
      if (airlines.isNotEmpty) airlinesItems.assignAll(airlines);
      for (int i = 0; i < airlines.length; i++) {
        airlineName.add(Get.find<GetStorage>().read('language') == 'ar'
            ? airlines[i].nameAr.toString()
            : airlines[i].nameEn.toString());
      }
      update();
      load.value = true;
    } else if (airlines.isNotEmpty) {
      final response = await _airlineRepo.getAirlines();
      airlines1.assignAll(response);
      if (airlines.isNotEmpty && airlines.length != airlines1.length) {
        for (int i = 0; i < airlines1.length; i++) {
          airlineName.add(Get.find<GetStorage>().read('language') == 'ar'
              ? airlines1[i].nameAr.toString()
              : airlines1[i].nameEn.toString());
        }
        update();
        load.value = true;
      } else if (airlines.isNotEmpty) {
        for (int i = 0; i < airlines.length; i++) {
          airlineName.add(Get.find<GetStorage>().read('language') == 'ar'
              ? airlines[i].nameAr.toString()
              : airlines[i].nameEn.toString());
        }
        update();
      }
    }
  }

  void searchFlight({required bool roundTrip}) {
    if (Get.find<GetStorage>().hasData('token')) {
      if (departureText.isNotEmpty &&
          destinationText.isNotEmpty &&
          departureDate.isNotEmpty) {
        FlightRequest flightRequest = FlightRequest(
            adults: adult.value==1?0:adult.value,
            searchForDirect: direct.value,
            airlineId: airlineId.value,
            searchForRefundable: refundable.value,
            cabinClass: enumName.value,
            children: children.value,
            departureDate: departureDate.value,
            returnDate: roundTrip ? returnDate.value : '',
            destinationLocationCode: destinationLocationCode.value,
            originLocationCode: originLocationCode.value,
            goingOpen: false,
            returnOpen: false);
        Get.toNamed(Routes.FLIGHT, arguments: {
          'flightRequest': flightRequest,
          'Edit': false,
          'isMulti': false
        });
      } else
        showSnackBar(
            title: 'error'.tr, message: 'Please fill in all fields'.tr);
    } else {
      if (!searchAvailable.value) {
        Get.toNamed(Routes.AUTH);
      } else if (departureText.isNotEmpty &&
          destinationText.isNotEmpty &&
          departureDate.isNotEmpty) {
        FlightRequest flightRequest = FlightRequest(
            adults: adult.value==1?0:adult.value,
            airlineId: airlineId.value,
            cabinClass: enumName.value,
            children: children.value,
            searchForDirect: direct.value,
            searchForRefundable: refundable.value,
            departureDate: departureDate.value,
            returnDate: roundTrip ? returnDate.value : '',
            destinationLocationCode: destinationLocationCode.value,
            originLocationCode: originLocationCode.value,
            goingOpen: false,
            returnOpen: false);
        Get.toNamed(Routes.FLIGHT, arguments: {
          'flightRequest': flightRequest,
          'Edit': false,
          'isMulti': false
        });
      } else
        showSnackBar(
            title: 'error'.tr, message: 'Please fill in all fields'.tr);
    }
  }

  void searchFlightMultiCity() {
    if (multiCityFlight[1].departureText.isNotEmpty &&
        multiCityFlight[1].destinationText.isNotEmpty &&
        multiCityFlight[1].departureDate.isNotEmpty) {
      FlightRequest flightRequest = FlightRequest(
          adults: adult.value==1?0:adult.value,
          airlineId: airlineId.value,
          searchForDirect: direct.value,
          searchForRefundable: refundable.value,
          cabinClass: enumName.value,
          children: children.value,
          multiCityFlights: multiCityFlight,
          departureDate: '',
          returnDate: '',
          originLocationCode: multiCityFlight.first.originLocationCode.value,
          destinationLocationCode:
              multiCityFlight.last.destinationLocationCode.value,
          isMultiCity: true,
          goingOpen: false,
          returnOpen: false);
      Get.toNamed(Routes.FLIGHT, arguments: {
        'flightRequest': flightRequest,
        'Edit': false,
        'isMulti': true
      });
    } else
      showSnackBar(title: 'error'.tr, message: 'Please fill in all fields'.tr);
  }

  void searchByAirline({required bool roundTrip}) {
    if (Get.find<GetStorage>().hasData('token')) {
      if (departureText.isNotEmpty &&
          destinationText.isNotEmpty &&
          departureDate.isNotEmpty) {
        FlightRequest flightRequest = FlightRequest(
            adults: adult.value==1?0:adult.value,
            airlineId: airlineId.value,
            searchForDirect: direct.value,
            searchForRefundable: refundable.value,
            cabinClass: enumName.value,
            includedAirlineCodes: airlineCode.value,
            children: children.value,
            departureDate: departureDate.value,
            returnDate: roundTrip ? returnDate.value : '',
            destinationLocationCode: destinationLocationCode.value,
            originLocationCode: originLocationCode.value,
            goingOpen: false,
            returnOpen: false);
        Get.toNamed(Routes.FLIGHT, arguments: {
          'flightRequest': flightRequest,
          'Edit': false,
          'isMulti': false
        });
      } else
        showSnackBar(
            title: 'error'.tr, message: 'Please fill in all fields'.tr);
    } else {
      if (!searchAvailable.value) {
        Get.toNamed(Routes.AUTH);
      } else if (departureText.isNotEmpty &&
          destinationText.isNotEmpty &&
          departureDate.isNotEmpty) {
        FlightRequest flightRequest = FlightRequest(
            adults: adult.value==1?0:adult.value,
            airlineId: airlineId.value,
            cabinClass: enumName.value,
            children: children.value,
            searchForDirect: direct.value,
            searchForRefundable: refundable.value,
            departureDate: departureDate.value,
            returnDate: roundTrip ? returnDate.value : '',
            destinationLocationCode: destinationLocationCode.value,
            originLocationCode: originLocationCode.value,
            goingOpen: false,
            returnOpen: false);
        Get.toNamed(Routes.FLIGHT, arguments: {
          'flightRequest': flightRequest,
          'Edit': false,
          'isMulti': false
        });
      } else
        showSnackBar(
            title: 'error'.tr, message: 'Please fill in all fields'.tr);
    }
  }
}
