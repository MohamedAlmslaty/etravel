import 'package:e_travel/app/modules/home/views/widgets/multi_stop_view.dart';
import 'package:e_travel/app/modules/home/views/widgets/one_way_view.dart';
import 'package:e_travel/app/modules/widgets/slider.dart';
import 'package:e_travel/app/util/consts/colors.dart';
import 'package:e_travel/app/modules/widgets/custom_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import '../controllers/home_controller.dart';

class HomeView extends GetView<HomeController> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<HomeController>(
      init: HomeController(),
      builder: (_con) => CustomWidget(
        title: 'FlightsSearch'.tr,
        image: 'assets/icons/search.svg',
        main: true,
        child: Padding(
          padding: EdgeInsets.only(top: 20.h, right: 20.w, left: 20.w),
          child: DefaultTabController(
              length: 3,
              child: Column(
                children: [
                  _con.sliders.isNotEmpty?
                  SliderImageWidget(sliders: _con.sliders,height: 80.h):SizedBox(),
                  TabBar(
                    indicatorWeight: 5,
                    labelColor: primaryColor,
                    unselectedLabelColor: primaryColor.withOpacity(0.5),
                    indicatorColor: Color(0xff312871),
                    labelPadding: EdgeInsets.only(bottom: 10.h),
                    tabs: [
                      Text(
                        'RoundTrip'.tr,
                        textScaleFactor: 1.0,
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 18.sp),
                      ),
                      Text(
                        'OneWay'.tr,
                        textScaleFactor: 1.0,
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 18.sp),
                      ),
                      Text(
                        'Multi-Stop'.tr,
                        textScaleFactor: 1.0,
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 18.sp),
                      ),
                    ],
                  ),
                  Expanded(
                      child: TabBarView(
                    children: [
                      OneWayView(
                        roundTrip: true,
                      ),
                      OneWayView(),
                      MultiStopView(),
                    ],
                  )),
                ],
              )),
        ),
      ),
    );
  }
}
