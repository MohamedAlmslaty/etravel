import 'package:e_travel/app/data/models/airline_screen.dart';
import 'package:e_travel/app/data/models/cabin_class.dart';
import 'package:e_travel/app/data/models/multiCityFlights.dart';
import 'package:e_travel/app/data/models/select_date.dart';
import 'package:e_travel/app/modules/home/controllers/home_controller.dart';
import 'package:e_travel/app/modules/widgets/custom_button.dart';
import 'package:e_travel/app/modules/widgets/custom_button_with_icon.dart';
import 'package:e_travel/app/modules/widgets/custom_row.dart';
import 'package:e_travel/app/modules/widgets/custom_text.dart';
import 'package:e_travel/app/routes/app_pages.dart';
import 'package:e_travel/app/util/consts/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_spinbox/flutter_spinbox.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class MultiStopView extends GetView<HomeController> {
  MultiStopView();

  @override
  Widget build(BuildContext context) {
    return GetBuilder<HomeController>(
      init: HomeController(),
      initState: (_) {},
      builder: (_con) => SingleChildScrollView(
        physics: ScrollPhysics(),
        child: Column(
          children: [
            SizedBox(
              height: 20.h,
            ),
            Obx(
              () => ListView.builder(
                padding: EdgeInsets.zero,
                physics: NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                itemCount: _con.multiCityFlight.length,
                itemBuilder: (context, index) {
                  return widget(con: _con, index: index);
                },
              ),
            ),
            SizedBox(
              height: 10.h,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Obx(() => _con.multiCityFlight.length <= 3
                    ? CustomButtonWithIcon(
                        text: 'add flight'.tr,
                        onClick: () => _con.multiCityFlight.add(MultiCity()),
                        icon: Icons.add,
                        textColor: primaryColor,
                        iconColor: primaryColor,
                        backgroundButton: whiteColor)
                    : SizedBox()),
                SizedBox(
                  width: 10.w,
                ),
                Obx(() => _con.multiCityFlight.length > 2
                    ? CustomButtonWithIcon(
                        text: 'remove flight'.tr,
                        onClick: () => _con.multiCityFlight.removeLast(),
                        icon: Icons.remove,
                        textColor: Colors.red,
                        iconColor: Colors.red,
                        backgroundButton: whiteColor)
                    : SizedBox())
              ],
            ),
            CustomRow(
              title: 'CabinClass'.tr,
              text: '${_con.Class.value}'.tr,
              image: 'assets/icons/cabinClass.svg',
              onClick: () => CabinClass(_con),
            ),
            SizedBox(
              height: 10.h,
            ),
            Obx(() => CustomRow(
                  title: 'Search options'.tr,
                  text: 'adult'.tr +
                      '  ${_con.adult.value} , ' +
                      'child'.tr +
                      '  ${_con.children.value}',
                  image: 'assets/icons/searchOptions.svg',
                  onClick: () => Passengers(_con),
                )),
            SizedBox(
              height: 20.h,
            ),
            CustomButton(
                text: 'SearchFlights'.tr,
                width: 200.w,
                onClick: () => _con.searchFlightMultiCity()),
          ],
        ),
      ),
    );
  }
}

void CabinClass(_con) {
  Get.bottomSheet(
      Container(
        height: Get.height / 2,
        child: Padding(
          padding: const EdgeInsets.all(28.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                  child: CustomText(
                text: 'CabinClass'.tr,
                fontSize: 20,
                color: blackColor,
              )),
              Container(
                padding: EdgeInsets.all(20),
                child: Column(
                  children: List.generate(cabin.length, (index) {
                    var _lang = cabin.elementAt(index);
                    return Obx(() => RadioListTile(
                          value: _lang,
                          groupValue: _con.selectCabinClass.value,
                          onChanged: (dynamic value) {
                            _con.selectCabinClass.value = value;
                            _con.Class.value = value.name;
                            _con.enumName.value = value.enumName;
                            Get.back();
                          },
                          title: CustomText(
                            text: _lang.name!.tr,
                            fontWeight: FontWeight.bold,
                            fontSize: 16,
                            color: primaryColor,
                          ),
                        ));
                  }).toList(),
                ),
              ),
            ],
          ),
        ),
      ),
      elevation: 20.0,
      enableDrag: false,
      backgroundColor: whiteColor,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
        topLeft: Radius.circular(30.0),
        topRight: Radius.circular(30.0),
      )));
}

void Passengers(_con) {
  Get.bottomSheet(
      Container(
        height: Get.height / 1.4,
        child: Padding(
          padding: const EdgeInsets.all(28.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      child: Text(
                        'Search options'.tr,
                        style: TextStyle(
                          fontSize: 20.sp,
                          color: blackColor,
                        ),
                      ),
                    ),
                    Obx(
                      () => CheckboxListTile(
                          fillColor: MaterialStateColor.resolveWith(
                              (states) => secondaryColor),
                          value: _con.direct.value,
                          controlAffinity: ListTileControlAffinity.leading,
                          onChanged: (value) {
                            _con.direct.value = value!;
                          },
                          title: CustomText(
                            text: 'Non-stop flight'.tr,
                            color: primaryColor,
                          )),
                    ),
                    Obx(
                      () => CheckboxListTile(
                          fillColor: MaterialStateColor.resolveWith(
                              (states) => secondaryColor),
                          value: _con.refundable.value,
                          controlAffinity: ListTileControlAffinity.leading,
                          onChanged: (value) {
                            _con.refundable.value = value!;
                          },
                          title: CustomText(
                            text: 'Refundable reservation'.tr,
                            color: primaryColor,
                          )),
                    ),
                    Container(
                      margin: EdgeInsets.only(
                        top: 10.h,
                        bottom: 10.h,
                      ),
                      child: Column(
                        children: <Widget>[
                          Container(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Container(
                                  child: Row(
                                    children: <Widget>[
                                      Container(
                                        child: Icon(
                                          Icons.person,
                                          color: primaryColor,
                                          size: 25,
                                        ),
                                      ),
                                      SizedBox(
                                        width: 10.w,
                                      ),
                                      Container(
                                        padding: EdgeInsets.only(
                                          left: 20.w,
                                        ),
                                        child: CustomText(
                                          text: 'adult'.tr,
                                          fontSize: 18.sp,
                                          color: primaryColor,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Container(
                                    width: 150.w,
                                    child: CupertinoSpinBox(
                                      incrementIcon: Icon(
                                        Icons.add_circle_outline,
                                        color: secondaryColor,
                                      ),
                                      decrementIcon: Icon(
                                        Icons.remove_circle_outline,
                                        color: secondaryColor,
                                      ),
                                      decoration: BoxDecoration(
                                        color: whiteColor,
                                      ),
                                      textStyle: TextStyle(
                                        color: primaryColor,
                                      ),
                                      min: 1,
                                      max: 10,
                                      value: _con.adult.value.toDouble(),
                                      onChanged: (value) {
                                        _con.adult.value = value.toInt();
                                      },
                                    )),
                              ],
                            ),
                          ),
                          Container(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Container(
                                  child: Row(
                                    children: <Widget>[
                                      Container(
                                        child: Icon(
                                          Icons.boy,
                                          size: 20,
                                          color: primaryColor,
                                        ),
                                      ),
                                      SizedBox(
                                        width: 10.w,
                                      ),
                                      Container(
                                        padding: EdgeInsets.only(
                                          left: 20.w,
                                        ),
                                        child: CustomText(
                                          text: 'child'.tr,
                                          fontSize: 18.sp,
                                          color: primaryColor,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Container(
                                  width: 150.w,
                                  child: CupertinoSpinBox(
                                    incrementIcon: Icon(
                                      Icons.add_circle_outline,
                                      color: secondaryColor,
                                    ),
                                    decrementIcon: Icon(
                                      Icons.remove_circle_outline,
                                      color: secondaryColor,
                                    ),
                                    decoration: BoxDecoration(
                                      color: whiteColor,
                                    ),
                                    textStyle: TextStyle(
                                      color: primaryColor,
                                    ),
                                    min: 0,
                                    max: 10,
                                    value: _con.children.value.toDouble(),
                                    onChanged: (value) {
                                      _con.children.value = value.toInt();
                                    },
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Container(
                                  child: Row(
                                    children: <Widget>[
                                      Container(
                                        child: Icon(
                                          Icons.child_friendly_sharp,
                                          size: 20,
                                          color: primaryColor,
                                        ),
                                      ),
                                      SizedBox(
                                        width: 10.w,
                                      ),
                                      Container(
                                        padding: EdgeInsets.only(
                                          left: 20.w,
                                        ),
                                        child: CustomText(
                                          text: 'infant'.tr,
                                          fontSize: 18.sp,
                                          color: primaryColor,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Container(
                                  width: 150.w,
                                  child: CupertinoSpinBox(
                                    incrementIcon: Icon(
                                      Icons.add_circle_outline,
                                      color: secondaryColor,
                                    ),
                                    decrementIcon: Icon(
                                      Icons.remove_circle_outline,
                                      color: secondaryColor,
                                    ),
                                    decoration: BoxDecoration(
                                      color: whiteColor,
                                    ),
                                    textStyle: TextStyle(
                                      color: primaryColor,
                                    ),
                                    min: 0,
                                    max: 10,
                                    value: _con.children.value.toDouble(),
                                    onChanged: (value) {
                                      _con.children.value = value.toInt();
                                    },
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
      elevation: 20.0,
      enableDrag: false,
      backgroundColor: whiteColor,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
        topLeft: Radius.circular(30.0),
        topRight: Radius.circular(30.0),
      )));
}

Widget SearchPlace(
    {required String title,
    required String text,
    required String image,
    required bool roundTrip,
    required int index,
    required HomeController con}) {
  return Obx(
    () => CustomRow(
      title: title,
      text:
          '${roundTrip ? con.multiCityFlight[index].destinationText.value : con.multiCityFlight[index].departureText.value}',
      image: image,
      onClick: () async {
        await con.loadAirports();
        AirlineScrees result =
            await Get.toNamed(Routes.SELECT_PLACE, arguments: {
          'airport': true,
          'roundTrip': roundTrip,
          'airports': con.airports,
          'airportsItems': con.airportsItems,
          'airportName': con.airportName,
          'loading': con.load.value,
          'departureText': con.departureText.value,
          'destinationText': con.destinationText.value
        });
        if (result.departureText.value.isNotEmpty) {
          con.multiCityFlight[index].departureText.value =
              result.departureText.value;
          con.multiCityFlight[index].originLocationCode.value =
              result.originLocationCode.value;
        } else if (result.destinationText.value.isNotEmpty) {
          con.multiCityFlight[index].destinationText.value =
              result.destinationText.value;
          con.multiCityFlight[index].destinationLocationCode.value =
              result.destinationLocationCode.value;
        }
      },
    ),
  );
}

Widget widget({required HomeController con, required int index}) {
  return Column(
    children: [
      CustomText(
        text: 'flight'.tr + ' ${index + 1}',
        fontSize: 16.sp,
        color: blackColor,
        fontWeight: FontWeight.bold,
      ),
      SearchPlace(
          title: 'From'.tr,
          text: 'Departure'.tr,
          image: 'assets/icons/departure.svg',
          roundTrip: false,
          con: con,
          index: index),
      SizedBox(
        height: 10.h,
      ),
      SearchPlace(
          title: 'To'.tr,
          text: 'Destination'.tr,
          image: 'assets/icons/destination.svg',
          roundTrip: true,
          index: index,
          con: con),
      SizedBox(
        height: 10.h,
      ),
      Obx(() => CustomRow(
          title: 'DepartureDate'.tr,
          text: con.multiCityFlight[index].departureDate.value.isEmpty
              ? 'DepartureDate'.tr
              : con.multiCityFlight[index].departureDate.value,
          image: 'assets/icons/chooseDate.svg',
          onClick: () async {
            SelectDate result =
                await Get.toNamed(Routes.SELECT_DATE, arguments: 0);
            if (result.departureDate.value.isNotEmpty) {
              con.multiCityFlight[index].departureDate.value =
                  result.departureDate.value;
              print(con.multiCityFlight[index].departureDate.value);
            }
          })),
      SizedBox(
        height: 10.h,
      ),
    ],
  );
}
