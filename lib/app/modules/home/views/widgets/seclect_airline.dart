import 'package:e_travel/app/modules/home/controllers/airline_controller.dart';
import 'package:e_travel/app/modules/widgets/custom_text.dart';
import 'package:e_travel/app/modules/widgets/custom_widget.dart';
import 'package:e_travel/app/util/consts/base.dart';
import 'package:e_travel/app/util/consts/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class SelectAirline extends GetView<AirlineController> {
  bool local = Get.find<GetStorage>().read('language') == 'ar';

  @override
  Widget build(BuildContext context) {
    return GetBuilder<AirlineController>(
        init: AirlineController(),
        initState: (_) {},
        builder: (_) => Scaffold(
                body: SingleChildScrollView(
                    child: CustomWidget(
              title: 'airlines'.tr,
              height: Get.height / 1.2,
              child: _.loading.value
                  ? Obx(() => Container(
                      padding: EdgeInsets.symmetric(
                          horizontal: 30.w, vertical: 10.h),
                      decoration: BoxDecoration(
                        color: whiteColor,
                        borderRadius: BorderRadius.all(Radius.circular(20.r)),
                      ),
                      child: Column(
                        children: [
                          Container(
                            margin: EdgeInsets.fromLTRB(16, 16, 10, 10),
                            child: TextField(
                              controller: _.Controller,
                              decoration: InputDecoration(
                                prefixIcon: Icon(
                                  Icons.search,
                                  color: primaryColor,
                                ),
                                suffixIcon: InkWell(
                                    onTap: () {
                                      _.airlinesItems.clear();
                                      _.airlinesItems.assignAll(_.airlines);
                                      _.Controller.clear();
                                    },
                                    child: Icon(
                                      Icons.cancel_outlined,
                                      color: Colors.grey,
                                    )),
                                hintText: 'airlines'.tr,
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(20),
                                  borderSide: BorderSide(color: secondaryColor),
                                ),
                                focusedBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(20),
                                  borderSide: BorderSide(color: primaryColor),
                                ),
                                enabledBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(20),
                                  borderSide: BorderSide(color: secondaryColor),
                                ),
                              ),
                              onChanged: _.getAirlines,
                            ),
                          ),
                          Expanded(
                              child: Scrollbar(
                                  controller: _.scrollController,
                                  thickness: 5,
                                  radius: Radius.circular(50),
                                  trackVisibility: true,
                                  thumbVisibility: true,
                                  child: ListView.builder(
                                      controller: _.scrollController,
                                      padding: EdgeInsets.zero,
                                      itemCount: _.airlinesItems.length,
                                      itemBuilder: (context, index) {
                                        final airline = _.airlinesItems[index];
                                        return Container(
                                            color: _.Controller.text ==
                                                    (local
                                                        ? airline.nameAr
                                                        : airline.nameEn)
                                                ? primaryColor.withOpacity(0.5)
                                                : Colors.transparent,
                                            child: ListTile(
                                              onTap: () {
                                                _.Controller.text = local
                                                    ? airline.nameAr.toString()
                                                    : airline.nameEn.toString();
                                                _.airlineScrees.airlineText
                                                    .value = _.Controller.text;
                                                _.airlineScrees.airlineCode
                                                    .value = airline.iataCode;
                                                _.airlineScrees.airlineId
                                                    .value = airline.id;
                                                Get.back(
                                                    result: _.airlineScrees);
                                                _.Controller.clear();

                                              },
                                              leading: Image.network(
                                                imageUrl(airline.imageUrl),
                                                width: 35.w,
                                                height: 35.w,
                                              ),
                                              title: CustomText(
                                                text: local
                                                    ? airline.nameAr.toString()
                                                    : airline.nameEn.toString(),
                                                fontWeight: FontWeight.bold,
                                                color: blackColor,
                                              ),
                                               ));
                                      })))
                        ],
                      )))
                  : Center(child: CircularProgressIndicator()),
            ))));
  }
}
