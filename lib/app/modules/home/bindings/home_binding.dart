import 'package:e_travel/app/modules/home/controllers/airline_controller.dart';
import 'package:get/get.dart';

import '../controllers/home_controller.dart';

class HomeBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<HomeController>(
          () => HomeController(),
    );
    Get.lazyPut<AirlineController>(
          () => AirlineController(),
    );
  }
}
