import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:firebase_performance/firebase_performance.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class FirebaseController extends GetxController {
  // Firebase instance
  late FirebaseApp _app;

  // Firebase Analytics instance
  late FirebaseAnalytics _analytics;

  // Firebase Performance instance
  late FirebasePerformance _performance;

  // Firebase Crashlytics instance
  late FirebaseCrashlytics _crashlytics;

  // Firebase Messaging instance
  late FirebaseMessaging _messaging;

  @override
  void onInit() {
    super.onInit();
    _init();
  }

  Future<void> _init() async {
    _app = await Firebase.initializeApp();
    _analytics = FirebaseAnalytics.instance;
    _performance = FirebasePerformance.instance;
    _crashlytics = FirebaseCrashlytics.instance;
    _messaging = FirebaseMessaging.instance;

    NotificationSettings settings = await _messaging.requestPermission(
      alert: true,
      badge: true,
      sound: true,
    );
    print('User granted permission: ${settings.authorizationStatus}');

    FirebaseMessaging.onMessage.listen((RemoteMessage message) {
      print('Got a message whilst in the foreground!');
      print('Message data: ${message.data}');
    });

    FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) {
      print('Message clicked!');
    });

    FlutterError.onError = FirebaseCrashlytics.instance.recordFlutterError;
  }

  // Getter methods
  FirebaseApp get app => _app;

  FirebaseAnalytics get analytics => _analytics;

  FirebasePerformance get performance => _performance;

  FirebaseCrashlytics get crashlytics => _crashlytics;

  FirebaseMessaging get messaging => _messaging;
}
