import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_mobile_ads/google_mobile_ads.dart';

class AdController extends GetxController {
  Rx<NativeAd?> nativeAd1 = Rx<NativeAd?>(null);
  Rx<NativeAd?> nativeAd2 = Rx<NativeAd?>(null);
  Rx<AppOpenAd?> appOpenAd1 = Rx<AppOpenAd?>(null);

  RxBool nativeAd1IsLoaded = false.obs;
  RxBool nativeAd2IsLoaded = false.obs;

  String testAdUnit = 'ca-app-pub-3940256099942544/2247696110';

  String adUnitId1 = 'ca-app-pub-3078030595343032/6795580857';
  String adUnitId2 = '';

  String adOpenAppUnitId1 = '';

  @override
  void onInit() {
    super.onInit();
  }

  Future<void> loadAd1() async {
    // await settingsController.loadPreferences();

    // if(settingsController.showAds.value) {
    nativeAd1.value = NativeAd(
      adUnitId: testAdUnit,
      request: AdRequest(),
      listener: NativeAdListener(
        onAdLoaded: (Ad ad) {
          print('$NativeAd loaded.');
          nativeAd1IsLoaded.value = true;
        },
        onAdFailedToLoad: (Ad ad, LoadAdError error) {
          print('$NativeAd failedToLoad: $error');
          ad.dispose();
        },
        onAdOpened: (Ad ad) => print('$NativeAd onAdOpened.'),
        onAdClosed: (Ad ad) => print('$NativeAd onAdClosed.'),
      ),
      nativeTemplateStyle: NativeTemplateStyle(
        templateType: TemplateType.small,
        mainBackgroundColor: Colors.white12,
        callToActionTextStyle: NativeTemplateTextStyle(
          textColor: Colors.white,
          backgroundColor: Colors.indigo,
          style: NativeTemplateFontStyle.bold,
          size: 16.0,
        ),
        primaryTextStyle: NativeTemplateTextStyle(
          textColor: Colors.indigo,
          backgroundColor: Colors.white,
          style: NativeTemplateFontStyle.bold,
          size: 14.0,
        ),
        secondaryTextStyle: NativeTemplateTextStyle(
          textColor: Colors.grey[700],
          backgroundColor: Colors.white,
          style: NativeTemplateFontStyle.normal,
          size: 14.0,
        ),
        tertiaryTextStyle: NativeTemplateTextStyle(
          textColor: Colors.grey[700],
          backgroundColor: Colors.white,
          style: NativeTemplateFontStyle.normal,
          size: 14.0,
        ),
        cornerRadius: 12.0,
      ),
    )..load();
    // }
  }

  // Future<void> loadAd3() async {
  //   await settingsController.loadPreferences();
  //
  //   AppOpenAd.load(
  //     adUnitId: adOpenAppUnitId1,
  //     orientation: AppOpenAd.orientationPortrait,
  //     request: AdRequest(),
  //     adLoadCallback: AppOpenAdLoadCallback(
  //       onAdLoaded: (ad) {
  //         appOpenAd1.value = ad;
  //
  //         appOpenAd1.value?.fullScreenContentCallback = FullScreenContentCallback(
  //           onAdShowedFullScreenContent: (ad) {
  //             print('$ad onAdShowedFullScreenContent');
  //           },
  //           onAdFailedToShowFullScreenContent: (ad, error) {
  //             print('$ad onAdFailedToShowFullScreenContent: $error');
  //           },
  //           onAdDismissedFullScreenContent: (ad) {
  //             print('$ad onAdDismissedFullScreenContent');
  //             //loadAd3();
  //           },
  //         );
  //
  //
  //       },
  //       onAdFailedToLoad: (error) {
  //         print('AppOpenAd failed to load: $error');
  //         // Handle the error.
  //       },
  //     ),
  //   );
  // }

  @override
  void onClose() {
    super.onClose();
    nativeAd1.value?.dispose();
    // nativeAd2.value?.dispose();
  }
}
