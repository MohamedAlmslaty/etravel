const URL = 'http://etravel.server.ly:8080/';
//const URL = 'http://154.73.128.219/';
//const URL = 'https://app.etravel.ly/';
const baseURL = URL + 'api/';
const imageBase = baseURL + 'public/file/download/';

String imageUrl(String url) {
  return '$imageBase$url';
}
