import 'package:e_travel/app/util/consts/colors.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';

///Loading
loading() {
  EasyLoading.instance
    ..displayDuration = const Duration(milliseconds: 2000)
    ..indicatorType = EasyLoadingIndicatorType.pulse
    ..loadingStyle = EasyLoadingStyle.custom
    ..indicatorSize = 45.0
    ..radius = 10.0
    ..progressColor = primaryColor
    ..backgroundColor = whiteColor
    ..indicatorColor = primaryColor
    ..textColor = blackColor
    ..maskColor = blackColor.withOpacity(0.5)
    ..userInteractions = false
    ..dismissOnTap = true;
  EasyLoading.show(status: 'loading...'.tr, maskType: EasyLoadingMaskType.custom);
}
