import 'package:flutter/material.dart';

///Colors
const Color primaryColor = Color(0xff31287C);
const Color secondaryColor = Color(0xffFFDD00);
const Color backgroundColor = Color(0xffF2F4F6);
const Color textColor = Color(0xffC7C5D2);
const Color subtextColor = Color(0xffB6B3C6);
const Color blackColor = Colors.black;
const Color whiteColor = Colors.white;
const Color greyColor = Colors.grey;
