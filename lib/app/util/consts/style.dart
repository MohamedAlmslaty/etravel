import 'package:e_travel/app/util/consts/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';

InputDecoration formInputStyle = InputDecoration(
  filled: true,

  fillColor: Colors.transparent,
  hintStyle: TextStyle(
    color: primaryColor,
  ),
  labelStyle: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
  focusedBorder: OutlineInputBorder(borderRadius: BorderRadius.all(Radius.circular(50)), borderSide: BorderSide(color: primaryColor)),
  enabledBorder: OutlineInputBorder(borderRadius: BorderRadius.all(Radius.circular(50)), borderSide: BorderSide(color: secondaryColor)),
  errorBorder: OutlineInputBorder(borderRadius: BorderRadius.all(Radius.circular(50)), borderSide: BorderSide(color: Colors.red)),
  // focusColor: primColor,
);

InputDecoration dialogInputStyle = InputDecoration(
  filled: true,
  fillColor: Colors.transparent,
  labelStyle: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
  focusedBorder: OutlineInputBorder(borderRadius: BorderRadius.all(Radius.circular(50)), borderSide: BorderSide(color: primaryColor)),
  enabledBorder: OutlineInputBorder(borderRadius: BorderRadius.all(Radius.circular(50)), borderSide: BorderSide(color: primaryColor)),
  errorBorder: OutlineInputBorder(borderRadius: BorderRadius.all(Radius.circular(50)), borderSide: BorderSide(color: Colors.red)),
  // focusColor: primColor,
);

InputDecoration searchInputStyle = InputDecoration(
  prefixIconConstraints: BoxConstraints(minWidth: Get.width * .1, minHeight: Get.width * .1),
  prefixIcon: Padding(
    padding: const EdgeInsets.only(bottom: 12, left: 15, right: 15, top: 8),
    child: SvgPicture.asset(
      'assets/svgs/search.svg',
      height: Get.height * .025,
    ),
  ),
  filled: true,
  fillColor: Color(0xffF6F6F6),
  hintText: "البحث عن العناصر",
  hintStyle: TextStyle(
    color: Color(0xffBCBCBC),
    fontSize: Get.width * .04,
  ),

  focusedBorder: OutlineInputBorder(borderRadius: BorderRadius.all(Radius.circular(100)), borderSide: BorderSide(color: Color(0xffBCBCBC))),
  enabledBorder: OutlineInputBorder(borderRadius: BorderRadius.all(Radius.circular(100)), borderSide: BorderSide(color: Colors.transparent)),
  errorBorder: OutlineInputBorder(borderRadius: BorderRadius.all(Radius.circular(100)), borderSide: BorderSide(color: Colors.red)),
  // focusColor: primColor,
);

showSnackBar({title, required message}) {
  Get.snackbar(
    title,
    message,
    colorText: primaryColor,
    backgroundColor: secondaryColor,
    duration: Duration(seconds: 4),
    isDismissible: true,
    dismissDirection: DismissDirection.horizontal,
    forwardAnimationCurve: Curves.easeOutBack,
  );
}
