import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:get/get.dart';

class FbConfig {
  static Future<void> init() async {
    Get.put<FirebaseMessaging>(FirebaseMessaging.instance);

    FirebaseMessaging.instance
      ..requestPermission(
        alert: true,
        announcement: true,
        badge: true,
        carPlay: true,
        criticalAlert: true,
        provisional: true,
        sound: true,
      )
      ..subscribeToTopic('all');

    FirebaseMessaging.onMessage.listen((RemoteMessage message) {
      RemoteNotification? notification = message.notification;
      AndroidNotification? android = message.notification?.android;
      if (notification != null && android != null) {
        print('onMessage: $message');
        //   Get.snackbar(
        //   'New Message!',
        //   notification.body ?? 'Empty body',
        //   snackPosition: SnackPosition.BOTTOM,
        // );
      }
    });


    FirebaseMessaging.onBackgroundMessage(_firebaseMessagingBackgroundHandler);
  }
}

Future<void> _firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  // If you're going to use other Firebase services in the background, such as Firestore,
  // make sure you call `initializeApp` before using other Firebase services.
  await Firebase.initializeApp();
}
