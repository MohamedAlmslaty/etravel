import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:dio/dio.dart';
import 'package:e_travel/app/data/models/error_model.dart';
import 'package:e_travel/app/modules/main/controllers/main_controller.dart';
import 'package:e_travel/app/util/consts/style.dart';
import 'package:get/get_core/get_core.dart';
import 'package:get/get_instance/get_instance.dart';
import 'package:get/get_navigation/get_navigation.dart';
import 'package:get/get_utils/src/extensions/internacionalization.dart';
import 'package:get_storage/get_storage.dart';

import '../../../routes/app_pages.dart';

class AppInterceptors extends Interceptor {
  @override
  void onRequest(RequestOptions options, RequestInterceptorHandler handler) async {
    if (Get.find<GetStorage>().hasData('token')) {
      options.headers["Authorization"] = "Bearer " + Get.find<GetStorage>().read('token');
    }
    check().then((intenet) {
      if (intenet != null && intenet) {
        if (Get.find<GetStorage>().hasData('token')) {
          options.headers["Authorization"] = "Bearer " + Get.find<GetStorage>().read('token');
        }
      } else {
        showSnackBar(title: 'noInternet'.tr.tr, message: 'Connect to the Internet and try again'.tr);
      }
    });

    super.onRequest(options, handler);
  }

  @override
  void onError(DioError err, ErrorInterceptorHandler handler) {
    String message = '';
    switch (err.type) {
      case DioErrorType.cancel:
        message = "Request to API server was cancelled".tr;
        break;
      case DioErrorType.connectTimeout:
        message = "Connection timeout with API server".tr;
        break;
      case DioErrorType.receiveTimeout:
        message = "Receive timeout in connection with API server".tr;
        break;
      case DioErrorType.response:
        if (err.response?.statusCode != null) {
          message = handleError(err.response!.statusCode!, ErrorModel.fromJson(err.response!.data));
        } else {
          message = handleError(0, 'error');
        }
        break;
      case DioErrorType.sendTimeout:
        message = "Send timeout in connection with API server".tr;
        break;
      default:
        message = "something went wrong".tr;
        break;
    }
    if (err.response!.statusCode != 401 || ErrorModel.fromJson(err.response!.data).detail!.contains("Bad credentials")) {
      showSnackBar(title: 'error'.tr, message: message);
    }
    super.onError(err, handler);
  }

  @override
  Future<void> onResponse(Response response, ResponseInterceptorHandler handler) async {
    if (response.data is Map<String, dynamic>) {
      Map<String, dynamic> data = response.data;
      if (data.containsKey('id_token')) {
        await Get.find<GetStorage>().write("token", data['id_token']);
        MainController().onInit();
        Get.offAllNamed(Routes.MAIN);
      }
    }
    super.onResponse(response, handler);
  }

  Future<bool> check() async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile) {
      return true;
    } else if (connectivityResult == ConnectivityResult.wifi) {
      return true;
    }
    return false;
  }

  static String handleError(int statusCode, dynamic error) {
    switch (statusCode) {
      case 400:
        return '${error.title}'.tr;
      case 422:
        return 'the data enter not valid'.tr;
      case 404:
        return error["message"];
      case 401:
        Get.find<GetStorage>().remove('token');
        return '${error.detail}'.tr;
      case 500:
        return 'Internal server error'.tr;
      default:
        return 'something went wrong'.tr;
    }
  }
}
