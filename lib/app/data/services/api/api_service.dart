import 'dart:async';
import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:dio_http_cache/dio_http_cache.dart';
import 'package:e_travel/app/util/consts/base.dart';
import 'package:get/get_instance/get_instance.dart';
import 'package:get/route_manager.dart';
import 'package:get_storage/get_storage.dart';

import 'interceptors.dart';

class ApiService {
  ApiService()
      : dio = Dio(
          BaseOptions(
            baseUrl: baseURL,
            headers: {Headers.contentTypeHeader: Headers.jsonContentType, 'charset': 'utf-8'},
          ),
        )..interceptors.addAll([
            LogInterceptor(request: true, responseBody: true, requestBody: true),
            DioCacheManager(CacheConfig(
              baseUrl: baseURL,
            )).interceptor,
            AppInterceptors()
          ]);

  factory ApiService.getInstance() {
    return ApiService();
  }

  Dio dio;
  final box = Get.find<GetStorage>();

  Options options = Options();

  Future<void> _base(
    String url,
    Map<String, dynamic> extraHeaders,
  ) async {
    Map<String, dynamic>? headers = {};
    headers.addAll(extraHeaders);
    options = Options(headers: headers);
  }

  Future<Response> get(
    String url, {
    Map<String, dynamic>? queryParameters,
    Map<String, dynamic> extraHeaders = const {},
  }) async {
    await _base(url, extraHeaders);
print(url);
print(queryParameters);

    Response response = await dio.get(
      url,
      options: options,
      queryParameters: queryParameters,
    );
    return response;
  }

  Future<Response> post(
    String url, {
    dynamic data,
    bool requireAuthorization = true,
    Map<String, dynamic>? queryParameters,
    Map<String, dynamic> extraHeaders = const {},
    ProgressCallback? onSendProgress,
  }) async {
    await _base(url, extraHeaders);

    Response response = await dio.post(
      url,
      data: data,
      options: options,
      queryParameters: queryParameters,
      onSendProgress: onSendProgress,
    );

    return response;
  }

  Future<Response> put(
    String url, {
    dynamic data,
    bool requireAuthorization = true,
    Map<String, dynamic>? queryParameters,
    Map<String, dynamic> extraHeaders = const {},
  }) async {
    await _base(url, extraHeaders);

    Response response = await dio.put(
      url,
      data: data,
      options: options,
      queryParameters: queryParameters,
    );
    return response;
  }

  Future<Response> patch(
    String url, {
    dynamic data,
    bool requireAuthorization = true,
    Map<String, dynamic>? queryParameters,
    Map<String, dynamic> extraHeaders = const {},
  }) async {
    await _base(url, extraHeaders);

    Response response = await dio.patch(
      url,
      data: data,
      options: options,
      queryParameters: queryParameters,
    );
    return response;
  }

  Future<Response> delete(
    String url, {
    dynamic data,
    bool requireAuthorization = true,
    Map<String, dynamic>? queryParameters,
    Map<String, dynamic> extraHeaders = const {},
  }) async {
    await _base(url, extraHeaders);

    Response response = await dio.delete(
      url,
      data: data,
      options: options,
      queryParameters: queryParameters,
    );
    return response;
  }

  //TODO :: WILL BE USED TO IMPROVE PERFORMANCE OF THE REQUEST
  Stream<Map<String, dynamic>> getNdJsonStream(String url, {Map<String, dynamic> extraHeaders = const {}}) {
    final StreamController<Map<String, dynamic>> ndJsonController = StreamController<Map<String, dynamic>>();
    String collectedData = '';

    _base(url, extraHeaders).then((_) async {
      try {
        Response<ResponseBody> response = await dio.get<ResponseBody>(
          url,
          options: Options(
            responseType: ResponseType.stream,
            headers: {
              'Accept': 'application/x-ndjson',
            },
          ),
        );

        response.data!.stream.listen(
          (List<int> onData) {
            collectedData += utf8.decode(onData);
            while (collectedData.contains('\n')) {
              final index = collectedData.indexOf('\n');
              final jsonStr = collectedData.substring(0, index);
              collectedData = collectedData.substring(index + 1);

              final jsonMap = json.decode(jsonStr);
              ndJsonController.add(jsonMap);
            }
          },
          onDone: () => ndJsonController.close(),
          onError: (e) => ndJsonController.addError(e),
        );
      } catch (e) {
        ndJsonController.addError(e);
      }
    });

    return ndJsonController.stream;
  }
}
