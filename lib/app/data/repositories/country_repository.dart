import 'dart:developer';
import 'package:e_travel/app/data/models/country.dart';
import 'package:e_travel/app/data/services/api/api_service.dart';

class CountryRepository extends ApiService {
  static CountryRepository get instance => CountryRepository();

  Future<List<Country>> getCountries() async {
    String url='public/countries?sort=menuOrder&page=0&size=1000';
    dynamic responseBody;
    await get(
        url
    ).then((response) async {
      if (response.statusCode == 200) {
        final data = response.data as List;
        responseBody =
        List<Country>.from(data.map((e) => Country.fromJson(e)));
      }
    }).catchError((onError) async {
      log('error : ${onError} ${onError.toString().isEmpty}');
      responseBody = <Country>[];
    });

    return responseBody;
  }
}
