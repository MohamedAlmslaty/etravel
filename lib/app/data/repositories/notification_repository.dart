import 'dart:developer';

import 'package:e_travel/app/data/models/notification.dart';
import 'package:e_travel/app/data/models/query_models/query.dart';
import 'package:e_travel/app/data/services/api/api_service.dart';

class NotificationRepository extends ApiService {
  static NotificationRepository get instance => NotificationRepository();

  Future<List<Notification>> getNotifications() async {
    String url='notifications';
    dynamic responseBody;
    Map<String, dynamic> queryParam = {};
    queryParam..addAll(Query(sort: ["id,desc"]).toMap());
    await get(
        url,queryParameters: queryParam
    ).then((response) async {
      if (response.statusCode == 200) {
        final data = response.data as List;
        responseBody =
        List<Notification>.from(data.map((e) => Notification.fromJson(e)));
      }
    }).catchError((onError) async {
      log('error : ${onError} ${onError.toString().isEmpty}');
      responseBody = <Notification>[];
    });

    return responseBody;
  }
}
