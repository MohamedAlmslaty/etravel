import 'dart:developer';
import 'package:e_travel/app/data/models/flight_offer.dart';
import 'package:e_travel/app/data/services/api/api_service.dart';

class FlightOfferRepository extends ApiService {
  static FlightOfferRepository get instance => FlightOfferRepository();

  Future<FlightOffer> getFlightOffers() async {
    String url='flight-offers';
    dynamic responseBody;
    await get(
        url
    ).then((response) async {
      if (response.statusCode == 200) {
        final data = response.data as List;
        responseBody =
        List<FlightOffer>.from(data.map((e) => FlightOffer.fromJson(e)));
      }
    }).catchError((onError) async {
      log('error : ${onError} ${onError.toString().isEmpty}');
      responseBody = <FlightOffer>[];
    });

    return responseBody;
  }
}
