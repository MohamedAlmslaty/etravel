import 'dart:developer';

import 'package:e_travel/app/data/models/passenger.dart';
import 'package:e_travel/app/data/services/api/api_service.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';

class PassengerRepository extends ApiService {
  static PassengerRepository get instance => PassengerRepository();

  Future<List<Passenger>> getPassengers(int currentPage,int pageSize) async {
    String url = 'passengers?page=$currentPage&size=$pageSize';
    dynamic responseBody;
    await get(
      url,
    ).then((response) async {
      if (response.statusCode == 200) {
        final data = response.data as List;
        responseBody = List<Passenger>.from(data.map((e) => Passenger.fromJson(e)));
      }
    }).catchError((onError) async {
      log('error : ${onError} ${onError.toString().isEmpty}');
      responseBody = <Passenger>[];
    });

    return responseBody;
  }

  Future<void> addNewPassenger(Passenger passenger) async {
    String url = 'passengers';
    await post(url, data: passenger.toJson(), extraHeaders: {'Accept': 'application/json'}).then((response) async {
      if (response.statusCode == 201 || response.statusCode == 200) Get.back();
    }).catchError((onError) async {
      log('error : ${onError} ${onError.toString().isEmpty}');
      EasyLoading.dismiss();
    });
  }

  Future<bool> deletePassenger(int id) async {
    String url = 'passengers/$id';
    dynamic responseBody = false;
    await delete(url, extraHeaders: {'Accept': 'application/json'}).then((response) async {
      if (response.statusCode == 204) responseBody = true;
    }).catchError((onError) async {
      log('error : ${onError} ${onError.toString().isEmpty}');
      EasyLoading.dismiss();
      responseBody = false;
    });
    return responseBody;
  }

  Future<void> updatePassenger(int id, Passenger passenger) async {
    String url = 'passengers/$id';
    await put(url, data: passenger.toJson(), extraHeaders: {'Accept': 'application/json'}).then((response) async {
      if (response.statusCode == 200) Get.back();
    }).catchError((onError) async {
      log('error : ${onError} ${onError.toString().isEmpty}');
      EasyLoading.dismiss();
    });
  }
}
