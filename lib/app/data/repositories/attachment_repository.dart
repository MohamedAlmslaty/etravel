import 'dart:convert';
import 'dart:developer';

import 'package:e_travel/app/data/models/attachment.dart';
import 'package:e_travel/app/data/services/api/api_service.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';

class AttachmentRepository extends ApiService {
  static AttachmentRepository get instance => AttachmentRepository();

  Future<bool> addNewAttachment(List<Attachment> attachment,String pnr) async {
    String url = 'attachments/pnr/$pnr/bulk';
    dynamic responseBody;
    String jsonBody = jsonEncode(attachment.map((obj) => obj.toJson()).toList());
    await post(url, data: jsonBody, extraHeaders: {'Accept': 'application/json'}).then((response) async {
      if (response.statusCode == 201 || response.statusCode == 200) responseBody=true;
    }).catchError((onError) async {
      log('error : ${onError} ${onError.toString().isEmpty}');
      EasyLoading.dismiss();
      responseBody=false;
    });
    return responseBody;
  }

 }
