import 'dart:developer';
import 'package:e_travel/app/data/models/airline.dart';
import 'package:e_travel/app/data/models/referral.dart';
import 'package:e_travel/app/data/services/api/api_service.dart';

class ReferralRepository extends ApiService {
  static ReferralRepository get instance => ReferralRepository();

  Future<List<Referral>> getReferrals() async {
    String url='referrals?page=0&size=1000';
    dynamic responseBody;
      await get(
        url
      ).then((response) async {
        if (response.statusCode == 200) {
          final data = response.data as List;
          responseBody =
          List<Referral>.from(data.map((e) => Referral.fromJson(e)));
        }
      }).catchError((onError) async {
        log('error : ${onError} ${onError.toString().isEmpty}');
        responseBody = <Referral>[];
      });

    return responseBody;
  }

Future<Referral> generateReferral() async {
    String url='referrals/generate';
    dynamic responseBody;
      await post(
        url
      ).then((response) async {
        if (response.statusCode == 201 ||response.statusCode == 200) {
          responseBody=Referral.fromJson(response.data);
        }
      }).catchError((onError) async {
        log('error : ${onError} ${onError.toString().isEmpty}');
        responseBody = Referral();
      });

    return responseBody;
  }
}
