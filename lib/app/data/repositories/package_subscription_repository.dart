import 'dart:developer';

import 'package:e_travel/app/data/models/package_subscription.dart';
import 'package:e_travel/app/data/services/api/api_service.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';

import '../models/query_models/query.dart';

class PackageSubscriptionRepository extends ApiService {
  static PackageSubscriptionRepository get instance => PackageSubscriptionRepository();

  Future<List<PackageSubscription>> getPackageSubscriptions() async {
    String url = 'package-subscriptions';
    dynamic responseBody;
    Map<String, dynamic> queryParam = {};
    queryParam..addAll(Query(sort: ["id,desc"], size: 100).toMap());
    await get(url, queryParameters: queryParam).then((response) async {
      if (response.statusCode == 200) {
        final data = response.data as List;
        responseBody = List<PackageSubscription>.from(data.map((e) => PackageSubscription.fromJson(e)));
      }
    }).catchError((onError) async {
      log('error : ${onError} ${onError.toString().isEmpty}');
      responseBody = <PackageSubscription>[];
    });

    return responseBody;
  }

  Future<PackageSubscription> findPackageSubscriptionById(int id) async {
    String url = 'package-subscriptions/$id';
    dynamic responseBody;
    await get(
      url,
    ).then((response) async {
      if (response.statusCode == 200) {
        responseBody = PackageSubscription.fromJson(response.data);
      }
    }).catchError((onError) async {
      log('error : ${onError} ${onError.toString().isEmpty}');
      responseBody = PackageSubscription();
    });

    return responseBody;
  }

  Future<bool> addPackageSubscription(PackageSubscription packageSubscription) async {
    String url = 'package-subscriptions';
    dynamic responseBody;
    await post(url, data: packageSubscription.toJson(), extraHeaders: {'Accept': 'application/json'}).then((response) async {
      if (response.statusCode == 201 || response.statusCode == 200) responseBody = true;
    }).catchError((onError) async {
      log('error : ${onError} ${onError.toString().isEmpty}');
      responseBody = false;
      EasyLoading.dismiss();
    });
    return responseBody;
  }
}
