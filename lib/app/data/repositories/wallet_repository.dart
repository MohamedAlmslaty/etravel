import 'dart:convert';
import 'dart:developer';

import 'package:e_travel/app/data/models/etravel_fee.dart';
import 'package:e_travel/app/data/models/query_models/query.dart';
import 'package:e_travel/app/data/models/wallet.dart';
import 'package:e_travel/app/data/services/api/api_service.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';

RxDouble walletBalance = 0.0.obs;
RxBool loadingBalance = false.obs;

class WalletRepository extends ApiService {
  static WalletRepository get instance => WalletRepository();

  Future<double> getWalletBalance() async {
    String url = 'customer-wallets/balance';
    dynamic responseBody;

    await get(
      url,
    ).then((response) async {
      if (response.statusCode == 200) {
        responseBody = response.data;
      }
    }).catchError((onError) async {
      log('error : ${onError} ${onError.toString().isEmpty}');
      responseBody = 0.0;
    });

    return responseBody;
  }

  Future<List<Wallet>> getWalletTransactions() async {
    String url = 'customer-wallets';
    dynamic responseBody;
    Map<String, dynamic> queryParam = {};
    queryParam..addAll(Query(sort: ["id,desc"]).toMap());
    await get(url, queryParameters: queryParam).then((response) async {
      if (response.statusCode == 200) {
        final data = response.data as List;
        responseBody = List<Wallet>.from(data.map((e) => Wallet.fromJson(e)));
      }
    }).catchError((onError) async {
      log('error : ${onError} ${onError.toString().isEmpty}');
      responseBody = <Wallet>[];
    });

    return responseBody;
  }

  Future<bool> walletTransfer(
      double amount, String toCustomer, String otp) async {
    String url =
        'customer-wallets/transfer?amount=$amount&toCustomerPublicKey=$toCustomer&otp=$otp';
    dynamic responseBody;
    await post(
      url,
    ).then((response) async {
      if (response.statusCode == 200) {
        log('response : ${response.data}');
        responseBody = true;
      }
    }).catchError((onError) async {
      responseBody = false;
      log('error : ${onError} ${onError.toString().isEmpty}');
    });
    return responseBody;
  }

  Future<bool> walletTransferByPhoneNumber(double amount, String phoneNumber, String otp) async {
    String url = 'customer-wallets/transfer-mobile?mobileNo=${phoneNumber.replaceAll('+', '%2B')}&amount=$amount&otp=$otp';
    dynamic responseBody;
    await post(url).then((response) async {
      if (response.statusCode == 200 || response.statusCode == 201) {
        log('response : ${response.data}');
        responseBody = true;
      }
    }).catchError((onError) async {
      log('error : ${onError} ${onError.toString().isEmpty}');
      EasyLoading.dismiss();
      responseBody = false;
    });
    return responseBody;
  }

  Future<List<EtravelFee>> getTransferFee(double amount) async {
    String url = 'customer-wallets/transfer-fees?amount=$amount';
    dynamic responseBody;
    await post(url).then((response) async {
      if (response.statusCode == 200) {
        log('response : ${jsonDecode(response.data)}');
        final data = jsonDecode(response.data) as List;
        responseBody =
            List<EtravelFee>.from(data.map((e) => EtravelFee.fromJson(e)));
      }
    }).catchError((onError) async {
      log('error : ${onError} ${onError.toString().isEmpty}');
      EasyLoading.dismiss();
      responseBody = <EtravelFee>[];
    });
    return responseBody;
  }

  Future<bool> getOTP() async {
    String url = 'activation/customer/send-otp';
    dynamic responseBody;
    await post(
      url,
    ).then((response) async {
      if (response.statusCode == 200) {
        responseBody = true;
        print("===============");
        print(response.data);
        print("===============");
      }
    }).catchError((onError) async {
      log('error : ${onError} ${onError.toString().isEmpty}');
      EasyLoading.dismiss();
      responseBody = false;
    });

    return responseBody;
  }

  Future<bool> getOTPEmail() async {
    String url = 'activation/email-otp';
    dynamic responseBody;
    await post(
      url,
    ).then((response) async {
      if (response.statusCode == 200) {
        responseBody = true;
      }
    }).catchError((onError) async {
      log('error : ${onError} ${onError.toString().isEmpty}');
      EasyLoading.dismiss();
      responseBody = false;
    });

    return responseBody;
  }
}
