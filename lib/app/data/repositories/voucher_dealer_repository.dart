import 'dart:developer';
import 'package:e_travel/app/data/models/voucher_dealer.dart';
import 'package:e_travel/app/data/services/api/api_service.dart';

class VoucherDealerRepository extends ApiService {
  static VoucherDealerRepository get instance => VoucherDealerRepository();

  Future<List<VoucherDealer>> getVoucherDealers() async {
    String url='public/voucher-dealers?page=0&size=1000';
    dynamic responseBody;
      await get(
        url
      ).then((response) async {
        if (response.statusCode == 200) {
          final data = response.data as List;
          responseBody =
          List<VoucherDealer>.from(data.map((e) => VoucherDealer.fromJson(e)));
        }
      }).catchError((onError) async {
        log('error : ${onError} ${onError.toString().isEmpty}');
        responseBody = <VoucherDealer>[];
      });

    return responseBody;
  }
}
