import 'dart:developer';

import 'package:e_travel/app/data/models/transactions.dart';
import 'package:e_travel/app/data/services/api/api_service.dart';

import '../models/query_models/query.dart';

class TransactionsRepository extends ApiService {
  static TransactionsRepository get instance => TransactionsRepository();

  Future<List<Transactions>> getTransactions() async {
    String url = 'transactions';
    Map<String, dynamic> queryParam = {};
    queryParam..addAll(Query(sort: ["id,desc"]).toMap());
    dynamic responseBody;
    await get(url, queryParameters: queryParam).then((response) async {
      if (response.statusCode == 200) {
        final data = response.data as List;
        responseBody = List<Transactions>.from(data.map((e) => Transactions.fromJson(e)));
      }
    }).catchError((onError) async {
      log('error : ${onError} ${onError.toString().isEmpty}');
      responseBody = <Transactions>[];
    });

    return responseBody;
  }
}
