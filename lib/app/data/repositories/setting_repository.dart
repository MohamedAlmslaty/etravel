import 'dart:developer';
import 'package:e_travel/app/data/models/setting.dart';
import 'package:e_travel/app/data/services/api/api_service.dart';

class SettingRepository extends ApiService {
  static SettingRepository get instance => SettingRepository();

  Future<Setting> getSetting(String key) async {
    String url = 'public/settings/by-key/$key';
    dynamic responseBody;
    await get(
      url,
    ).then((response) async {
      if (response.statusCode == 200) {
        responseBody = Setting.fromJson(response.data);
      }
    }).catchError((onError) async {
      log('error : ${onError} ${onError.toString().isEmpty}');
      responseBody = Setting();
    });

    return responseBody;
  }
}
