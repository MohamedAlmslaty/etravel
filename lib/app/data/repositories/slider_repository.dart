import 'dart:developer';
import 'package:e_travel/app/data/models/slider.dart';
import 'package:e_travel/app/data/services/api/api_service.dart';

class SliderRepository extends ApiService {
  static SliderRepository get instance => SliderRepository();

  Future<List<Slider>> getSliders(String type) async {
    String url = 'public/sliders?sort=menuOrder&sliderType.equals=$type';
    dynamic responseBody;
    await get(
      url,
    ).then((response) async {
      if (response.statusCode == 200) {
        final data = response.data as List;
        responseBody = List<Slider>.from(data.map((e) => Slider.fromJson(e)));
      }
    }).catchError((onError) async {
      log('error : ${onError} ${onError.toString().isEmpty}');
      responseBody = <Slider>[];
    });

    return responseBody;
  }
}
