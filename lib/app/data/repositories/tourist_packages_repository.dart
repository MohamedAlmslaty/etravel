import 'dart:developer';

import 'package:e_travel/app/data/models/tourist_packages.dart';
import 'package:e_travel/app/data/services/api/api_service.dart';

import '../models/query_models/query.dart';

class TouristPackagesRepository extends ApiService {
  static TouristPackagesRepository get instance => TouristPackagesRepository();

  Future<List<TouristPackages>> getTouristPackages() async {
    String url = 'public/tourist-packages';
    dynamic responseBody;
    Map<String, dynamic> queryParam = {};
    queryParam..addAll(Query(sort: ["id,desc"], size: 100).toMap());
    await get(url, queryParameters: queryParam).then((response) async {
      if (response.statusCode == 200) {
        final data = response.data as List;
        responseBody = List<TouristPackages>.from(data.map((e) => TouristPackages.fromJson(e)));
      }
    }).catchError((onError) async {
      log('error : ${onError} ${onError.toString().isEmpty}');
      responseBody = <TouristPackages>[];
    });

    return responseBody;
  }

  Future<TouristPackages> getTouristPackagePublic(int id) async {
    String url = 'public/tourist-packages/$id';
    dynamic responseBody;

    await get(
      url,
    ).then((response) async {
      if (response.statusCode == 200) {
        responseBody = TouristPackages.fromJson(response.data);
      }
    }).catchError((onError) async {
      log('error : ${onError} ${onError.toString().isEmpty}');
      responseBody = TouristPackages();
    });

    return responseBody;
  }
}
