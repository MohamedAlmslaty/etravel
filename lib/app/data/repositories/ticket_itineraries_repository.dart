import 'dart:developer';
import 'package:e_travel/app/data/models/query_models/query.dart';
import 'package:e_travel/app/data/models/ticket-itineraries.dart';
import 'package:e_travel/app/data/services/api/api_service.dart';

class TicketItinerariesRepository extends ApiService {
  static TicketItinerariesRepository get instance => TicketItinerariesRepository();

  Future<List<TicketItineraries>> getAllTicketItineraries({Query? query, IsActiveQuery? isActiveQuery}) async {
    String url = 'ticket-itineraries';
    dynamic responseBody;
    Map<String, dynamic> queryParam = {};
    queryParam
      ..addAll(isActiveQuery?.toMap() ?? {})
      ..addAll(query?.toMap() ?? {});
    await get(
      url,
      queryParameters: queryParam
    ).then((response) async {
      if (response.statusCode == 200) {
        final data = response.data as List;
        responseBody = List<TicketItineraries>.from(data.map((e) => TicketItineraries.fromJson(e)));
      }
    }).catchError((onError) async {
      log('error : ${onError} ${onError.toString().isEmpty}');
      responseBody = <TicketItineraries>[];
    });

    return responseBody;
  }
}
