import 'dart:developer';
import 'package:e_travel/app/data/models/airline.dart';
import 'package:e_travel/app/data/services/api/api_service.dart';

class AirlineRepository extends ApiService {
  static AirlineRepository get instance => AirlineRepository();

  Future<List<Airline>> getAirlines() async {
    String url='public/airlines?sort=menuOrder&page=0&size=1000';
    dynamic responseBody;
      await get(
        url
      ).then((response) async {
        if (response.statusCode == 200) {
          final data = response.data as List;
          responseBody =
          List<Airline>.from(data.map((e) => Airline.fromJson(e)));
        }
      }).catchError((onError) async {
        log('error : ${onError} ${onError.toString().isEmpty}');
        responseBody = <Airline>[];
      });

    return responseBody;
  }
}
