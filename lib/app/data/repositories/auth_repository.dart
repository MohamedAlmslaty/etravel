import 'dart:developer';

import 'package:e_travel/app/data/models/customer.dart';
import 'package:e_travel/app/data/models/key_and_password.dart';
import 'package:e_travel/app/data/models/login.dart';
import 'package:e_travel/app/data/services/api/api_service.dart';
import 'package:e_travel/app/util/consts/style.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class AuthRepository extends ApiService {
  static AuthRepository get instance => AuthRepository();

  Future<bool> registerAccount(Customer customer) async {
    bool result = false;
    String url = 'public/customers/register';
    await post(url,
        data: customer.toJson(),
        extraHeaders: {'Accept': 'application/json'}).then((response) async {
      if (response.statusCode == 201 || response.statusCode == 200) {
        authorize(LoginVM(
            password: customer.newPassword!,
            username: customer.email!,
            firebaseId: customer.firebaseId!));
        result = true;
        return true;
      } else {
        result = false;
        return false;
      }
    }).catchError((onError) async {
      log('error ::: ${onError.type} ');
      EasyLoading.dismiss();
      result = false;
      return false;
    });
    return result;
  }

  Future<bool> checkEmail(Customer customer) async {
    bool result = false;
    String url = 'public/customers/is-email-registered';
    await post(url,
        data: customer.toJson(),
        extraHeaders: {'Accept': 'application/json'}).then((response) async {
      if (response.statusCode == 200) {
        result = response.data;
      }
    }).catchError((onError) async {
      log('error ::: ${onError.type} ');
      EasyLoading.dismiss();
      result = false;
    });
    return result;
  }

  Future<void> authorize(LoginVM login) async {
    String url = 'authenticate';
    await post(url,
            extraHeaders: {'Accept': 'application/json'}, data: login.toJson())
        .then((response) async {
      if (response.data.containsKey('id_token')) {
        await Get.find<GetStorage>().write("token", response.data['id_token']);
      }
    }).catchError((onError) async {
      log('error ::: ${onError.type} ');
      EasyLoading.dismiss();
    });
  }

  Future<String> isAuthenticated() async {
    String url = 'authenticate';
    dynamic responseBody;
    await get(
      url,
    ).then((response) async {
      responseBody = response.data;
    }).catchError((onError) async {
      log('error ::: ${onError.type} ');
      responseBody = '';
    });
    return responseBody;
  }

  Future<void> activateAccount(String key) async {
    try {
      await get('users/authorities', queryParameters: {'key': key});
    } catch (e) {
      throw e;
    }
  }

  Future<void> requestPasswordReset(String email) async {
    try {
      await post('account/reset-password/init', data: email);
    } catch (e) {}
  }


  Future<void> sendResetPassword(String key, String newPassword) async {
    try {
      await post('account/reset-password/finish',
          data: {"key": key, "newPassword": newPassword}).then((value)  {
            if(value.statusCode==200||value.statusCode==201){
              Get.back();
              Get.back();
              showSnackBar(title: 'successful', message: 'Password changed successfully');
            }else {
              Get.back();
              Get.back();
              showSnackBar(title: 'error', message: 'something went wrong');
            }
      });
    } catch (e) {
    }
  }

  Future<bool> activationAccount(String typeConfirm, bool checked) async {
    String urlEmail = 'public/activation/email-otp?email=$typeConfirm';
    String urlMobile =
        'public/activation/sms-otp?mobileNo=${typeConfirm.replaceAll('+', '%2B')}';
    dynamic responseBody;
    await post(
      checked ? urlEmail : urlMobile,
    ).then((response) async {
      if (response.statusCode == 200) {
        responseBody = true;
      }
    }).catchError((onError) async {
      log('error : ${onError} ${onError.toString().isEmpty}');
      EasyLoading.dismiss();
      responseBody = false;
    });

    return responseBody;
  }

  Future<void> authorizeApple(String token) async {
    try {
      await post('/authenticate/appApple', data: token);
    } catch (e) {
      Get.back();
      throw e;
    }
  }

  Future<void> authorizeFacebook(String token) async {
    try {
      await post('/authenticate/appFacebook', data: token);
    } catch (e) {
      Get.back();
      throw e;
    }
  }

  Future<void> authorizeGoogle(String token) async {
    try {
      await post('/authenticate/appGoogle', data: token);
    } catch (e) {
      Get.back();
      throw e;
    }
  }

  Future<String> getPhoneNamber(String token) async {
    dynamic responseBody;
    await get(
            'https://graph.facebook.com/v12.0/me?fields=phone&access_token=${token}')
        .then((response) async {
      if (response.statusCode == 200) {
        final Map<String, dynamic> profile = response.data;
        responseBody = profile['phone'];
      }
    }).catchError((onError) async {
      log('error : ${onError} ${onError.toString().isEmpty}');
      EasyLoading.dismiss();
      responseBody = null;
    });

    return responseBody;
    ;
  }
}
