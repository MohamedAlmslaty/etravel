import 'dart:developer';

import 'package:e_travel/app/data/models/hotel_booking.dart';
import 'package:e_travel/app/data/services/api/api_service.dart';

import '../models/query_models/query.dart';

class HotelBookingRepository extends ApiService {
  static HotelBookingRepository get instance => HotelBookingRepository();

  Future<List<HotelBooking>> getHotelBookings() async {
    String url = 'hotel-bookings';
    dynamic responseBody;
    Map<String, dynamic> queryParam = {};
    queryParam..addAll(Query(sort: ["id,desc"], size: 100).toMap());
    await get(url, queryParameters: queryParam).then((response) async {
      if (response.statusCode == 200) {
        final data = response.data as List;
        responseBody = List<HotelBooking>.from(data.map((e) => HotelBooking.fromJson(e)));
      }
    }).catchError((onError) async {
      log('error : ${onError} ${onError.toString().isEmpty}');
      responseBody = <HotelBooking>[];
    });

    return responseBody;
  }
}
