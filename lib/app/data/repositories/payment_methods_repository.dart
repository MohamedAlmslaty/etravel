import 'dart:convert';
import 'dart:developer';
import 'package:e_travel/app/data/models/edfali.dart';
import 'package:e_travel/app/data/models/mastercard.dart';
import 'package:e_travel/app/data/models/moamalat.dart';
import 'package:e_travel/app/data/models/payment_method.dart';
import 'package:e_travel/app/data/models/query_models/query.dart';
import 'package:e_travel/app/data/models/sadad.dart';
import 'package:e_travel/app/data/models/sadad_otp.dart';
import 'package:e_travel/app/data/models/tadawal.dart';
import 'package:e_travel/app/data/models/transaction.dart';
import 'package:e_travel/app/data/models/uni_pay.dart';
import 'package:e_travel/app/data/services/api/api_service.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
class PaymentMethodsRepository extends ApiService {
  static PaymentMethodsRepository get instance => PaymentMethodsRepository();

   Future<List<PaymentMethods>> getAllPaymentMethodPublic({Query? query, IsActiveQuery? isActiveQuery}) async {
    String url='public/payment-methods?sort=menuOrder';
    dynamic responseBody;
    Map<String, dynamic> queryParam = {};
    queryParam
      ..addAll(isActiveQuery?.toMap() ?? {})
      ..addAll(query?.toMap() ?? {});
    await get(
      url,
        queryParameters: queryParam
    ).then((response) async {
      if (response.statusCode == 200) {
        final data = response.data as List;
        responseBody =
        List<PaymentMethods>.from(data.map((e) => PaymentMethods.fromJson(e)));
      }
    }).catchError((onError) async {
      log('error : ${onError} ${onError.toString().isEmpty}');
      responseBody = <PaymentMethods>[];
    });

    return responseBody;
  }

  Future<String> getOTPEdfali(String amount,String mobileNo) async {
    String url='payment-methods/edfali-api-otp-request';
    dynamic responseBody;
    await post(
        url,
        data: {'amount':'$amount','mobileNumber':'$mobileNo'}
    ).then((response) async {
      if (response.statusCode == 200) {
        responseBody =response.data;
      }
    }).catchError((onError) async {
      log('error : ${onError} ${onError.toString().isEmpty}');
      EasyLoading.dismiss();
      responseBody = null;
    });

    return responseBody;
  }
  Future<bool> payEdfali(Edfali edfali) async {
    String url='payment-methods/edfali-api-pay';
    dynamic responseBody;
    print(edfali.toJson());
    await post(
        url,
        data: edfali.toJson()
    ).then((response) async {
      if (response.statusCode == 200) {

        responseBody =true;
      }
    }).catchError((onError) async {
      log('error : ${onError} ${onError.toString().isEmpty}');
      EasyLoading.dismiss();
      responseBody = false;
    });

    return responseBody;
  }
  Future<SadadOTP> getOTPSadad(String mobileNo,String birthYear,double amount) async {
    String url='payment-methods/sadad-api-otp-request';
    dynamic responseBody;
    await post(
        url,
        data: {'amount':'$amount','mobileNumber':'$mobileNo','birthYear':'$birthYear'}
    ).then((response) async {
      if (response.statusCode == 200) {
        final body = json.decode(response.data);
       if(body['statusCode']==0) responseBody = SadadOTP.fromJson(body);
      } else
        SadadOTP();
    }).catchError((onError) async {
      log('error OTP: ${onError} ${onError.toString().isEmpty}');
      EasyLoading.dismiss();
      responseBody = SadadOTP();
    });

    return responseBody;
  }

  Future<bool> paySadad(Sadad sadad) async {
    String url = 'payment-methods/sadad-api-pay';
    dynamic responseBody;
    await post(url, data: sadad.toJson()).then((response) async {
      if (response.statusCode == 201) {
        responseBody = true;
      } else {
        responseBody = false;
      }
    }).catchError((onError) async {
      log('error : ${onError} ${onError.toString().isEmpty}');
      EasyLoading.dismiss();
      responseBody = false;
    });

    return responseBody;
  }

  Future<String> payTadawal(Tadawal tadawal) async {
    String url = 'payment-methods/tadawul-request';
    dynamic responseBody;
    await post(url, data: tadawal.toJson()).then((response) async {
      if (response.statusCode == 200) {
        responseBody = json.decode(response.data);
        responseBody = responseBody['url'];
        print('responseBody: $responseBody');
      } else {
        responseBody = null;
      }
    }).catchError((onError) async {
      log('error : ${onError} ${onError.toString().isEmpty}');
      EasyLoading.dismiss();
      responseBody = null;
    });

    return responseBody;
  }

  Future<String> payMobicash(Tadawal tadawal) async {
    String url = 'payment-methods/mobicash-request';
    dynamic responseBody;
    await post(url, data: tadawal.toJson()).then((response) async {
      if (response.statusCode == 200) {
        responseBody = json.decode(response.data);
        responseBody = responseBody['url'];
        print('responseBody: $responseBody');
      } else {
        responseBody = null;
      }
    }).catchError((onError) async {
      log('error : ${onError} ${onError.toString().isEmpty}');
      EasyLoading.dismiss();
      responseBody = null;
    });

    return responseBody;
  }

  Future<bool> payMoamalat(Moamalat moamalat) async {
    String url = 'payment-methods/moamalat-api-pay';
    dynamic responseBody;
    await post(url, data: moamalat.toJson()).then((response) async {
      if (response.statusCode == 200) {
        responseBody = true;
      }
    }).catchError((onError) async {
      log('error : ${onError} ${onError.toString().isEmpty}');
      EasyLoading.dismiss();
      responseBody = false;
    });

    return responseBody;
  }

  Future<Mastercard> payMasterCard(String usdAmount) async {
    String url = 'payment-methods/mastercard-pay-init';
    dynamic responseBody;
    await post(url, data: {'usdAmount': usdAmount}).then((response) async {
      if (response.statusCode == 200 || response.statusCode == 201) {
        responseBody = Mastercard.fromJson(response.data);
      }
    }).catchError((onError) async {
      log('error : ${onError} ${onError.toString().isEmpty}');
      EasyLoading.dismiss();
      responseBody = Mastercard();
    });

    return responseBody;
  }

  Future<bool> getOTP() async {
    String url = 'payment-methods/sadad-api-resend-otp';
    dynamic responseBody;
    await post(
      url,
    ).then((response) async {
      if (response.statusCode == 200) {
        responseBody = true;
        print("===============");
        print(response.data);
        print("===============");
      }
    }).catchError((onError) async {
      log('error : ${onError} ${onError.toString().isEmpty}');
      EasyLoading.dismiss();
      responseBody = false;
    });

    return responseBody;
  }
  Future<Transaction> initPay(String amount,String paymentType) async {
    String url='transactions/init-pay';
    dynamic responseBody;
    await post(
        url,
        data: {'amount':'$amount','paymentType':'$paymentType'}
    ).then((response) async {
      if (response.statusCode == 201 || response.statusCode == 200) {
        log('responseBody ${response.data}');
        responseBody =Transaction.fromJson(response.data);
      }
    }).catchError((onError) async {
      log('error : ${onError} ${onError.toString().isEmpty}');
      EasyLoading.dismiss();
      responseBody = Transaction();
    });

    return responseBody;
  }
  Future<bool> payUni(UniPay uniPay) async {
    String url='payment-methods/uni-api-pay';
    dynamic responseBody;
    await post(
        url,
        data: uniPay.toJson()
    ).then((response) async {
      if (response.statusCode == 200) {
        responseBody =true;
      }
    }).catchError((onError) async {
      log('error : ${onError} ${onError.toString().isEmpty}');
      EasyLoading.dismiss();
      responseBody = false;
    });

    return responseBody;
  }
}
