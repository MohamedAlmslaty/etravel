import 'dart:convert';
import 'dart:developer';

import 'package:e_travel/app/data/models/booking_total.dart';
import 'package:e_travel/app/data/models/etravel_fee.dart';
import 'package:e_travel/app/data/models/final_booking.dart';
import 'package:e_travel/app/data/models/flight_booking.dart';
import 'package:e_travel/app/data/services/api/api_service.dart';
import 'package:e_travel/app/util/consts/style.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';

import '../models/query_models/query.dart';

class FlightBookingRepository extends ApiService {
  static FlightBookingRepository get instance => FlightBookingRepository();

  Future<List<FlightBooking>> getFlightBookings() async {
    String url = 'flight-bookings';
    dynamic responseBody;
    Map<String, dynamic> queryParam = {};
    queryParam..addAll(Query(sort: ["id,desc"], size: 100).toMap());
    await get(url, queryParameters: queryParam).then((response) async {
      if (response.statusCode == 200) {
        final data = response.data as List;
        responseBody = List<FlightBooking>.from(data.map((e) => FlightBooking.fromJson(e)));
      }
    }).catchError((onError) async {
      log('error : ${onError} ${onError.toString().isEmpty}');
      responseBody = <FlightBooking>[];
    });

    return responseBody;
  }

  Future<String> getTotalBooking(FinalBooking finalBooking) async {
    String url = 'flight-requests/booking-total';
    dynamic responseBody;

    await post(url, data: finalBooking.toJson()).then((response) async {
      if (response.statusCode == 200) {
        responseBody = response.data;
        print('responseBody : $responseBody');
      }
    }).catchError((onError) async {
      log('error : ${onError} ${onError.toString().isEmpty}');
      EasyLoading.dismiss();
      responseBody = null;
    });

    return responseBody;
  }
  Future<String> getTotalBookingMulti(FinalBookingMulti finalBookingMulti) async {
    String url = 'flight-requests/booking-total';
    dynamic responseBody;

    await post(url, data: finalBookingMulti.toJson()).then((response) async {
      if (response.statusCode == 200) {
        responseBody = response.data;
        print('responseBody : $responseBody');
      }
    }).catchError((onError) async {
      log('error : ${onError} ${onError.toString().isEmpty}');
      EasyLoading.dismiss();
      responseBody = null;
    });

    return responseBody;
  }

  void printLongString(String str) {
    const chunkSize = 500; // You can tweak this size as needed
    for (var i = 0; i < str.length; i += chunkSize) {
      var end = (i + chunkSize < str.length) ? i + chunkSize : str.length;
      print(str.substring(i, end));
    }
  }

  Future<List<BookingTotalDetails>> getTotalBookingDetails(FinalBooking finalBooking) async {
    String url = 'flight-requests/booking-total-details';
    List<BookingTotalDetails> responseBody = <BookingTotalDetails>[];

    await post(url, data: finalBooking.toJson()).then((response) async {
      if (response.statusCode == 200) {
        final data = response.data as List;
        responseBody = List<BookingTotalDetails>.from(data.map((e) => BookingTotalDetails.fromJson(e)));
      }
    }).catchError((onError) async {
      log('error : ${onError} ${onError.toString().isEmpty}');
      EasyLoading.dismiss();
      responseBody = <BookingTotalDetails>[];
    });

    printLongString("-------------------------------------    \n \n ${responseBody.toString()}");

    return responseBody;
  }

  Future<List<BookingTotalDetails>> getTotalBookingMultiDetails(FinalBookingMulti finalBookingMulti) async {
    String url = 'flight-requests/booking-total-details';
    dynamic responseBody;

    await post(url, data: finalBookingMulti.toJson()).then((response) async {
      if (response.statusCode == 200) {
        final data = response.data as List;
        responseBody = List<BookingTotalDetails>.from(data.map((e) => BookingTotalDetails.fromJson(e)));
      }
    }).catchError((onError) async {
      log('error : ${onError} ${onError.toString().isEmpty}');
      EasyLoading.dismiss();
      responseBody = <BookingTotalDetails>[];
    });

    return responseBody;
  }

  Future<bool> dividePnr(id) async {
    String url = 'flight-bookings/divide-pnr/$id';
    dynamic responseBody;

    await get(url).then((response) async {
      if (response.statusCode == 200) {
        responseBody = true;
        print('responseBody : $responseBody');
      }
    }).catchError((onError) async {
      log('error : ${onError} ${onError.toString().isEmpty}');
      EasyLoading.dismiss();
      responseBody = false;
      showSnackBar(title: 'error'.tr, message: onError.type);
    });

    return responseBody;
  }

  Future<FlightBooking> initBooking(FinalBooking finalBooking) async {
    String url = 'public/flight-requests/initial-booking';
    dynamic responseBody;

    await post(url, data: finalBooking.toJson()).then((response) async {
      if (response.statusCode == 200) {
        responseBody = FlightBooking.fromJson(response.data);
      }
    }).catchError((onError) async {
      log('error : ${onError} ${onError.toString().isEmpty}');
      EasyLoading.dismiss();
      responseBody = FlightBooking();
    });

    return responseBody;
  }
  Future<FlightBooking> initBookingMulti(FinalBookingMulti finalBooking) async {
    String url = 'public/flight-requests/initial-booking';
    dynamic responseBody;

    await post(url, data: finalBooking.toJson()).then((response) async {
      if (response.statusCode == 200) {
        responseBody = FlightBooking.fromJson(response.data);
      }
    }).catchError((onError) async {
      log('error : ${onError} ${onError.toString().isEmpty}');
      EasyLoading.dismiss();
      responseBody = FlightBooking();
    });

    return responseBody;
  }

  Future<FlightBooking> flightBooking(FinalBooking finalBooking) async {
    String url = 'public/flight-requests/final-booking';
    dynamic responseBody;

    await post(url, data: finalBooking.toJson()).then((response) async {
      if (response.statusCode == 200) {
        responseBody = FlightBooking.fromJson(response.data);
      }
    }).catchError((onError) async {
      log('error : ${onError} ${onError.toString().isEmpty}');
      EasyLoading.dismiss();
      responseBody = FlightBooking();
    });

    return responseBody;
  }
  Future<FlightBooking> flightBookingMulti(FinalBookingMulti finalBooking) async {
    String url = 'public/flight-requests/final-booking';
    dynamic responseBody;

    await post(url, data: finalBooking.toJson()).then((response) async {
      if (response.statusCode == 200) {
        responseBody = FlightBooking.fromJson(response.data);
      }
    }).catchError((onError) async {
      log('error : ${onError} ${onError.toString().isEmpty}');
      EasyLoading.dismiss();
      responseBody = FlightBooking();
    });

    return responseBody;
  }

  Future<FlightBooking> editBooking(pnr, FinalBooking finalBooking) async {
    String url = 'public/flight-bookings/change-booking/$pnr';
    dynamic responseBody;

    await post(url, data: finalBooking.toJson()).then((response) async {
      if (response.statusCode == 200) {
        responseBody = FlightBooking.fromJson(response.data);
      }
    }).catchError((onError) async {
      log('error : ${onError} ${onError.toString().isEmpty}');
      EasyLoading.dismiss();
      responseBody = FlightBooking();
    });

    return responseBody;
  }

  Future<bool> deleteFlightBooking(int id) async {
    String url = 'flight-bookings/cancel/$id';
    dynamic responseBody = false;
    await get(url, extraHeaders: {'Accept': 'application/json'}).then((response) async {
      if (response.statusCode == 200) responseBody = true;
    }).catchError((onError) async {
      log('error : ${onError} ${onError.toString().isEmpty}');
      EasyLoading.dismiss();
      responseBody = false;
    });
    return responseBody;
  }

  Future<bool> changeTOFinalBooking(int id) async {
    String url = 'flight-bookings/initial-to-final-booking/$id';
    dynamic responseBody = false;
    await get(url, extraHeaders: {'Accept': 'application/json'}).then((response) async {
      if (response.statusCode == 200) responseBody = true;
    }).catchError((onError) async {
      log('error : ${onError} ${onError.toString().isEmpty}');
      EasyLoading.dismiss();
      responseBody = false;
    });
    return responseBody;
  }

  Future<String> getTotalInitialBooking(FinalBooking flightRequest) async {
    String url = 'flight-requests/initial-booking-total';
    dynamic responseBody;

    await post(url, data: flightRequest.toJson()).then((response) async {
      if (response.statusCode == 200) {
        responseBody = response.data;
        print('responseBody : $responseBody');
      }
    }).catchError((onError) async {
      log('error : ${onError} ${onError.toString().isEmpty}');
      EasyLoading.dismiss();
      responseBody = null;
    });

    return responseBody;
  }
  Future<String> getTotalInitialBookingMulti(FinalBookingMulti finalBookingMulti) async {
    String url = 'flight-requests/initial-booking-total';
    dynamic responseBody;

    await post(url, data: finalBookingMulti.toJson()).then((response) async {
      if (response.statusCode == 200) {
        responseBody = response.data;
        print('responseBody : $responseBody');
      }
    }).catchError((onError) async {
      log('error : ${onError} ${onError.toString().isEmpty}');
      EasyLoading.dismiss();
      responseBody = null;
    });

    return responseBody;
  }

  Future<List<EtravelFee>> getCancelFee(int id) async {
    String url = 'flight-bookings/cancel-fees/$id';
    dynamic responseBody;
    await get(url).then((response) async {
      if (response.statusCode == 200) {
        log('response : ${jsonDecode(response.data)}');
        final data = jsonDecode(response.data) as List;
        responseBody =
            List<EtravelFee>.from(data.map((e) => EtravelFee.fromJson(e)));
      }
    }).catchError((onError) async {
      log('error : ${onError} ${onError.toString().isEmpty}');
      EasyLoading.dismiss();
      responseBody = <EtravelFee>[];
    });
    return responseBody;
  }

  Future<List<EtravelFee>> getEditFee(
      String pnr, FinalBooking finalBooking) async {
    String url = 'public/flight-bookings/change-booking-fees/$pnr';
    dynamic responseBody;
    await post(url, data: finalBooking.toJson()).then((response) async {
      if (response.statusCode == 200) {
        log('response : ${jsonDecode(response.data)}');
        final data = jsonDecode(response.data) as List;
        responseBody =
            List<EtravelFee>.from(data.map((e) => EtravelFee.fromJson(e)));
      }
    }).catchError((onError) async {
      log('error : ${onError} ${onError.toString().isEmpty}');
      EasyLoading.dismiss();
      responseBody = <EtravelFee>[];
    });
    return responseBody;
  }
}
