import 'dart:developer';
import 'package:e_travel/app/data/models/airport.dart';
import 'package:e_travel/app/data/services/api/api_service.dart';

class AirportRepository extends ApiService {
  static AirportRepository get instance => AirportRepository();

  Future<List<Airport>> getAirports() async {
    String url='public/airports?sort=menuOrder&page=0&size=1000';
    dynamic responseBody;
    await get(
        url
    ).then((response) async {
      if (response.statusCode == 200) {
        final data = response.data as List;
        responseBody =
        List<Airport>.from(data.map((e) => Airport.fromJson(e)));
      }
    }).catchError((onError) async {
      log('error : ${onError} ${onError.toString().isEmpty}');
      responseBody = <Airport>[];
    });

    return responseBody;

  }
}
