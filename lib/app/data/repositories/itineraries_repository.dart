import 'dart:async';
import 'dart:convert';
import 'dart:developer';

import 'package:dio/dio.dart';
import 'package:e_travel/app/data/models/fligh_rq.dart';
import 'package:e_travel/app/data/models/flight_calender.dart';
import 'package:e_travel/app/data/models/flight_req.dart';
import 'package:e_travel/app/data/services/api/api_service.dart';

class ItinerariesRepository extends ApiService {
  static ItinerariesRepository get instance => ItinerariesRepository();

  Future<List<FlightRq>> getItineraries(FlightRequest flightRequest) async {
    String url = 'public/flight-requests?page=0&size=1000';
    dynamic responseBody;
    await get(url, queryParameters: flightRequest.toJson(), extraHeaders: {
      Headers.contentTypeHeader: 'application/json',
      Headers.acceptHeader: 'application/json'
    }).then((response) async {
      if (response.statusCode == 200) {
        final data = response.data as List;
        responseBody = List<FlightRq>.from(data.map((e) => FlightRq.fromJson(e)));
      }
    }).catchError((onError) async {
      log('error : ${onError} ${onError.toString().isEmpty}');
      responseBody = <FlightRq>[];
    });

    return responseBody;
  }
  Future<List<FlightCalender>> getDatesAvailable(FlightRequest flightRequest) async {
    String url = 'public/flight-requests/calender?page=0&size=1000';
    dynamic responseBody;
    await get(url, queryParameters: flightRequest.toCalender(), extraHeaders: {
      Headers.contentTypeHeader: 'application/json',
      Headers.acceptHeader: 'application/json'
    }).then((response) async {
      if (response.statusCode == 200) {
        final data = response.data as List;
        responseBody = List<FlightCalender>.from(data.map((e) => FlightCalender.fromJson(e)));
      }
    }).catchError((onError) async {
      log('error : ${onError} ${onError.toString().isEmpty}');
      responseBody = <FlightCalender>[];
    });

    return responseBody;
  }

  Future<List<FlightRq>> getItinerariesMultiCity(FlightRequest flightRequest) async {
    Map<String, dynamic> queryParameters = flightRequest.toJsonMultiCity();
    // Build the base URL
    String baseUrl = 'public/flight-requests';
    // Start building the query string
    List<String> queryStringParts = [];
    queryParameters.forEach((key, value) {
      if (value is List) {
        for (int i = 0; i < value.length; i++) {
          queryStringParts.add("multiCityFlights[$i].destinationLocationCode=${value[i]['destinationLocationCode']}");
          queryStringParts.add("multiCityFlights[$i].departureDate=${value[i]['departureDate']}");
          queryStringParts.add("multiCityFlights[$i].originLocationCode=${value[i]['originLocationCode']}");
        }
      } else {
        queryStringParts.add('$key=$value');
      }
    });
    String finalUrl = '$baseUrl?' + queryStringParts.join('&');

    dynamic responseBody;
    await get(finalUrl).then((response) {
      if (response.statusCode == 200) {
        final data = response.data as List;
        responseBody = List<FlightRq>.from(data.map((e) => FlightRq.fromJson(e)));
      }
    }).catchError((onError) {
      print('error: $onError');
      responseBody = <FlightRq>[];
    });

    return responseBody;
  }
  //   Future<List<FlightRq>> getItinerariesMultiCity(FlightRequest flightRequest) async {
  //   String url = 'public/flight-requests?page=0&size=1000';
  //   dynamic responseBody;
  //   print(flightRequest.toJsonMultiCity());
  //   await get(url, queryParameters: flightRequest.toJsonMultiCity()).then((response) async {
  //     if (response.statusCode == 200) {
  //       final data = response.data as List;
  //       responseBody = List<FlightRq>.from(data.map((e) => FlightRq.fromJson(e)));
  //     }
  //   }).catchError((onError) async {
  //     log('error : ${onError} ${onError.toString().isEmpty}');
  //     responseBody = <FlightRq>[];
  //   });
  //
  //   return responseBody;
  // }

  //TODO :: WILL BE USED TO IMPROVE PERFORMANCE OF THE REQUEST
  // New NDJSON stream method
  Stream<FlightRq> getItinerariesStream(FlightRequest flightRequest) {
    final StreamController<FlightRq> streamController = StreamController<FlightRq>();

    getNdJsonStream(
      'public/flight-requests/flux', // Replace with your NDJSON endpoint
      extraHeaders: {
        Headers.contentTypeHeader: 'application/json',
        Headers.acceptHeader: 'application/x-ndjson', // Assuming server sends NDJSON
      },
    ).listen(
      (data) {
        FlightRq flightRq = FlightRq.fromJson(data);
        streamController.add(flightRq);
      },
      onError: (error) {
        streamController.addError(error);
      },
      onDone: () {
        streamController.close();
      },
    );

    return streamController.stream;
  }
}
