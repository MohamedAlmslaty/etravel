import 'dart:developer';
import 'package:e_travel/app/data/models/fligths_show.dart';
import 'package:e_travel/app/data/services/api/api_service.dart';

class ServicesRepository extends ApiService {
  static ServicesRepository get instance => ServicesRepository();

  Future<List<InternationalArrival>> getInternationalArrival(String airport,String lang) async {
    String url='public/$airport/$lang/international-arrival';
    dynamic responseBody;
    await get(
        url
    ).then((response) async {
      if (response.statusCode == 200) {
        final data = response.data as List;
        responseBody =
        List<InternationalArrival>.from(data.map((e) => InternationalArrival.fromJson(e)));
      }
    }).catchError((onError) async {
      log('error : ${onError} ${onError.toString().isEmpty}');
      responseBody = <InternationalArrival>[];
    });

    return responseBody;
  }
  Future<List<InternationalArrival>> getInternationalDeparture(String airport,String lang) async {
    String url='public/$airport/$lang/international-departure';
    dynamic responseBody;
    await get(
        url
    ).then((response) async {
      if (response.statusCode == 200) {
        final data = response.data as List;
        responseBody =
        List<InternationalArrival>.from(data.map((e) => InternationalArrival.fromJson(e)));
      }
    }).catchError((onError) async {
      log('error : ${onError} ${onError.toString().isEmpty}');
      responseBody = <InternationalArrival>[];
    });

    return responseBody;
  }
  Future<List<InternationalArrival>> getDomesticArrival(String airport,String lang) async {
    String url='public/$airport/$lang/domestic-arrival';
    dynamic responseBody;
    await get(
        url
    ).then((response) async {
      if (response.statusCode == 200) {
        final data = response.data as List;
        responseBody =
        List<InternationalArrival>.from(data.map((e) => InternationalArrival.fromJson(e)));
      }
    }).catchError((onError) async {
      log('error : ${onError} ${onError.toString().isEmpty}');
      responseBody = <InternationalArrival>[];
    });

    return responseBody;
  }
  Future<List<InternationalArrival>> getDomesticDeparture(String airport,String lang) async {
    String url='public/$airport/$lang/domestic-departure';
    dynamic responseBody;
    await get(
        url
    ).then((response) async {
      if (response.statusCode == 200) {
        final data = response.data as List;
        responseBody =
        List<InternationalArrival>.from(data.map((e) => InternationalArrival.fromJson(e)));
      }
    }).catchError((onError) async {
      log('error : ${onError} ${onError.toString().isEmpty}');
      responseBody = <InternationalArrival>[];
    });

    return responseBody;
  }
}
