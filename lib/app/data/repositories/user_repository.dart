import 'dart:developer';

import 'package:e_travel/app/data/models/customer.dart';
import 'package:e_travel/app/data/models/user.dart';
import 'package:e_travel/app/data/services/api/api_service.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';

class UserRepository extends ApiService {
  static UserRepository get instance => UserRepository();

  Future<void> saveAccount(User user) async {
    String url = 'account';
    await post(url, data: user.toJson()).then((response) async {
      if (response.statusCode == 200) {
        log('response : ${response.data}');
      }
    }).catchError((onError) async {
      log('error : ${onError} ${onError.toString().isEmpty}');
    });
  }

  Future<void> getAuthorities() async {
    String url = 'users/authorities';
    await get(
      url,
    ).then((response) async {
      if (response.statusCode == 200) {
        log('response : ${response.data}');
      }
    }).catchError((onError) async {
      log('error : ${onError} ${onError.toString().isEmpty}');
    });
  }

  Future<User> getAccount() async {
    String url = 'account';
    dynamic responseBody;

    await get(
      url,
    ).then((response) async {
      if (response.statusCode == 200) {
        responseBody = User.fromJson(response.data);
      }
    }).catchError((onError) async {
      log('error : ${onError} ${onError.toString().isEmpty}');
      responseBody = User();
    });

    return responseBody;
  }

  Future<Customer> getCustomerAccount() async {
    String url = 'customers/get-profile';
    dynamic responseBody;

    await get(
      url,
    ).then((response) async {
      if (response.statusCode == 200) {
        responseBody = Customer.fromJson(response.data);
      }
    }).catchError((onError) async {
      log('error : ${onError} ${onError.toString().isEmpty}');
      responseBody = Customer();
    });

    return responseBody;
  }

  Future<Customer> getCustomerByPhone(String phone) async {
    String url = 'customers/by-mobile/${phone.replaceAll('+', '%2B')}';
    print(url);
    dynamic responseBody;

    await get(
      url,
    ).then((response) async {
      if (response.statusCode == 200) {
        responseBody = Customer.fromJson(response.data);
      }
    }).catchError((onError) async {
      EasyLoading.dismiss();
      log('error : ${onError} ${onError.toString().isEmpty}');
      responseBody = null;
    });

    return responseBody;
  }

  Future<Customer> getCustomerByWalletPublicKey(String walletPublicKey) async {
    String url = 'customers/by-public-key/$walletPublicKey';
    dynamic responseBody;

    await get(
      url,
    ).then((response) async {
      if (response.statusCode == 200) {
        responseBody = Customer.fromJson(response.data);
      }
    }).catchError((onError) async {
      log('error : ${onError} ${onError.toString().isEmpty}');
      responseBody = null;
    });

    return responseBody;
  }

  Future<Customer> updateCustomerAccount(Customer customer) async {
    String url = 'customers/update-profile';
    dynamic responseBody;
    await post(url, data: customer.toJson()).then((response) async {
      if (response.statusCode == 200) {
        responseBody = Customer.fromJson(response.data);
      }
    }).catchError((onError) async {
      log('error : ${onError} ${onError.toString().isEmpty}');
      responseBody = Customer();
    });
    return responseBody;
  }

  Future<bool> changePassword(String currentPassword, String newPassword) async {
    String url = 'account/change-password';
    dynamic responseBody = false;

    await post(url, data: {"currentPassword": currentPassword, "newPassword": newPassword}).then((response) async {
      if (response.statusCode == 200) {
        responseBody = true;
        print('responseBody : $responseBody');
      }
    }).catchError((onError) async {
      log('error : ${onError} ${onError.toString().isEmpty}');
      EasyLoading.dismiss();
      responseBody = false;
    });

    return responseBody;
  }

  Future<bool> deleteAccount() async {
    String url = 'customers/request-delete';
    dynamic responseBody = false;

    await get(url).then((response) async {
      if (response.statusCode == 200) {
        responseBody = true;
        print('responseBody : $responseBody');
      }
    }).catchError((onError) async {
      log('error : ${onError} ${onError.toString().isEmpty}');
      EasyLoading.dismiss();
      responseBody = false;
    });

    return responseBody;
  }
}
