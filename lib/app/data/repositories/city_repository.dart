import 'dart:developer';
import 'package:e_travel/app/data/models/city.dart';
import 'package:e_travel/app/data/services/api/api_service.dart';

class CityRepository extends ApiService {
  static CityRepository get instance => CityRepository();

  Future<List<City>> getCities() async {
    String url='public/cities';
    dynamic responseBody;
    await get(
        url
    ).then((response) async {
      if (response.statusCode == 200) {
        final data = response.data as List;
        responseBody =
        List<City>.from(data.map((e) => City.fromJson(e)));
      }
    }).catchError((onError) async {
      log('error : ${onError} ${onError.toString().isEmpty}');
      responseBody = <City>[];
    });

    return responseBody;
  }
}
