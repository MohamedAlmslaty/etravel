import 'dart:developer';

import 'package:e_travel/app/data/models/customer_point.dart';
import 'package:e_travel/app/data/models/query_models/query.dart';
import 'package:e_travel/app/data/services/api/api_service.dart';

class LoyalRepository extends ApiService {
  static LoyalRepository get instance => LoyalRepository();

  Future<double> getCustomerPointsBalance() async {
    String url = 'customer-points/balance';
    dynamic responseBody;
    await get(
      url,
    ).then((response) async {
      if (response.statusCode == 200) {
        responseBody = response.data;
      }
    }).catchError((onError) async {
      log('error : ${onError} ${onError.toString().isEmpty}');
      responseBody = 0.0;
    });
    return responseBody;
  }

  Future<String> redeem(int id, int discount, ticketPrice) async {
    String url = 'flight-bookings/redeemprice/$id/$discount/$ticketPrice';
    dynamic responseBody;
    await get(
      url,
    ).then((response) async {
      if (response.statusCode == 200) {
        responseBody = response.data;
      }
    }).catchError((onError) async {
      log('error : ${onError} ${onError.toString().isEmpty}');
      responseBody = null;
    });
    return responseBody;
  }
  Future<String> libyanaOTP(String phone) async {
    String url = 'customer-points/Libyanaotp/%2B218$phone';
    dynamic responseBody;
    await get(
      url,
    ).then((response) async {
      if (response.statusCode == 200) {
        responseBody = response.data;
      }
    }).catchError((onError) async {
      log('error : ${onError} ${onError.toString().isEmpty}');
      responseBody = null;
    });
    return responseBody;
  }
  Future<String> getLibyanaPoint(String phone) async {
    String url = 'customer-points/Libyana/218$phone';
    dynamic responseBody;
    await get(
      url,
    ).then((response) async {
      if (response.statusCode == 200) {
        responseBody = response.data;
      }
    }).catchError((onError) async {
      log('error : ${onError} ${onError.toString().isEmpty}');
      responseBody = null;
    });
    return responseBody;
  }
  Future<bool> confirmRedeemDiscount(int id, String token) async {
    String url = 'flight-bookings/confirmRedeemDiscount/$id/$token';
    dynamic responseBody;
    await get(
      url,
    ).then((response) async {
      if (response.statusCode == 200) {
        responseBody = true;
      }
    }).catchError((onError) async {
      log('error : ${onError} ${onError.toString().isEmpty}');
      responseBody = false;
    });
    return responseBody;
  }

  Future<bool> redeemCancellation(String token) async {
    String url =
        'flight-bookings/redeempricecancellation/$token';
    dynamic responseBody;
    await get(
      url,
    ).then((response) async {
      if (response.statusCode == 200) {
        responseBody = true;
      }
    }).catchError((onError) async {
      log('error : ${onError} ${onError.toString().isEmpty}');
      responseBody = false;
    });
    return responseBody;
  }

  Future<List<CustomerPoint>> getCustomerPoints() async {
    String url = 'customer-points';
    dynamic responseBody;
    Map<String, dynamic> queryParam = {};
    queryParam..addAll(Query(sort: ["id,desc"]).toMap());
    await get(url, queryParameters: queryParam).then((response) async {
      if (response.statusCode == 200) {
        final data = response.data as List;
        responseBody = List<CustomerPoint>.from(data.map((e) => CustomerPoint.fromJson(e)));
        log('responseBody+ $responseBody');
      }
    }).catchError((onError) async {
      log('error : ${onError} ${onError.toString().isEmpty}');
      responseBody = <CustomerPoint>[];
    });

    return responseBody;
  }

}
