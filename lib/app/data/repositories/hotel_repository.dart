import 'dart:developer';
import 'package:e_travel/app/data/models/hotel.dart';
import 'package:e_travel/app/data/models/hotel_request.dart';
import 'package:e_travel/app/data/services/api/api_service.dart';

class HotelRepository extends ApiService {
  static HotelRepository get instance => HotelRepository();

  Future<List<Hotel>> getHotels({
    required int adults,
    required String checkInDate,
    required String checkoutDate,
    required int children,
    required String cityId,
  }) async {
    String url =
        'public/hotels/availability?adults=$adults&checkinDate=$checkInDate&checkoutDate=$checkoutDate&children=$children&cityId=$cityId';
    dynamic responseBody;
    await get(url).then((response) async {
      if (response.statusCode == 200) {
        final data = response.data as List;
        responseBody = List<Hotel>.from(data.map((e) => Hotel.fromJson(e)));
      }
    }).catchError((onError) async {
      log('error : ${onError} ${onError.toString().isEmpty}');
      responseBody = <Hotel>[];
    });

    return responseBody;
  }
  Future<bool> hotelBooking({
    required   HotelRequestBooking hotelRequestBooking
  }) async {
    String url =
        'hotel-bookings/final-booking';
    dynamic responseBody;
    await post(url,data:hotelRequestBooking.toJson() ).then((response) async {
      if (response.statusCode == 200 ||response.statusCode == 201) {

        responseBody = true;
      }
    }).catchError((onError) async {
      log('error : ${onError} ${onError.toString().isEmpty}');
      responseBody = false;
    });

    return responseBody;
  }
}
