import 'package:e_travel/app/data/models/available_room_types.dart';
import 'package:e_travel/app/data/models/city.dart';
import 'package:e_travel/app/data/models/user.dart';

class Hotel {
  int? id;
  String? nameEn;
  String? nameAr;
  String? hotelType;
  String? detailsAr;
  String? detailsEn;
  String? addressAr;
  String? addressEn;
  String? email;
  String? mobileNo;
  int? menuOrder;
  double? lat;
  double? lng;
  bool? isActive;
  int? stars;
  String? imageFileUrl;
  String? imageFile;
  String? imageFileContentType;
  User? user;
  City? city;
  List<AvailableRoomTypes>? availableRoomTypes;

  Hotel(
      {this.id,
      this.nameEn,
      this.nameAr,
      this.hotelType,
      this.detailsAr,
      this.detailsEn,
      this.addressAr,
      this.addressEn,
      this.email,
      this.mobileNo,
      this.menuOrder,
      this.lat,
      this.lng,
      this.isActive,
      this.stars,
      this.imageFileUrl,
      this.imageFile,
      this.imageFileContentType,
      this.user,
      this.city,
      this.availableRoomTypes});

  Hotel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    nameEn = json['nameEn'];
    nameAr = json['nameAr'];
    hotelType = json['hotelType'];
    detailsAr = json['detailsAr'];
    detailsEn = json['detailsEn'];
    addressAr = json['addressAr'];
    addressEn = json['addressEn'];
    email = json['email'];
    mobileNo = json['mobileNo'];
    menuOrder = json['menuOrder'];
    lat = json['lat'];
    lng = json['lng'];
    isActive = json['isActive'];
    stars = json['stars'];
    imageFileUrl = json['imageFileUrl'];
    imageFile = json['imageFile'];
    imageFileContentType = json['imageFileContentType'];
    // user = json['user'] != null ? new User.fromJson(json['user']) : User();
     city = json['city'] != null ? new City.fromJson(json['city']) : City();
    if (json['availableRoomTypes'] != null) {
      availableRoomTypes = <AvailableRoomTypes>[];
      json['availableRoomTypes'].forEach((v) {
        availableRoomTypes!.add(new AvailableRoomTypes.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['nameEn'] = this.nameEn;
    data['nameAr'] = this.nameAr;
    data['hotelType'] = this.hotelType;
    data['detailsAr'] = this.detailsAr;
    data['detailsEn'] = this.detailsEn;
    data['addressAr'] = this.addressAr;
    data['addressEn'] = this.addressEn;
    data['email'] = this.email;
    data['mobileNo'] = this.mobileNo;
    data['menuOrder'] = this.menuOrder;
    data['lat'] = this.lat;
    data['lng'] = this.lng;
    data['isActive'] = this.isActive;
    data['stars'] = this.stars;
    data['imageFileUrl'] = this.imageFileUrl;
    data['imageFile'] = this.imageFile;
    data['imageFileContentType'] = this.imageFileContentType;
    if (this.user != null) {
      data['user'] = this.user!.toJson();
    }
    if (this.city != null) {
      data['city'] = this.city!.toJson();
    }
    if (this.availableRoomTypes != null) {
      data['availableRoomTypes'] =
          this.availableRoomTypes!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
