import 'package:e_travel/app/data/models/itineraries.dart';

class Query {
  int? page;
  String? query;
  List<String>? sort;
  int? size;

  Query({
    this.page,
    this.query,
    this.sort,
    this.size,
  });

  Map<String, dynamic> toMap() {
    return {
      'page': page,
      'query': query,
      'sort': sort,
      'size': size,
    };
  }
}



class IsActiveQuery {
  bool? equals;
  List<bool>? iN;
  bool? notEquals;
  List<bool>? notIn;
  bool? specified;

  IsActiveQuery({
    this.equals,
    this.iN,
    this.notEquals,
    this.notIn,
    this.specified,
  });

  Map<String, dynamic> toMap() {
    return {
      'isActive.equals': equals,
      'isActive.in': iN,
      'isActive.notEquals': notEquals,
      'isActive.notIn': notIn,
      'isActive.specified': specified,
    };
  }
}
