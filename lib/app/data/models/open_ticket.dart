import 'package:get/get.dart';

class OpenTicket{
  OpenTicket({required this.name, required this.isCheck});
  String name;
  RxBool isCheck=false.obs;

  static List<OpenTicket> getOpenTicket(bool oneWay) {
    return oneWay?
    <OpenTicket>[
      OpenTicket(
          name: 'goingOpen',
        isCheck: false.obs,
          ),

    ]:<OpenTicket>[
      OpenTicket(
        name: 'goingOpen',
        isCheck: false.obs,
      ),
      OpenTicket(
        name: 'returnOpen',
        isCheck: false.obs,
      ),

    ];
  }
}