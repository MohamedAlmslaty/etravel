import 'package:get/get.dart';

class Airline {
  Airline();

  late final int id;
  late final String name;
  late final String nameAr;
  late final String nameEn;
  late final String iataCode;
  late final String icaoCode;
  late final String digitsCode;
  late final String imageUrl;
  late final String image;
  late final String imageContentType;
  late final String countryName;
  late final double feePercentage;
  late final bool isActive;
  late final bool requireDocumentsForBooking;
  late final bool requireDocumentsForRefund;
  RxBool checked=false.obs;

  Airline.fromJson(Map<String, dynamic> json) {
    id = json['id'] ?? null;
    name = json['name'] ?? '';
    nameAr = json['nameAr'] ?? '';
    nameEn = json['nameEn'] ?? '';
    iataCode = json['iataCode'] ?? '';
    icaoCode = json['icaoCode'] ?? '';
    digitsCode = json['digitsCode'] ?? '';
    imageUrl = json['imageUrl'] ?? '';
    image = json['image'] ?? '';
    imageContentType = json['imageContentType'] ?? '';
    countryName = json['countryName'] ?? '';
    feePercentage = json['feePercentage'] ?? 0.0;
    isActive = json['isActive'];
    requireDocumentsForBooking = json['requireDocumentsForBooking']??false;
    requireDocumentsForRefund = json['requireDocumentsForRefund']??false;
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['id'] = id;
    _data['name'] = name;
    _data['nameAr'] = nameAr;
    _data['nameEn'] = nameEn;
    _data['iataCode'] = iataCode;
    _data['icaoCode'] = icaoCode;
    _data['digitsCode'] = digitsCode;
    _data['imageUrl'] = imageUrl;
    _data['image'] = image;
    _data['imageContentType'] = imageContentType;
    _data['countryName'] = countryName;
    _data['feePercentage'] = feePercentage;
    _data['isActive'] = isActive;
    _data['requireDocumentsForBooking'] = requireDocumentsForBooking;
    _data['requireDocumentsForRefund'] = requireDocumentsForRefund;
    return _data;
  }
}
