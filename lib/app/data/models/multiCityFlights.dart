import 'package:get/get.dart';

class MultiCity {
  MultiCity();

  RxString destinationText=''.obs;
  RxString destinationLocationCode=''.obs;
  RxString departureText=''.obs;
   RxString departureDate=''.obs;
  RxString originLocationCode=''.obs;

  MultiCity.fromJson(Map<String, dynamic> json) {
    destinationLocationCode.value = json['destinationLocationCode'];
    departureDate.value = json['departureDate'];
    originLocationCode.value = json['originLocationCode'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['destinationLocationCode'] = destinationLocationCode.value;
    _data['departureDate'] = departureDate.value;
    _data['originLocationCode'] = originLocationCode.value;
    return _data;
  }
}
