class Attachment {
  String? attachmentType;
  String? details;
  String? file;
  String? fileContentType;
  String? fileUrl;
  int? id;
  String? name;
  String? notes;
  String? pnr;

  Attachment(
      {this.attachmentType,
        this.details,
        this.file,
        this.fileContentType,
       });

  Attachment.fromJson(Map<String, dynamic> json) {
    attachmentType = json['attachmentType'];
    details = json['details'];
    file = json['file'];
    fileContentType = json['fileContentType'];
    fileUrl = json['fileUrl'];
    id = json['id'];
    name = json['name'];
    notes = json['notes'];
    pnr = json['pnr'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['attachmentType'] = this.attachmentType;
    data['details'] = this.details;
    data['file'] = this.file;
    data['fileContentType'] = this.fileContentType;
    data['fileUrl'] = this.fileUrl;
    data['id'] = this.id;
    data['name'] = this.name;
    data['notes'] = this.notes;
    data['pnr'] = this.pnr;
    return data;
  }
}
