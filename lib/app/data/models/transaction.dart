import 'package:e_travel/app/data/models/customer.dart';

class Transaction {
  double? amount;
  String? createdBy;
  String? createdDate;
  Customer? customer;
  double? fees;
  int? id;
  String? lastModifiedBy;
  String? lastModifiedDate;
  String? notes;
  String? paymentType;
  double? total;
  String? transactionReference;
  String? transactionStatus;
  String? vendorMessage;
  String? vendorReference;

  Transaction(
      {this.amount,
        this.createdBy,
        this.createdDate,
        this.customer,
        this.fees,
        this.id,
        this.lastModifiedBy,
        this.lastModifiedDate,
        this.notes,
        this.paymentType,
        this.total,
        this.transactionReference,
        this.transactionStatus,
        this.vendorMessage,
        this.vendorReference});

  Transaction.fromJson(Map<String, dynamic> json) {
    amount = json['amount'];
    createdBy = json['createdBy'];
    createdDate = json['createdDate'];
    customer = json['customer'] != null
        ? new Customer.fromJson(json['customer'])
        : null;
    fees = json['fees'];
    id = json['id'];
    lastModifiedBy = json['lastModifiedBy'];
    lastModifiedDate = json['lastModifiedDate'];
    notes = json['notes'];
    paymentType = json['paymentType'];
    total = json['total'];
    transactionReference = json['transactionReference'];
    transactionStatus = json['transactionStatus'];
    vendorMessage = json['vendorMessage'];
    vendorReference = json['vendorReference'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['amount'] = this.amount;
    data['createdBy'] = this.createdBy;
    data['createdDate'] = this.createdDate;
    if (this.customer != null) {
      data['customer'] = this.customer!.toJson();
    }
    data['fees'] = this.fees;
    data['id'] = this.id;
    data['lastModifiedBy'] = this.lastModifiedBy;
    data['lastModifiedDate'] = this.lastModifiedDate;
    data['notes'] = this.notes;
    data['paymentType'] = this.paymentType;
    data['total'] = this.total;
    data['transactionReference'] = this.transactionReference;
    data['transactionStatus'] = this.transactionStatus;
    data['vendorMessage'] = this.vendorMessage;
    data['vendorReference'] = this.vendorReference;
    return data;
  }
}
