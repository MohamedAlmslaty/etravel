import 'package:get/get.dart';

class AvailableRoomTypes {
  int? id;
  String? nameEn;
  String? nameAr;
  double? price;
  double? freeCancellationAdditionalCost;
  bool? isFreeCancellation;
  int? freeCancellationDays;
  int? menuOrder;
  bool? isAvaliable;
  bool? seasonPricing;
  int? bedsNumber;
  int? maxOccupancy;
  String? bedTypes;
  String? imageFileUrl;
  bool? isShownOnline;
  int? numberOfAvailableRooms;
  RxInt? requestedNumberOfRooms=1.obs;
  RxBool checked=false.obs;
  RxInt quantity = 1.obs;
  AvailableRoomTypes(
      {this.id,
        this.nameEn,
        this.nameAr,
        this.price,
        this.freeCancellationAdditionalCost,
        this.isFreeCancellation,
        this.freeCancellationDays,
        this.menuOrder,
        this.isAvaliable,
        this.seasonPricing,
        this.bedsNumber,
        this.maxOccupancy,
        this.bedTypes,
        this.imageFileUrl,
        this.isShownOnline,
        this.numberOfAvailableRooms,
        this.requestedNumberOfRooms });

  AvailableRoomTypes.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    nameEn = json['nameEn'];
    nameAr = json['nameAr'];
    price = json['price'];
    imageFileUrl = json['imageFileUrl'];
    freeCancellationAdditionalCost = json['freeCancellationAdditionalCost'];
    isFreeCancellation = json['isFreeCancellation'];
    freeCancellationDays = json['freeCancellationDays'];
    menuOrder = json['menuOrder'];
    isAvaliable = json['isAvaliable'];
    seasonPricing = json['seasonPricing'];
    bedsNumber = json['bedsNumber'];
    maxOccupancy = json['maxOccupancy'];
    bedTypes = json['bedTypes'];
    isShownOnline = json['isShownOnline'];
    numberOfAvailableRooms = json['numberOfAvailableRooms'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['nameEn'] = this.nameEn;
    data['nameAr'] = this.nameAr;
    data['price'] = this.price;
    data['imageFileUrl'] = this.imageFileUrl;
    data['freeCancellationAdditionalCost'] =
        this.freeCancellationAdditionalCost;
    data['isFreeCancellation'] = this.isFreeCancellation;
    data['freeCancellationDays'] = this.freeCancellationDays;
    data['menuOrder'] = this.menuOrder;
    data['isAvaliable'] = this.isAvaliable;
    data['seasonPricing'] = this.seasonPricing;
    data['bedsNumber'] = this.bedsNumber;
    data['maxOccupancy'] = this.maxOccupancy;
    data['bedTypes'] = this.bedTypes;
    data['isShownOnline'] = this.isShownOnline;
       data['numberOfAvailableRooms'] = this.numberOfAvailableRooms;
    data['requestedNumberOfRooms'] = this.requestedNumberOfRooms!.value;
    return data;
  }
}