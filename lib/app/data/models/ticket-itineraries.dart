import 'package:e_travel/app/data/models/airline.dart';
import 'package:e_travel/app/data/models/airport.dart';
import 'package:e_travel/app/data/models/flight_booking.dart';

class TicketItineraries {
  // void updateDuration() {
  //   DateTime departure = DateTime.parse(departureDateTime!);
  //   DateTime arrival = DateTime.parse(arrivalDateTime!);
  //   Duration duration = arrival.difference(departure);
  //   this.duration = '${duration.inHours}:${(duration.inMinutes % 60).toString().padLeft(2, '0')}';
  // }


    int? id;
    String? pnr;
    String? flightNo;
    String? airlineText;
    String? departureAirportText;
    String? arrivalAirportText;
    String? departureDateTime;
    String? arrivalDateTime;
    String? cabinClass;
    String? ticketClass;
    String? duration;
    int? stops;
    String? airplane;
    String? seatNo;
    String? meal;
    String? bags;
    FlightBooking? flightBooking;
    Airport? departureAirport;
    Airport? arrivalAirport;
    Airline? airline;
    bool? isSuggestion;
   TicketItineraries({
     this.id,
     this.pnr,
     this.flightNo,
     this.isSuggestion,
     this.airlineText,
     this.departureAirportText,
     this.arrivalAirportText,
     this.departureDateTime,
     this.arrivalDateTime,
     this.cabinClass,
     this.flightBooking,
     this.stops,
     this.arrivalAirport,
     this.departureAirport,
     this.airplane,
     this.airline,
     this.bags,
     this.duration,
     this.meal,
     this.seatNo,
     this.ticketClass,
   });
  TicketItineraries.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    pnr = json['pnr'];
    flightNo = json['flightNo'];
    airlineText = json['airlineText'];
    departureAirportText = json['departureAirportText'];
    arrivalAirportText = json['arrivalAirportText'];
    departureDateTime = json['departureDateTime'];
    arrivalDateTime = json['arrivalDateTime'];
    cabinClass = json['cabinClass'];
    ticketClass = json['ticketClass'];
    duration = json['duration'];
    stops = json['stops'];
    airplane = json['airplane'];
    seatNo = json['seatNo'];
    meal = json['meal'];
    bags = json['bags'];
    isSuggestion = json['isSuggestion'];
    flightBooking = FlightBooking.fromJson(json['flightBooking']);
    ;
    departureAirport = Airport.fromJson(json['departureAirport']);
    ;
    arrivalAirport = Airport.fromJson(json['arrivalAirport']);
    ;
    airline = Airline.fromJson(json['airline']);
    ;
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
     _data['id'] = id;
    _data['pnr'] = pnr;
    _data['isSuggestion'] = isSuggestion;
    _data['flightNo'] = flightNo;
    _data['airlineText'] = airlineText;
    _data['departureAirportText'] = departureAirportText;
    _data['arrivalAirportText'] = arrivalAirportText;
    _data['departureDateTime'] = departureDateTime;
    _data['arrivalDateTime'] = arrivalDateTime;
    _data['cabinClass'] = cabinClass;
    _data['ticketClass'] = ticketClass;
    _data['duration'] = duration;
    _data['stops'] = stops;
    _data['airplane'] = airplane;
    _data['seatNo'] = seatNo;
    _data['meal'] = meal;
    _data['bags'] = bags;
    _data['flightBooking'] = flightBooking;
    _data['departureAirport'] = departureAirport;
    _data['arrivalAirport'] = arrivalAirport;
    _data['airline'] = airline;
    return _data;
  }
}
