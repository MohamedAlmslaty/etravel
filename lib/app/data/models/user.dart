class User {
  late final bool activated;
  late final List<String> authorities;
  late final String createdBy;
  late final String createdDate;
  late final String email;
  late final String firebaseId;
  late final String firstName;
  late final int id;
  late final String imageUrl;
  late final String langKey;
  late final String lastModifiedBy;
  late final String lastModifiedDate;
  late final String lastName;
  late final String login;
  late final String password;
  late final String phone;

  User();

  User.fromJson(Map<String, dynamic> json) {
    activated = json['activated'];
    authorities = json['authorities'].cast<String>();
    createdBy = json['createdBy'];
    createdDate = json['createdDate'];
    email = json['email'];
    firebaseId = json['firebaseId'];
    firstName = json['firstName'];
    id = json['id'];
    imageUrl = json['imageUrl'];
    langKey = json['langKey'];
    lastModifiedBy = json['lastModifiedBy'];
    lastModifiedDate = json['lastModifiedDate'];
    lastName = json['lastName'];
    login = json['login'];
    password = json['password'];
    phone = json['phone'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['activated'] = this.activated;
    data['authorities'] = this.authorities;
    data['createdBy'] = this.createdBy;
    data['createdDate'] = this.createdDate;
    data['email'] = this.email;
    data['firebaseId'] = this.firebaseId;
    data['firstName'] = this.firstName;
    data['id'] = this.id;
    data['imageUrl'] = this.imageUrl;
    data['langKey'] = this.langKey;
    data['lastModifiedBy'] = this.lastModifiedBy;
    data['lastModifiedDate'] = this.lastModifiedDate;
    data['lastName'] = this.lastName;
    data['login'] = this.login;
    data['password'] = this.password;
    data['phone'] = this.phone;
    return data;
  }
}
