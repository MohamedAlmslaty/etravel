class Wallet {
  late final DateTime createdDate;
  late final double amount;
  late final int customerId;
  late final String customerName;
  late final int id;
  late final String notes;
  late final double totalAfterAction;
  late final double totalBeforeAction;
  late final int transactionId;
  late final String transactionTransactionNo;
  late final String walletAction;
  late final String walletNo;

  Wallet();

  Map<String, dynamic> toJson() {
    return {
      'createdDate': createdDate,
      'amount': amount,
      'customerId': customerId,
      'customerName': customerName,
      'id': id,
      'notes': notes,
      'totalAfterAction': totalAfterAction,
      'totalBeforeAction': totalBeforeAction,
      'transactionId': transactionId,
      'transactionTransactionNo': transactionTransactionNo,
      'walletAction': walletAction,
      'walletNo': walletNo,
    };
  }

  Wallet.fromJson(Map<String, dynamic> map) {
    createdDate = (map['createdDate'] != null ? DateTime.tryParse(map['createdDate']) : DateTime.now())!;
    amount = map['amount'] ?? 0.0;
    customerId = map['customerId'] ?? 0;
    customerName = map['customerName'] ?? '';
    id = map['id'];
    notes = map['notes'] ?? '';
    totalAfterAction = map['totalAfterAction'] ?? 0.0;
    totalBeforeAction = map['totalBeforeAction'] ?? 0.0;
    transactionId = map['transactionId'] ?? 0;
    transactionTransactionNo = map['transactionTransactionNo'] ?? '';
    walletAction = map['walletAction'] ?? '';
    walletNo = map['walletNo'] ?? "";
  }
}
