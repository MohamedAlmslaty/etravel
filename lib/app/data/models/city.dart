import 'package:e_travel/app/data/models/country.dart';

class City {
  City();

  int? id;
  String? nameEn;
  String? nameAr;
  Country? country;

  City.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    nameEn = json['nameEn'];
    nameAr = json['nameAr'];
    country = json['country'] != null ? new Country.fromJson(json['country']) : Country();
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['id'] = id;
    _data['nameEn'] = nameEn;
    _data['nameAr'] = nameAr;
    _data['country'] = country;
    return _data;
  }
}
