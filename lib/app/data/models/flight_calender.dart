import 'package:get/get.dart';

class FlightCalender {
  FlightCalender();

  String? airlineCode;
  String? currency;
  DateTime? date;
  String? fromCode;
  String? toCode;
  double? price;
  bool? returnFlight;


  FlightCalender.fromJson(Map<String, dynamic> json) {
    airlineCode = json['airlineCode'];
    currency = json['currency'];
    date = DateTime.parse(json['date']);
    fromCode = json['fromCode'];
    toCode = json['toCode'];
    price = json['price'];
    returnFlight = json['return'];

  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['airlineCode'] = airlineCode;
    _data['currency'] = currency;
    _data['date'] = date;
    _data['toCode'] = toCode;
    _data['fromCode'] = fromCode;
    _data['price'] = price;
    _data['return'] = returnFlight;
    return _data;
  }
}
