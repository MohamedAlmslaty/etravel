class TouristPackages {
  TouristPackages();

  late final int id;
  late final String nameEn;
  late final String nameAr;
  late final String details;
  late final String detailsAr;
  late final String detailsEn;
  late final double totalPrice;
  late final String imageUrl;
  late final String activeFrom;
  late final String activeUntil;
  late final int quantityAvailable;
  late final bool isActive;

  TouristPackages.fromJson(Map<String, dynamic> json) {
    id = json['id'] ?? 1;
    nameEn = json['nameEn'] ?? '';
    nameAr = json['nameAr'] ?? '';
    details = json['details'] ?? '';
    detailsAr = json['detailsAr'] ?? '';
    detailsEn = json['detailsEn'] ?? '';
    totalPrice = json['totalPrice'] ?? 0.0;
    imageUrl = json['imageUrl'] ?? '';
    activeFrom = json['activeFrom'] ?? '';
    activeUntil = json['activeUntil'] ?? '';
    quantityAvailable = json['quantityAvailable'] ?? 0;
    isActive = json['isActive'] ?? false;
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['id'] = id;
    _data['nameEn'] = nameEn;
    _data['nameAr'] = nameAr;
    _data['details'] = details;
    _data['detailsAr'] = detailsAr;
    _data['detailsEn'] = detailsEn;
    _data['totalPrice'] = totalPrice;
    _data['imageUrl'] = imageUrl;
    _data['activeFrom'] = activeFrom;
    _data['activeUntil'] = activeUntil;
    _data['quantityAvailable'] = quantityAvailable;
    _data['isActive'] = isActive;
    return _data;
  }
}
