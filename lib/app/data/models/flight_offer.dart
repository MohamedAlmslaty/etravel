class FlightOffer {
  FlightOffer();

  late final int id;
  late final String offerId;
  late final bool oneWay;
  late final int numberOfBookableSeats;
  late final String currency;
  late final int total;
  late final List<dynamic> itineraries;

  FlightOffer.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    offerId = json['offerId'];
    oneWay = json['oneWay'];
    numberOfBookableSeats = json['numberOfBookableSeats'];
    currency = json['currency'];
    total = json['total'];
    itineraries = List.castFrom<dynamic, dynamic>(json['itineraries']);
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['id'] = id;
    _data['offerId'] = offerId;
    _data['oneWay'] = oneWay;
    _data['numberOfBookableSeats'] = numberOfBookableSeats;
    _data['currency'] = currency;
    _data['total'] = total;
    _data['itineraries'] = itineraries;
    return _data;
  }
}
