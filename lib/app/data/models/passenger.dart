import 'package:e_travel/app/data/models/country.dart';

class Passenger {
  Passenger({this.id});

  int? id;
  String? title;
  String? firstName;
  String? lastName;
  String? passportNo;
  String? birthday;
  String? passport;
  String? passportContentType;
  String? passportUrl;
  String? extraFile;
  String? extraFileContentType;
  String? extraFileUrl;
  String? nationalityCode;
  String? passportExpiryDate;
  String? passportIssueCode;
  bool? isDefault;
  bool? isForeign;
  bool? isVerified;
  Country? country;

  Passenger.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'] ?? '';
    firstName = json['firstName'] ?? '';
    lastName = json['lastName'] ?? '';
    passportNo = json['passportNo'] ?? '';
    birthday = json['birthday'] ?? '';
    passport = json['passport'] ?? '';
    passportContentType = json['passportContentType'] ?? '';
    passportUrl = json['passportUrl'] ?? '';
    nationalityCode = json['nationalityCode'] ?? '';
    passportExpiryDate = json['passportExpiryDate'] ?? '';
    passportIssueCode = json['passportIssueCode'] ?? '';
    extraFile = json['extraFile'] ?? '';
    extraFileContentType = json['extraFileContentType'] ?? '';
    extraFileUrl = json['extraFileUrl'] ?? '';
    isDefault = json['isDefault'];
    isForeign = json['isForeign'] ?? false;
    isVerified = json['isVerified'];
    country =
        json['country'] != null ? Country.fromJson(json['country']) : null;
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['id'] = id;
    _data['title'] = title;
    _data['firstName'] = firstName;
    _data['lastName'] = lastName;
    _data['passportNo'] = passportNo;
    _data['birthday'] = birthday;
    _data['passport'] = passport;
    _data['nationalityCode'] = nationalityCode;
    _data['passportExpiryDate'] = passportExpiryDate;
    _data['passportIssueCode'] = passportIssueCode;
    _data['passportContentType'] = passportContentType;
    _data['passportUrl'] = passportUrl;
    _data['extraFile'] = extraFile;
    _data['extraFileContentType'] = extraFileContentType;
    _data['extraFileUrl'] = extraFileUrl;
    _data['isDefault'] = isDefault;
    _data['isForeign'] = isForeign;
    _data['isVerified'] = isVerified;
    _data['country'] = country;
    return _data;
  }
}
