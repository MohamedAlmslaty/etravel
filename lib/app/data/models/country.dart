class Country {
  Country();

  int? id;
  String? nameEn;
  String? nameAr;
  String? iso2;
  String? iso3;
  int? isoNo;

  Country.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    nameEn = json['nameEn'] ?? '';
    nameAr = json['nameAr'] ?? '';
    iso2 = json['iso2'] ?? '';
    iso3 = json['iso3'] ?? '';
    isoNo = json['isoNo'] ?? 0;
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['id'] = id;
    _data['nameEn'] = nameEn;
    _data['nameAr'] = nameAr;
    _data['iso2'] = iso2;
    _data['iso3'] = iso3;
    _data['isoNo'] = isoNo;
    return _data;
  }
}
