import 'package:e_travel/app/data/models/itineraries.dart';

class FlightRq {
  FlightRq();

  String? id;
  String? offerId;
  String? oneWay;
  String? numberOfBookableSeats;
  bool? containsSuggestion;
  bool? isMultiCity;
  String? currency;
  double? total;
  int? returnStops;
  int? goingStops;
  List<Itineraries>? itineraries;

  FlightRq.fromJson(Map<String, dynamic> json) {
    id = json['id'] ?? '';
    offerId = json['offerId'] ?? '';
    oneWay = json['oneWay'] ?? '';
    goingStops = json['goingStops'] ?? 0;
    containsSuggestion = json['containsSuggestion'] ?? false;
    isMultiCity = json['multiCity'] ?? false;
    returnStops = json['returnStops'] ?? 0;
    numberOfBookableSeats = json['numberOfBookableSeats'] ?? '';
    currency = json['currency'] ?? '';
    total = json['total'] ?? 0.0;
    itineraries = List.from(json['itineraries'])
        .map((e) => Itineraries.fromJson(e))
        .toList();
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['id'] = id;
    _data['offerId'] = offerId;
    _data['goingStops'] = goingStops;
    _data['multiCity'] = isMultiCity;
    _data['containsSuggestion'] = containsSuggestion;
    _data['returnStops'] = returnStops;
    _data['oneWay'] = oneWay;
    _data['numberOfBookableSeats'] = numberOfBookableSeats;
    _data['currency'] = currency;
    _data['total'] = total;
    _data['itineraries'] = itineraries!.map((e) => e.toJson()).toList();
    return _data;
  }
}

