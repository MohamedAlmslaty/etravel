class Moamalat {
  int? amount;
  String? cardExpiration;
  String? cardHolderName;
  String? cardNumber;
  String? customerIp;
  String? invoiceNo;
  String? returnUrl;

  Moamalat(
      {this.amount,
        this.cardExpiration,
        this.cardHolderName,
        this.cardNumber,
        this.customerIp,
        this.invoiceNo,
        this.returnUrl});

  Moamalat.fromJson(Map<String, dynamic> json) {
    amount = json['amount'];
    cardExpiration = json['cardExpiration'];
    cardHolderName = json['cardHolderName'];
    cardNumber = json['cardNumber'];
    customerIp = json['customerIp'];
    invoiceNo = json['invoiceNo'];
    returnUrl = json['returnUrl'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['amount'] = this.amount;
    data['cardExpiration'] = this.cardExpiration;
    data['cardHolderName'] = this.cardHolderName;
    data['cardNumber'] = this.cardNumber;
    data['customerIp'] = this.customerIp;
    data['invoiceNo'] = this.invoiceNo;
    data['returnUrl'] = this.returnUrl;
    return data;
  }
}
