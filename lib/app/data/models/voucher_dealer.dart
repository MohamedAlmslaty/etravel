class VoucherDealer {
  int? id;
  String? name;
  String? details;
  String? address;
  String? mobileNo;
  double? lat;
  double? lng;
  String? cityName;
  String? countryName;
  bool? isActive;

  VoucherDealer(
      {this.id,
        this.name,
        this.details,
        this.address,
        this.mobileNo,
        this.lat,
        this.lng,
        this.cityName,
        this.countryName,
        this.isActive});

  VoucherDealer.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    details = json['details'];
    address = json['address'];
    mobileNo = json['mobileNo'];
    lat = json['lat'];
    lng = json['lng'];
    cityName = json['cityName'];
    countryName = json['countryName'];
    isActive = json['isActive'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['details'] = this.details;
    data['address'] = this.address;
    data['mobileNo'] = this.mobileNo;
    data['lat'] = this.lat;
    data['lng'] = this.lng;
    data['cityName'] = this.cityName;
    data['countryName'] = this.countryName;
    data['isActive'] = this.isActive;
    return data;
  }
}
