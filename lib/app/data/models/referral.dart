import 'package:e_travel/app/data/models/customer.dart';

class Referral {
  String? createdBy;
  String? createdDate;
  String? lastModifiedBy;
  String? lastModifiedDate;
  int? id;
  String? referralCode;
  double? referrerAmount;
  double? referredCustomerAmount;
  String? expiryDate;
  String? referralStatus;
  Customer? referredCustomer;
  Customer? referrerCustomer;

  Referral(
      {this.createdBy,
        this.createdDate,
        this.lastModifiedBy,
        this.lastModifiedDate,
        this.id,
        this.referralCode,
        this.referrerAmount,
        this.referredCustomerAmount,
        this.expiryDate,
        this.referralStatus,
        this.referredCustomer,
        this.referrerCustomer});

  Referral.fromJson(Map<String, dynamic> json) {
    createdBy = json['createdBy'];
    createdDate = json['createdDate'];
    lastModifiedBy = json['lastModifiedBy'];
    lastModifiedDate = json['lastModifiedDate'];
    id = json['id'];
    referralCode = json['referralCode'];
    referrerAmount = json['referrerAmount'];
    referredCustomerAmount = json['referredCustomerAmount'];
    expiryDate = json['expiryDate'];
    referralStatus = json['referralStatus'];
    referredCustomer = json['referredCustomer'] != null
        ? new Customer.fromJson(json['referredCustomer'])
        : Customer();
    referrerCustomer = json['referrerCustomer'] != null
        ? new Customer.fromJson(json['referrerCustomer'])
        : Customer();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['createdBy'] = this.createdBy;
    data['createdDate'] = this.createdDate;
    data['lastModifiedBy'] = this.lastModifiedBy;
    data['lastModifiedDate'] = this.lastModifiedDate;
    data['id'] = this.id;
    data['referralCode'] = this.referralCode;
    data['referrerAmount'] = this.referrerAmount;
    data['referredCustomerAmount'] = this.referredCustomerAmount;
    data['expiryDate'] = this.expiryDate;
    data['referralStatus'] = this.referralStatus;
    data['referredCustomer'] = this.referredCustomer;
    if (this.referrerCustomer != null) {
      data['referrerCustomer'] = this.referrerCustomer!.toJson();
    }
    return data;
  }
}

