class Tadawal {
  double? amount;
  String? customerEmail;
  String? customerPhone;

  Tadawal({this.amount, this.customerEmail, this.customerPhone});

  Tadawal.fromJson(Map<String, dynamic> json) {
    amount = json['amount'];
    customerEmail = json['customerEmail'];
    customerPhone = json['customerPhone'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['amount'] = this.amount;
    data['customerEmail'] = this.customerEmail;
    data['customerPhone'] = this.customerPhone;
    return data;
  }
}
