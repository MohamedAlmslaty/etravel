import 'package:e_travel/app/data/models/passenger.dart';

class BookingTotalDetails {
  double? fare;
  Passenger? passenger;
  double? tax;
  double? total;

  BookingTotalDetails({this.fare, this.passenger, this.tax, this.total});

  BookingTotalDetails.fromJson(Map<String, dynamic> json) {
    fare = json['fare'];
    passenger = json['passengerDTO'] != null
        ? new Passenger.fromJson(json['passengerDTO'])
        : Passenger();
    tax = json['tax'];
    total = json['total'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['fare'] = this.fare;
    if (this.passenger != null) {
      data['passengerDTO'] = this.passenger!.toJson();
    }
    data['tax'] = this.tax;
    data['total'] = this.total;
    return data;
  }
}

