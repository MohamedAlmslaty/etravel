import 'package:e_travel/app/data/models/customer.dart';

class Transactions {
  int? id;
  String? transactionReference;
  String? paymentType;
  String? transactionStatus;
  String? vendorReference;
  String? vendorMessage;
  double? amount;
  double? fees;
  double? total;
  String? notes;
  Customer? customer;

  Transactions(
      {this.id,
        this.transactionReference,
        this.paymentType,
        this.transactionStatus,
        this.vendorReference,
        this.vendorMessage,
        this.amount,
        this.fees,
        this.total,
        this.notes,
        this.customer});

  Transactions.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    transactionReference = json['transactionReference'];
    paymentType = json['paymentType'];
    transactionStatus = json['transactionStatus'];
    vendorReference = json['vendorReference'];
    vendorMessage = json['vendorMessage'];
    amount = json['amount'];
    fees = json['fees'];
    total = json['total'];
    notes = json['notes'];
    customer = json['customer'] != null
        ? new Customer.fromJson(json['customer'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['transactionReference'] = this.transactionReference;
    data['paymentType'] = this.paymentType;
    data['transactionStatus'] = this.transactionStatus;
    data['vendorReference'] = this.vendorReference;
    data['vendorMessage'] = this.vendorMessage;
    data['amount'] = this.amount;
    data['fees'] = this.fees;
    data['total'] = this.total;
    data['notes'] = this.notes;
    if (this.customer != null) {
      data['customer'] = this.customer!.toJson();
    }
    return data;
  }
}

