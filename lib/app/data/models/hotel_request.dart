import 'package:e_travel/app/data/models/available_room_types.dart';

class HotelRequestBooking {
  HotelRequestBooking({
    this.hotelRoomTypes,
    this.checkInDate,
    this.children,
    this.adults,
    this.checkOutDate,
    this.hotelId,
    this.personEmail,
    this.personFirstName,
    this.personLastName,
    this.personMobileNo,
});

  int? adults ;
  int? children ;
  int? hotelId ;
  String? checkInDate ;
  String? checkOutDate ;
  List<AvailableRoomTypes>? hotelRoomTypes;
  String? personEmail ;
  String? personFirstName ;
  String? personLastName ;
  String? personMobileNo ;

  HotelRequestBooking.fromJson(Map<String, dynamic> json) {
    adults  = json['adults '];
    children   = json['children  '];
    hotelId  = json['hotelId '];
    checkInDate  = json['checkinDate '];
    checkOutDate = json['checkoutDate '];
    personEmail = json['personEmail '];
    personFirstName = json['personFirstName '];
    personLastName = json['personLastName '];
    personMobileNo = json['personMobileNo '];
    if (json['hotelRoomTypes'] != null) {
      hotelRoomTypes = <AvailableRoomTypes>[];
      json['hotelRoomTypes'].forEach((v) {
        hotelRoomTypes!.add(new AvailableRoomTypes.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['adults'] = adults ;
    _data['children'] = children  ;
    _data['hotelId'] = hotelId ;
    _data['checkinDate'] = checkInDate ;
    _data['checkoutDate'] = checkOutDate;
    _data['personEmail'] = personEmail;
    _data['personFirstName'] = personFirstName;
    _data['personLastName'] = personLastName;
    _data['personMobileNo'] = personMobileNo;
    if (this.hotelRoomTypes != null) {
      _data['hotelRoomTypes'] =
          this.hotelRoomTypes!.map((v) => v.toJson()).toList();
    }
    return _data;
  }
}
