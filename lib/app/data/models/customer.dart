class Customer {
  int? id;
  int? userId;
  String? appleId;
  String? googleId;
  String? facebookId;
  String? firebaseId;
  String? name;
  String? email;
  String? mobileNo;
  String? userLogin;
  String? createdBy;
  String? createdDate;
  String? newPassword;
  bool? isBanned;
  bool? isPhoneVerified;
  bool? isVerified;
  bool? verifiedByEmail;
  bool? verifiedByMobileNo;
  bool? verifiedBySocialId;
  String? lastModifiedBy;
  String? lastModifiedDate;
  String? walletPublicKey;
  String? otp;
  String? referralCode;

  Customer({
    this.email,
    this.userLogin,
    this.id,
    this.mobileNo,
    this.name,
    this.newPassword,
    this.appleId,
    this.createdBy,
    this.createdDate,
    this.facebookId,
    this.firebaseId,
    this.googleId,
    this.isBanned,
    this.isVerified,
    this.verifiedByEmail,
    this.verifiedByMobileNo,
    this.verifiedBySocialId,
    this.lastModifiedBy,
    this.lastModifiedDate,
    this.userId,
    this.isPhoneVerified,
    this.otp,
    this.referralCode,
  });

  Map<String, dynamic> toJson() {
    return {
      'userId': userId,
      'appleId': appleId,
      'googleId': googleId,
      'facebookId': facebookId,
      'firebaseId': firebaseId,
      'name': name,
      'otp': otp,
      'referralCode': referralCode,
      'email': email,
      'mobileNo': mobileNo,
      'userLogin': userLogin,
      'newPassword': newPassword,
      'isBanned': isBanned,
      'isPhoneVerified': isPhoneVerified,
      'isVerified': isVerified,
      'verifiedByEmail': verifiedByEmail,
      'verifiedByMobileNo': verifiedByMobileNo,
      'verifiedBySocialId': verifiedBySocialId,
    };
  }

  Customer.fromJson(Map<String, dynamic> map) {
    id = map['id'];
    userId = map['userId'] ?? 0;
    appleId = map['appleId'] ?? '';
    googleId = map['googleId'] ?? '';
    facebookId = map['facebookId'] ?? '';
    firebaseId = map['firebaseId'] ?? '';
    name = map['name']!=null?map['name'] :'';
    email = map['email'] ?? '';
    mobileNo = map['mobileNo'] ?? '';
    userLogin = map['userLogin'] ?? '';
    createdBy = map['createdBy'] ?? '';
    createdDate = map['createdDate'] ?? '';
    newPassword = map['newPassword'] ?? '';
    otp = map['otp'] ?? '';
    referralCode = map['referralCode'] ?? '';
    isBanned = map['isBanned'] ?? false;
    verifiedByMobileNo = map['verifiedByMobileNo'] ?? false;
    verifiedBySocialId = map['verifiedBySocialId'] ?? false;
    verifiedByEmail = map['verifiedByEmail'] ?? false;
    isBanned = map['isBanned'] ?? false;
    isPhoneVerified = map['isPhoneVerified'] ?? false;
    isVerified = map['isVerified'] ?? false;
    lastModifiedBy = map['lastModifiedBy'] ?? '';
    lastModifiedDate = map['lastModifiedDate'] ?? '';
    walletPublicKey = map['walletPublicKey'] ?? '';
  }
}
