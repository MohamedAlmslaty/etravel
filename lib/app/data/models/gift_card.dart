class GiftCard {
  late final int id;
  late final int customerWalletId;
  late final bool isRedeemed;
  late final String code;
  late final double amount;
  late final String notes;
  late final String createdBy;
  late final String createdDate;
  late final String customerWalletWalletNo;
  late final String lastModifiedBy;
  late final String lastModifiedDate;

  GiftCard();

  GiftCard.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    customerWalletId = json['customerWalletId'];
    isRedeemed = json['isRedeemed'];
    code = json['code'];
    amount = json['amount'];
    notes = json['notes'];
    createdBy = json['createdBy'];
    createdDate = json['createdDate'];
    customerWalletWalletNo = json['customerWalletWalletNo'];
    lastModifiedBy = json['lastModifiedBy'];
    lastModifiedDate = json['lastModifiedDate'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['id'] = id;
    _data['customerWalletId'] = customerWalletId;
    _data['isRedeemed'] = isRedeemed;
    _data['code'] = code;
    _data['amount'] = amount;
    _data['notes'] = notes;
    _data['createdBy'] = createdBy;
    _data['createdDate'] = createdDate;
    _data['customerWalletWalletNo'] = customerWalletWalletNo;
    _data['lastModifiedBy'] = lastModifiedBy;
    _data['lastModifiedDate'] = lastModifiedDate;
    return _data;
  }
}
