class PasswordChange {
  late final String currentPassword;
  late final String newPassword;

  PasswordChange();

  PasswordChange.fromJson(Map<String, dynamic> json) {
    currentPassword = json['currentPassword'];
    newPassword = json['newPassword'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['currentPassword'] = currentPassword;
    _data['newPassword'] = newPassword;
    return _data;
  }
}
