class CabinClass {
  String? name;
  String? enumName;

  CabinClass({this.name, this.enumName});
}

List<CabinClass> cabin = [
  new CabinClass(name: 'ECONOMY', enumName: 'ECONOMY'),
  new CabinClass(name: 'ECONOMY_STANDARD', enumName: 'ECONOMY_STANDARD'),
  new CabinClass(name: 'ECONOMY_PREMIUM', enumName: 'ECONOMY_PREMIUM'),
  new CabinClass(name: 'BUSINESS', enumName: 'BUSINESS'),
  new CabinClass(name: 'FIRST', enumName: 'FIRST'),
];
