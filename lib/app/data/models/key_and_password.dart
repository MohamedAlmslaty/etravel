class KeyAndPasswordVm {
  late final String key;
  late final String newPassword;

  KeyAndPasswordVm();

  KeyAndPasswordVm.fromJson(Map<String, dynamic> json) {
    key = json['key'];
    newPassword = json['newPassword'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['key'] = key;
    _data['newPassword'] = newPassword;

    return _data;
  }
}
