import 'package:e_travel/app/data/models/city.dart';
import 'package:e_travel/app/data/models/customer.dart';
import 'package:e_travel/app/data/models/passenger.dart';
import 'package:e_travel/app/data/models/user.dart';

class HotelBooking {
  String? addressMobileNo;
  String? billingAddress;
  String? bookedAt;
  String? bookedFrom;
  String? bookedUntil;
  String? bookingPinCode;
  String? bookingReference;
  CompanyAccount? companyAccount;
  String? currency;
  Customer? customer;
  double? discount;
  Hotel? hotel;
  String? hotelBookingStatus;
  String? hotelBookingType;
  HotelRoom? hotelRoom;
  HotelRoomType? hotelRoomType;
  int? id;
  String? invoiceNo;
  bool? isPayed;
  int? numberOfNights;
  int? numberOfRooms;
  Passenger? passenger;
  double? paxPrice;
  double? paxTax;
  double? paxTotal;
  String? paxType;
  String? payemntDateTime;
  String? paymentReference;
  String? paymentType;
  int? pepoleNo;
  String? personBirthday;
  String? personFirstName;
  String? personLastName;
  String? personMobileNo;
  String? personPassportNo;
  List<String>? relatedBookings;
  double? total;

  HotelBooking(
      {this.addressMobileNo,
        this.billingAddress,
        this.bookedAt,
        this.bookedFrom,
        this.bookedUntil,
        this.bookingPinCode,
        this.bookingReference,
        this.companyAccount,
        this.currency,
        this.customer,
        this.discount,
        this.hotel,
        this.hotelBookingStatus,
        this.hotelBookingType,
        this.hotelRoom,
        this.hotelRoomType,
        this.id,
        this.invoiceNo,
        this.isPayed,
        this.numberOfNights,
        this.numberOfRooms,
        this.passenger,
        this.paxPrice,
        this.paxTax,
        this.paxTotal,
        this.paxType,
        this.payemntDateTime,
        this.paymentReference,
        this.paymentType,
        this.pepoleNo,
        this.personBirthday,
        this.personFirstName,
        this.personLastName,
        this.personMobileNo,
        this.personPassportNo,
        this.relatedBookings,
        this.total});

  HotelBooking.fromJson(Map<String, dynamic> json) {
    addressMobileNo = json['addressMobileNo'];
    billingAddress = json['billingAddress'];
    bookedAt = json['bookedAt'];
    bookedFrom = json['bookedFrom'];
    bookedUntil = json['bookedUntil'];
    bookingPinCode = json['bookingPinCode'];
    bookingReference = json['bookingReference'];
    companyAccount = json['companyAccount'] != null
        ? new CompanyAccount.fromJson(json['companyAccount'])
        : CompanyAccount();
    currency = json['currency'];
    customer = json['customer'] != null
        ? new Customer.fromJson(json['customer'])
        : Customer();
    discount = json['discount'];
    hotel = json['hotel'] != null ? new Hotel.fromJson(json['hotel']) : Hotel();
    hotelBookingStatus = json['hotelBookingStatus'];
    hotelBookingType = json['hotelBookingType'];
    hotelRoom = json['hotelRoom'] != null
        ? new HotelRoom.fromJson(json['hotelRoom'])
        : HotelRoom();
    hotelRoomType = json['hotelRoomType'] != null
        ? new HotelRoomType.fromJson(json['hotelRoomType'])
        : HotelRoomType();
    id = json['id'];
    invoiceNo = json['invoiceNo'];
    isPayed = json['isPayed'];
    numberOfNights = json['numberOfNights'];
    numberOfRooms = json['numberOfRooms'];
    passenger = json['passenger'] != null
        ? new Passenger.fromJson(json['passenger'])
        : Passenger();
    paxPrice = json['paxPrice'];
    paxTax = json['paxTax'];
    paxTotal = json['paxTotal'];
    paxType = json['paxType'];
    payemntDateTime = json['payemntDateTime'];
    paymentReference = json['paymentReference'];
    paymentType = json['paymentType'];
    pepoleNo = json['pepoleNo'];
    personBirthday = json['personBirthday'];
    personFirstName = json['personFirstName'];
    personLastName = json['personLastName'];
    personMobileNo = json['personMobileNo'];
    personPassportNo = json['personPassportNo'];
    relatedBookings = json['relatedBookings'].cast<String>();
    total = json['total'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['addressMobileNo'] = this.addressMobileNo;
    data['billingAddress'] = this.billingAddress;
    data['bookedAt'] = this.bookedAt;
    data['bookedFrom'] = this.bookedFrom;
    data['bookedUntil'] = this.bookedUntil;
    data['bookingPinCode'] = this.bookingPinCode;
    data['bookingReference'] = this.bookingReference;
    if (this.companyAccount != null) {
      data['companyAccount'] = this.companyAccount!.toJson();
    }
    data['currency'] = this.currency;
    if (this.customer != null) {
      data['customer'] = this.customer!.toJson();
    }
    data['discount'] = this.discount;
    if (this.hotel != null) {
      data['hotel'] = this.hotel!.toJson();
    }
    data['hotelBookingStatus'] = this.hotelBookingStatus;
    data['hotelBookingType'] = this.hotelBookingType;
    if (this.hotelRoom != null) {
      data['hotelRoom'] = this.hotelRoom!.toJson();
    }
    if (this.hotelRoomType != null) {
      data['hotelRoomType'] = this.hotelRoomType!.toJson();
    }
    data['id'] = this.id;
    data['invoiceNo'] = this.invoiceNo;
    data['isPayed'] = this.isPayed;
    data['numberOfNights'] = this.numberOfNights;
    data['numberOfRooms'] = this.numberOfRooms;
    if (this.passenger != null) {
      data['passenger'] = this.passenger!.toJson();
    }
    data['paxPrice'] = this.paxPrice;
    data['paxTax'] = this.paxTax;
    data['paxTotal'] = this.paxTotal;
    data['paxType'] = this.paxType;
    data['payemntDateTime'] = this.payemntDateTime;
    data['paymentReference'] = this.paymentReference;
    data['paymentType'] = this.paymentType;
    data['pepoleNo'] = this.pepoleNo;
    data['personBirthday'] = this.personBirthday;
    data['personFirstName'] = this.personFirstName;
    data['personLastName'] = this.personLastName;
    data['personMobileNo'] = this.personMobileNo;
    data['personPassportNo'] = this.personPassportNo;
    data['relatedBookings'] = this.relatedBookings;
    data['total'] = this.total;
    return data;
  }
}

class CompanyAccount {
  String? email;
  Hotel? hotel;
  int? id;
  String? mobileNo;
  String? name;

  CompanyAccount({this.email, this.hotel, this.id, this.mobileNo, this.name});

  CompanyAccount.fromJson(Map<String, dynamic> json) {
    email = json['email'];
    hotel = json['hotel'] != null ? new Hotel.fromJson(json['hotel']) : null;
    id = json['id'];
    mobileNo = json['mobileNo'];
    name = json['name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['email'] = this.email;
    if (this.hotel != null) {
      data['hotel'] = this.hotel!.toJson();
    }
    data['id'] = this.id;
    data['mobileNo'] = this.mobileNo;
    data['name'] = this.name;
    return data;
  }
}

class Hotel {
  String? addressAr;
  String? addressEn;
  List<String>? availableRoomTypes;
  City? city;
  String? detailsAr;
  String? detailsEn;
  String? email;
  String? hotelType;
  int? id;
  String? imageFile;
  String? imageFileContentType;
  String? imageFileUrl;
  bool? isActive;
  int? lat;
  int? lng;
  int? menuOrder;
  String? mobileNo;
  String? nameAr;
  String? nameEn;
  int? stars;
  User? user;

  Hotel(
      {this.addressAr,
        this.addressEn,
        this.availableRoomTypes,
        this.city,
        this.detailsAr,
        this.detailsEn,
        this.email,
        this.hotelType,
        this.id,
        this.imageFile,
        this.imageFileContentType,
        this.imageFileUrl,
        this.isActive,
        this.lat,
        this.lng,
        this.menuOrder,
        this.mobileNo,
        this.nameAr,
        this.nameEn,
        this.stars,
        this.user});

  Hotel.fromJson(Map<String, dynamic> json) {
    addressAr = json['addressAr'];
    addressEn = json['addressEn'];
    availableRoomTypes = json['availableRoomTypes'].cast<String>();
    city = json['city'] != null ? new City.fromJson(json['city']) : City();
    detailsAr = json['detailsAr'];
    detailsEn = json['detailsEn'];
    email = json['email'];
    hotelType = json['hotelType'];
    id = json['id'];
    imageFile = json['imageFile'];
    imageFileContentType = json['imageFileContentType'];
    imageFileUrl = json['imageFileUrl'];
    isActive = json['isActive'];
    lat = json['lat'];
    lng = json['lng'];
    menuOrder = json['menuOrder'];
    mobileNo = json['mobileNo'];
    nameAr = json['nameAr'];
    nameEn = json['nameEn'];
    stars = json['stars'];
    user = json['user'] != null ? new User.fromJson(json['user']) : User();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['addressAr'] = this.addressAr;
    data['addressEn'] = this.addressEn;
    data['availableRoomTypes'] = this.availableRoomTypes;
    if (this.city != null) {
      data['city'] = this.city!.toJson();
    }
    data['detailsAr'] = this.detailsAr;
    data['detailsEn'] = this.detailsEn;
    data['email'] = this.email;
    data['hotelType'] = this.hotelType;
    data['id'] = this.id;
    data['imageFile'] = this.imageFile;
    data['imageFileContentType'] = this.imageFileContentType;
    data['imageFileUrl'] = this.imageFileUrl;
    data['isActive'] = this.isActive;
    data['lat'] = this.lat;
    data['lng'] = this.lng;
    data['menuOrder'] = this.menuOrder;
    data['mobileNo'] = this.mobileNo;
    data['nameAr'] = this.nameAr;
    data['nameEn'] = this.nameEn;
    data['stars'] = this.stars;
    if (this.user != null) {
      data['user'] = this.user!.toJson();
    }
    return data;
  }
}


class HotelRoom {
  Hotel? hotel;
  String? hotelRoomStatus;
  HotelRoomType? hotelRoomType;
  int? id;
  bool? isAvaliableOnline;
  List<RoomFacilities>? roomFacilities;
  String? roomNo;

  HotelRoom(
      {this.hotel,
        this.hotelRoomStatus,
        this.hotelRoomType,
        this.id,
        this.isAvaliableOnline,
        this.roomFacilities,
        this.roomNo});

  HotelRoom.fromJson(Map<String, dynamic> json) {
    hotel = json['hotel'] != null ? new Hotel.fromJson(json['hotel']) : null;
    hotelRoomStatus = json['hotelRoomStatus'];
    hotelRoomType = json['hotelRoomType'] != null
        ? new HotelRoomType.fromJson(json['hotelRoomType'])
        : null;
    id = json['id'];
    isAvaliableOnline = json['isAvaliableOnline'];
    if (json['roomFacilities'] != null) {
      roomFacilities = <RoomFacilities>[];
      json['roomFacilities'].forEach((v) {
        roomFacilities!.add(new RoomFacilities.fromJson(v));
      });
    }
    roomNo = json['roomNo'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.hotel != null) {
      data['hotel'] = this.hotel!.toJson();
    }
    data['hotelRoomStatus'] = this.hotelRoomStatus;
    if (this.hotelRoomType != null) {
      data['hotelRoomType'] = this.hotelRoomType!.toJson();
    }
    data['id'] = this.id;
    data['isAvaliableOnline'] = this.isAvaliableOnline;
    if (this.roomFacilities != null) {
      data['roomFacilities'] =
          this.roomFacilities!.map((v) => v.toJson()).toList();
    }
    data['roomNo'] = this.roomNo;
    return data;
  }
}

class HotelRoomType {
  String? bedTypes;
  int? bedsNumber;
  int? freeCancellationAdditionalCost;
  int? freeCancellationDays;
  Hotel? hotel;
  int? id;
  bool? isAvaliable;
  bool? isFreeCancellation;
  bool? isShownOnline;
  int? maxOccupancy;
  int? menuOrder;
  String? nameAr;
  String? nameEn;
  int? numberOfAvailableRooms;
  int? price;
  int? requestedNumberOfRooms;
  bool? seasonPricing;

  HotelRoomType(
      {this.bedTypes,
        this.bedsNumber,
        this.freeCancellationAdditionalCost,
        this.freeCancellationDays,
        this.hotel,
        this.id,
        this.isAvaliable,
        this.isFreeCancellation,
        this.isShownOnline,
        this.maxOccupancy,
        this.menuOrder,
        this.nameAr,
        this.nameEn,
        this.numberOfAvailableRooms,
        this.price,
        this.requestedNumberOfRooms,
        this.seasonPricing});

  HotelRoomType.fromJson(Map<String, dynamic> json) {
    bedTypes = json['bedTypes'];
    bedsNumber = json['bedsNumber'];
    freeCancellationAdditionalCost = json['freeCancellationAdditionalCost'];
    freeCancellationDays = json['freeCancellationDays'];
    hotel = json['hotel'] != null ? new Hotel.fromJson(json['hotel']) : null;
    id = json['id'];
    isAvaliable = json['isAvaliable'];
    isFreeCancellation = json['isFreeCancellation'];
    isShownOnline = json['isShownOnline'];
    maxOccupancy = json['maxOccupancy'];
    menuOrder = json['menuOrder'];
    nameAr = json['nameAr'];
    nameEn = json['nameEn'];
    numberOfAvailableRooms = json['numberOfAvailableRooms'];
    price = json['price'];
    requestedNumberOfRooms = json['requestedNumberOfRooms'];
    seasonPricing = json['seasonPricing'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['bedTypes'] = this.bedTypes;
    data['bedsNumber'] = this.bedsNumber;
    data['freeCancellationAdditionalCost'] =
        this.freeCancellationAdditionalCost;
    data['freeCancellationDays'] = this.freeCancellationDays;
    if (this.hotel != null) {
      data['hotel'] = this.hotel!.toJson();
    }
    data['id'] = this.id;
    data['isAvaliable'] = this.isAvaliable;
    data['isFreeCancellation'] = this.isFreeCancellation;
    data['isShownOnline'] = this.isShownOnline;
    data['maxOccupancy'] = this.maxOccupancy;
    data['menuOrder'] = this.menuOrder;
    data['nameAr'] = this.nameAr;
    data['nameEn'] = this.nameEn;
    data['numberOfAvailableRooms'] = this.numberOfAvailableRooms;
    data['price'] = this.price;
    data['requestedNumberOfRooms'] = this.requestedNumberOfRooms;
    data['seasonPricing'] = this.seasonPricing;
    return data;
  }
}

class RoomFacilities {
  int? id;
  bool? isActive;
  String? nameAr;
  String? nameEn;

  RoomFacilities({this.id, this.isActive, this.nameAr, this.nameEn});

  RoomFacilities.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    isActive = json['isActive'];
    nameAr = json['nameAr'];
    nameEn = json['nameEn'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['isActive'] = this.isActive;
    data['nameAr'] = this.nameAr;
    data['nameEn'] = this.nameEn;
    return data;
  }
}
