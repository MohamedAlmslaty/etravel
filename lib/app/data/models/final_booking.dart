import 'package:e_travel/app/data/models/attachment.dart';
import 'package:e_travel/app/data/models/flight_req.dart';
import 'package:e_travel/app/data/models/passenger.dart';

class FinalBooking {
  FlightRequest? flightRequest;
  List<Passenger>? passengers;
  List<Attachment>? attachments;
  FinalBooking({this.flightRequest, this.passengers,this.attachments});

  FinalBooking.fromJson(Map<String, dynamic> json) {
    flightRequest = json['flightRequest'] != null
        ? new FlightRequest.fromJson(json['flightRequest'])
        : null;
    if (json['passengers'] != null) {
      passengers = <Passenger>[];
      json['passengers'].forEach((v) {
        passengers!.add(new Passenger.fromJson(v));
      });
    }
    if (json['attachments'] != null) {
      attachments = <Attachment>[];
      json['attachments'].forEach((v) {
        attachments!.add(new Attachment.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.flightRequest != null) {
      data['flightRequest'] = this.flightRequest!.toJson();
    }
    if (this.passengers != null) {
      data['passengers'] = this.passengers!.map((v) => v.toJson()).toList();
    }
    if (this.attachments != null) {
      data['attachments'] = this.attachments!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class FinalBookingMulti {
  FlightRequestMulti? flightRequestMulti;
  List<Passenger>? passengers;
  List<Attachment>? attachments;

  FinalBookingMulti(
      {this.flightRequestMulti, this.passengers, this.attachments});

  FinalBookingMulti.fromJson(Map<String, dynamic> json) {
    flightRequestMulti = json['flightRequest'] != null
        ? new FlightRequestMulti.fromJson(json['flightRequest'])
        : null;
    if (json['passengers'] != null) {
      passengers = <Passenger>[];
      json['passengers'].forEach((v) {
        passengers!.add(new Passenger.fromJson(v));
      });
    }
      if (json['attachments'] != null) {
        attachments = <Attachment>[];
        json['attachments'].forEach((v) {
          attachments!.add(new Attachment.fromJson(v));
        });
      }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.flightRequestMulti != null) {
      data['flightRequest'] = this.flightRequestMulti!.toJson();
    }
    if (this.passengers != null) {
      data['passengers'] = this.passengers!.map((v) => v.toJson()).toList();
    }
    if (this.attachments != null) {
      data['attachments'] = this.attachments!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
