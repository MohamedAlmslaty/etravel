class SadadOTP {
  int? statusCode;
  Result? result;

  SadadOTP({this.statusCode, this.result});

  SadadOTP.fromJson(Map<String, dynamic> json) {
    statusCode = json['statusCode'];
    result =
    json['result'] != null ? new Result.fromJson(json['result']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['statusCode'] = this.statusCode;
    if (this.result != null) {
      data['result'] = this.result!.toJson();
    }
    return data;
  }
}

class Result {
  int? transactionId;

  Result(
      {this.transactionId,}
  );

  Result.fromJson(Map<String, dynamic> json) {
    transactionId = json['transactionId'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['transactionId'] = this.transactionId;
    return data;
  }
}
