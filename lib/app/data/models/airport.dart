import 'package:e_travel/app/data/models/city.dart';

class Airport {
  int? id;
  String? name;
  String? nameAr;
  String? nameEn;
  String? iataCode;
  String? icaoCode;
  double? lat;
  double? lng;
  String? cityName;
  String? countryName;
  int? menuOrder;
  bool? isActive;
  City? city;

  Airport(
      {this.id,
        this.name,
        this.nameAr,
        this.nameEn,
        this.iataCode,
        this.icaoCode,
        this.lat,
        this.lng,
        this.cityName,
        this.countryName,
        this.menuOrder,
        this.isActive,
        this.city});

  Airport.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    nameAr = json['nameAr'];
    nameEn = json['nameEn'];
    iataCode = json['iataCode'];
    icaoCode = json['icaoCode'];
    lat = json['lat'];
    lng = json['lng'];
    cityName = json['cityName'];
    countryName = json['countryName'];
    menuOrder = json['menuOrder'];
    isActive = json['isActive'];
    city = json['city'] != null ? new City.fromJson(json['city']) : City();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['nameAr'] = this.nameAr;
    data['nameEn'] = this.nameEn;
    data['iataCode'] = this.iataCode;
    data['icaoCode'] = this.icaoCode;
    data['lat'] = this.lat;
    data['lng'] = this.lng;
    data['cityName'] = this.cityName;
    data['countryName'] = this.countryName;
    data['menuOrder'] = this.menuOrder;
    data['isActive'] = this.isActive;
    if (this.city != null) {
      data['city'] = this.city!.toJson();
    }
    return data;
  }
}


