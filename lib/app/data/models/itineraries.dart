import 'package:e_travel/app/data/models/airline.dart';
import 'package:e_travel/app/data/models/airport.dart';
import 'package:e_travel/app/data/models/flight_booking.dart';

class Itineraries {
  Itineraries();

  int? id;
  String? pnr;
  int? order;
  int? segment;
  String? flightNo;
  String? airlineText;
  String? departureAirportText;
  String? arrivalAirportText;
  DateTime? departureDateTime;
  DateTime? arrivalDateTime;
  String? cabinClass;
  String? ticketClass;
  String? duration;
  int? stops;
  String? airplane;
  String? seatNo;
  String? meal;
  String? bags;
  Airport? departureAirport;
  Airport? arrivalAirport;
  Airline? airline;
  String? classLetter;
  bool? isSuggestion;

  Itineraries.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    pnr = json['pnr'] ?? '';
    order = json['order'];
    segment = json['segment'];
    classLetter = json['classLetter'] ?? '';
    flightNo = json['flightNo'] ?? '';
    airlineText = json['airlineText'] ?? '';
    departureAirportText = json['departureAirportText'] ?? '';
    arrivalAirportText = json['arrivalAirportText'] ?? '';
    isSuggestion = json['isSuggestion'] ?? false;
    departureDateTime =
        DateTime.tryParse(json['departureDateTime']) ?? DateTime.now();
    arrivalDateTime =
        DateTime.tryParse(json['arrivalDateTime']) ?? DateTime.now();
    cabinClass = json['cabinClass'] ?? '';
    ticketClass = json['ticketClass'] ?? '';
    duration = json['duration'] ?? '';
    stops = json['stops'] ?? 0;
    airplane = json['airplane'] ?? '';
    seatNo = json['seatNo'] ?? '';
    meal = json['meal'] ?? '';
    bags = json['bags'] ?? '';
    departureAirport = json['departureAirport'] != null ? Airport.fromJson(json['departureAirport']) : Airport();
    arrivalAirport = json['arrivalAirport'] != null ? Airport.fromJson(json['arrivalAirport']) : Airport();
    airline = json['arrivalAirport'] != null ? Airline.fromJson(json['airline']) : Airline();
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['id'] = id;
    _data['pnr'] = pnr;
    _data['order'] = order;
    _data['isSuggestion'] = isSuggestion;
    _data['segment'] = segment;
    _data['classLetter'] = classLetter;
    _data['flightNo'] = flightNo;
    _data['airlineText'] = airlineText;
    _data['departureAirportText'] = departureAirportText;
    _data['arrivalAirportText'] = arrivalAirportText;
    _data['departureDateTime'] = departureDateTime?.toIso8601String();
    _data['arrivalDateTime'] = arrivalDateTime?.toIso8601String();
    _data['cabinClass'] = cabinClass;
    _data['ticketClass'] = ticketClass;
    _data['duration'] = duration;
    _data['stops'] = stops;
    _data['airplane'] = airplane;
    _data['seatNo'] = seatNo;
    _data['meal'] = meal;
    _data['bags'] = bags;
    _data['departureAirport'] = departureAirport!.toJson();
    _data['arrivalAirport'] = arrivalAirport!.toJson();
    _data['airline'] = airline!.toJson();
    return _data;
  }
}
