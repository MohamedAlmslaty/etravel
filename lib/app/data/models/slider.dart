class Slider {
  int? id;
  String? details;
  int? menuOrder;
  String? sliderType;
  String? imageUrl;
  String? image;
  String? imageContentType;
  String? url;

  Slider(
      {this.id,
        this.details,
        this.menuOrder,
        this.sliderType,
        this.imageUrl,
        this.image,
        this.imageContentType,
        this.url});

  Slider.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    details = json['details'];
    menuOrder = json['menuOrder'];
    sliderType = json['sliderType'];
    imageUrl = json['imageUrl'];
    image = json['image'];
    imageContentType = json['imageContentType'];
    url = json['url'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['details'] = this.details;
    data['menuOrder'] = this.menuOrder;
    data['sliderType'] = this.sliderType;
    data['imageUrl'] = this.imageUrl;
    data['image'] = this.image;
    data['imageContentType'] = this.imageContentType;
    data['url'] = this.url;
    return data;
  }
}
