import 'package:e_travel/app/data/models/tourist_packages.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class PackageSubscription {
  PackageSubscription();

  int? id;
  double? totalPrice;
  String? activeFrom;
  String? activeUntil;
  DateTime? createdDate;
  bool? isActive;
  String? paymentType;
  int? persons;
  String? paymentReference;
  TouristPackages? touristPackage;

  PackageSubscription.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    totalPrice = json['totalPrice'] ?? 0.0;
    activeFrom = json['activeFrom'] ?? '';
    activeUntil = json['activeUntil'] ?? '';

    createdDate = (json['createdDate'] != null ? DateTime.tryParse(json['createdDate']) : DateTime.now())!;
    persons = json['persons'] ?? 1;
    isActive = json['isActive'] ?? false;
    paymentType = json['paymentType'] ?? '';
    paymentReference = json['paymentReference'] ?? '';
    touristPackage = TouristPackages.fromJson(json['touristPackage']);
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['id'] = id;
    _data['totalPrice'] = totalPrice;
    _data['activeFrom'] = activeFrom;
    _data['activeUntil'] = activeUntil;
    _data['createdDate'] = createdDate;
    _data['isActive'] = isActive;
    _data['persons'] = persons;
    _data['paymentType'] = paymentType;
    _data['paymentReference'] = paymentReference;
    _data['touristPackage'] = touristPackage;
    return _data;
  }

  Color getColor() {
    if (this.isActive!) {
      return Colors.green;
    } else {
      return Colors.red;
    }
  }

  String getText() {
    if (this.isActive!) {
      return 'active'.tr;
    } else {
      return 'not active'.tr;
    }
  }
}
