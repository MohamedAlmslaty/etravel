import 'package:e_travel/app/data/models/itineraries.dart';
import 'package:e_travel/app/data/models/multiCityFlights.dart';

class FlightRequest {
  int? adults;
  String? cabinClass;
  String? departureTicketClass;
  String? returnTicketClass;
  int? children;
  String? departureDate;
  String? destinationLocationCode;
  String? flightNumber;
  String? returnFlightNumber;
  int? id;
  int? airlineId;
  String? includedAirlineCodes;
  int? maxStops;
  String? originLocationCode;
  String? returnDate;
  bool? goingOpen;
  bool? returnOpen;
  bool? searchForDirect;
  bool? searchForRefundable;
  bool? searchForFlightsWithLuggage;
  bool? isMultiCity;
  List<MultiCity>?multiCityFlights ;



  FlightRequest(
      {this.adults,
        this.cabinClass,
        this.isMultiCity,
        this.multiCityFlights,
        this.searchForDirect,
        this.searchForFlightsWithLuggage,
        this.searchForRefundable,
        this.departureTicketClass,
        this.returnTicketClass,
        this.children,
        this.departureDate,
        this.destinationLocationCode,
        this.flightNumber,
        this.returnFlightNumber,
        this.id,
        this.airlineId,
        this.includedAirlineCodes,
        this.maxStops,
        this.originLocationCode,
        this.goingOpen,
        this.returnOpen,
        this.returnDate});

  FlightRequest.fromJson(Map<String, dynamic> json) {
    adults = json['adults'];
    cabinClass = json['cabinClass'];
    airlineId = json['airlineId'];
    departureTicketClass = json['departureTicketClass'];
    returnTicketClass = json['returnTicketClass'];
    children = json['children'];
    goingOpen = json['goingOpen'];
    returnOpen = json['returnOpen'];
    departureDate = json['departureDate'];
    destinationLocationCode = json['destinationLocationCode'];
    flightNumber = json['flightNumber'];
    returnFlightNumber = json['returnFlightNumber'];
    id = json['id'];
    isMultiCity = json['isMultiCity'];
    includedAirlineCodes = json['includedAirlineCodes'];
    maxStops = json['maxStops'];
    originLocationCode = json['originLocationCode'];
    returnDate = json['returnDate'];
    searchForDirect = json['searchForDirect']??false;
    searchForRefundable = json['searchForRefundable']??false;
    searchForFlightsWithLuggage = json['searchForFlightsWithLuggage']??false;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['adults'] = this.adults;
    data['airlineId'] = this.airlineId;
    data['cabinClass'] = this.cabinClass;
    data['departureTicketClass'] = this.departureTicketClass;
    data['returnTicketClass'] = this.returnTicketClass;
    data['children'] = this.children;
    data['departureDate'] = this.departureDate;
    data['destinationLocationCode'] = this.destinationLocationCode;
    data['flightNumber'] = this.flightNumber;
    data['returnFlightNumber'] = this.returnFlightNumber;
    data['id'] = this.id;
    data['includedAirlineCodes'] = this.includedAirlineCodes;
    data['maxStops'] = this.maxStops;
    data['originLocationCode'] = this.originLocationCode;
    data['returnDate'] = this.returnDate;
    data['returnOpen'] = this.returnOpen;
    data['goingOpen'] = this.goingOpen;
    data['searchForRefundable'] = this.searchForRefundable;
    data['searchForDirect'] = this.searchForDirect;

    return data;
  }
  Map<String, dynamic> toCalender() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['destinationLocationCode'] = this.destinationLocationCode;
    data['includedAirlineCodes'] = this.includedAirlineCodes;
    data['originLocationCode'] = this.originLocationCode;
    return data;
  }
  Map<String, dynamic> toJsonMultiCity() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['adults'] = this.adults;
    data['airlineId'] = this.airlineId;
    data['cabinClass'] = this.cabinClass;
    data['children'] = this.children;
    data['isMultiCity'] = this.isMultiCity;
    data['multiCityFlights'] = this.multiCityFlights!.map((v) => v.toJson()).toList();
    data['returnOpen'] = this.returnOpen;
    data['goingOpen'] = this.goingOpen;
    data['originLocationCode'] = this.originLocationCode;
    data['destinationLocationCode'] = this.destinationLocationCode;
    data['searchForRefundable'] = this.searchForRefundable;
    data['searchForDirect'] = this.searchForDirect;

    return data;
  }
}


class FlightRequestMulti {
  int? adults;
  String? cabinClass;
  String? departureTicketClass;
  String? returnTicketClass;
  int? children;
  String? departureDate;
  String? destinationLocationCode;
  String? flightNumber;
  String? returnFlightNumber;
  int? id;
  int? airlineId;
  String? includedAirlineCodes;
  int? maxStops;
  String? originLocationCode;
  String? returnDate;
  bool? goingOpen;
  bool? returnOpen;
  bool? isMultiStop;
  bool? isMultiCity;
  List<Itineraries>? itineraries;


  FlightRequestMulti(
      {this.adults,
        this.cabinClass,
        this.departureTicketClass,
        this.returnTicketClass,
        this.children,
        this.departureDate,
        this.destinationLocationCode,
        this.flightNumber,
        this.returnFlightNumber,
        this.id,
        this.airlineId,
        this.includedAirlineCodes,
        this.maxStops,
        this.originLocationCode,
        this.goingOpen,
        this.returnOpen,
        this.isMultiCity,
        this.itineraries,
        this.isMultiStop,
        this.returnDate});

  FlightRequestMulti.fromJson(Map<String, dynamic> json) {
    adults = json['adults'];
    cabinClass = json['cabinClass'];
    airlineId = json['airlineId'];
    departureTicketClass = json['departureTicketClass'];
    returnTicketClass = json['returnTicketClass'];
    children = json['children'];
    goingOpen = json['goingOpen'];
    isMultiStop = json['isMultiStop'];
    returnOpen = json['returnOpen'];
    departureDate = json['departureDate'];
    destinationLocationCode = json['destinationLocationCode'];
    flightNumber = json['flightNumber'];
    returnFlightNumber = json['returnFlightNumber'];
    id = json['id'];
    includedAirlineCodes = json['includedAirlineCodes'];
    maxStops = json['maxStops'];
    originLocationCode = json['originLocationCode'];
    returnDate = json['returnDate'];
     itineraries = json['itineraries'] != null && (json['itineraries'] as List).length > 0 ? List.from(json['itineraries']).map((e) => Itineraries.fromJson(e)).toList() : [];

  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['adults'] = this.adults;
    data['airlineId'] = this.airlineId;
    data['cabinClass'] = this.cabinClass;
    data['departureTicketClass'] = this.departureTicketClass;
    data['returnTicketClass'] = this.returnTicketClass;
    data['children'] = this.children;
    data['departureDate'] = this.departureDate;
    data['destinationLocationCode'] = this.destinationLocationCode;
    data['flightNumber'] = this.flightNumber;
    data['returnFlightNumber'] = this.returnFlightNumber;
    data['id'] = this.id;
    data['isMultiCity'] = this.isMultiCity;
    data['includedAirlineCodes'] = this.includedAirlineCodes;
    data['maxStops'] = this.maxStops;
    data['isMultiStop'] = this.isMultiStop;
    data['originLocationCode'] = this.originLocationCode;
    data['returnDate'] = this.returnDate;
    data['returnOpen'] = this.returnOpen;
    data['goingOpen'] = this.goingOpen;
    data['itineraries'] = itineraries!.map((e) => e.toJson()).toList();

    return data;
  }
}