class ErrorModel {
  String? type;
  String? title;
  int? status;
  String? detail;
  String? path;
  String? message;

  ErrorModel(
  {this.type,
  this.title,
  this.status,
  this.detail,
  this.path,
  this.message});

  ErrorModel.fromJson(Map<String, dynamic> json) {
  type = json['type'];
  title = json['title'];
  status = json['status'];
  detail = json['detail'];
  path = json['path'];
  message = json['message'];
  }

  Map<String, dynamic> toJson() {
  final Map<String, dynamic> data = new Map<String, dynamic>();
  data['type'] = this.type;
  data['title'] = this.title;
  data['status'] = this.status;
  data['detail'] = this.detail;
  data['path'] = this.path;
  data['message'] = this.message;
  return data;
  }
  }
