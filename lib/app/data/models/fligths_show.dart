class InternationalArrival {
  String? flightType;
  String? iata;
  String? airlineName;
  String? flightNumber;
  String? cityName;
  String? optionalCityViaName;
  String? scheduledTime;
  String? expectedTime;
  String? assignedResource;
  String? remark;
  String? scheduledDate;

  InternationalArrival(
  {this.flightType,
  this.iata,
  this.airlineName,
  this.flightNumber,
  this.cityName,
  this.optionalCityViaName,
  this.scheduledTime,
  this.expectedTime,
  this.assignedResource,
  this.remark,
  this.scheduledDate});

  InternationalArrival.fromJson(Map<String, dynamic> json) {
  flightType = json['flightType'];
  iata = json['iata'];
  airlineName = json['airlineName'];
  flightNumber = json['flightNumber'];
  cityName = json['cityName'];
  optionalCityViaName = json['optionalCityViaName'];
  scheduledTime = json['scheduledTime'];
  expectedTime = json['expectedTime'];
  assignedResource = json['assignedResource'];
  remark = json['remark'];
  scheduledDate = json['scheduledDate'];
  }

  Map<String, dynamic> toJson() {
  final Map<String, dynamic> data = new Map<String, dynamic>();
  data['flightType'] = this.flightType;
  data['iata'] = this.iata;
  data['airlineName'] = this.airlineName;
  data['flightNumber'] = this.flightNumber;
  data['cityName'] = this.cityName;
  data['optionalCityViaName'] = this.optionalCityViaName;
  data['scheduledTime'] = this.scheduledTime;
  data['expectedTime'] = this.expectedTime;
  data['assignedResource'] = this.assignedResource;
  data['remark'] = this.remark;
  data['scheduledDate'] = this.scheduledDate;
  return data;
  }
  }
