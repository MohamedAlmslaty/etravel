enum PaymentType { CASH_ON_DELEVERY, SADAD, SADAD_ON_DELEVERY, TADAWUL, TADAWUL_ON_DELEVERY, MOBICASH, MOBICASH_ON_DELEVERY, MIZA, MIZA_ON_DELEVERY, MOAMALAT_ON_DELEVERY, WALLET }

class PaymentMethods {
  PaymentMethods();

    int? id;
    String? nameAr;
    String? nameEn;
    int? menuOrder;
    String? imageUrl;
    String? details;
    double? feePercentage;
    String? paymentType;
    bool? isActive;
    String? notes;

  PaymentMethods.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    nameAr = json['nameAr'];
    nameEn = json['nameEn'];
    menuOrder = json['menuOrder'];
    imageUrl = json['imageUrl'];
    details = json['details'];
    feePercentage = json['feePercentage'];
    paymentType = json['paymentType'];
    isActive = json['isActive'];
    notes = json['notes'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['id'] = id;
    _data['nameAr'] = nameAr;
    _data['nameEn'] = nameEn;
    _data['menuOrder'] = menuOrder;
    _data['imageUrl'] = imageUrl;
    _data['details'] = details;
    _data['feePercentage'] = feePercentage;
    _data['paymentType'] = paymentType;
    _data['isActive'] = isActive;
    _data['notes'] = notes;
    return _data;
  }
}
