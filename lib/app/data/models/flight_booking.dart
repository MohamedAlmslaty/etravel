import 'package:e_travel/app/data/models/customer.dart';
import 'package:e_travel/app/data/models/itineraries.dart';
import 'package:e_travel/app/data/models/passenger.dart';
import 'package:get/get.dart';

class FlightBooking {
  FlightBooking();

  int? id;
  String? passengerFirstName;
  String? passengerLastName;
  String? passengerPassportNo;
  String? passengerBirthday;
  String? passengerMobileNo;
  String? bookingType;
  String? pnr;
  int? paxNo;
  String? eTicketNo;
  String? currency;
  double? discount;
  double? total;
  String? issuedAt;
  String? flightType;
  String? billingAddress;
  String? addressMobileNo;
  bool? oneWay;
  bool? isPayed;
  bool? isMultiCity;
  int? goingStops;
  int? returnStops;
  String? paymentType;
  String? paymentReference;
  String? payemntDateTime;
  String? gdsType;
  DateTime? initialBookingExpiration;
  Passenger? passenger;
  Customer? customer;
  List<Itineraries>? itineraries;

  FlightBooking.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    passengerFirstName = json['passengerFirstName'] ?? '';
    passengerLastName = json['passengerLastName'] ?? '';
    flightType = json['flightType'] ?? null;
    passengerPassportNo = json['passengerPassportNo'] ?? '';
    passengerBirthday = json['passengerBirthday'] ?? '';
    passengerMobileNo = json['passengerMobileNo'] ?? '';
    passengerMobileNo = json['passengerMobileNo'] ?? '';
    returnStops = json['returnStops'] ?? 0;
    goingStops = json['goingStops'] ?? 0;
    bookingType = json['bookingType'] ?? '';
    pnr = json['pnr'] ?? '';
    gdsType = json['gdsType'] ?? '';
    paxNo = json['paxNo'] ?? 1;
    eTicketNo = json['eTicketNo'] ?? '';
    currency = json['currency'] ?? '';
    discount = json['discount'] ?? 0.0;
    total = json['total'] ?? 0.0;
    issuedAt = json['issuedAt'] ?? '';
    billingAddress = json['billingAddress'] ?? '';
    addressMobileNo = json['addressMobileNo'] ?? '';
    isPayed = json['isPayed'];
    isMultiCity = json['isMultiCity']??false;
    oneWay = json['oneWay'];
    paymentType = json['paymentType'] ?? '';
    paymentReference = json['paymentReference'] ?? '';
    payemntDateTime = json['payemntDateTime'] ?? '';
    initialBookingExpiration = json['initialBookingExpiration']!=null ?DateTime.tryParse(json['initialBookingExpiration']): DateTime.now();
    // passenger = json['passenger']!=null?Passenger.fromJson(json['passenger']):Passenger();
    // customer =json['customer']!=null? Customer.fromJson(json['customer']):Customer();
    itineraries = json['itineraries'] != null && (json['itineraries'] as List).length > 0 ? List.from(json['itineraries']).map((e) => Itineraries.fromJson(e)).toList() : [];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['id'] = id;
    _data['isMultiCity'] = isMultiCity;
    _data['passengerFirstName'] = passengerFirstName;
    _data['passengerLastName'] = passengerLastName;
    _data['passengerPassportNo'] = passengerPassportNo;
    _data['passengerBirthday'] = passengerBirthday;
    _data['passengerMobileNo'] = passengerMobileNo;
    _data['flightType'] = flightType;
    _data['paxNo'] = paxNo;
    _data['gdsType'] = gdsType;
    _data['bookingType'] = bookingType;
    _data['pnr'] = pnr;
    _data['returnStops'] = returnStops;
    _data['goingStops'] = goingStops;
    _data['eTicketNo'] = eTicketNo;
    _data['currency'] = currency;
    _data['discount'] = discount;
    _data['total'] = total;
    _data['issuedAt'] = issuedAt;
    _data['billingAddress'] = billingAddress;
    _data['addressMobileNo'] = addressMobileNo;
    _data['isPayed'] = isPayed;
    _data['paymentType'] = paymentType;
    _data['paymentReference'] = paymentReference;
    _data['payemntDateTime'] = payemntDateTime;
    _data['passenger'] = passenger!.toJson();
    _data['customer'] = customer!.toJson();
    _data['itineraries'] = itineraries!.map((e) => e.toJson()).toList();
    return _data;
  }
}
String mapStringToFlightType(String type,int segment) {
  switch (type) {
    case "NORMAL_ONEWAY":
      return 'departure'.tr;
    case "NORMAL_RETURN":
      String data='';
      if(segment==1){
        data='departure'.tr;
      }else data='return'.tr;
      return data;
    case "MULTI_STOPS_ONEWAY":
      return 'departure'.tr;
    case "MULTI_STOPS_RETURN":
      String data='';
      if(segment==1){
        data='departure'.tr;
      }else data='return'.tr;
      return data;
    case "MULTI_CITY":
      return 'departure'.tr;
    default:
    // Handle unknown or unexpected response here, e.g., throw an exception or return a default value.
      throw ArgumentError("Invalid Flight Type: $type");
  }
}