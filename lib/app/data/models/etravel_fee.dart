class EtravelFee {
  String? name;
  double? total;

  EtravelFee({this.name, this.total});

  EtravelFee.fromJson(Map<String, dynamic> json) {
    name = json['name'] ?? "";
    total = json['total'].toDouble() ?? 0.0;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['total'] = this.total;
    return data;
  }
}
