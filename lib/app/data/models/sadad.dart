
class Sadad {
  String? birthYear;
  String? code;
  String? customerIp;
  String? invoiceNo;
  String? mobileNumber;
  String? processId;
  double? amount;
  double? fees;
  String? paymentType;
  double? total;
  int? sadadTransactionId;

  Sadad(
      {this.amount,
        this.fees,
        this.paymentType,
        this.total,
        this.birthYear,
        this.code,
        this.customerIp,
        this.invoiceNo,
        this.mobileNumber,
        this.processId,
        this.sadadTransactionId,
       });

  Sadad.fromJson(Map<String, dynamic> json) {
    amount = json['amount'];
    fees = json['fees'];
    paymentType = json['paymentType'];
    total = json['total'];
    birthYear = json['birthYear'];
    code = json['code'];
    customerIp = json['customerIp'];
    invoiceNo = json['invoiceNo'];
    mobileNumber = json['mobileNumber'];
    processId = json['processId'];
    sadadTransactionId = json['sadadTransactionId'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['code'] = this.code;
    data['sadadTransactionId'] = this.sadadTransactionId;
    return data;
  }
}

