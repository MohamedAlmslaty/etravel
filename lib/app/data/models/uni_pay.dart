class UniPay {
  int? amountToConsume;
  int? codePin;
  String? rechargeCode;

  UniPay({this.amountToConsume, this.codePin, this.rechargeCode});

  UniPay.fromJson(Map<String, dynamic> json) {
    amountToConsume = json['amountToConsume'];
    codePin = json['codePin'];
    rechargeCode = json['rechargeCode'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['amountToConsume'] = this.amountToConsume;
    data['codePin'] = this.codePin;
    data['rechargeCode'] = this.rechargeCode;
    return data;
  }
}
