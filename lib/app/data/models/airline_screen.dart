import 'package:get/get.dart';

class AirlineScrees{
  RxString departureText = ''.obs;
  RxString destinationText = ''.obs;
  RxString cityName = ''.obs;
  RxString cityId = ''.obs;
  RxString destinationLocationCode = ''.obs;
  RxString originLocationCode = ''.obs;
  RxString airlineText = ''.obs;
  RxString airlineCode = ''.obs;
  RxInt airlineId = 0.obs;
}