class Edfali {
  int? amount;
  String? pin;
  String? sessionID;
  String? mobileNumber;

  Edfali(
      {this.amount,
        this.pin,
        this.sessionID,
        this.mobileNumber,
        });

  Edfali.fromJson(Map<String, dynamic> json) {
    amount = json['amount'];
    pin = json['pin'];
    sessionID = json['sessionID'];
    mobileNumber = json['mobileNumber'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['amount'] = this.amount;
    data['pin'] = this.pin;
    data['sessionID'] = this.sessionID;
    data['mobileNumber'] = this.mobileNumber;
    return data;
  }
}
