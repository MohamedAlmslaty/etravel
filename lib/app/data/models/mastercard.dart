class Mastercard {
  int? id;
  String? transactionReference;
  String? paymentType;
  String? transactionStatus;
  double? amount;
  double? fees;
  double? total;
  String? paymentLink;

  Mastercard(
      {this.id,
      this.transactionReference,
      this.paymentType,
      this.transactionStatus,
      this.amount,
      this.fees,
      this.total,
      this.paymentLink});

  Mastercard.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    transactionReference = json['transactionReference'];
    paymentType = json['paymentType'];
    transactionStatus = json['transactionStatus'];
    amount = json['amount'];
    fees = json['fees'];
    total = json['total'];
    paymentLink = json['paymentLink'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['transactionReference'] = this.transactionReference;
    data['paymentType'] = this.paymentType;
    data['transactionStatus'] = this.transactionStatus;
    data['amount'] = this.amount;
    data['fees'] = this.fees;
    data['total'] = this.total;
    data['paymentLink'] = this.paymentLink;
    return data;
  }
}
