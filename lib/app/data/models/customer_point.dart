import 'package:e_travel/app/data/models/customer.dart';

class CustomerPoint {
  int? id;
  String? transactionNo;
  int? amount;
  String? walletAction;
  double? totalBeforeAction;
  double? totalAfterAction;
  String? notes;
  Customer? customer;

  CustomerPoint(
      {this.id,
        this.transactionNo,
        this.amount,
        this.walletAction,
        this.totalBeforeAction,
        this.totalAfterAction,
        this.notes,
        this.customer});

  CustomerPoint.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    transactionNo = json['transactionNo'];
    amount = json['amount']!=null?json['amount']:0;
    walletAction = json['walletAction'];
    totalBeforeAction = json['totalBeforeAction']!= null?json['totalBeforeAction']:0.0;
    totalAfterAction = json['totalAfterAction']!= null?json['totalAfterAction']:0.0;
    notes = json['notes'];
    customer = json['customer'] != null
        ? new Customer.fromJson(json['customer'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['transactionNo'] = this.transactionNo;
    data['amount'] = this.amount;
    data['walletAction'] = this.walletAction;
    data['totalBeforeAction'] = this.totalBeforeAction;
    data['totalAfterAction'] = this.totalAfterAction;
    data['notes'] = this.notes;
    if (this.customer != null) {
      data['customer'] = this.customer!.toJson();
    }
    return data;
  }
}

