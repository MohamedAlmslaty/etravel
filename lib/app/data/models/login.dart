class LoginVM {
  late final String password;
  late final bool? rememberMe;
  late final String username;
  late final String firebaseId;

  LoginVM(
      {required this.username,
      required this.password,
      required this.firebaseId,
      this.rememberMe});

  LoginVM.fromJson(Map<String, dynamic> json) {
    password = json['password'];
    rememberMe = json['rememberMe'];
    username = json['username'];
    firebaseId = json['firebaseId'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['password'] = password;
    _data['rememberMe'] = rememberMe;
    _data['username'] = username;
    _data['firebaseId'] = firebaseId;

    return _data;
  }
}
