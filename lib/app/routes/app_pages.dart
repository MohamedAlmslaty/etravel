import 'package:e_travel/app/modules/hotels/home/views/widgets/seclect_cities.dart';
import 'package:e_travel/app/modules/hotels/hotel/bindings/hotel_binding.dart';
import 'package:e_travel/app/modules/hotels/hotel/views/hotel_view.dart';
import 'package:e_travel/app/modules/hotels/hotel_confirm/bindings/hotel_confirm_binding.dart';
import 'package:e_travel/app/modules/hotels/hotel_confirm/views/hotel_confirm_view.dart';
import 'package:e_travel/app/modules/hotels/hotel_confirm/views/success.dart';
import 'package:e_travel/app/modules/hotels/hotel_details/bindings/hotel_details_binding.dart';
import 'package:e_travel/app/modules/hotels/hotel_details/views/hotel_details_view.dart';
import 'package:e_travel/app/modules/hotels/main/bindings/main_binding.dart';
import 'package:e_travel/app/modules/hotels/main/views/main_view.dart';
import 'package:get/get.dart';

import '../modules/about/about_view.dart';
import '../modules/auth/bindings/auth_binding.dart';
import '../modules/auth/views/auth_view.dart';
import '../modules/book_flight/bindings/book_flight_binding.dart';
import '../modules/book_flight/views/book_flight_view.dart';
import '../modules/book_flight/views/confirm_booking.dart';
import '../modules/booking/views/edit_booking_view.dart';
import '../modules/booking/views/edit_dates_view.dart';
import '../modules/booking/views/flight_details_view.dart';
import '../modules/contact_us/contact_us_view.dart';
import '../modules/customer_points/bindings/customer_points_binding.dart';
import '../modules/customer_points/views/customer_points_view.dart';
import '../modules/customer_points/views/libyana_otp.dart';
import '../modules/customer_points/views/otp_view.dart';
import '../modules/customer_points/views/phone_view.dart';
import '../modules/customer_points/views/replace_view.dart';
import '../modules/customer_points/views/success_view.dart';
import '../modules/flight/bindings/flight_binding.dart';
import '../modules/flight/views/flight_view.dart';
import '../modules/home/views/widgets/seclect_airline.dart';
import '../modules/home/views/widgets/seclect_date.dart';
import '../modules/home/views/widgets/seclect_place.dart';
import '../modules/hotels/home/bindings/home_binding.dart';
import '../modules/hotels/home/views/home_view.dart';
import '../modules/language/bindings/language_binding.dart';
import '../modules/language/views/language_view.dart';
import '../modules/main/bindings/main_binding.dart';
import '../modules/main/views/main_view.dart';
import '../modules/notifications/views/notifications_view.dart';
import '../modules/onboard/bindings/onboard_binding.dart';
import '../modules/onboard/views/onboard_view.dart';
import '../modules/package_details/bindings/package_details_binding.dart';
import '../modules/package_details/views/confirm_subscription_view.dart';
import '../modules/package_details/views/package_details_view.dart';
import '../modules/package_subscription/bindings/package_subscription_binding.dart';
import '../modules/package_subscription/views/package_subscription_view.dart';
import '../modules/package_subscription_details/views/package_subscription_details_view.dart';
import '../modules/passenger/bindings/passenger_binding.dart';
import '../modules/passenger/views/passenger_view.dart';
import '../modules/payment/bindings/payment_binding.dart';
import '../modules/payment/views/payment.dart';
import '../modules/payment/views/payment_view.dart';
import '../modules/payment/views/sadad/sadad_otp.dart';
import '../modules/payment/views/sadad/sadad_phone.dart';
import '../modules/payment/views/sadad/success_payment.dart';
import '../modules/payment/views/uni_pay_view.dart';
import '../modules/profile/bindings/profile_binding.dart';
import '../modules/profile/views/profile_view.dart';
import '../modules/referral/bindings/referral_binding.dart';
import '../modules/referral/views/referral_view.dart';
import '../modules/show_flights/bindings/show_flights_binding.dart';
import '../modules/show_flights/views/show_flights_view.dart';
import '../modules/splash/bindings/splash_binding.dart';
import '../modules/splash/views/splash_view.dart';
import '../modules/splash/views/welcome_view.dart';
import '../modules/transaction/bindings/transaction_binding.dart';
import '../modules/transaction/views/transaction_view.dart';
import '../modules/voucher/bindings/voucher_binding.dart';
import '../modules/voucher/views/voucher_view.dart';
import '../modules/wallet/bindings/wallet_binding.dart';
import '../modules/wallet/views/deposit_view.dart';
import '../modules/wallet/views/transfer_by_phone.dart';
import '../modules/wallet/views/wallet_view.dart';

part 'app_routes.dart';

class AppPages {
  AppPages._();

  static const INITIAL = Routes.SPLASH;

  static final routes = [
    GetPage(
      name: _Paths.MAIN,
      page: () => MainView(),
      binding: MainBinding(),
    ),
    GetPage(
      name: _Paths.MAIN_HOTEL,
      page: () => MainHotelView(),
      binding: MainHotelBinding(),
    ),
    GetPage(
      name: _Paths.OnBOARD,
      page: () => OnboardView(),
      binding: OnboardBinding(),
    ),
    GetPage(
      name: _Paths.SPLASH,
      page: () => SplashView(),
      binding: SplashBinding(),
    ),
    GetPage(
      name: _Paths.WELCOME,
      page: () => WelcomeView(),
      binding: SplashBinding(),
    ),
    GetPage(
      name: _Paths.HOME,
      page: () => HomeView(),
      binding: HomeBinding(),
    ),
    GetPage(
      name: _Paths.FLIGHT,
      page: () => FlightView(),
      binding: FlightBinding(),
    ),
    GetPage(
      name: _Paths.BOOK_FLIGHT,
      page: () => BookFlightView(),
      binding: BookFlightBinding(),
    ),
    GetPage(
      name: _Paths.PROFILE,
      page: () => ProfileView(),
      binding: ProfileBinding(),
    ),
    GetPage(
      name: _Paths.FLIGHT_DETAILS,
      page: () => FlightDetailsView(),
    ),
    GetPage(
        name: Routes.LANGUAGE,
        page: () => LanguageView(),
        binding: LanguageBinding()),
    GetPage(
      name: _Paths.PASSENGER,
      page: () => PassengersView(),
      binding: PassengerBinding(),
    ),
    GetPage(
      name: _Paths.SUCCESS_PAY,
      page: () => SuccessPayment(),
      binding: PaymentBinding(),
    ),
    GetPage(
      name: _Paths.SADAD_OTP,
      page: () => SadadOTPView(),
      binding: PaymentBinding(),
    ),
    GetPage(
      name: _Paths.SADAD_PHONE,
      page: () => SadadPhoneView(),
      binding: PaymentBinding(),
    ),
    GetPage(
      name: _Paths.PAYMENT,
      page: () => PaymentView(),
      binding: PaymentBinding(),
    ),
    GetPage(
      name: _Paths.EDIT_BOOKING,
      page: () => EditBookingView(),
    ),
    GetPage(
      name: _Paths.EDIT_DATES,
      page: () => EditDatesView(),
    ),
    GetPage(
      name: _Paths.PAYMENT_METHOD,
      page: () => PaymentMethod(),
      binding: PaymentBinding(),
    ),
    GetPage(
      name: _Paths.UNIPAY,
      page: () => UniPayView(),
      binding: PaymentBinding(),
    ),
    GetPage(
      name: _Paths.SHOW_FLIGHTS,
      page: () => ShowFlightsView(),
      binding: ShowFlightsBinding(),
    ),
    GetPage(
      name: _Paths.PACKAGE_SUBSCRIPTION,
      page: () => PackageSubscriptionView(),
      binding: PackageSubscriptionBinding(),
    ),
    GetPage(
      name: _Paths.PACKAGE_SUBSCRIPTION_DETAILS,
      page: () => PackageSubscriptionDetailsView(),
    ),
    GetPage(
      name: _Paths.SELECT_DATE,
      page: () => SelectDateView(),
    ),
    GetPage(
      name: _Paths.SELECT_PLACE,
      page: () => SelectPlace(),
    ),
    GetPage(
      name: _Paths.SELECT_CITIES,
      page: () => SelectCities(),
    ),
    GetPage(
      name: _Paths.SELECT_AIRLINE,
      page: () => SelectAirline(),
    ),
    GetPage(
      name: _Paths.AUTH,
      page: () => AuthView(),
      binding: AuthBinding(),
    ),
    GetPage(
      name: _Paths.WALLET,
      page: () => WalletView(),
      binding: WalletBinding(),
    ),
    GetPage(
      name: _Paths.TRANSFER_BY_PHONE,
      page: () => TransferByPhone(),
      binding: WalletBinding(),
    ),
    GetPage(
      name: _Paths.VOUCHER_DEALER,
      page: () => VoucherView(),
      binding: VoucherBinding(),
    ),
    GetPage(
      name: _Paths.LOYAL,
      page: () => CustomerPointView(),
      binding: CustomerPointBinding(),
    ),
    GetPage(
      name: _Paths.PHONE,
      page: () => PhoneView(),
      binding: CustomerPointBinding(),
    ),
    GetPage(
      name: _Paths.REPLACE,
      page: () => ReplaceView(),
      binding: CustomerPointBinding(),
    ),
    GetPage(
      name: _Paths.LIBYANA_OTP,
      page: () => LibyanaOTPView(),
      binding: CustomerPointBinding(),
    ),
    GetPage(
      name: _Paths.OTP,
      page: () => OTPView(),
      binding: CustomerPointBinding(),
    ),
    GetPage(
      name: _Paths.SUCCESS_REPLACE,
      page: () => SuccessView(),
      binding: CustomerPointBinding(),
    ),
    GetPage(
      name: _Paths.SUCCESS_HOTELS,
      page: () => SuccessHotel(),
    ),
    GetPage(
      name: _Paths.EDIT,
      page: () => WalletView(),
      binding: WalletBinding(),
    ),
    GetPage(
      name: _Paths.NOTIFICATIONS,
      page: () => NotificationsView(),
    ),
    GetPage(
      name: _Paths.TRANSACTIONS,
      page: () => TransactionsView(),
      binding: TransactionsBinding(),
    ),
    GetPage(
      name: _Paths.PACKAGE_DETAILS,
      page: () => PackageDetailsView(),
      binding: PackageDetailsBinding(),
    ),
    GetPage(
      name: _Paths.CONFIRM_SUBSCRIPTION,
      page: () => ConfirmSubscriptionView(),
      binding: PackageDetailsBinding(),
    ),
    GetPage(
      name: _Paths.CONFIRM_BOOKING,
      page: () => ConfirmBooking(),
      binding: BookFlightBinding(),
    ),
    GetPage(
      name: _Paths.ABOUT,
      page: () => AboutView(),
    ),
    GetPage(
      name: _Paths.CONTACTUS,
      page: () => ContactUsView(),
    ),
    GetPage(
      name: _Paths.DEPOSIT,
      page: () => DepositView(),
    ),
    GetPage(
      name: _Paths.REFERRAL,
      page: () => const ReferralView(),
      binding: ReferralBinding(),
    ),
    GetPage(
      name: _Paths.HOTEL,
      page: () => const HotelView(),
      binding: HotelBinding(),
    ),
    GetPage(
      name: _Paths.HOTEL_DETAILS,
      page: () => const HotelDetailsView(),
      binding: HotelDetailsBinding(),
    ),
    GetPage(
      name: _Paths.HOTEL_CONFIRM,
      page: () => const HotelConfirmView(),
      binding: HotelConfirmBinding(),
    ),
  ];
}
