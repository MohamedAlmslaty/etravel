import 'dart:io';

import 'package:e_travel/app/translation/lang_service.dart';
import 'package:e_travel/app/util/consts/colors.dart';
import 'package:e_travel/app/util/http_override.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'app/routes/app_pages.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await GetStorage.init();
  Get.put<GetStorage>(GetStorage());
  initializeDateFormatting();
  await Firebase.initializeApp();
  HttpOverrides.global = MyHttpOverrides();
  ErrorWidget.builder = (FlutterErrorDetails errorDetails) {
    return Container();
  };
  SystemChrome.setPreferredOrientations(
      [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    Locale locale;
    if (Get.find<GetStorage>().hasData('language')) {
        locale = LocalizationService().getLocaleFromLanguage(Get.find<GetStorage>().read('language')) ?? LocalizationService.locale;
      } else {
      LocalizationService().setDefaultLanguage(Get.deviceLocale.toString().substring(0, 2));
      locale = LocalizationService().getLocaleFromLanguage(Get.find<GetStorage>().read('language')) ?? LocalizationService.locale;
    }

    return ScreenUtilInit(
      designSize: Size(375, 812),
      builder: (ctx, child) => GetMaterialApp(
        title: "ETravel",
        localizationsDelegates: [
          GlobalCupertinoLocalizations.delegate,
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
        ],
        supportedLocales: [
          const Locale('en', 'US'), // English
          const Locale('fr', 'FR'), // French
          const Locale('ar', ), // French
          // Add more locales as needed
        ],
        debugShowCheckedModeBanner: false,
        builder: EasyLoading.init(),
        theme: ThemeData(
          fontFamily: 'JannaLT',
          primaryColor: primaryColor,
          appBarTheme: AppBarTheme(color: primaryColor),
          scaffoldBackgroundColor: primaryColor,
        ),
        initialRoute: AppPages.INITIAL,
        getPages: AppPages.routes,
        locale: locale,
        fallbackLocale: LocalizationService.fallbackLocale,
        translations: LocalizationService(),
      ),
    );
  }
   // startRoutes() {
   //   if (!Get.find<GetStorage>().hasData('PERMISSION') && Platform.isAndroid) {
   //     return Routes.PERMISSION;
   //   }else return AppPages.INITIAL;
   // }
}
